import 'package:flutter/services.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/survey_list/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/SurveyModel.dart';
import 'package:sapawarga/repositories/SurveyRepository.dart';
import 'package:test/test.dart';

class MockSurveyRepository extends Mock implements SurveyRepository {}

void main() {
  MockSurveyRepository mockSurveyRepository;
  SurveyListBloc surveyListBloc;

  setUp(() {
    mockSurveyRepository = MockSurveyRepository();
    surveyListBloc = SurveyListBloc(surveyRepository: mockSurveyRepository);
  });

  test('SurveyList initial state is correct', () {
    expect(surveyListBloc.initialState, SurveyListInitial());
  });

  test('SurveyListLoad to String', () {
    expect(SurveyListLoad(page: 1).toString(), 'Event SurveyListLoad');
  });

  test('SurveyListLoad props', () {
    expect(SurveyListLoad(page: 1).props, []);
  });

  test('SurveyList dispose does not emit new states', () {
    expectLater(
      surveyListBloc,
      emitsInOrder([]),
    );
    surveyListBloc.close();
  });

  test('emits [SurveyListInitial, SurveyListLoading, SurveyListLoaded]', () {
    const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getAll') {
        return <String, dynamic>{}; // set initial values here if desired
      }
      return null;
    });

    List<SurveyModel> records = [];

    when(mockSurveyRepository.fetchRecords(1))
        .thenAnswer((_) => Future.value(records));

    final expectedResponse = [
      SurveyListInitial(),
      SurveyListLoading(),
      SurveyListLoaded(records: records, maxData: 0, isLoad: false)
    ];

    expectLater(
      surveyListBloc,
      emitsInOrder(expectedResponse),
    );

    surveyListBloc.add(SurveyListLoad(page: 1));
  });

  test('emits [SurveyListInitial, SurveyListLoading, SurveyListFailed]', () {
    when(mockSurveyRepository.fetchRecords(1))
        .thenThrow(ErrorException.timeoutException);

    final expectedResponse = [
      SurveyListInitial(),
      SurveyListLoading(),
      SurveyListFailure(
          error: CustomException.onConnectionException(
              ErrorException.timeoutException))
    ];

    expectLater(
      surveyListBloc,
      emitsInOrder(expectedResponse),
    );

    surveyListBloc.add(SurveyListLoad(page: 1));
  });
}
