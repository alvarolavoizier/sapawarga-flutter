import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:test/test.dart';

void main() {
  group('Name Test', () {
    test('Empty Name Test', () {
      var result = Validations.nameValidation('');
      expect(result, Dictionary.errorEmptyName);
    });

    test('Minimum Name Test', () {
      var result = Validations.nameValidation('a');
      expect(result, Dictionary.errorMinimumName);
    });

    test('Maximum Name Test', () {
      var result = Validations.nameValidation('aaaaaaaaaaaaaaaaasasasasas'
          'sasasasasasasasasasasasasasasasasasasasasasaasasasasasasasasasasasasas'
          'asasasasasasasasasasasasasasasasasasasasasasasasasasasassasasasasasasa'
          'asasassasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasa'
          'asasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas'
          'asasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas');
      expect(result, Dictionary.errorMaximumName);
    });

    test('Invalid Name Test', () {
      var result = Validations.nameValidation('User_RW');
      expect(result, Dictionary.errorInvalidName);
    });

    test('Valid Name Test', () {
      var result = Validations.nameValidation('User Rw');
      expect(result, null);
    });
  });

  group('Username Test', () {
    test('Empty Username Test', () {
      var result = Validations.usernameValidation('');
      expect(result, Dictionary.errorEmptyUsername);
    });

    test('Minimum Username Test', () {
      var result = Validations.usernameValidation('a');
      expect(result, Dictionary.errorMinimumUsername);
    });

    test('Maximum Username Test', () {
      var result = Validations.usernameValidation('aaaaaaaaaaaaaaaaasasasasas'
          'sasasasasasasasasasasasasasasasasasasasasasaasasasasasasasasasasasasas'
          'asasasasasasasasasasasasasasasasasasasasasasasasasasasassasasasasasasa'
          'asasassasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasa'
          'asasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas'
          'asasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas');
      expect(result, Dictionary.errorMaximumUsername);
    });

    test('Invalid Username Test', () {
      var result = Validations.usernameValidation('UserRW');
      expect(result, Dictionary.errorInvalidUsername);
    });

    test('Valid Username Test', () {
      var result = Validations.usernameValidation('user');
      expect(result, null);
    });
  });

  group('Password Test', () {
    test('Empty Password Test', () {
      var result = Validations.passwordValidation('');
      expect(result, Dictionary.errorEmptyPassword);
    });

    test('Valid Password Test', () {
      var result = Validations.passwordValidation('123456');
      expect(result, null);
    });
  });

  group('Confirmation Password Test', () {
    test('Empty Confirmation Password Test', () {
      var result = Validations.confirmPasswordValidation('123456', '');
      expect(result, Dictionary.errorEmptyConfirmPassword);
    });

    test('Not Match Confirmation Password Test', () {
      var result = Validations.confirmPasswordValidation('123456', 'efgh');
      expect(result, Dictionary.errorNotMatchPassword);
    });

    test('Match Confirmation Password Test', () {
      var result = Validations.confirmPasswordValidation('123456', '123456');
      expect(result, null);
    });
  });

  group('Email Test', () {
    test('Empty Email Test', () {
      var result = Validations.emailValidation('');
      expect(result, Dictionary.errorEmptyEmail);
    });

    test('Valid Email Test', () {
      var result = Validations.emailValidation('test@email.com');
      expect(result, null);
    });

    test('Invalid Email Test', () {
      var result = Validations.emailValidation('test.');
      expect(result, Dictionary.errorInvalidEmail);
    });
  });

  group('Phone Test', () {
    test('Empty Phone Test', () {
      var result = Validations.phoneValidation('');
      expect(result, Dictionary.errorEmptyPhone);
    });

    test('Minimum Phone Test', () {
      var result = Validations.phoneValidation('0812');
      expect(result, Dictionary.errorInvalidPhone);
    });

    test('Maximum Phone Test', () {
      var result = Validations.phoneValidation('0812345678910123');
      expect(result, Dictionary.errorMaximumPhone);
    });

    test('Invalid Phone Test', () {
      var result = Validations.phoneValidation('777777777');
      expect(result, Dictionary.errorInvalidPhone);
    });

    test('Valid Phone Test', () {
      var result = Validations.phoneValidation('081320111222');
      expect(result, null);
    });
  });

  group('Address Validation Test', () {
    test('Empty Address Test', () {
      var result = Validations.addressValidation('');
      expect(result, Dictionary.errorEmptyAddress);
    });

    test('Maximum Address Test', () {
      var result = Validations.addressValidation(
          'aaaaaaaaaaaaaaaaasasasasas'
              'sasasasasasasasasasasasasasasasasasasasasasaasasasasasasasasasasasasas'
              'asasasasasasasasasasasasasasasasasasasasasasasasasasasassasasasasasasa'
              'asasassasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasa'
              'asasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas'
              'asasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas');
      expect(result, Dictionary.errorMaximumAddress);
    });

    test('Valid Address Test', () {
      var result = Validations.addressValidation('Jl. Burangrang');
      expect(result, null);
    });
  });

  group('RT Validation Test', () {
    test('Empty RT Test', () {
      var result = Validations.rtValidation('');
      expect(result, Dictionary.errorEmptyRT);
    });

    test('Minimum RT Test', () {
      var result = Validations.rtValidation('0');
      expect(result, Dictionary.errorMaximumRT);
    });

    test('Maximum RT Test', () {
      var result = Validations.rtValidation('0004');
      expect(result, Dictionary.errorMaximumRT);
    });

    test('Format RT Test', () {
      var result = Validations.rtValidation('abc');
      expect(result, Dictionary.errorInvalidRT);
    });

    test('Valid RT Test', () {
      var result = Validations.rtValidation('004');
      expect(result, null);
    });
  });

  group('Instagram Validation Test', () {
    test('Empty Instagram Test', () {
      var result = Validations.instagramValidation('');
      expect(result, null);
    });

    test('Format Instagram Test', () {
      var result = Validations.instagramValidation('abc%^');
      expect(result, Dictionary.errorInvalidInstagram);
    });

    test('Valid Instagram Test', () {
      var result = Validations.instagramValidation('geek_des');
      expect(result, null);
    });
  });

  group('Twitter Validation Test', () {
    test('Empty Twitter Test', () {
      var result = Validations.twitterlidation('');
      expect(result, null);
    });

    test('Format Twitter Test', () {
      var result = Validations.twitterlidation('abc%^');
      expect(result, Dictionary.errorInvalidTwitter);
    });

    test('Valid Twitter Test', () {
      var result = Validations.twitterlidation('geek_des');
      expect(result, null);
    });
  });

  group('Facebook Validation Test', () {
    test('Empty Facebook Test', () {
      var result = Validations.facebooklidation('');
      expect(result, null);
    });

    test('Maximum Facebook Test', () {
      var result = Validations.facebooklidation(
          'asdsdadsdasdasdasdasdadsdsadadasd');
      expect(result, Dictionary.errorInvalidFacebook);
    });

    test('Format Facebook Test', () {
      var result = Validations.facebooklidation('jack');
      expect(result, Dictionary.errorInvalidFacebook);
    });

    test('Valid Facebook Test', () {
      var result = Validations.facebooklidation('https://facebook.com/abcd');
      expect(result, null);
    });
  });

  group('Usulan Validation Test', () {
    test('Title Usulan Empty Test', () {
      var result = Validations.titleUsulanValidation('');
      expect(result, Dictionary.errorEmptyTitleUsulan);
    });

    test('Title Usulan Minimum Length Test', () {
      var result = Validations.titleUsulanValidation('asdasd');
      expect(result, Dictionary.errorMinimumTitleUsulan);
    });

    test('Title Usulan Maximum Length Test', () {
      var result = Validations.titleUsulanValidation(
          'asdsafasasdsasdsasdsawdsasdwqsdsasdfeasdsawrrwqwsdwqwsdsaswertrr');
      expect(result, Dictionary.errorMaximumTitleUsulan);
    });

    test('Title Usulan Format Test', () {
      var result = Validations.titleUsulanValidation('asdasd...! asdasd');
      expect(result, Dictionary.errorTitleFormat);
    });

    test('Desc Usulan Empty Test', () {
      var result = Validations.descUsulanValidation('');
      expect(result, Dictionary.errorEmptyDescUsulan);
    });

    test('ValidationException to String Test', () {
      expect(ValidationException().toString(), 'ValidationException');
    });
  });

  group('Comment Input Validation Test', () {
    test('Empty Comment Test', () {
      var result = Validations.commentValidation('');
      expect(result, Dictionary.errorEmptyComment);
    });

    test('Minimum Comment Input Test', () {
      var result = Validations.commentValidation(
          'abcd');
      expect(result, Dictionary.errorMinimumComment);
    });

    test('Valid Comment Input Test', () {
      var result = Validations.commentValidation('Test Comment Valid');
      expect(result, null);
    });
  });
}
