import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/gamifications/gamification_detail/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_detail_mission/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_list/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_list_history/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_list_onprogress/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_my_badge/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GamificationDetailOnProgressModel.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/models/GamificationsModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';

class MockImportantGamificationRepository extends Mock
    implements GamificationsRepository {}

void main() {
  MockImportantGamificationRepository mockRepo;
  GamificationsListBloc gamificationsListBloc;
  GamificationsDetailBloc gamificationsDetailBloc;
  GamificationsListHistoryBloc gamificationsListHistoryBloc;
  GamificationsListOnProgressBloc gamificationsListOnProgressBloc;
  GamificationsDetailMissionBloc gamificationsDetailMissionBloc;
  GamificationsMyBadgeBloc gamificationsMyBadgeBloc;


  setUp(() {
    mockRepo = MockImportantGamificationRepository();
    gamificationsListBloc =
        GamificationsListBloc(gamificationsRepository: mockRepo);
    gamificationsDetailBloc =
        GamificationsDetailBloc(gamificationsRepository: mockRepo);
    gamificationsListHistoryBloc =
        GamificationsListHistoryBloc(gamificationsRepository: mockRepo);
    gamificationsListOnProgressBloc =
        GamificationsListOnProgressBloc(gamificationsRepository: mockRepo);
    gamificationsDetailMissionBloc =
        GamificationsDetailMissionBloc(gamificationsRepository: mockRepo);
    gamificationsMyBadgeBloc =
        GamificationsMyBadgeBloc(gamificationsRepository: mockRepo);
  });

  // LIST NEW MISSION TEST
  group('Gamification list new mission Test', () {
    test('Gaimification list new mission state is correct', () {
      expect(gamificationsListBloc.initialState, GamificationsListInitial());
    });

    test('GamificationsListLoad to String', () {
      expect(GamificationsNewMissionListLoad(page: 1).toString(),
          'Event GamificationsListLoad');
    });

    test('GamificationsListLoad props', () {
      expect(GamificationsNewMissionListLoad(page: 1).props, [1]);
    });

    test('GamificationsList dispose does not emit new states', () {
      expectLater(
        gamificationsListBloc,
        emitsInOrder([]),
      );
      gamificationsListBloc.close();
    });

    test(
        'emits [GamificationsListInitial, GamificationsListLoading, GamificationsListLoaded]',
        () {
      GamificationModel records;

      when(mockRepo.getNewMission(page: 1, status: 10))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        GamificationsListInitial(),
        GamificationsListLoading(),
        GamificationsListLoaded(records: records)
      ];

      expectLater(
        gamificationsListBloc,
        emitsInOrder(expectedResponse),
      );

      gamificationsListBloc.add(GamificationsNewMissionListLoad(page: 1));
    });

    test(
        'emits [ImportantInfoHomeInitial, ImportantInfoHomeLoading, ImportantInfoHomeFailure]',
        () {
      when(mockRepo.getNewMission(page: 1, status: 10))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        GamificationsListInitial(),
        GamificationsListLoading(),
        GamificationsListFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException)),
      ];

      expectLater(
        gamificationsListBloc,
        emitsInOrder(expectedResponse),
      );

      gamificationsListBloc.add(GamificationsNewMissionListLoad(page: 1));
    });
  });

  //DETAIL NEW MISSION
  group('Gamification detail new mission Test', () {
    test('Gaimification detail new mission state is correct', () {
      expect(
          gamificationsDetailBloc.initialState, GamificationsDetailInitial());
    });

    test('GamificationsDetailLoad to String', () {
      expect(GamificationsDetailLoad(id: 1).toString(),
          'Event GamificationsDetailLoad');
    });

    test('GamificationsDetailLoad props', () {
      expect(GamificationsDetailLoad(id: 1).props, [1]);
    });

    test('GamificationsTakeMission to String', () {
      expect(GamificationsTakeMission(id: 1).toString(),
          'Event GamificationsTakeMission');
    });

    test('GamificationsTakeMission props', () {
      expect(GamificationsTakeMission(id: 1).props, [1]);
    });

    test('GamificationsDetail dispose does not emit new states', () {
      expectLater(
        gamificationsDetailBloc,
        emitsInOrder([]),
      );
      gamificationsDetailBloc.close();
    });

    test(
        'emits [GamificationsDetailInitial, GamificationsDetailLoading, GamificationsDetailLoaded]',
        () {
      ItemGamification records;

      when(mockRepo.getNewMissionDetail(id: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        GamificationsDetailInitial(),
        GamificationsDetailLoading(),
        GamificationsDetailLoaded(records: records)
      ];

      expectLater(
        gamificationsDetailBloc,
        emitsInOrder(expectedResponse),
      );

      gamificationsDetailBloc.add(GamificationsDetailLoad(id: 1));
    });

    test(
        'emits [GamificationsDetailInitial, GamificationsDetailLoading, GamificationsDetailFailure]',
        () {
      when(mockRepo.getNewMissionDetail(id: 1))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        GamificationsDetailInitial(),
        GamificationsDetailLoading(),
        GamificationsDetailFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException)),
      ];

      expectLater(
        gamificationsDetailBloc,
        emitsInOrder(expectedResponse),
      );

      gamificationsDetailBloc.add(GamificationsDetailLoad(id: 1));
    });

    test(
        'emits [GamificationsDetailInitial, GamificationsTakeMissionlLoading, GamificationsTakeMissionLoaded]',
            () {
          when(mockRepo.takeMission(id: 1))
              .thenAnswer((_) => Future.value(true));

          final expectedResponse = [
            GamificationsDetailInitial(),
            GamificationsTakeMissionlLoading(),
            GamificationsTakeMissionLoaded(isTakeMission: true)
          ];

          expectLater(
            gamificationsDetailBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsDetailBloc.add(GamificationsTakeMission(id: 1));
        });

    test(
        'emits [GamificationsDetailInitial, GamificationsTakeMissionlLoading, GamificationsTakeMissionFailure]',
            () {
          when(mockRepo.takeMission(id: 1))
              .thenThrow(Exception(ErrorException.timeoutException));

          final expectedResponse = [
            GamificationsDetailInitial(),
            GamificationsTakeMissionlLoading(),
            GamificationsTakeMissionFailure(
                error: CustomException.onConnectionException(
                    ErrorException.timeoutException)),
          ];

          expectLater(
            gamificationsDetailBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsDetailBloc.add(GamificationsTakeMission(id: 1));
        });
  });

  // LIST NEW MISSION TEST
  group('Gamification list new mission Test', () {
    test('Gaimification list new mission state is correct', () {
      expect(gamificationsListBloc.initialState, GamificationsListInitial());
    });

    test('GamificationsListLoad to String', () {
      expect(GamificationsNewMissionListLoad(page: 1).toString(),
          'Event GamificationsListLoad');
    });

    test('GamificationsListLoad props', () {
      expect(GamificationsNewMissionListLoad(page: 1).props, [1]);
    });

    test('GamificationsList dispose does not emit new states', () {
      expectLater(
        gamificationsListBloc,
        emitsInOrder([]),
      );
      gamificationsListBloc.close();
    });

    test(
        'emits [GamificationsListInitial, GamificationsListLoading, GamificationsListLoaded]',
            () {
          GamificationModel records;

          when(mockRepo.getNewMission(page: 1, status: 10))
              .thenAnswer((_) => Future.value(records));

          final expectedResponse = [
            GamificationsListInitial(),
            GamificationsListLoading(),
            GamificationsListLoaded(records: records)
          ];

          expectLater(
            gamificationsListBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsListBloc.add(GamificationsNewMissionListLoad(page: 1));
        });

    test(
        'emits [ImportantInfoHomeInitial, ImportantInfoHomeLoading, ImportantInfoHomeFailure]',
            () {
          when(mockRepo.getNewMission(page: 1, status: 10))
              .thenThrow(Exception(ErrorException.timeoutException));

          final expectedResponse = [
            GamificationsListInitial(),
            GamificationsListLoading(),
            GamificationsListFailure(
                error: CustomException.onConnectionException(
                    ErrorException.timeoutException)),
          ];

          expectLater(
            gamificationsListBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsListBloc.add(GamificationsNewMissionListLoad(page: 1));
        });
  });

  // LIST  MISSION HISTORY TEST
  group('Gamification list history mission Test', () {
    test('Gaimification list history mission state is correct', () {
      expect(gamificationsListHistoryBloc.initialState, GamificationsListHistoryInitial());
    });

    test('GamificationsHistoryListLoad to String', () {
      expect(GamificationsHistoryListLoad(page: 1).toString(),
          'Event GamificationsHistoryListLoad');
    });

    test('GamificationsHistoryListLoad props', () {
      expect(GamificationsHistoryListLoad(page: 1).props, [1]);
    });

    test('GamificationsHistoryList dispose does not emit new states', () {
      expectLater(
        gamificationsListHistoryBloc,
        emitsInOrder([]),
      );
      gamificationsListHistoryBloc.close();
    });

    test(
        'emits [GamificationsListHistoryInitial, GamificationsListHistoryLoading, GamificationsListHistoryLoaded]',
            () {
          GamificationOnprogressModel records;

          when(mockRepo.getMissionHistory(page: 1))
              .thenAnswer((_) => Future.value(records));

          final expectedResponse = [
            GamificationsListHistoryInitial(),
            GamificationsListHistoryLoading(),
            GamificationsListHistoryLoaded(records: records)
          ];

          expectLater(
            gamificationsListHistoryBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsListHistoryBloc.add(GamificationsHistoryListLoad(page: 1));
        });

    test(
        'emits [GamificationsListHistoryInitial, GamificationsListHistoryLoading, GamificationsListHistoryFailure]',
            () {
          when(mockRepo.getMissionHistory(page: 1))
              .thenThrow(Exception(ErrorException.timeoutException));

          final expectedResponse = [
            GamificationsListHistoryInitial(),
            GamificationsListHistoryLoading(),
            GamificationsListHistoryFailure(
                error: CustomException.onConnectionException(
                    ErrorException.timeoutException)),
          ];

          expectLater(
            gamificationsListHistoryBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsListHistoryBloc.add(GamificationsHistoryListLoad(page: 1));
        });
  });

  // LIST  MISSION ONPROGRESS TEST
  group('Gamification list onprogress mission Test', () {
    test('Gaimification list onprogress mission state is correct', () {
      expect(gamificationsListOnProgressBloc.initialState, GamificationsListOnProgressInitial());
    });

    test('GamificationsOnProgressListLoad to String', () {
      expect(GamificationsOnProgressListLoad(page: 1).toString(),
          'Event GamificationsOnProgressListLoad');
    });

    test('GamificationsOnProgressListLoad props', () {
      expect(GamificationsOnProgressListLoad(page: 1).props, [1]);
    });

    test('GamificationsOnProgressList dispose does not emit new states', () {
      expectLater(
        gamificationsListOnProgressBloc,
        emitsInOrder([]),
      );
      gamificationsListHistoryBloc.close();
    });

    test(
        'emits [GamificationsListOnProgressInitial, GamificationsListOnProgressLoading, GamificationsListOnProgressLoaded]',
            () {
          GamificationOnprogressModel records;

          when(mockRepo.getMissionOnProgress(page: 1))
              .thenAnswer((_) => Future.value(records));

          final expectedResponse = [
            GamificationsListOnProgressInitial(),
            GamificationsListOnProgressLoading(),
            GamificationsListOnProgressLoaded(records: records)
          ];

          expectLater(
            gamificationsListOnProgressBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsListOnProgressBloc.add(GamificationsOnProgressListLoad(page: 1));
        });

    test(
        'emits [GamificationsListOnProgressInitial, GamificationsListHistoryLoading, GamificationsListOnProgressFailure]',
            () {
          when(mockRepo.getMissionOnProgress(page: 1))
              .thenThrow(Exception(ErrorException.timeoutException));

          final expectedResponse = [
            GamificationsListOnProgressInitial(),
            GamificationsListOnProgressLoading(),
            GamificationsListOnProgressFailure(
                error: CustomException.onConnectionException(
                    ErrorException.timeoutException)),
          ];

          expectLater(
            gamificationsListOnProgressBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsListOnProgressBloc.add(GamificationsOnProgressListLoad(page: 1));
        });
  });

  //DETAIL MISSION HISTORY AND PROGRESS
  group('Gamification detail History and Progress mission Test', () {
    test('Gaimification detail History and Progress state is correct', () {
      expect(
          gamificationsDetailMissionBloc.initialState, GamificationsDetailMissionInitial());
    });

    test('GamificationsDetailMissionLoad to String', () {
      expect(GamificationsDetailMissionLoad(id: 1).toString(),
          'Event GamificationsDetailMissionLoad');
    });

    test('GamificationsDetailMissionLoad props', () {
      expect(GamificationsDetailMissionLoad(id: 1).props, [1]);
    });

    test('GamificationsDetailMission History and Progress dispose does not emit new states', () {
      expectLater(
        gamificationsDetailMissionBloc,
        emitsInOrder([]),
      );
      gamificationsDetailMissionBloc.close();
    });

    test(
        'emits [GamificationsDetailInitial, GamificationsDetailLoading, GamificationsDetailLoaded]',
            () {
          GamificationDetailOnProgressModel records;

          when(mockRepo.getDetailMission(id: 1))
              .thenAnswer((_) => Future.value(records));

          final expectedResponse = [
            GamificationsDetailMissionInitial(),
            GamificationsDetailMissionLoading(),
            GamificationsDetailMissionLoaded(records: records)
          ];

          expectLater(
            gamificationsDetailMissionBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsDetailMissionBloc.add(GamificationsDetailMissionLoad(id: 1));
        });

    test(
        'emits [GamificationsDetailMissionInitial, GamificationsDetailMissionLoading, GamificationsDetailMissionFailure]',
            () {
          when(mockRepo.getDetailMission(id: 1))
              .thenThrow(Exception(ErrorException.timeoutException));

          final expectedResponse = [
            GamificationsDetailMissionInitial(),
            GamificationsDetailMissionLoading(),
            GamificationsDetailMissionFailure(
                error: CustomException.onConnectionException(
                    ErrorException.timeoutException)),
          ];

          expectLater(
            gamificationsDetailMissionBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsDetailMissionBloc.add(GamificationsDetailMissionLoad(id: 1));
        });
  });

  // MY BAGDE TEST
  group('Gamification My Badge Test', () {
    test('Gaimification My Badge state is correct', () {
      expect(gamificationsMyBadgeBloc.initialState, GamificationsMyBadgeInitial());
    });

    test('GamificationsMyBadgeLoad to String', () {
      expect(GamificationsMyBadgeLoad(page: 1).toString(),
          'Event GamificationsMyBadgeLoad');
    });

    test('GamificationsMyBadgeLoad props', () {
      expect(GamificationsMyBadgeLoad(page: 1).props, [1]);
    });

    test('GamificationsMyBadge dispose does not emit new states', () {
      expectLater(
        gamificationsMyBadgeBloc,
        emitsInOrder([]),
      );
      gamificationsMyBadgeBloc.close();
    });

    test(
        'emits [GamificationsMyBadgeInitial, GamificationsMyBadgeLoading, GamificationsMyBadgeLoaded]',
            () {
          GamificationOnprogressModel records;

          when(mockRepo.getMyBadge(page: 1))
              .thenAnswer((_) => Future.value(records));

          final expectedResponse = [
            GamificationsMyBadgeInitial(),
            GamificationsMyBadgeLoading(),
            GamificationsMyBadgeLoaded(records: records)
          ];

          expectLater(
            gamificationsMyBadgeBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsMyBadgeBloc.add(GamificationsMyBadgeLoad(page: 1));
        });

    test(
        'emits [GamificationsMyBadgeInitial, GamificationsMyBadgeLoading, GamificationsMyBadgeFailure]',
            () {
          when(mockRepo.getMyBadge(page: 1))
              .thenThrow(Exception(ErrorException.timeoutException));

          final expectedResponse = [
            GamificationsMyBadgeInitial(),
            GamificationsMyBadgeLoading(),
            GamificationsMyBadgeFailure(
                error: CustomException.onConnectionException(
                    ErrorException.timeoutException)),
          ];

          expectLater(
            gamificationsMyBadgeBloc,
            emitsInOrder(expectedResponse),
          );

          gamificationsMyBadgeBloc.add(GamificationsMyBadgeLoad(page: 1));
        });
  });

}
