import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/humasjaba'
    'r_list/HumasJabarListBloc.dart';
import 'package:sapawarga/blocs/humasjabar_list/HumasJabarListEvent.dart';
import 'package:sapawarga/blocs/humasjabar_list/HumasJabarListState.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/HumasJabarModel.dart';
import 'package:sapawarga/repositories/HumasJabarRepository.dart';

class MockHumasJabar extends Mock implements HumasJabarRepository {}

void main() {
  HumasJabarListBloc humasJabarListBloc;
  HumasJabarRepository mockHumasJabar;

  setUp(() {
    mockHumasJabar = MockHumasJabar();
    humasJabarListBloc =
        HumasJabarListBloc(humasJabarRepository: mockHumasJabar);
  });

  group('HumasJabarList', () {
    test('HumasJabarList initial state is correct', () {
      expect(humasJabarListBloc.initialState, HumasJabarlInitial());
    });

    test('HumasJabarLoad to String', () {
      expect(HumasJabarLoad().toString(), 'Event HumasJabarListLoaded');
    });

    test('HumasJabarLoad props', () {
      expect(HumasJabarLoad().props, []);
    });

    test('HumasJabarFailure to String', () {
      expect(HumasJabarFailure(error: null).toString(),
          'State HumasJabarListFailure');
    });

    test('HumasJabarList dispose does not emit new states', () {
      expectLater(
        humasJabarListBloc,
        emitsInOrder([]),
      );
      humasJabarListBloc.close();
    });

    test(
        'emits [HumasJabarListInitial, HumasJabarListLoading, HumasJabarListLoaded]',
        () {
      List<HumasJabarModel> records = [];

      when(mockHumasJabar.getRecordsDataHumasJabar(forceRefresh: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        HumasJabarlInitial(),
        HumasJabarLoading(),
        HumasJabarLoaded(records: records)
      ];

      expectLater(
        humasJabarListBloc,
        emitsInOrder(expectedResponse),
      );

      humasJabarListBloc.add(HumasJabarLoad());
    });

    test(
        'emits [HumasJabarListInitial, HumasJabarListLoading, HumasJabarFailure]',
        () {
      when(mockHumasJabar.getRecordsDataHumasJabar(forceRefresh: true))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        HumasJabarlInitial(),
        HumasJabarLoading(),
        HumasJabarFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        humasJabarListBloc,
        emitsInOrder(expectedResponse),
      );

      humasJabarListBloc.add(HumasJabarLoad());
    });
  });
}
