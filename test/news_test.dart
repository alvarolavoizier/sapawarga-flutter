import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/news_detail/Bloc.dart';
import 'package:sapawarga/blocs/news_list/Bloc.dart';
import 'package:sapawarga/blocs/news_list_featured/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';

class MockNewsRepository extends Mock implements NewsRepository {}

void main() {
  MockNewsRepository mockNewsRepository;
  NewsListBloc newsListBloc;
  NewsListFeaturedBloc newsListFeaturedBloc;
  NewsDetailBloc newsDetailBloc;

  setUp(() {
    mockNewsRepository = MockNewsRepository();
    newsListBloc = NewsListBloc(newsRepository: mockNewsRepository);
    newsDetailBloc = NewsDetailBloc(newsRepository: mockNewsRepository);
    newsListFeaturedBloc =
        NewsListFeaturedBloc(newsRepository: mockNewsRepository);
  });

  test('NewsListLoad to String  ', () {
    expect(NewsListLoad(isIdKota: null, page: 0).toString(), 'Event NewsListLoad');
  });

  test('NewsDetailLike props  ', () {
    expect(NewsDetailLike(newsId: 1).props, [1]);
  });

  test('NewsListLoad props  ', () {
    expect(NewsListLoad(isIdKota: null, page: 0).props, [null]);
  });

  test('NewsListFeaturedLoad to String  ', () {
    expect(NewsListFeaturedLoad(isIdKota: null).toString(),
        'Event NewsListFeaturedLoad');
  });

  test('NewsListFeaturedLoad props  ', () {
    expect(NewsListFeaturedLoad(isIdKota: null).props,
        [null]);
  });

  group('NewsList Province', () {
    test('NewsList initial state is correct', () {
      expect(newsListBloc.initialState, NewsListInitial());
    });

    test('NewsList dispose does not emit new states', () {
      expectLater(
        newsListBloc,
        emitsInOrder([]),
      );
      newsListBloc.close();
    });

    test('emits [NewsListInitial, NewsListLoading, NewsListLoaded]', () {
      const MethodChannel('plugins.flutter.io/shared_preferences')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getAll') {
          return <String, dynamic>{}; // set initial values here if desired
        }
        return null;
      });

      List<NewsModel> records = [];

      when(mockNewsRepository.getNews(isKotaId: false, page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        NewsListInitial(),
        NewsListLoading(),
        NewsListLoaded(listNews: records, maxData: 0, isLoad: false)
      ];

      expectLater(
        newsListBloc,
        emitsInOrder(expectedResponse),
      );

      newsListBloc.add(NewsListLoad(isIdKota: false, page: 1));
    });
  });

  group('NewsList City', () {
    test('NewsList initial state is correct', () {
      expect(newsListBloc.initialState, NewsListInitial());
    });

    test('NewsList dispose does not emit new states', () {
      expectLater(
        newsListBloc,
        emitsInOrder([]),
      );
      newsListBloc.close();
    });

    test('emits [NewsListInitial, NewsListLoading, NewsListLoaded]', () {

      List<NewsModel> records = [];

      when(mockNewsRepository.getNews(isKotaId: true, page: 1))
          .thenAnswer((_) => Future.value(records));

      when(mockNewsRepository.getCityName())
          .thenAnswer((_) => Future.value('Bandung'));

      final expectedResponse = [
        NewsListInitial(),
        NewsListLoading(),
        NewsListLoaded(listNews: records, cityName: 'Bandung', isLoad: true, maxData: 1)
      ];

      expectLater(
        newsListBloc,
        emitsInOrder(expectedResponse),
      );

      newsListBloc.add(NewsListLoad(isIdKota: true, page: 1));
    });

    test('emits [NewsListInitial, NewsListLoading, NewsListLoaded]', () {
      List<NewsModel> records = [];

      when(mockNewsRepository.getNews(isKotaId: true, page: 1))
          .thenAnswer((_) => Future.value(records));

      when(mockNewsRepository.getCityName())
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        NewsListInitial(),
        NewsListLoading(),
        NewsListLoaded(listNews: records, maxData: 0, isLoad: true)
      ];

      expectLater(
        newsListBloc,
        emitsInOrder(expectedResponse),
      );

      newsListBloc.add(NewsListLoad(isIdKota: null, page: 1));
    });

    test('emits [NewsListInitial, NewsListLoading, NewsListFailure]', () {

      when(mockNewsRepository.getNews(isKotaId: null, page: 1))
          .thenThrow(ErrorException.timeoutException);

      when(mockNewsRepository.getCityName())
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        NewsListInitial(),
        NewsListLoading(),
        NewsListFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        newsListBloc,
        emitsInOrder(expectedResponse),
      );

      newsListBloc.add(NewsListLoad(isIdKota: null, page: 1));
    });
  });

  group('NewsListFeatured Province', () {
    test('NewsListFeatured initial state is correct', () {
      expect(newsListFeaturedBloc.initialState, NewsListFeaturedInitial());
    });

    test('NewsListFeatured dispose does not emit new states', () {
      expectLater(
        newsListFeaturedBloc,
        emitsInOrder([]),
      );
      newsListFeaturedBloc.close();
    });

    test('emits [NewsListFeatured, NewsListFeatured, NewsListFeaturedLoaded]',
        () {
      List<NewsModel> records = [];

      when(mockNewsRepository.getFeaturedNews(isKotaId: false))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        NewsListFeaturedInitial(),
        NewsListFeaturedLoading(),
        NewsListFeatuedLoaded(listtNews: records)
      ];

      expectLater(
        newsListFeaturedBloc,
        emitsInOrder(expectedResponse),
      );

      newsListFeaturedBloc.add(NewsListFeaturedLoad(isIdKota: false));
    });

    test('emits [NewsListFeatured, NewsListFeatured, NewsListFeaturedLoaded]',
        () {
      List<NewsModel> records = [];

      when(mockNewsRepository.getFeaturedNews(isKotaId: false))
          .thenAnswer((_) => Future.value(records));

      when(mockNewsRepository.getCityName()).thenThrow(Exception());

      final expectedResponse = [
        NewsListFeaturedInitial(),
        NewsListFeaturedLoading(),
        NewsListFeatuedLoaded(listtNews: [])
      ];

      expectLater(
        newsListFeaturedBloc,
        emitsInOrder(expectedResponse),
      );

      newsListFeaturedBloc.add(NewsListFeaturedLoad(isIdKota: false));
    });

    test(
        'emits [NewsListFeatured, NewsListFeatured, NewsListNewsListFeaturedFailure]',
        () {
      when(mockNewsRepository.getFeaturedNews(isKotaId: false))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      when(mockNewsRepository.getCityName()).thenThrow(Exception());

      final expectedResponse = [
        NewsListFeaturedInitial(),
        NewsListFeaturedLoading(),
        NewsListFeaturedFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        newsListFeaturedBloc,
        emitsInOrder(expectedResponse),
      );

      newsListFeaturedBloc.add(NewsListFeaturedLoad(isIdKota: false));
    });
  });

  group('NewsListFeatured City', () {
    test('NewsListFeatured initial state is correct', () {
      expect(newsListFeaturedBloc.initialState, NewsListFeaturedInitial());
    });

    test('NewsListFeatured dispose does not emit new states', () {
      expectLater(
        newsListFeaturedBloc,
        emitsInOrder([]),
      );
      newsListFeaturedBloc.close();
    });

    test('emits [NewsListFeatured, NewsListFeatured, NewsListFeatured]', () {
      List<NewsModel> records = [];

      when(mockNewsRepository.getFeaturedNews(isKotaId: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        NewsListFeaturedInitial(),
        NewsListFeaturedLoading(),
        NewsListFeatuedLoaded(listtNews: records)
      ];

      expectLater(
        newsListFeaturedBloc,
        emitsInOrder(expectedResponse),
      );

      newsListFeaturedBloc.add(NewsListFeaturedLoad(isIdKota: true));
    });
  });

  group('NewsDetail', () {
    test('NewsDetail initial state is correct', () {
      expect(newsDetailBloc.initialState, NewsDetailInitial());
    });

    test('NewsDetailLoad to String', () {
      expect(NewsDetailLoad(newsId: 1).toString(),
          'Event NewsDetailLoad{newsId: 1}');
    });

    test('NewsDetailLike to String', () {
      expect(NewsDetailLike(newsId: 1).toString(),
          'Event NewsDetailLike{newsId: 1}');
    });

    test('NewsDetailLoad props', () {
      expect(NewsDetailLoad(newsId: 1).props,
          [1]);
    });

    test('NewsDetail dispose does not emit new states', () {
      expectLater(
        newsDetailBloc,
        emitsInOrder([]),
      );
      newsDetailBloc.close();
    });

    test('emits [NewsDetailInitial, NewsDetailLoading, NewsDetailLoaded]', () {
      NewsModel _data = NewsModel(id: 1, title: 'a');

      when(mockNewsRepository.getNewsDetail(newsId: 1))
          .thenAnswer((_) => Future.value(_data));

      final expectedResponse = [
        NewsDetailInitial(),
        NewsDetailLoading(),
        NewsDetailLoaded(newsDetail: _data)
      ];

      expectLater(
        newsDetailBloc,
        emitsInOrder(expectedResponse),
      );

      newsDetailBloc.add(NewsDetailLoad(newsId: 1));
    });

    test('emits [NewsDetailInitial, NewsDetailLoading, NewsDetailLoaded]', () {
      NewsModel _data = NewsModel(id: 1, title: 'a');

      when(mockNewsRepository.getNewsDetail(newsId: 1))
          .thenAnswer((_) => Future.value(_data));

      when(mockNewsRepository.getLatestNews(newsId: 1))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        NewsDetailInitial(),
        NewsDetailLoading(),
        NewsDetailLoaded(newsDetail: _data, latestNews: [])
      ];

      expectLater(
        newsDetailBloc,
        emitsInOrder(expectedResponse),
      );

      newsDetailBloc.add(NewsDetailLoad(newsId: 1));
    });

    test('emits [NewsDetailInitial, NewsDetailLoading, NewsDetailFailure]', () {
      when(mockNewsRepository.getNewsDetail(newsId: 3))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        NewsDetailInitial(),
        NewsDetailLoading(),
        NewsDetailFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        newsDetailBloc,
        emitsInOrder(expectedResponse),
      );

      newsDetailBloc.add(NewsDetailLoad(newsId: 3));
    });

    test('emits [NewsDetailInitial, NewsDetailLike]', () {
      NewsModel _data = NewsModel(id: 1, title: 'a');

      when(mockNewsRepository.sendLike(id: _data.id))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        NewsDetailInitial()
      ];

      expectLater(
        newsDetailBloc,
        emitsInOrder(expectedResponse),
      );
      newsDetailBloc.add(NewsDetailLike(newsId: _data.id));
    });


    test('emits [NewsDetailInitial, NewsDetailLikeFailure]', () {
      when(mockNewsRepository.sendLike(id: 1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        NewsDetailInitial(),
      ];

      expectLater(
        newsDetailBloc,
        emitsInOrder(expectedResponse),
      );

      newsDetailBloc.add(NewsDetailLike(newsId: 1));
    });


  });
}
