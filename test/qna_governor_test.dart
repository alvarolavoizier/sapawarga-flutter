import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/qna_governor/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/QnACommentModel.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';

class MockQnAGovernorRepository extends Mock implements QnAGovernorRepository {}

void main() {
  MockQnAGovernorRepository mockRepo;
  QnAGovernorAddBloc qnAGovernorAddBloc;
  QnAGovernorListBloc qnAGovernorListBloc;
  QnAGovernorDetailBloc qnAGovernorDetailBloc;
  QnAGovernorCommentListBloc qnAGovernorCommentListBloc;

  setUp(() {
    mockRepo = MockQnAGovernorRepository();
    qnAGovernorAddBloc = QnAGovernorAddBloc(mockRepo);
    qnAGovernorListBloc = QnAGovernorListBloc(mockRepo);
    qnAGovernorDetailBloc = QnAGovernorDetailBloc(mockRepo);
    qnAGovernorCommentListBloc = QnAGovernorCommentListBloc(mockRepo);
  });

  // Q&A GOVERNOR ADD TEST
  group('QnAGovernorAdd Test', () {
    test('QnAGovernorAdd initial state is correct', () {
      expect(qnAGovernorAddBloc.initialState, QnAGovernorAddInitial());
    });

    test('QnAGovernorAdd to String', () {
      expect(QnAGovernorAdd(question: '').toString(), 'Event QnAGovernorAdd{question: }');
    });

    test('QnAGovernorAdd to props', () {
      expect(
          QnAGovernorAdd(question: '')
              .props,
          ['']);
    });


    test('QnAGovernorAdd dispose does not emit new states', () {
      expectLater(
        qnAGovernorAddBloc,
        emitsInOrder([]),
      );
      qnAGovernorAddBloc.close();
    });

    test('emits [QnAGovernorAddInitial, QnAGovernorAddLoading, QnAGovernorAdded]', () {
      QnAModel record = QnAModel();

      when(mockRepo.addQuestion(question: 'test'))
          .thenAnswer((_) => Future.value(record));

      final expectedResponse = [
        QnAGovernorAddInitial(),
        QnAGovernorAddLoading(),
        QnAGovernorAdded(record)
      ];

      expectLater(
        qnAGovernorAddBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorAddBloc.add(QnAGovernorAdd(question: 'test'));
    });

    test('emits [QnAGovernorAddInitial, QnAGovernorAddLoading, QnAGovernorAddFailure]', () {

      when(mockRepo.addQuestion(question: 'test'))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        QnAGovernorAddInitial(),
        QnAGovernorAddLoading(),
        QnAGovernorAddFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        qnAGovernorAddBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorAddBloc.add(QnAGovernorAdd(question: 'test'));
    });

  });

  // Q&A GOVERNOR LIST TEST
  group('QnAGovernorList Test', () {
    test('QnAGovernorList initial state is correct', () {
      expect(qnAGovernorListBloc.initialState, QnAGovernorListInitial());
    });

    test('QnAGovernorListLoad to String', () {
      expect(QnAGovernorListLoad(3).toString(), 'Event QnAGovernorListLoad{page: 3}');
    });

    test('QnAGovernorListLike to String', () {
      expect(QnAGovernorListLike(3).toString(), 'Event QnAGovernorListLike{id: 3}');
    });

    test('QnAGovernorListLoad to props', () {
      expect(
          QnAGovernorListLoad(3)
              .props,
          [3]);
    });

    test('QnAGovernorListLike to props', () {
      expect(
          QnAGovernorListLike(3)
              .props,
          [3]);
    });

    test('QnAGovernorList dispose does not emit new states', () {
      expectLater(
        qnAGovernorListBloc,
        emitsInOrder([]),
      );
      qnAGovernorListBloc.close();
    });

    test('emits [QnAGovernorListInitial, QnAGovernorListLoading, QnAGovernorListLoaded]', () {
      const MethodChannel('plugins.flutter.io/shared_preferences')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getAll') {
          return <String, dynamic>{};
        }
        return null;
      });

      List<QnAModel> records = [];

      when(mockRepo.fetchRecords(page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        QnAGovernorListInitial(),
        QnAGovernorListLoading(),
        QnAGovernorListLoaded(records, 0)
      ];

      expectLater(
        qnAGovernorListBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorListBloc.add(QnAGovernorListLoad(1));
    });

    test('emits [QnAGovernorListInitial, QnAGovernorListLoading, QnAGovernorListFailure]', () {

      when(mockRepo.fetchRecords(page: 1))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        QnAGovernorListInitial(),
        QnAGovernorListLoading(),
        QnAGovernorListFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        qnAGovernorListBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorListBloc.add(QnAGovernorListLoad(1));
    });

    test('emits [QnAGovernorListInitial]', () {

      when(mockRepo.sendLike(id: 3))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        QnAGovernorListInitial()
      ];

      expectLater(
        qnAGovernorListBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorListBloc.add(QnAGovernorListLike(3));
    });

    test('emits [QnAGovernorListInitial, QnAGovernorListFailure]', () {

      when(mockRepo.sendLike(id: 3))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        QnAGovernorListInitial(),
        QnAGovernorListFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        qnAGovernorListBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorListBloc.add(QnAGovernorListLike(3));
    });

  });

  // Q&A GOVERNOR COMMENT LIST TEST
  group('QnAGovernorCommentList Test', () {
    test('QnAGovernorCommentList initial state is correct', () {
      expect(qnAGovernorCommentListBloc.initialState, QnAGovernorCommentListInitial());
    });

    test('QnAGovernorCommentListLoad to String', () {
      expect(QnAGovernorCommentListLoad(questionId: 1, page: 1).toString(), 'Event QnAGovernorCommentListLoad{questionId: 1, page: 1}');
    });

    test('QnAGovernorCommentListLoad to props', () {
      expect(
          QnAGovernorCommentListLoad(questionId: 1, page: 1)
              .props,
          [1,1]);
    });

    test('QnAGovernorCommentList dispose does not emit new states', () {
      expectLater(
        qnAGovernorCommentListBloc,
        emitsInOrder([]),
      );
      qnAGovernorCommentListBloc.close();
    });

    test('emits [QnAGovernorCommentListInitial, QnAGovernorCommentListLoading, QnAGovernorCommentListLoaded]', () {
      List<QnACommentModel> records = [];

      when(mockRepo.fetchComments(questionId: 1, page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        QnAGovernorCommentListInitial(),
        QnAGovernorCommentListLoading(),
        QnAGovernorCommentListLoaded(records)
      ];

      expectLater(
        qnAGovernorCommentListBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorCommentListBloc.add(QnAGovernorCommentListLoad(questionId: 1, page: 1));
    });

    test('emits [QnAGovernorCommentListInitial, QnAGovernorCommentListLoading, QnAGovernorCommentListFailure]', () {

      when(mockRepo.fetchComments(questionId: 1, page: 1))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        QnAGovernorCommentListInitial(),
        QnAGovernorCommentListLoading(),
        QnAGovernorCommentListFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        qnAGovernorCommentListBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorCommentListBloc.add(QnAGovernorCommentListLoad(questionId: 1, page: 1));
    });

  });

  // Q&A GOVERNOR DETAIL TEST
  group('QnAGovernorDetail Test', () {
    test('QnAGovernorDetail initial state is correct', () {
      expect(qnAGovernorDetailBloc.initialState, QnAGovernorDetailInitial());
    });

    test('QnAGovernorDetailLoad to String', () {
      expect(QnAGovernorDetailLoad(3).toString(), 'Event QnAGovernorDetailLoad{id: 3}');
    });

    test('QnAGovernorDetailLike to String', () {
      expect(QnAGovernorDetailLike(3).toString(), 'Event QnAGovernorDetailLike{id: 3}');
    });

    test('QnAGovernorDetailLoad to props', () {
      expect(
          QnAGovernorDetailLoad(3)
              .props,
          [3]);
    });

    test('QnAGovernorDetailLike to props', () {
      expect(
          QnAGovernorDetailLike(3)
              .props,
          [3]);
    });

    test('QnAGovernorDetail dispose does not emit new states', () {
      expectLater(
        qnAGovernorDetailBloc,
        emitsInOrder([]),
      );
      qnAGovernorDetailBloc.close();
    });

    test('emits [QnAGovernorDetailInitial, QnAGovernorDetailLoading, QnAGovernorDetailLoaded]', () {
      QnAModel record = QnAModel();

      when(mockRepo.getDetail(id: 3))
          .thenAnswer((_) => Future.value(record));

      final expectedResponse = [
        QnAGovernorDetailInitial(),
        QnAGovernorDetailLoading(),
        QnAGovernorDetailLoaded(record)
      ];

      expectLater(
        qnAGovernorDetailBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorDetailBloc.add(QnAGovernorDetailLoad(3));
    });

    test('emits [QnAGovernorDetailInitial, QnAGovernorDetailLoading, QnAGovernorDetailFailure]', () {

      when(mockRepo.getDetail(id: 3))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        QnAGovernorDetailInitial(),
        QnAGovernorDetailLoading(),
        QnAGovernorDetailFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        qnAGovernorDetailBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorDetailBloc.add(QnAGovernorDetailLoad(3));
    });

    test('emits [QnAGovernorDetailInitial]', () {

      when(mockRepo.sendLike(id: 3))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        QnAGovernorDetailInitial()
      ];

      expectLater(
        qnAGovernorDetailBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorDetailBloc.add(QnAGovernorDetailLike(3));
    });

    test('emits [QnAGovernorDetailInitial, QnAGovernorDetailFailure]', () {

      when(mockRepo.sendLike(id: 3))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        QnAGovernorDetailInitial(),
        QnAGovernorDetailFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        qnAGovernorDetailBloc,
        emitsInOrder(expectedResponse),
      );

      qnAGovernorDetailBloc.add(QnAGovernorDetailLike(3));
    });

  });

}