
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';

main() {
  group('ImportantInformationRepository test', () {
    test('general test', () {

      List<ImportantInfoModel> list = [];

      ImportantInfoRepository repository = ImportantInfoRepository();
      var data = repository.fetchRecords(0, limit: 3);
      data.then((value){
        list = value;
      });

      repository.searchRecords(keyword: 'info', page: 1);
      repository.getTopFiveRecords(0);
      repository.getOtherRecords(0);
      repository.getDetail(111);
      repository.fetchCommentRecords(id: 111, page: 1);
      repository.postComment(id: 111, text: 'Unit Test');
      repository.sendLike(id: 111);


      assert(list != null);
      expect(list.isNotEmpty, false);
    });
  });

}