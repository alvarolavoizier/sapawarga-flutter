
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';
import 'package:sapawarga/models/HumasJabarModel.dart';
import 'package:sapawarga/models/MessageModel.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';
import 'package:sapawarga/repositories/CounterHoaxRepository.dart';
import 'package:sapawarga/repositories/HumasJabarRepository.dart';

main() {
  group('HumasJabarRepository test', () {
    test('general test', () {

      List<HumasJabarModel> list = [];

      HumasJabarRepository repository = HumasJabarRepository();
      var data = repository.getRecordsDataHumasJabar();
      data.then((value){
        list = value;
      });

      repository.fetchHumasJabarList();
      repository.truncateLocal();
      repository.insertDatabase(null);
      repository.getRecord(1);
      repository.getRecordsLocal();
      repository.hasLocalData();


      assert(list != null);
      expect(list.isNotEmpty, false);
    });
  });

}