import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/video_list/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/AreaModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/VideoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/VideoRepository.dart';
import 'package:test/test.dart';

class MockVideoRepository extends Mock implements VideoRepository {}

class MockAuthProfileRepository extends Mock implements AuthProfileRepository {}

void main() {
  MockVideoRepository mockVideoRepository;
  MockAuthProfileRepository mockAuthProfileRepository;
  VideoListJabarBloc videoListJabarBloc;
  VideoListKokabBloc videoListKokabBloc;

  setUp(() {
    mockVideoRepository = MockVideoRepository();
    mockAuthProfileRepository = MockAuthProfileRepository();
    videoListJabarBloc =
        VideoListJabarBloc(videoRepository: mockVideoRepository);
    videoListKokabBloc = VideoListKokabBloc(
        videoRepository: mockVideoRepository,
        authProfileRepository: mockAuthProfileRepository);
  });

  group('Video List Jabar Test', () {
    test('VideoList initial state is correct', () {
      expect(videoListJabarBloc.initialState, VideoListJabarInitial());
    });

    test('VideoListJabarLoad to String', () {
      expect(VideoListJabarLoad().toString(), 'Event VideoListJabarLoad');
    });

    test('VideoListJabarLoad props', () {
      expect(VideoListJabarLoad().props, []);
    });

    test('VideoList dispose does not emit new states', () {
      expectLater(
        videoListJabarBloc,
        emitsInOrder([]),
      );
      videoListJabarBloc.close();
    });

    test('emits [VideoListInitial, VideoListLoading, VideoListLoaded]', () {
      List<VideoModel> records = [];

      when(mockVideoRepository.fetchRecords())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        VideoListJabarInitial(),
        VideoListJabarLoading(),
        VideoListJabarLoaded(records: records)
      ];

      expectLater(
        videoListJabarBloc,
        emitsInOrder(expectedResponse),
      );

      videoListJabarBloc.add(VideoListJabarLoad());
    });

    test(
        'emits [VideoListInitial, VideoListLoading, VideoListLoaded] => Load from local',
        () {
      List<VideoModel> records = [];

      when(mockVideoRepository.fetchRecords())
          .thenThrow(Exception(ErrorException.unauthorizedException));

      when(mockVideoRepository.hasVideosJabarLocal())
          .thenAnswer((_) => Future.value(true));

      when(mockVideoRepository.getVideosJabarLocal())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        VideoListJabarInitial(),
        VideoListJabarLoading(),
        VideoListJabarLoaded(records: records)
      ];

      expectLater(
        videoListJabarBloc,
        emitsInOrder(expectedResponse),
      );

      videoListJabarBloc.add(VideoListJabarLoad());
    });

    test('emits [VideoListInitial, VideoListLoading, VideoListFailure]', () {
      List<VideoModel> records = [];

      when(mockVideoRepository.fetchRecords())
          .thenThrow(Exception(ErrorException.unauthorizedException));

      when(mockVideoRepository.hasVideosJabarLocal())
          .thenAnswer((_) => Future.value(false));

      when(mockVideoRepository.getVideosJabarLocal())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        VideoListJabarInitial(),
        VideoListJabarLoading(),
        VideoListJabarFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        videoListJabarBloc,
        emitsInOrder(expectedResponse),
      );

      videoListJabarBloc.add(VideoListJabarLoad());
    });
  });

  group('Video List Kokab', () {
    test('VideoList initial state is correct', () {
      expect(videoListKokabBloc.initialState, VideoListKokabInitial());
    });

    test('VideoList dispose does not emit new states', () {
      expectLater(
        videoListKokabBloc,
        emitsInOrder([]),
      );
      videoListKokabBloc.close();
    });

    test('VideoListKokabLoad to String', () {
      expect(VideoListKokabLoad().toString(), 'Event VideoListKokabLoad');
    });

    test('VideoListKokabLoad props', () {
      expect(VideoListKokabLoad().props, []);
    });

    test('emits [VideoListInitial, VideoListLoading, VideoListLoaded]', () {
      List<VideoModel> records = [];

      UserInfoModel userInfoModel = UserInfoModel(
          id: 1, name: 'jack', kabkota: Kabkota(id: 12, name: "Cirebon"));

      when(mockAuthProfileRepository.getUserInfo())
          .thenAnswer((_) => Future.value(userInfoModel));

      when(mockVideoRepository.fetchRecords(kokabId: userInfoModel.kabkotaId))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        VideoListKokabInitial(),
        VideoListKokabLoading(),
        VideoListKokabLoaded(
            records: records, kokab: userInfoModel.kabkota.name)
      ];

      expectLater(
        videoListKokabBloc,
        emitsInOrder(expectedResponse),
      );

      videoListKokabBloc.add(VideoListKokabLoad());
    });

    test(
        'emits [VideoListInitial, VideoListLoading, VideoListLoaded] => Load From Local',
        () {
      List<VideoModel> records = [];

      UserInfoModel userInfoModel = UserInfoModel(
          id: 1, name: 'jack', kabkota: Kabkota(id: 12, name: "Cirebon"));

      when(mockAuthProfileRepository.getUserInfo())
          .thenAnswer((_) => Future.value(userInfoModel));

      when(mockVideoRepository.fetchRecords(kokabId: userInfoModel.kabkotaId))
          .thenThrow(Exception(ErrorException.timeoutException));

      when(mockAuthProfileRepository.getUserInfo())
          .thenAnswer((_) => Future.value(userInfoModel));

      when(mockVideoRepository.hasVideosKokabLocal())
          .thenAnswer((_) => Future.value(true));

      when(mockVideoRepository.getVideosKokabLocal())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        VideoListKokabInitial(),
        VideoListKokabLoading(),
        VideoListKokabLoaded(
            records: records, kokab: userInfoModel.kabkota.name)
      ];

      expectLater(
        videoListKokabBloc,
        emitsInOrder(expectedResponse),
      );

      videoListKokabBloc.add(VideoListKokabLoad());
    });

    test('emits [VideoListInitial, VideoListLoading, VideoListFailure]', () {
      UserInfoModel userInfoModel = UserInfoModel(
          id: 1, name: 'jack', kabkota: Kabkota(id: 12, name: "Cirebon"));

      when(mockAuthProfileRepository.getUserInfo())
          .thenAnswer((_) => Future.value(userInfoModel));

      when(mockVideoRepository.fetchRecords(kokabId: userInfoModel.kabkotaId))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      when(mockAuthProfileRepository.getUserInfo())
          .thenAnswer((_) => Future.value(userInfoModel));

      when(mockVideoRepository.hasVideosKokabLocal())
          .thenAnswer((_) => Future.value(false));

      final expectedResponse = [
        VideoListKokabInitial(),
        VideoListKokabLoading(),
        VideoListKokabFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        videoListKokabBloc,
        emitsInOrder(expectedResponse),
      );

      videoListKokabBloc.add(VideoListKokabLoad());
    });
  });
}
