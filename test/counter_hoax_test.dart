import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/counter_hoax/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';
import 'package:sapawarga/repositories/CounterHoaxRepository.dart';

class MockHoaxRepository extends Mock implements CounterHoaxRepository {}

void main() {
  MockHoaxRepository mockHoaxRepository;
  CounterHoaxListBloc hoaxListBloc;
  CounterHoaxDetailBloc hoaxDetailBloc;

  setUp(() {
    mockHoaxRepository = MockHoaxRepository();
    hoaxListBloc =
        CounterHoaxListBloc(counterHoaxRepository: mockHoaxRepository);
    hoaxDetailBloc = CounterHoaxDetailBloc();
  });

  group('Counter Hoax List Test', () {
    test('CounterHoaxList initial state is correct', () {
      expect(hoaxListBloc.initialState, CounterHoaxListInitial());
    });

    test('CounterHoaxList dispose does not emit new states', () {
      expectLater(
        hoaxListBloc,
        emitsInOrder([]),
      );
      hoaxListBloc.close();
    });

    test('CounterHoaxListLoad toString', () {
      expect(
          CounterHoaxListLoad(page: 1).toString(), 'Event ConterHoaxListLoad');
    });

    test('CounterHoaxListLoad props', () {
      expect(
          CounterHoaxListLoad(page: 1).props, []);
    });

    test('CounterHoaxListRefresh toString', () {
      expect(
          CounterHoaxListRefresh().toString(), 'Event ConterHoaxListRefresh');
    });

    test('CounterHoaxListRefresh props', () {
      expect(
          CounterHoaxListRefresh().props, []);
    });

    test(
        'emits [CounterHoaxListInitial, CounterHoaxListLoading, CounterHoaxListLoaded] on load',
        () {

      const MethodChannel('plugins.flutter.io/shared_preferences')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getAll') {
          return <String, dynamic>{}; // set initial values here if desired
        }
        return null;
      });

      List<CounterHoaxModel> records = [];

      when(mockHoaxRepository.fetchRecords(1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        CounterHoaxListInitial(),
        CounterHoaxListLoading(),
        CounterHoaxListLoaded(records: records, maxLengthData: 0, isLoad: false)
      ];

      expectLater(
        hoaxListBloc,
        emitsInOrder(expectedResponse),
      );

      hoaxListBloc.add(CounterHoaxListLoad(page: 1));
    });

    test(
        'emits [CounterHoaxListInitial, CounterHoaxListLoading, CounterHoaxListLoaded] on refresh',
        () {
      List<CounterHoaxModel> records = [];

      when(mockHoaxRepository.fetchRecords(1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        CounterHoaxListInitial(),
        CounterHoaxListLoading(),
        CounterHoaxListLoaded(records: records, maxLengthData: 0, isLoad: false)
      ];

      expectLater(
        hoaxListBloc,
        emitsInOrder(expectedResponse),
      );

      hoaxListBloc.add(CounterHoaxListRefresh());
    });

    test(
        'emits [CounterHoaxListInitial, CounterHoaxListLoading, CounterHoaxListFailure]',
        () {
      when(mockHoaxRepository.fetchRecords(1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        CounterHoaxListInitial(),
        CounterHoaxListLoading(),
        CounterHoaxListFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        hoaxListBloc,
        emitsInOrder(expectedResponse),
      );

      hoaxListBloc.add(CounterHoaxListLoad(page: 1));
    });

    test(
        'emits [CounterHoaxListInitial, CounterHoaxListLoading, CounterHoaxListFailure] on refresh',
        () {
      when(mockHoaxRepository.fetchRecords(1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        CounterHoaxListInitial(),
        CounterHoaxListLoading(),
        CounterHoaxListFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        hoaxListBloc,
        emitsInOrder(expectedResponse),
      );

      hoaxListBloc.add(CounterHoaxListRefresh());
    });
  });

  group('Counter Hoax Detail Test', () {
    test('CounterHoaxDetail initial state is correct', () {
      expect(hoaxDetailBloc.initialState, CounterHoaxDetailInitial());
    });

    test('CounterHoaxDetail dispose does not emit new states', () {
      expectLater(
        hoaxDetailBloc,
        emitsInOrder([]),
      );
      hoaxListBloc.close();
    });

    test('CounterHoaxDetailLoaded assertation', () {
      expect(
          CounterHoaxDetailLoaded(counterHoaxModel: CounterHoaxModel())
                  .counterHoaxModel !=
              null,
          true);
    });

    test('CounterHoaxDetailLoaded props', () {
      CounterHoaxModel counterHoaxModel = CounterHoaxModel();
      expect(
          CounterHoaxDetailLoad(record: counterHoaxModel).props,
          [counterHoaxModel]);
    });

    test(
        'emits [CounterHoaxDetailInitial, CounterHoaxDetailLoading, CounterHoaxDetailLoaded] on load',
        () {
      CounterHoaxModel record = CounterHoaxModel(
          id: 01, title: 'Hoax Jabar', content: 'Content Hoax');

      final expectedResponse = [
        CounterHoaxDetailInitial(),
        CounterHoaxDetailLoading(),
        CounterHoaxDetailLoaded(counterHoaxModel: record)
      ];

      expectLater(
        hoaxDetailBloc,
        emitsInOrder(expectedResponse),
      );

      hoaxDetailBloc.add(CounterHoaxDetailLoad(record: record));
    });

    test('CounterHoaxDetailLoad toString', () {
      CounterHoaxModel record = CounterHoaxModel(
          id: 01, title: 'Hoax Jabar', content: 'Content Hoax');
      expect(CounterHoaxDetailLoad(record: record).toString(),
          'Event CounterHoaxDetailLoad');
    });
  });
}
