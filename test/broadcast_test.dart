import 'package:flutter/services.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/broadcast/broadcast_list/Bloc.dart';
import 'package:sapawarga/blocs/message_badge/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/MessageModel.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';
import 'package:test/test.dart';

class MockBroadcastRepository extends Mock implements BroadcastRepository {}

void main() {
  MockBroadcastRepository mockBroadcastRepository;
  BroadcastListBloc broadcastListBloc;
  MessageBadgeBloc messageBadgeBloc;

  setUp(() {
    mockBroadcastRepository = MockBroadcastRepository();
    broadcastListBloc =
        BroadcastListBloc(broadcastRepository: mockBroadcastRepository);
    messageBadgeBloc =
        MessageBadgeBloc(broadcastRepository: mockBroadcastRepository);
  });

  test('BroadcastList initial state is correct', () {
    expect(broadcastListBloc.initialState, InitialBroadcastListState());
  });

  test('MessageBadge initial state is correct', () {
    expect(messageBadgeBloc.initialState, MessageBadgeInitial());
  });

  test('BroadcastList dispose does not emit new states', () {
    expectLater(
      broadcastListBloc,
      emitsInOrder([]),
    );
    broadcastListBloc.close();
  });

  test('MessageBadge dispose does not emit new states', () {
    expectLater(
      messageBadgeBloc,
      emitsInOrder([]),
    );
    messageBadgeBloc.close();
  });

  test('BroadcastListLoad to String', () {
    expect(BroadcastListLoad(page: 1).toString(), 'Event BroadcastListLoad');
  });

  test('BroadcastListLoad props', () {
    expect(BroadcastListLoad(page: 1).props, []);
  });

  test('BroadcastListTap to String', () {
    expect(BroadcastListTap(id: null).toString(), 'Event BroadcastListTap');
  });

  test('BroadcastListTap props', () {
    expect(BroadcastListTap(id: null).props, [null]);
  });

  test('BroadcastListDelete to String', () {
    expect(
        BroadcastListDelete(id: null).toString(), 'Event BroadcastListDelete');
  });

  test('BroadcastListDelete props', () {
    expect(
        BroadcastListDelete(id: null).props, [null]);
  });

  test('BroadcastMultipleDelete to String', () {
    expect(BroadcastMultipleDelete(record: null).toString(),
        'Event BroadcastMultipleDelete');
  });

  test('BroadcastMultipleDelete props', () {
    expect(BroadcastMultipleDelete(record: null).props,
        [null]);
  });

  test('MessageBadge to String', () {
    expect(CheckMessageBadge().toString(), 'Event MessageBadgeCheck');
  });

  test('MessageBadge props', () {
    expect(CheckMessageBadge().props, [false]);
  });

  group('Broadcast List Load', () {
    test(
        'emits [BroadcastListInitial, BroadcastListLoading, BroadcastListLoaded]',
        () {
      const MethodChannel('plugins.flutter.io/shared_preferences')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getAll') {
          return <String, dynamic>{}; // set initial values here if desired
        }
        return null;
      });

      List<MessageModel> records = [];

      when(mockBroadcastRepository.getRecords(forceRefresh: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListLoading(),
        BroadcastListLoaded(records: records)
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastListLoad(page: 1));
    });

    test(
        'emits [BroadcastListInitial, BroadcastListLoading, BroadcastListFailed]',
        () {
      when(mockBroadcastRepository.getRecords(forceRefresh: true))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListLoading(),
        BroadcastListFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastListLoad(page: 1));
    });
  });

  group('Broadcast List Tap', () {
    test('emits [BroadcastListInitial, BroadcastListLoaded]', () {
      List<MessageModel> records = [];

      when(mockBroadcastRepository.updateReadData('1'))
          .thenAnswer((_) => Future.value(1));

      when(mockBroadcastRepository.getRecords())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListLoaded(records: records, isDeleteSuccess: false)
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastListTap(id: '1'));
    });

    test('emits [BroadcastListInitial, BroadcastListFailed]', () {
      when(mockBroadcastRepository.updateReadData('1'))
          .thenThrow(ErrorException.timeoutException);

      when(mockBroadcastRepository.getRecords(forceRefresh: true))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastListTap(id: '1'));
    });
  });

  group('Broadcast List Delete', () {
    test('emits [BroadcastListInitial, BroadcastListLoaded]', () {
      List<MessageModel> records = [];

      when(mockBroadcastRepository.delete('wy1x'))
          .thenAnswer((_) => Future.value(true));

      when(mockBroadcastRepository.getRecords())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListLoaded(records: records, isDeleteSuccess: false)
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastListDelete(id: 'wy1x'));
    });

    test('emits [BroadcastListInitial, BroadcastListFailed]', () {
      when(mockBroadcastRepository.delete('wy1x'))
          .thenThrow(ErrorException.timeoutException);

      when(mockBroadcastRepository.getRecords(forceRefresh: true))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastListDelete(id: 'wy1x'));
    });
  });

  group('Broadcast List Multiple Delete', () {
    test(
        'emits [BroadcastListInitial,BroadcastListLoading, BroadcastListLoaded]',
        () {
      List<MessageModel> records = [];

      when(mockBroadcastRepository.deleteListRecord(null))
          .thenAnswer((_) => Future.value(true));

      when(mockBroadcastRepository.getRecords())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListLoading(),
        BroadcastListLoaded(records: records, isDeleteSuccess: true)
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastMultipleDelete(record: null));
    });

    test(
        'emits [BroadcastListInitial, BroadcastListLoading, BroadcastListFailed]',
        () {
      when(mockBroadcastRepository.deleteListRecord(null))
          .thenThrow(ErrorException.timeoutException);

      when(mockBroadcastRepository.getRecords(forceRefresh: true))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        InitialBroadcastListState(),
        BroadcastListLoading(),
        BroadcastListFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        broadcastListBloc,
        emitsInOrder(expectedResponse),
      );

      broadcastListBloc.add(BroadcastMultipleDelete(record: null));
    });
  });

  group('Message Badge', () {
    test('emits [MessageBadgeInitial, MessageBadgeShow]', () {
      when(mockBroadcastRepository.hasUnreadData())
          .thenAnswer((_) => Future.value(1));

      final expectedResponse = [MessageBadgeInitial(), MessageBadgeLoading(), MessageBadgeShow(count: 1)];

      expectLater(
        messageBadgeBloc,
        emitsInOrder(expectedResponse),
      );

      messageBadgeBloc.add(CheckMessageBadge());
    });

    test('emits [MessageBadgeInitial, MessageBadgeHide]', () {
      when(mockBroadcastRepository.hasUnreadData())
          .thenAnswer((_) => Future.value(0));

      final expectedResponse = [MessageBadgeInitial(), MessageBadgeLoading(), MessageBadgeHide()];

      expectLater(
        messageBadgeBloc,
        emitsInOrder(expectedResponse),
      );

      messageBadgeBloc.add(CheckMessageBadge());
    });

    test('emits [MessageBadgeInitial, Error]', () {
      when(mockBroadcastRepository.hasUnreadData()).thenThrow(Exception());

      final expectedResponse = [MessageBadgeInitial()];

      expectLater(
        messageBadgeBloc,
        emitsInOrder(expectedResponse),
      );

      messageBadgeBloc.add(CheckMessageBadge());
    });
  });
}
