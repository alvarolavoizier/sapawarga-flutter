import 'dart:convert';

import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/change_password/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:test/test.dart';

class MockAuthProfileRepository extends Mock implements AuthProfileRepository {}

void main() {
  MockAuthProfileRepository mockAuthProfileRepository;
  ChangePasswordBloc changePasswordBloc;

  setUp(() {
    mockAuthProfileRepository = MockAuthProfileRepository();
    changePasswordBloc =
        ChangePasswordBloc(authProfileRepository: mockAuthProfileRepository);
  });

  group('Change Password Test', () {
    test('Initial state is correct', () {
      expect(changePasswordBloc.initialState, ChangePasswordInitial());
    });

    test('Dispose does not emit new states', () {
      expectLater(
        changePasswordBloc,
        emitsInOrder([]),
      );
      changePasswordBloc.close();
    });

    test('SendChangedPassword to String', () {
      expect(
          SendChangedPassword(oldPass: '', newPass: '', confNewPass: '')
              .toString(),
          'Event SendChangedPassword');
    });

    test('SendChangedPassword to props', () {
      expect(
          SendChangedPassword(oldPass: '', newPass: '', confNewPass: '')
              .props,
          ['','','']);
    });

    test(
        'emits [ChangePasswordInitial, ChangePasswordLoading, ChangePasswordSuccess]',
        () {
      when(mockAuthProfileRepository.changePassword(
              oldPass: '123456', newPass: '1234567', confNewPass: '1234567'))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        ChangePasswordInitial(),
        ChangePasswordLoading(),
        ChangePasswordSuccess()
      ];

      expectLater(
        changePasswordBloc,
        emitsInOrder(expectedResponse),
      );

      changePasswordBloc.add(SendChangedPassword(
          oldPass: '123456', newPass: '1234567', confNewPass: '1234567'));
    });

    test(
        'emits [ChangePasswordInitial, ChangePasswordLoading, ValidationError]',
        () {
      String response = '{"password": ["Password tidak sesuai."]}';

      Map<String, dynamic> error = json.decode(response);

      when(mockAuthProfileRepository.changePassword(
              oldPass: '123456', newPass: '1234567', confNewPass: '1234567'))
          .thenThrow(ValidationException(error));

      final expectedResponse = [
        ChangePasswordInitial(),
        ChangePasswordLoading(),
        ValidationError(errors: error)
      ];

      expectLater(
        changePasswordBloc,
        emitsInOrder(expectedResponse),
      );

      changePasswordBloc.add(SendChangedPassword(
          oldPass: '123456', newPass: '1234567', confNewPass: '1234567'));
    });

    test(
        'emits [ChangePasswordInitial, ChangePasswordLoading, ChangePasswordFailure]',
        () {
      when(mockAuthProfileRepository.changePassword(
              oldPass: '123456', newPass: '1234567', confNewPass: '1234567'))
          .thenThrow('error');

      final expectedResponse = [
        ChangePasswordInitial(),
        ChangePasswordLoading(),
        ChangePasswordFailure(error: Dictionary.somethingWrong)
      ];

      expectLater(
        changePasswordBloc,
        emitsInOrder(expectedResponse),
      );

      changePasswordBloc.add(SendChangedPassword(
          oldPass: '123456', newPass: '1234567', confNewPass: '1234567'));
    });
  });
}
