import 'dart:convert';
import 'dart:io';

import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:test/test.dart';

class MockProfileRepository extends Mock implements AuthProfileRepository {}

void main() {
  MockProfileRepository mockProfileRepository;
  AccountProfileBloc accountProfileBloc;
  AccountProfileEditBloc accountProfileEditBloc;

  setUp(() {
    mockProfileRepository = MockProfileRepository();
    accountProfileBloc = AccountProfileBloc.profile(
        authProfileRepository: mockProfileRepository);
    accountProfileEditBloc =
        AccountProfileEditBloc(authProfileRepository: mockProfileRepository);
  });

  group('Profile', () {
    test('Profile initial state is correct', () {
      expect(accountProfileBloc.initialState, AccountProfileInitial());
    });

    test('Profile dispose does not emit new states', () {
      expectLater(
        accountProfileBloc,
        emitsInOrder([]),
      );
      accountProfileBloc.close();
    });
  });

  group('Account Profil Load', () {
    test('AccountProfileLoad to String', () {
      expect(AccountProfileLoad().toString(), 'Event AccountProfileLoad');
    });

    test('AccountProfileLoad props', () {
      expect(AccountProfileLoad().props, []);
    });

    test(
        'emits [AccountProfileInitial, AccountProfileLoading, AccountProfileLoaded]',
        () {
      UserInfoModel records;

      when(mockProfileRepository.getUserInfo())
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        AccountProfileInitial(),
        AccountProfileLoading(),
        AccountProfileLoaded(record: records)
      ];

      expectLater(
        accountProfileBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileBloc.add(AccountProfileLoad());
    });

    test(
        'emits [AccountProfileInitial, AccountProfileLoading, AccountProfileFailure]',
        () {
      when(mockProfileRepository.getUserInfo())
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        AccountProfileInitial(),
        AccountProfileLoading(),
        AccountProfileFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        accountProfileBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileBloc.add(AccountProfileLoad());
    });
  });

  group('Account Profil Change', () {
    test('AccountProfileChanged to String', () {
      expect(AccountProfileChanged().toString(), 'Event AccountProfileChanged');
    });

    test('AccountProfileChanged props', () {
      expect(AccountProfileChanged().props, []);
    });

    test(
        'emits [AccountProfileInitial, AccountProfileLoading, AccountProfileLoaded]',
        () {
      UserInfoModel records;

      when(mockProfileRepository.getUserInfo(forceFetch: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        AccountProfileInitial(),
        AccountProfileLoading(),
        AccountProfileLoaded(record: records)
      ];

      expectLater(
        accountProfileBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileBloc.add(AccountProfileChanged());
    });

    test(
        'emits [AccountProfileInitial, AccountProfileLoading, AccountProfileFailure]',
        () {
      when(mockProfileRepository.getUserInfo(forceFetch: true))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        AccountProfileInitial(),
        AccountProfileLoading(),
        AccountProfileFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        accountProfileBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileBloc.add(AccountProfileChanged());
    });
  });

  group('Account Profile Edit Photo', () {
    test('AccountProfileEditPhotoSubmit to String', () {
      expect(AccountProfileEditPhotoSubmit(image: null).toString(),
          'Event AccountProfileEditPhotoSubmit');
    });

    test('AccountProfileEditPhotoSubmit props', () {
      expect(AccountProfileEditPhotoSubmit(image: null).props,
          []);
    });

    test('AccountProfileEditPhotoUpdated to String', () {
      expect(AccountProfileEditPhotoUpdated(image: null).toString(),
          'State AccountProfileEditPhotoUpdated');
    });

    test('AccountProfileEditPhotoLoading to String', () {
      expect(AccountProfileEditPhotoLoading().toString(),
          'State AccountProfileEditPhotoLoading');
    });

    test('AccountProfileEditPhotoFailure to String', () {
      expect(AccountProfileEditPhotoFailure(error: null).toString(),
          'State AccountProfileEditPhotoFailure');
    });

    test('AccountProfileEditPhotoFailure props', () {
      expect(AccountProfileEditPhotoFailure(error: null).props,
          [null]);
    });
  });

  test('AccountProfileEditBloc assertation', () {
    expect(
        AccountProfileEditBloc(authProfileRepository: AuthProfileRepository())
            .authProfileRepository,
        isNot(null));
  });

  group('Account Profile Edit Submit', () {
    test('AccountProfileEditSubmit to String', () {
      expect(
          AccountProfileEditSubmit(userInfoModel: UserInfoModel(name: null, username: null, email: null, address: null, phone: null, rt: null, facebook: null, instagram: null, twitter: null))
              .toString(),
          'Event AccountProfileEditSubmit');
    });

    test('AccountProfileEditSubmit props', () {
      expect(
          AccountProfileEditSubmit(userInfoModel: UserInfoModel(name: null, username: null, email: null, address: null, phone: null, rt: null, facebook: null, instagram: null, twitter: null))
              .props,
          []);
    });

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditLoading, AccountProfileEditUpdated]',
        () {
      when(mockProfileRepository.updateProfile(UserInfoModel(name: '', username: '', email: '', address: '', phone: '', rt: '', facebook: '', instagram: '', twitter: '')))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditLoading(),
        AccountProfileEditUpdated()
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(AccountProfileEditSubmit(userInfoModel: UserInfoModel(name: '', username: '', email: '', address: '', phone: '', rt: '', facebook: '', instagram: '', twitter: '')));
    });

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditLoading, AccountProfileEditValidationError]',
        () {
      String response = '{"email": ["Alamat email sudah digunakan."]}';

      Map<String, dynamic> error = json.decode(response);

      UserInfoModel model = UserInfoModel(name: 'asep', username: 'asep', email: 'abcd@gg.mail', address: 'abc', phone: '0123', rt: '01', facebook: 'asda', instagram: 'adsda', twitter: 'ascas');

      when(mockProfileRepository.updateProfile(model))
          .thenThrow(ValidationException(error));

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditLoading(),
        AccountProfileEditValidationError(errors: error)
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(AccountProfileEditSubmit(userInfoModel: model));
    });

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditLoading, AccountProfileEditFailure]',
        () {

          UserInfoModel model = UserInfoModel(name: 'asep', username: 'asep', email: 'abcd@gg.mail', address: 'abc', phone: '0123', rt: '01', facebook: 'asda', instagram: 'adsda', twitter: 'ascas');

      when(mockProfileRepository.updateProfile(model))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditLoading(),
        AccountProfileEditFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(AccountProfileEditSubmit(userInfoModel: model));
    });
  });

  group('Account ProfileEditPhoto ', () {
    File image;
    UserInfoModel model = UserInfoModel(name: 'asep', username: 'asep', email: 'abcd@gg.mail', address: 'abc', phone: '0123', rt: '01', facebook: 'asda', instagram: 'adsda', twitter: 'ascas');

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditPhotoLoading, AccountProfileEditPhotoUpdated]',
        () {
      when(mockProfileRepository.updatePhoto(image, model))
          .thenAnswer((_) => Future.value(image));

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditPhotoLoading(),
        AccountProfileEditPhotoUpdated(image: image)
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(AccountProfileEditPhotoSubmit(image: image, userInfoModel: model));
    });

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditPhotoLoading, AccountProfileEditFailure]',
        () {

          UserInfoModel model = UserInfoModel(name: 'asep', username: 'asep', email: 'abcd@gg.mail', address: 'abc', phone: '0123', rt: '01', facebook: 'asda', instagram: 'adsda', twitter: 'ascas');

      when(mockProfileRepository.updatePhoto(image, model))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditPhotoLoading(),
        AccountProfileEditFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(AccountProfileEditPhotoSubmit(image: image, userInfoModel: model));
    });
  });

  group('Submit Force Change Profile', () {
    test('CompleteProfileSubmit to String', () {
      expect(
          CompleteProfileSubmit(name: '', email: '', phone: '', address: '', jobId: '', educationId: '')
              .toString(),
          'Event CompleteProfileSubmit');
    });

    test('CompleteProfileSubmit Props', () {
      expect(
          CompleteProfileSubmit(name: '', email: '', phone: '', address: '', jobId: '', educationId: '')
              .props,
          []);
    });

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditLoading, AccountProfileEditUpdated]',
        () {
      when(mockProfileRepository.changeProfile(
              name: 'user',
              email: 'email@example.com',
              phone: '081234567',
              address: 'bandung',
              jobId: '12',
              educationId: '4'
      ))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditLoading(),
        AccountProfileEditUpdated()
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(CompleteProfileSubmit(
          name: 'user',
          email: 'email@example.com',
          phone: '081234567',
          address: 'bandung',
          jobId: '12',
          educationId: '4'
      ));
    });

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditLoading, AccountProfileEditValidationError]',
        () {
      String response = '{"email": ["Alamat email sudah digunakan."]}';

      Map<String, dynamic> error = json.decode(response);

      when(mockProfileRepository.changeProfile(
              name: 'user',
              email: 'email@example.com',
              phone: '081234567',
              address: 'bandung',
              jobId: '12',
              educationId: '4'
      ))
          .thenThrow(ValidationException(error));

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditLoading(),
        AccountProfileEditValidationError(errors: error)
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(CompleteProfileSubmit(
          name: 'user',
          email: 'email@example.com',
          phone: '081234567',
          address: 'bandung',
          jobId: '12',
          educationId: '4'
      ));
    });

    test(
        'emits [AccountProfileEditInitial, AccountProfileEditLoading, AccountProfileEditFailure]',
        () {
      when(mockProfileRepository.changeProfile(
              name: 'user',
              email: 'email@example.com',
              phone: '081234567',
              address: 'bandung',
              jobId: '12',
              educationId: '4'
      ))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        AccountProfileEditInitial(),
        AccountProfileEditLoading(),
        AccountProfileEditFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        accountProfileEditBloc,
        emitsInOrder(expectedResponse),
      );

      accountProfileEditBloc.add(CompleteProfileSubmit(
          name: 'user',
          email: 'email@example.com',
          phone: '081234567',
          address: 'bandung',
          jobId: '12',
          educationId: '4'
      ));
    });
  });
}
