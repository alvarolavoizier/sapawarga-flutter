import 'package:flutter/services.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/polling_detail/Bloc.dart';
import 'package:sapawarga/blocs/polling_list/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/PollingModel.dart';
import 'package:sapawarga/repositories/PollingRepository.dart';
import 'package:test/test.dart';

class MockPollingRepository extends Mock implements PollingRepository {}

void main() {
  MockPollingRepository mockPollingRepository;
  PollingListBloc pollingListBloc;
  PollingDetailBloc pollingDetailBloc;

  setUp(() {
    mockPollingRepository = MockPollingRepository();
    pollingListBloc = PollingListBloc(pollingRepository: mockPollingRepository);
    pollingDetailBloc =
        PollingDetailBloc(pollingRepository: mockPollingRepository);
  });

  group('Polling List', () {
    test('PollingList initial state is correct', () {
      expect(pollingListBloc.initialState, PollingListInitial());
    });

    test('PollingListLoad to String', () {
      expect(PollingListLoad(pollingId: 1).toString(), 'Event PollingListLoad');
    });

    test('PollingListLoad props', () {
      expect(PollingListLoad(pollingId: 1).props, [1]);
    });

    test('PollingListStatusLoading to String', () {
      expect(PollingListStatusLoading().toString(),
          'State PollingListStatusLoading');
    });

    test('PollingListStatusLoading proops', () {
      expect(PollingListStatusLoading().props,
          []);
    });

    test('PollingList dispose does not emit new states', () {
      expectLater(
        pollingListBloc,
        emitsInOrder([]),
      );
      pollingListBloc.close();
    });

    test('emits [PollingListInitial, PollingListLoading, PollingListLoaded]',
        () {

          const MethodChannel('plugins.flutter.io/shared_preferences')
              .setMockMethodCallHandler((MethodCall methodCall) async {
            if (methodCall.method == 'getAll') {
              return <String, dynamic>{}; // set initial values here if desired
            }
            return null;
          });

      List<PollingModel> records = [];

      when(mockPollingRepository.fetchRecords(1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        PollingListInitial(),
        PollingListLoading(),
        PollingListLoaded(records: records, maxData: 0, isLoad: false)
      ];

      expectLater(
        pollingListBloc,
        emitsInOrder(expectedResponse),
      );

      pollingListBloc.add(PollingListLoad(page: 1));
    });

    test('emits [PollingListInitial, PollingListLoading, PollingListFailure]',
        () {

      when(mockPollingRepository.fetchRecords(1))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        PollingListInitial(),
        PollingListLoading(),
        PollingListFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        pollingListBloc,
        emitsInOrder(expectedResponse),
      );

      pollingListBloc.add(PollingListLoad(page: 1));
    });
  });

  group('Polling Detail', () {
    test('PollingDetail initial state is correct', () {
      expect(pollingDetailBloc.initialState, PollingDetailInitial());
    });

    test('PollingDetailLoad to String', () {
      PollingModel pollingModel = PollingModel();
      expect(PollingDetailLoad(record: pollingModel).toString(),
          'Event PollingDetailLoad{record: $pollingModel}');
    });

    test('PollingDetailLoad props', () {
      PollingModel pollingModel = PollingModel();
      expect(PollingDetailLoad(record: pollingModel).props,
          [pollingModel]);
    });

    test('PollingDetailSendVote to String', () {
      expect(PollingDetailSendVote(pollingId: 1, answerId: 1).toString(),
          'Event PollingDetailSendVote{pollingId: 1, answerId: 1}');
    });

    test('PollingDetailSendVote props', () {
      expect(PollingDetailSendVote(pollingId: 1, answerId: 1).props,
          [1, 1]);
    });

    test('PollingDetailStatusVote to String', () {
      expect(PollingDetailStatusVote(pollingId: 1).toString(),
          'Event PollingDetailStatusVote{pollingId: 1}');
    });

    test('PollingDetailStatusVote props', () {
      expect(PollingDetailStatusVote(pollingId: 1).props,
          [1]);
    });

    test('PollingDetail dispose does not emit new states', () {
      expectLater(
        pollingDetailBloc,
        emitsInOrder([]),
      );
      pollingDetailBloc.close();
    });

    test('emits [PollingDetailInitial, PollingDetailLoaded]', () {
      PollingModel record = PollingModel();

      final expectedResponse = [
        PollingDetailInitial(),
        PollingDetailLoaded(record: record)
      ];

      expectLater(
        pollingDetailBloc,
        emitsInOrder(expectedResponse),
      );

      pollingDetailBloc.add(PollingDetailLoad(record: record));
    });

    test(
        'emits [PollingDetailInitial, PollingDetailLoading, PollingDetailVoted]',
        () {
      when(mockPollingRepository.sendVote(pollingId: 1, answer: 1))
          .thenAnswer((_) => Future.value(200));

      final expectedResponse = [
        PollingDetailInitial(),
        PollingDetailLoading(),
        PollingDetailVoted(status: 200)
      ];

      expectLater(
        pollingDetailBloc,
        emitsInOrder(expectedResponse),
      );

      pollingDetailBloc.add(PollingDetailSendVote(pollingId: 1, answerId: 1));
    });

    test(
        'emits [PollingDetailInitial, PollingDetailLoading, PollingDetailStatus]',
        () {
      when(mockPollingRepository.getVoteStatus(pollingId: 1))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        PollingDetailInitial(),
        PollingDetailLoading(),
        PollingDetailStatus(isVoted: true)
      ];

      expectLater(
        pollingDetailBloc,
        emitsInOrder(expectedResponse),
      );

      pollingDetailBloc.add(PollingDetailStatusVote(pollingId: 1));
    });

    test(
        'emits [PollingDetailInitial, PollingDetailLoading, PollingDetailFailure]',
        () {
      when(mockPollingRepository.getVoteStatus(pollingId: 1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        PollingDetailInitial(),
        PollingDetailLoading(),
        PollingDetailFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        pollingDetailBloc,
        emitsInOrder(expectedResponse),
      );

      pollingDetailBloc.add(PollingDetailStatusVote(pollingId: 1));
    });

    test(
        'emits [PollingDetailInitial, PollingDetailLoading, PollingDetailFailure]',
        () {
      when(mockPollingRepository.sendVote(pollingId: 1, answer: 1))
          .thenThrow(408);

      final expectedResponse = [
        PollingDetailInitial(),
        PollingDetailLoading(),
        PollingDetailFailure(error: Dictionary.somethingWrong)
      ];

      expectLater(
        pollingDetailBloc,
        emitsInOrder(expectedResponse),
      );

      pollingDetailBloc.add(PollingDetailSendVote(pollingId: 1, answerId: 1));
    });
  });
}
