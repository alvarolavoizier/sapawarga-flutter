import 'dart:io';

import 'package:flutter/services.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/usulan/addusulan/Bloc.dart';
import 'package:sapawarga/blocs/usulan/categoryusulan/AddCategoryUsulanBloc.dart';
import 'package:sapawarga/blocs/usulan/categoryusulan/AddCategoryUsulanEvent.dart';
import 'package:sapawarga/blocs/usulan/categoryusulan/AddCategoryUsulanState.dart';
import 'package:sapawarga/blocs/usulan/detailusulan/Bloc.dart';
import 'package:sapawarga/blocs/usulan/myusulan/Bloc.dart';
import 'package:sapawarga/blocs/usulan/usulanumum/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart' as prefix0;
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:test/test.dart';

class MockUsulanRepository extends Mock implements UsulanRepository {}

void main() {
  MockUsulanRepository usulanRepository;
  UsulanListBloc usulanListBloc;
  MyUsulanListBloc myUsulanListBloc;
  UsulanDetailBloc detailBloc;
  AddCategoryUsulanBloc addCategoryUsulanBloc;
  AddUsulanBloc addUsulanBloc;

  setUp(() {
    usulanRepository = MockUsulanRepository();
    usulanListBloc = UsulanListBloc(usulanRepository: usulanRepository);
    myUsulanListBloc = MyUsulanListBloc(usulanRepository: usulanRepository);
    detailBloc = UsulanDetailBloc(usulanRepository: usulanRepository);
    addCategoryUsulanBloc =
        AddCategoryUsulanBloc(usulanRepository: usulanRepository);
    addUsulanBloc = AddUsulanBloc(usulanRepository: usulanRepository);
  });

  group('GeneralUsulanList', () {
    test('GeneralUsulanList initial state is correct', () {
      expect(usulanListBloc.initialState, UsulanListInitial());
    });

    test('UsulanListLoad to String', () {
      expect(UsulanListLoad(page: 1).toString(), 'Event UsulanListLoad');
    });

    test('UsulanListLoad props', () {
      expect(UsulanListLoad(page: 1).props, [1]);
    });

    test('UsulanListRefresh to String', () {
      expect(UsulanListRefresh().toString(), 'Event UsulanListRefresh');
    });

    test('UsulanListRefresh props', () {
      expect(UsulanListRefresh().props, []);
    });

    test('UsulanListLoading to String', () {
      expect(UsulanListLoading().toString(), 'State UsulanListLoading');
    });

    test('UsulanListLoaded to String', () {
      expect(
          UsulanListLoaded(records: [], maxLengthData: 0, isLoad: true)
              .toString(),
          'State UsulanListLoaded');
    });

    test('UsulanListFailure to String', () {
      expect(UsulanListFailure(error: Dictionary.errorUnauthorized).toString(),
          'State UsulanListFailure{error: Izin akses ke server ditolak}');
    });

    test('GeneralUsulanList dispose does not emit new states', () {
      expectLater(
        usulanListBloc,
        emitsInOrder([]),
      );
      usulanListBloc.close();
    });

    test('emits [UsulanListInitial, UsulanListLoading, UsulanListLoaded]', () {
      const MethodChannel('plugins.flutter.io/shared_preferences')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getAll') {
          return <String, dynamic>{}; // set initial values here if desired
        }
        return null;
      });

      List<UsulanModel> records = [];

      when(usulanRepository.getUsulan(page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        UsulanListInitial(),
        UsulanListLoading(),
        UsulanListLoaded(records: records, maxLengthData: 0, isLoad: false)
      ];

      expectLater(
        usulanListBloc,
        emitsInOrder(expectedResponse),
      );

      usulanListBloc.add(UsulanListLoad(page: 1));
    });

    test('emits [UsulanListInitial, UsulanListLoading, UsulanListFailure]', () {
      when(usulanRepository.getUsulan(page: 1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        UsulanListInitial(),
        UsulanListLoading(),
        UsulanListFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        usulanListBloc,
        emitsInOrder(expectedResponse),
      );

      usulanListBloc.add(UsulanListLoad(page: 1));
    });

    test(
        'emits [UsulanListInitial, UsulanListLoading, UsulanListLoaded] => UsulanListRefresh',
        () {
      const MethodChannel('plugins.flutter.io/shared_preferences')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getAll') {
          return <String, dynamic>{}; // set initial values here if desired
        }
        return null;
      });

      List<UsulanModel> records = [];

      when(usulanRepository.getUsulan(page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        UsulanListInitial(),
        UsulanListLoading(),
        UsulanListLoaded(records: records, maxLengthData: 0, isLoad: true)
      ];

      expectLater(
        usulanListBloc,
        emitsInOrder(expectedResponse),
      );

      usulanListBloc.add(UsulanListRefresh());
    });

    test('emits [UsulanListInitial, UsulanListLoading, UsulanListFailure]', () {
      when(usulanRepository.getUsulan(page: 1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        UsulanListInitial(),
        UsulanListLoading(),
        UsulanListFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        usulanListBloc,
        emitsInOrder(expectedResponse),
      );

      usulanListBloc.add(UsulanListRefresh());
    });
  });

  group('Like Usulan', () {
    test('UsulanListLike to String', () {
      expect(UsulanListLike(id: 849).toString(), 'Event UsulanListLike');
    });

    test('UsulanListLike props', () {
      expect(UsulanListLike(id: 1).props, [1]);
    });

    test('UsulanDetailLike to String', () {
      expect(UsulanDetailLike(id: 849).toString(), 'Event UsulanDetailLike');
    });

    test('UsulanDetailLike props', () {
      expect(UsulanDetailLike(id: 849).props, [849]);
    });

    test('emits [UsulanListInitial]', () {
      when(usulanRepository.likeUsulan(id: 849))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [UsulanListInitial()];

      expectLater(
        usulanListBloc,
        emitsInOrder(expectedResponse),
      );

      usulanListBloc.add(UsulanListLike(id: 849));
    });

    test('emits [UsulanDetailInitial]', () {
      when(usulanRepository.likeUsulan(id: 849))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [UsulanDetailInitial()];

      expectLater(
        detailBloc,
        emitsInOrder(expectedResponse),
      );

      detailBloc.add(UsulanDetailLike(id: 849));
    });

    test('emits [UsulanListInitial, UsulanListFailure]', () {
      when(usulanRepository.likeUsulan(id: 849))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        UsulanListInitial(),
        UsulanListFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        usulanListBloc,
        emitsInOrder(expectedResponse),
      );

      usulanListBloc.add(UsulanListLike(id: 849));
    });

    test('emits [UsulanDetailInitial, UsulanDetailFailure]', () {
      when(usulanRepository.likeUsulan(id: 849))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        UsulanDetailInitial(),
        UsulanDetailFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        detailBloc,
        emitsInOrder(expectedResponse),
      );

      detailBloc.add(UsulanDetailLike(id: 849));
    });
  });

  group('MyUsulanList', () {
    test('MyUsulanList initial state is correct', () {
      expect(myUsulanListBloc.initialState, MyUsulanListInitial());
    });

    test('MyUsulanListLoad to String', () {
      expect(MyUsulanListLoad(page: 1).toString(), 'Event MyUsulanListLoad');
    });

    test('MyUsulanListLoad props', () {
      expect(MyUsulanListLoad(page: 1).props, [1]);
    });

    test('MyUsulanListRefresh to String', () {
      expect(MyUsulanListRefresh().toString(), 'Event MyUsulanListRefresh');
    });

    test('MyUsulanListRefresh props', () {
      expect(MyUsulanListRefresh().props, []);
    });

    test('MyUsulanListLoading to String', () {
      expect(MyUsulanListLoading().toString(), 'State MyUsulanListLoading');
    });

    test('MyUsulanListLoaded to String', () {
      expect(
          MyUsulanListLoaded(records: [], maxData: 0, isLoadData: true)
              .toString(),
          'State MyUsulanListLoaded');
    });

    test('MyUsulanListFailure to String', () {
      expect(
          MyUsulanListFailure(error: Dictionary.errorUnauthorized).toString(),
          'State MyUsulanListFailure{error: Izin akses ke server ditolak}');
    });

    test('MyUsulanList dispose does not emit new states', () {
      expectLater(
        myUsulanListBloc,
        emitsInOrder([]),
      );
      myUsulanListBloc.close();
    });

    test('emits [MyUsulanListInitial, MyUsulanListLoading, MyUsulanListLoaded]',
        () {
      List<UsulanModel> records = [];

      when(usulanRepository.getMyUsulan(page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        MyUsulanListInitial(),
        MyUsulanListLoading(),
        MyUsulanListLoaded(records: records, maxData: 0, isLoadData: true)
      ];

      expectLater(
        myUsulanListBloc,
        emitsInOrder(expectedResponse),
      );

      myUsulanListBloc.add(MyUsulanListLoad(page: 1));
    });

    test('emits [MyUsulanListInitial, MyUsulanListLoading, MyUsulanListLoaded]',
        () {
      List<UsulanModel> records = [];

      when(usulanRepository.getMyUsulan(page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        MyUsulanListInitial(),
        MyUsulanListLoading(),
        MyUsulanListLoaded(records: records, maxData: 0, isLoadData: true)
      ];

      expectLater(
        myUsulanListBloc,
        emitsInOrder(expectedResponse),
      );

      myUsulanListBloc.add(MyUsulanListRefresh());
    });

    test(
        'emits [MyUsulanListInitial, MyUsulanListLoading, MyUsulanListFailure]',
        () {
      when(usulanRepository.getMyUsulan(page: 1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        MyUsulanListInitial(),
        MyUsulanListLoading(),
        MyUsulanListFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        myUsulanListBloc,
        emitsInOrder(expectedResponse),
      );

      myUsulanListBloc.add(MyUsulanListRefresh());
    });

    test(
        'emits [MyUsulanListInitial, MyUsulanListLoading, MyUsulanListFailure]',
        () {
      when(usulanRepository.getMyUsulan(page: 1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        MyUsulanListInitial(),
        MyUsulanListLoading(),
        MyUsulanListFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        myUsulanListBloc,
        emitsInOrder(expectedResponse),
      );

      myUsulanListBloc.add(MyUsulanListLoad(page: 1));
    });
  });

  group('Detail Usulan', () {
    test('Detail Usulan initial state is correct', () {
      expect(detailBloc.initialState, UsulanDetailInitial());
    });

    test('UsulanDetailLoad to String', () {
      expect(UsulanDetailLoad(usulanId: 1).toString(),
          'Event UsulanDetailLoad{newsId: 1}');
    });

    test('UsulanDetailLoad props', () {
      expect(UsulanDetailLoad(usulanId: 1).props,
          [1]);
    });

    test('UsulanDetailLoading to String', () {
      expect(UsulanDetailLoading().toString(), 'State UsulanDetailLoading');
    });

    test('UsulanDetailLoaded to String', () {
      expect(
          UsulanDetailLoaded(usulanModel: null, categoryUsulan: []).toString(),
          'State UsulanDetailLoaded');
    });

    test('UsulanDetailFailure to String', () {
      expect(
          UsulanDetailFailure(error: Dictionary.errorUnauthorized).toString(),
          'UsulanDetailFailure{error: Izin akses ke server ditolak}');
    });

    test('Detail Usulan dispose does not emit new states', () {
      expectLater(
        detailBloc,
        emitsInOrder([]),
      );
      detailBloc.close();
    });

    test('emits [UsulanDetailInitial, UsulanDetailLoading, UsulanDetailLoaded]',
        () {
      UsulanModel records;

      when(usulanRepository.getUsulanDetail(id: 560))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        UsulanDetailInitial(),
        UsulanDetailLoading(),
        UsulanDetailLoaded(usulanModel: records)
      ];

      expectLater(
        detailBloc,
        emitsInOrder(expectedResponse),
      );

      detailBloc.add(UsulanDetailLoad(usulanId: 1));
    });

    test(
        'emits [UsulanDetailInitial, UsulanDetailLoading, UsulanDetailFailure]',
        () {
      when(usulanRepository.getUsulanDetail(id: 560))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        UsulanDetailInitial(),
        UsulanDetailLoading(),
        UsulanDetailFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        detailBloc,
        emitsInOrder(expectedResponse),
      );

      detailBloc.add(UsulanDetailLoad(usulanId: 560));
    });
  });

  group('CategoryUsulan', () {
    test('CategoryUsulan initial state is correct', () {
      expect(addCategoryUsulanBloc.initialState, AddCategoryUsulanInitial());
    });

    test('CategoryUsulan dispose does not emit new states', () {
      expectLater(
        addCategoryUsulanBloc,
        emitsInOrder([]),
      );
      addCategoryUsulanBloc.close();
    });

    test('AddCategoryUsulanLoad to String', () {
      expect(AddCategoryUsulanLoad(page: 1).toString(),
          'Event AddCategoryUsulanLoad');
    });

    test('AddCategoryUsulanLoad props', () {
      expect(AddCategoryUsulanLoad(page: 1).props,
          [1]);
    });

    test('AddCategoryUsulanLoading to String', () {
      expect(AddCategoryUsulanLoading().toString(),
          'State AddCategoryUsulanLoading');
    });

    test('AddCategoryUsulanLoaded to String', () {
      expect(AddCategoryUsulanLoaded(records: []).toString(),
          'State AddCategoryUsulanLoaded');
    });

    test('AddCategoryUsulanFailure to String', () {
      expect(
          AddCategoryUsulanFailure(error: Dictionary.errorUnauthorized)
              .toString(),
          'State AddCategoryUsulanFailure{error: Izin akses ke server ditolak}');
    });

    test(
        'emits [AddCategoryUsulanInitial, AddCategoryUsulanLoading, AddCategoryUsulanLoaded]',
        () {
      MasterCategoryModel categoryUsulanModel = MasterCategoryModel(
          id: 1,
          type: null,
          name: 'test',
          meta: null,
          status: 0,
          statusLabel: prefix0.StatusLabel.AKTIF,
          createdAt: 0,
          updatedAt: 0);
      MasterCategoryModel categoryUsulanModel1 = MasterCategoryModel(
          id: 2,
          type: null,
          name: 'Lainnya',
          meta: null,
          status: 1,
          statusLabel: prefix0.StatusLabel.AKTIF,
          createdAt: 0,
          updatedAt: 0);

      List<MasterCategoryModel> records = [];
      records.add(categoryUsulanModel);
      records.add(categoryUsulanModel1);

      when(usulanRepository.getRecordsCategoryUsulan(
              page: 1, forceRefresh: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        AddCategoryUsulanInitial(),
        AddCategoryUsulanLoading(),
        AddCategoryUsulanLoaded(records: records)
      ];

      expectLater(
        addCategoryUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addCategoryUsulanBloc.add(AddCategoryUsulanLoad(page: 1));
    });

    test(
        'emits [AddCategoryUsulanInitial, AddCategoryUsulanLoading, AddCategoryUsulanLoaded]',
        () {
      MasterCategoryModel categoryUsulanModel = MasterCategoryModel(
          id: 1,
          type: null,
          name: 'test',
          meta: null,
          status: 0,
          statusLabel: prefix0.StatusLabel.AKTIF,
          createdAt: 0,
          updatedAt: 0);

      List<MasterCategoryModel> records = [];
      records.add(categoryUsulanModel);

      when(usulanRepository.getRecordsCategoryUsulan(
              page: 1, forceRefresh: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        AddCategoryUsulanInitial(),
        AddCategoryUsulanLoading(),
        AddCategoryUsulanLoaded(records: records)
      ];

      expectLater(
        addCategoryUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addCategoryUsulanBloc.add(AddCategoryUsulanLoad(page: 1));
    });

    test(
        'emits [AddCategoryUsulanInitial, AddCategoryUsulanLoading, AddCategoryUsulanFailure]',
        () {
      when(usulanRepository.getRecordsCategoryUsulan(
              page: 1, forceRefresh: true))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        AddCategoryUsulanInitial(),
        AddCategoryUsulanLoading(),
        AddCategoryUsulanFailure(error: Dictionary.errorUnauthorized)
      ];

      expectLater(
        addCategoryUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addCategoryUsulanBloc.add(AddCategoryUsulanLoad(page: 1));
    });
  });

  group('Send Usulan ', () {
    test('CategoryUsulan initial state is correct', () {
      expect(addUsulanBloc.initialState, AddUsulanInitial());
    });

    test('AddUsulanPhotoLoading to String', () {
      expect(AddUsulanPhotoLoading().toString(), 'State AddUsulanPhotoLoading');
    });

    test('AddUsulanPhotoLoading props', () {
      expect(AddUsulanPhotoLoading().props, []);
    });

    test('AddUsulanUpdated to String', () {
      expect(AddUsulanUpdated().toString(), 'State AddUsulanUpdated');
    });

    test('AddUsulanFailure to String', () {
      expect(AddUsulanFailure(error: Dictionary.somethingWrong).toString(),
          'State AddUsulanFailure{error: Terjadi kesalahan}');
    });

    test('Dispose does not emit new states', () {
      expectLater(
        addUsulanBloc,
        emitsInOrder([]),
      );
      addUsulanBloc.close();
    });

    test('AddUsulanSubmit to String', () {
      List<AddPhotoModel> listPhoto = [];

      expect(
          AddUsulanSubmit(
                  title: '',
                  description: '',
                  status: '',
                  categoryId: '',
                  attachment: listPhoto)
              .toString(),
          'Event AddUsulanlLoad');
    });

    test('AddUsulanSubmit props', () {
      List<AddPhotoModel> listPhoto = [];

      expect(
          AddUsulanSubmit(
                  title: '',
                  description: '',
                  status: '',
                  categoryId: '',
                  attachment: listPhoto)
              .props,
          ['','','','',[]]);
    });

    test(
        'emits [AddUsulanInitial, AddUsulanLoading, AddUsulanUpdated] Direct Send Usulan',
        () {
      List<AddPhotoModel> listPhoto = [];
      when(usulanRepository.sendUsulan(
              title: 'Jalanan Rusak',
              description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
              status: '5',
              categoryId: '9',
              attachment: listPhoto))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        AddUsulanInitial(),
        AddUsulanLoading(),
        AddUsulanUpdated()
      ];

      expectLater(
        addUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addUsulanBloc.add(AddUsulanSubmit(
          title: 'Jalanan Rusak',
          description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
          status: '5',
          categoryId: '9',
          attachment: listPhoto));
    });

    test(
        'emits [AddUsulanInitial, AddUsulanLoading, AddUsulanUpdated] Send Draft Usulan',
        () {
      List<AddPhotoModel> listPhoto = [];
      when(usulanRepository.sendUsulan(
              title: 'Jalanan Rusak',
              description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
              status: '0',
              categoryId: '9',
              attachment: listPhoto))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        AddUsulanInitial(),
        AddUsulanLoading(),
        AddUsulanUpdated()
      ];

      expectLater(
        addUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addUsulanBloc.add(AddUsulanSubmit(
          title: 'Jalanan Rusak',
          description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
          status: '0',
          categoryId: '9',
          attachment: listPhoto));
    });

    test('emits [AddUsulanInitial, AddUsulanLoading, AddUsulanFailure]', () {
      List<AddPhotoModel> listPhoto = [];
      when(usulanRepository.sendUsulan(
              title: 'Jalanan Rusak',
              description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
              status: '0',
              categoryId: '9',
              attachment: listPhoto))
          .thenThrow('error');

      final expectedResponse = [
        AddUsulanInitial(),
        AddUsulanLoading(),
        AddUsulanFailure(error: Dictionary.somethingWrong)
      ];

      expectLater(
        addUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addUsulanBloc.add(AddUsulanSubmit(
          title: 'Jalanan Rusak',
          description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
          status: '0',
          categoryId: '9',
          attachment: listPhoto));
    });
  });

  group('Edit Usulan', () {
    test('EditUsulan to String', () {
      List<AddPhotoModel> listPhoto = [];

      expect(
          AddUsulanSubmit(
                  title: '',
                  description: '',
                  status: '',
                  categoryId: '',
                  attachment: listPhoto,
                  id: 1)
              .toString(),
          'Event AddUsulanlLoad');
    });

    test(
        'emits [AddUsulanInitial, AddUsulanLoading, AddUsulanUpdated] Edit & Send Usulan',
        () {
      List<AddPhotoModel> listPhoto = [];
      when(
        usulanRepository.sendUsulan(
            title: 'Jalanan Rusak',
            description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
            status: '5',
            categoryId: '9',
            attachment: listPhoto,
            id: 1),
      ).thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        AddUsulanInitial(),
        AddUsulanLoading(),
        AddUsulanUpdated()
      ];

      expectLater(
        addUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addUsulanBloc.add(AddUsulanSubmit(
          title: 'Jalanan Rusak',
          description: 'Jalanan Rusak di Cimindi mohon segera diperbaiki',
          status: '5',
          categoryId: '9',
          attachment: listPhoto,
          id: 1));
    });
  });

  group('Add Photo Usulan', () {
    test('AddUsulanPhotoDone to String', () {
      expect(AddUsulanPhotoDone().toString(), 'State AddUsulanPhotoDone');
    });

    test('AddUsulanPhotoDone to String', () {
      expect(
          AddUsulanPhotoFailure(error: Dictionary.errorUnauthorized).toString(),
          'State AddUsulanPhotoFailure{error: Izin akses ke server ditolak}');
    });

    test('AddPhotoSubmit to String', () {
      File image;
      expect(AddPhotoSubmit(image: image).toString(), 'Event AddPhotoSubmit');
    });

    test('AddPhotoSubmit props', () {
      File image;
      expect(AddPhotoSubmit(image: image).props, [image]);
    });

    test('emits [AddUsulanInitial, AddUsulanLoading, AddUsulanPhotoDone]', () {
      AddPhotoModel photoModel;
      File image;
      when(usulanRepository.addPhoto(image))
          .thenAnswer((_) => Future.value(photoModel));

      final expectedResponse = [
        AddUsulanInitial(),
        AddUsulanLoading(),
        AddUsulanPhotoDone()
      ];

      expectLater(
        addUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addUsulanBloc.add(AddPhotoSubmit(image: image));
    });

    test('emits [AddUsulanInitial, AddUsulanLoading, AddUsulanPhotoFailure]',
        () {
      File image;
      when(usulanRepository.addPhoto(image)).thenThrow('error');

      final expectedResponse = [
        AddUsulanInitial(),
        AddUsulanLoading(),
        AddUsulanPhotoFailure(error: Dictionary.somethingWrong)
      ];

      expectLater(
        addUsulanBloc,
        emitsInOrder(expectedResponse),
      );

      addUsulanBloc.add(AddPhotoSubmit(image: image));
    });
  });

  group('Delete Usulan', () {
    test('DeleteUsulan to String', () {
      expect(DeleteUsulan(usulanId: 1).toString(),
          'Event DeleteUsulan{usulanId: 1}');
    });

    test('DeleteUsulan props', () {
      expect(DeleteUsulan(usulanId: 1).props,
          [1]);
    });

    test('DeleteUsulanlLoading to String', () {
      expect(DeleteUsulanlLoading().toString(), 'State DeleteUsulanlLoading');
    });

    test('DeleteUsulanLoaded to String', () {
      expect(DeleteUsulanLoaded().toString(), 'State DeleteUsulanLoaded');
    });

    test('UsulanDetailFailure to String', () {
      expect(
          UsulanDetailFailure(
                  error: CustomException.onConnectionException(
                      ErrorException.timeoutException))
              .toString(),
          'UsulanDetailFailure{error: Server tidak merespon}');
    });

    test(
        'emits [UsulanDetailInitial, DeleteUsulanlLoading, DeleteUsulanLoaded]',
        () {
      when(usulanRepository.deleteUsulan(idUsulan: 815))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        UsulanDetailInitial(),
        DeleteUsulanlLoading(),
        DeleteUsulanLoaded()
      ];

      expectLater(
        detailBloc,
        emitsInOrder(expectedResponse),
      );

      detailBloc.add(DeleteUsulan(usulanId: 815));
    });

    test(
        'emits [UsulanDetailInitial, DeleteUsulanlLoading, UsulanDetailFailure]',
        () {
      when(usulanRepository.deleteUsulan(idUsulan: 815))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        UsulanDetailInitial(),
        DeleteUsulanlLoading(),
        UsulanDetailFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        detailBloc,
        emitsInOrder(expectedResponse),
      );

      detailBloc.add(DeleteUsulan(usulanId: 815));
    });
  });
}
