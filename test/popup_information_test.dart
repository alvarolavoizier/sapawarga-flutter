import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/popup_information/Bloc.dart';
import 'package:sapawarga/models/PopupInformationModel.dart';
import 'package:sapawarga/repositories/PopupInformationRepository.dart';
import 'package:test/test.dart';

class MockPopupInformationRepository extends Mock
    implements PopupInformationRepository {}

void main() {
  MockPopupInformationRepository repository;
  PopupInformationBloc popupInformationBloc;

  setUp(() {
    repository = MockPopupInformationRepository();
    popupInformationBloc = PopupInformationBloc(repository: repository);
  });

  test('Popup Information initial state is correct', () {
    expect(popupInformationBloc.initialState, PopupInformationInitial());
  });

  test('CheckPopupInformation to String', () {
    expect(CheckPopupInformation().toString(), 'Event CheckPopupInformation');
  });

  test('CheckPopupInformation props', () {
    expect(CheckPopupInformation().props, []);
  });

  test('Popup Information dispose does not emit new states', () {
    expectLater(
      popupInformationBloc,
      emitsInOrder([]),
    );
    popupInformationBloc.close();
  });

  test('emits [PopupInformationInitial, PopupInformationShow]', () {
    PopupInformationModel record = PopupInformationModel(
        id: 1, title: 'ABCD', endDate: DateTime.now().add(Duration(days: 3)));

    when(repository.hasShownPopupInformation())
        .thenAnswer((_) => Future.value(record));

    final expectedResponse = [
      PopupInformationInitial(),
      PopupInformationShow(record: record)
    ];

    expectLater(
      popupInformationBloc,
      emitsInOrder(expectedResponse),
    );

    popupInformationBloc.add(CheckPopupInformation());
  });

  test('emits [PopupInformationInitial, PopupInformationHasShown] => has shown',
      () {
    when(repository.hasShownPopupInformation())
        .thenAnswer((_) => Future.value(null));

    final expectedResponse = [
      PopupInformationInitial(),
      PopupInformationHasShown()
    ];

    expectLater(
      popupInformationBloc,
      emitsInOrder(expectedResponse),
    );

    popupInformationBloc.add(CheckPopupInformation());
  });
}
