
import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/AreaModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';

main(){
  group('userinfo model',(){

    Kabkota kabkota = Kabkota(
      id: 1,
      name: 'test test'
    );

    test('is correctly generated from userinfo Object', () {
     UserInfoModel userInfoModel = UserInfoModel(
       id: 1,
       username: 'test',
       email: '',
       roleId: '',
       roleLabel: '',
       name: '',
       phone: '',
       address: '',
       rt: '',
       rw: '',
       kelId: '',
       kelurahan: kabkota,
       kecId: '',
       kecamatan: kabkota,
       kabkotaId: '',
       kabkota: kabkota,
       lat: 0,
       lon: 0,
       photoUrl: '',
       facebook: '',
       twitter: '',
       instagram: '',
       lastLoginAt: 0,
       passwordUpdatedAt: 0,
       profileUpdatedAt: 0
     );

     String responString = userInfoModelToJson(userInfoModel);

     UserInfoModel respon = userInfoModelFromJson(responString);

     expect(respon.id, 1);
     expect(respon.username, 'test');

    });
  });

  group('EducationLevel', () {
    test('is correctly generated from EducationLevelModel Object', () {

      Map<String, Object> mock = {
        "id": 1,
        "title": "test",
        "seq": 1,
        "status": 1,
      };

      final response = EducationLevel.fromJson(json.decode(jsonEncode(mock)));

      assert(response != null);
      expect(response.id, 1);
      expect(response.title, 'test');
    });

    test('is correctly generated from ImportantInfoModel Array', () {

      String educationLevelModelToJson(EducationLevel data) => json.encode(data.toJson());

      EducationLevel attachment = EducationLevel(id: 1, title: "test", seq: 1, status: 1);

      expect(educationLevelModelToJson(attachment), jsonEncode(attachment.toJson()));

    });
  });
}