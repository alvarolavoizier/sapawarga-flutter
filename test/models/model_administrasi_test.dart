import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/AdministrasiModel.dart';

main(){
  group('administrasi model',(){

    test('is correctly generated from userinfo Object', () {
      AdministrasiModel administrasiModel = AdministrasiModel(
         id: 1,
        title: 'test',
        detail: 'test test test',
        name: 'polisi'
      );

      String responString = administrasiModelToJson(administrasiModel);

      AdministrasiModel respon = administrasiModelFromJson(responString);

      expect(respon.id, 1);
      expect(respon.title, 'test');

    });
  });
}