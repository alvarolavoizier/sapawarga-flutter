import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/GeneralTitleModel.dart';

main() {
  group('General title model', () {
    test('is correctly generated from GeneralTitleModel Object', () {
      GeneralTitleModel mock = GeneralTitleModel(
          id: 1, title: 'bersama kita bisa', status: 1, seq: 1);
      final respon = generalTitleModelFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock.title, 'bersama kita bisa');
      expect(mock.id, 1);
    });

    test('is correctly generated from GeneralTitleModel Array', () {
      GeneralTitleModel data1 = GeneralTitleModel(
          id: 1, title: 'bersama kita bisa', status: 1, seq: 1);

      GeneralTitleModel data2 = GeneralTitleModel(
          id: 1, title: 'bersama-sama kita ke masjid', status: 1, seq: 1);

      List<GeneralTitleModel> dataGeneralTitleModel = List<GeneralTitleModel>();
      dataGeneralTitleModel.add(data1);
      dataGeneralTitleModel.add(data2);

      List<GeneralTitleModel> respon =
          listGeneralTitleModelFromJson(jsonEncode(dataGeneralTitleModel));

      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].title, 'bersama kita bisa');
      expect(respon[1].title, 'bersama-sama kita ke masjid');
    });
  });
}
