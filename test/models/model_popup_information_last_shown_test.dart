import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/PopupInformationLastShownModel.dart';

main() {
  group('Popup Information Last Shown Model ', () {
    test('is correctly generated from Popup Information Last Shown Model Object', () {
      Map<String, Object> mock = {
        "id": 5,
        "last_shown": "2019-10-20 00:00:00"
      };

      final response = popupInformationLastShownFromJson(jsonEncode(mock));

      assert(response != null);
      expect(mock['id'], 5);
      expect(mock['last_shown'], '2019-10-20 00:00:00');
    });

    test('is correctly generated from Popup Information Last Shown Model Array', () {
      PopupInformationLastShownModel data1 = PopupInformationLastShownModel(
          id: 5,
          lastShown: DateTime.parse('2019-10-20 00:00:00'));

      PopupInformationLastShownModel data2 = PopupInformationLastShownModel(
          id: 6,
          lastShown: DateTime.parse('2019-10-20 00:00:00').add(Duration(days: 3)));

      List<PopupInformationLastShownModel> mock = List<PopupInformationLastShownModel>();
      mock.add(data1);
      mock.add(data2);

      List<PopupInformationLastShownModel> response =
      listPopupInformationLastShownFromJson(jsonEncode(mock));
      assert(response != null);
      expect(response.length, 2);
      expect(response[0].id, 5);
      expect(response[0].lastShown, DateTime.parse('2019-10-20 00:00:00'));
      expect(response[1].id, 6);
      expect(response[1].lastShown, DateTime.parse('2019-10-20 00:00:00').add(Duration(days: 3)));
    });
  });
}
