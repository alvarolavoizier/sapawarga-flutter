import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/MetaNotificationModel.dart';
import 'package:sapawarga/models/NotificationModel.dart';

main() {
  group('NotificationModel', () {
    test('is correctly generated from NotificationModel Array', () {
      var data = [
        {
          "id": 1415,
          "title": "Usulan Anda dengan judul telah Ditolak",
          "target": "notifikasi",
          "meta": {"id": 1033, "target": "aspirasi"}
        },
        {
          "id": 1418,
          "title": "Usulan Anda dengan judul telah Diterima",
          "target": "notifikasi",
          "meta": {"id": 1033, "target": "aspirasi"}
        }
      ];

      var data2 = [{"id": 1419, "title": "Usulan Anda dengan judul telah Ditolak", "target": "notifikasi", "meta": "{\"target\":\"aspirasi\",\"id\":1033,\"url\":null}", "read_at": null}];

      NotificationModel notificationModel = NotificationModel(
          id: 1419,
          title: "Usulan Anda dengan judul telah Ditolak",
          target: "notifikasi",
          meta: Meta(id: 1033, target: "aspirasi")
      );

      var data3 = notificationModel.toJson();


      List<NotificationModel> response =
          listNotificationFromJson(jsonEncode(data));

      List<NotificationModel> response2 = data2.isNotEmpty
          ? data2.map((c) => NotificationModel.fromDatabaseJson(c)).toList()
          : [];

      assert(response != null);
      expect(response.length, 2);
      expect(response[0].id, 1415);
      expect(response[0].title, 'Usulan Anda dengan judul telah Ditolak');
      expect(response[1].id, 1418);
      expect(response[1].meta.target, 'aspirasi');

      expect(response2.length, 1);
      expect(response2[0].id, 1419);
      expect(response2[0].target, 'notifikasi');

      expect(data3, data2[0]);
    });
  });
}
