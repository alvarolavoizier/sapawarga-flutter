import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/SurveyModel.dart';

main() {
  group('SurveyModel', () {
    test('is correctly generated from SurveyModel Object', () {
      Map<String, Object> mock = {
        "id": 1,
        "category_id": 20,
        "category": {"id": 20, "name": "Infrastruktur"},
        "title":
            "Usulan agar lokasi sekolah SD, SMP, dan SMA/SMK ada dalam satu gedung atau lokasi.",
        "external_url":
            "http://35.247.135.93.xip.io:5001/view/#!/forms/5d1598147051b3010096067e",
        "start_date": "2019-07-11",
        "end_date": "2019-07-12",
        "meta": null,
        "status": 10,
        "status_label": "Dipublikasikan",
        "kabkota_id": null,
        "kabkota": null,
        "kec_id": null,
        "kecamatan": null,
        "kel_id": null,
        "kelurahan": null,
        "rw": null,
        "created_at": 1562730581,
        "updated_at": 1562820125
      };

      final respon = surveyFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 1);
      expect(mock['title'],
          'Usulan agar lokasi sekolah SD, SMP, dan SMA/SMK ada dalam satu gedung atau lokasi.');
    });

    test('is correctly generated from SurveyModel Array', () {
      Category categoryData = Category(id: 20, name: 'Infrastruktur');

      SurveyModel data1 = SurveyModel(
          id: 1,
          categoryId: 20,
          category: categoryData,
          title:
              "Usulan agar lokasi sekolah SD, SMP, dan SMA/SMK ada dalam satu gedung atau lokasi.",
          externalUrl:
              "http://35.247.135.93.xip.io:5001/view/#!/forms/5d1598147051b3010096067e",
          startDate: DateTime.now(),
          endDate: DateTime.now(),
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          createdAt: 1562730581);

      SurveyModel data2 = SurveyModel(
          id: 2,
          categoryId: 20,
          category: categoryData,
          title: "Usulan",
          externalUrl:
              "http://35.247.135.93.xip.io:5001/view/#!/forms/5d1598147051b3010096067e",
          startDate: DateTime.now(),
          endDate: DateTime.now(),
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          createdAt: 1562730581);

      List<SurveyModel> mock = List<SurveyModel>();
      mock.add(data1);
      mock.add(data2);

      List<SurveyModel> respon = listSurveyFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 1);
      expect(respon[0].title,
          'Usulan agar lokasi sekolah SD, SMP, dan SMA/SMK ada dalam satu gedung atau lokasi.');
      expect(respon[1].id, 2);
      expect(respon[1].title, 'Usulan');
    });
  });
}
