import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/AuthorModel.dart';
import 'package:sapawarga/models/BroadcastModel.dart';
import 'package:sapawarga/models/CategoryModel.dart';

main() {
  group('Broadcast', () {
    test('is correctly generated from BroadcastModel Object', () {
      Map<String, Object> mock = {
        "id": 812,
        "author_id": 1,
        "author": {
          "id": 1,
          "name": "Administrator",
          "role_label": "Administrator"
        },
        "category_id": 5,
        "category": {"id": 5, "name": "Informasi"},
        "title": "Coba Lagi Broadcast",
        "description": "Coba Lagi Broadcast",
        "kabkota_id": null,
        "kabkota": null,
        "kec_id": null,
        "kecamatan": null,
        "kel_id": null,
        "kelurahan": null,
        "rw": null,
        "meta": null,
        "status": 10,
        "status_label": "Dipublikasikan",
        "data": null,
        "created_at": 1571124517,
        "updated_at": 1571124517
      };

      final respon = broadcastFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 812);
      expect(mock['title'], 'Coba Lagi Broadcast');
    });

    test('is correctly generated from BroadcastModel Array', () {
      BroadcastModel data1 = BroadcastModel(
          id: 812,
          authorId: 1,
          author: Author(id: 1, name: "Admin", roleLabel: "admin"),
          categoryId: 1,
          category: Category(id: 1, name: "Informasi"),
          title: "Coba Lagi Broadcast",
          description: "Coba Lagi Broadcast",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          createdAt: 1571124517,
          updatedAt: 1571124517);

      BroadcastModel data2 = BroadcastModel(
          id: 813,
          authorId: 2,
          author: Author(id: 2, name: "Provinsi", roleLabel: "provinsi"),
          categoryId: 2,
          category: Category(id: 2, name: "Kunjungan"),
          title: "Gubernur akan berkunjung",
          description: "Akan dilakukan kunjungan",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          createdAt: 1571124519,
          updatedAt: 1571124519);

      List<BroadcastModel> mock = List<BroadcastModel>();
      mock.add(data1);
      mock.add(data2);

      List<BroadcastModel> respon = listBroadcastFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 812);
      expect(respon[0].title, 'Coba Lagi Broadcast');
      expect(respon[1].id, 813);
      expect(respon[1].title, 'Gubernur akan berkunjung');
    });
  });

  group('Author', () {
    test('is correctly generated from BroadcastModel Object', () {
      Map<String, Object> mock = {
        "id": 812,
        "author_id": 1,
        "author": {
          "id": 1,
          "name": "Administrator",
          "role_label": "Administrator"
        },
        "category_id": 5,
        "category": {"id": 5, "name": "Informasi"},
        "title": "Coba Lagi Broadcast",
        "description": "Coba Lagi Broadcast",
        "kabkota_id": null,
        "kabkota": null,
        "kec_id": null,
        "kecamatan": null,
        "kel_id": null,
        "kelurahan": null,
        "rw": null,
        "meta": null,
        "status": 10,
        "status_label": "Dipublikasikan",
        "data": null,
        "created_at": 1571124517,
        "updated_at": 1571124517
      };

      final respon = broadcastFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 812);
      expect(mock['title'], 'Coba Lagi Broadcast');
    });

    test('is correctly generated from BroadcastModel Array', () {
      BroadcastModel data1 = BroadcastModel(
          id: 812,
          authorId: 1,
          author: Author(id: 1, name: "Admin", roleLabel: "admin"),
          categoryId: 1,
          category: Category(id: 1, name: "Informasi"),
          title: "Coba Lagi Broadcast",
          description: "Coba Lagi Broadcast",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          createdAt: 1571124517,
          updatedAt: 1571124517);

      BroadcastModel data2 = BroadcastModel(
          id: 813,
          authorId: 2,
          author: Author(id: 2, name: "Provinsi", roleLabel: "provinsi"),
          categoryId: 2,
          category: Category(id: 2, name: "Kunjungan"),
          title: "Gubernur akan berkunjung",
          description: "Akan dilakukan kunjungan",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          createdAt: 1571124519,
          updatedAt: 1571124519);

      List<BroadcastModel> mock = List<BroadcastModel>();
      mock.add(data1);
      mock.add(data2);

      List<BroadcastModel> respon = listBroadcastFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 812);
      expect(respon[0].title, 'Coba Lagi Broadcast');
      expect(respon[1].id, 813);
      expect(respon[1].title, 'Gubernur akan berkunjung');
    });
  });
}
