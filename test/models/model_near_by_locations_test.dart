import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/NearByLocationsModel.dart';

main() {
  group('Near By Locations Model', () {
    test('is correctly generated from Near By Locations Model Array', () {
      NearByLocationsModel data1 = NearByLocationsModel(
        id: '1',
        category: 'category',
        name: 'name',
        description: 'description',
        address: 'address',
        phoneNumbers: null,
        latitude: 'latitude',
        longitude: 'longitude',
        distance: 'distance',);

      NearByLocationsModel data2 = NearByLocationsModel(
        id: '2',
        category: 'category2',
        name: 'name2',
        description: 'description2',
        address: 'address2',
        phoneNumbers: null,
        latitude: 'latitude2',
        longitude: 'longitude2',
        distance: 'distance2',);

      List<NearByLocationsModel> mock = List<NearByLocationsModel>();
      mock.add(data1);
      mock.add(data2);

      List<NearByLocationsModel> response =
      nearByLocationsFromJson(jsonEncode(mock));
      assert(response != null);
      expect(response.length, 2);
      expect(response[0].id, '1');
      expect(response[0].category, 'category');
      expect(response[1].id, '2');
      expect(response[1].category, 'category2');
    });
  });
}
