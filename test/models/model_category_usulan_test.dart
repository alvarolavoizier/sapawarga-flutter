import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';

main() {
  group('Category Usulan', () {
    test('is correctly generated from Category Usulan Object', () {
      Map<String, Object> mock = {
        "id": 60,
        "type": "aspirasi",
        "name": "Hapus",
        "meta": "null",
        "status": 10,
        "status_label": "Aktif",
        "created_at": 1570763169,
        "updated_at": 1570763169
      };

      final respon = masterCategoryFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 60);
      expect(mock['type'], 'aspirasi');
    });

    test('is correctly generated from CategoryUsulanModel Array', () {
      MasterCategoryModel data1 = MasterCategoryModel(
          id: 1,
          type: null,
          name: 'Layanan Kesehatan',
          meta: null,
          status: 10,
          statusLabel: StatusLabel.AKTIF,
          createdAt: 1570612013,
          updatedAt: 1570612013);

      MasterCategoryModel data2 = MasterCategoryModel(
          id: 2,
          type: null,
          name: 'Layanan Ekonomi',
          meta: null,
          status: 10,
          statusLabel: StatusLabel.AKTIF,
          createdAt: 1570612013,
          updatedAt: 1570612013);

      List<MasterCategoryModel> mock = List<MasterCategoryModel>();
      mock.add(data1);
      mock.add(data2);

      List<MasterCategoryModel> respon =
          listMasterCategoryFromJson(jsonEncode(mock));

      expect(respon.length, 2);
      expect(respon[0].id, 1);
      expect(respon[0].type, null);
      expect(respon[0].name, 'Layanan Kesehatan');
      expect(respon[0].meta, null);
      expect(respon[0].status, 10);
      expect(respon[0].statusLabel, StatusLabel.AKTIF);
      expect(respon[0].createdAt, 1570612013);
      expect(respon[0].updatedAt, 1570612013);
      expect(respon[1].id, 2);
      expect(respon[1].type, null);
      expect(respon[1].name, 'Layanan Ekonomi');
      expect(respon[1].meta, null);
      expect(respon[1].status, 10);
      expect(respon[1].statusLabel, StatusLabel.AKTIF);
      expect(respon[1].createdAt, 1570612013);
      expect(respon[1].updatedAt, 1570612013);
    });
  });
}
