import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/PopupInformationModel.dart';

main() {
  group('Popup Information Model ', () {
    test('is correctly generated from Popup Information Model Object', () {
      Map<String, Object> mock = {
        "id": 5,
        "title": "Program Unggulan Sapawarga",
        "description": "Sapawarga merupakan superapp buatan Jabar Deigital Service.",
        "image_path": "popup/8hWMIesrfQ8qrsBsIM1x0Cpdib8A2S8s.jpg",
        "image_path_url": "https://d2o6nohz2yihhc.cloudfront.net/popup/8hWMIesrfQ8qrsBsIM1x0Cpdib8A2S8s.jpg",
        "type": "internal",
        "link_url": "",
        "internal_object_type": "survey",
        "internal_object_id": 1,
        "internal_object_name": "",
        "status": 10,
        "status_label": "Aktif",
        "start_date": "2019-10-20 00:00:00",
        "end_date": "2019-10-30 00:00:00",
        "created_at": 1571640819,
        "updated_at": 1571640819,
        "created_by": 1
      };

      final respon = popupInformationFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 5);
      expect(mock['title'], 'Program Unggulan Sapawarga');
    });

    test('is correctly generated from Popup Information Model Array', () {
      PopupInformationModel data1 = PopupInformationModel(
            id: 5,
            title: "Program Unggulan Sapawarga",
            description: "Sapawarga merupakan superapp buatan Jabar Deigital Service.",
            imagePath: "popup/8hWMIesrfQ8qrsBsIM1x0Cpdib8A2S8s.jpg",
            imagePathUrl: "https://d2o6nohz2yihhc.cloudfront.net/popup/8hWMIesrfQ8qrsBsIM1x0Cpdib8A2S8s.jpg",
            type: "internal",
            linkUrl: "",
            internalObjectType: "survey",
            internalObjectId: 1,
            internalObjectName: "",
            status: 10,
            statusLabel: "Aktif",
            startDate: DateTime.now(),
            endDate: DateTime.now().add(Duration(days: 3)),
            createdAt: 1571640819,
            updatedAt: 1571640819,
            createdBy: 1);

      PopupInformationModel data2 = PopupInformationModel(
          id: 6,
          title: "Program Sapawarga",
          description: "Sapawarga merupakan superapp buatan JDS.",
          imagePath: "popup/8hWMIesrfQ8qrsBsIM1x0Cpdib8A2S8s.jpg",
          imagePathUrl: "https://d2o6nohz2yihhc.cloudfront.net/popup/8hWMIesrfQ8qrsBsIM1x0Cpdib8A2S8s.jpg",
          type: "internal",
          linkUrl: "",
          internalObjectType: "survey",
          internalObjectId: 1,
          internalObjectName: "",
          status: 10,
          statusLabel: "Aktif",
          startDate: DateTime.now(),
          endDate: DateTime.now().add(Duration(days: 3)),
          createdAt: 1571640819,
          updatedAt: 1571640819,
          createdBy: 1);

      List<PopupInformationModel> mock = List<PopupInformationModel>();
      mock.add(data1);
      mock.add(data2);

      List<PopupInformationModel> respon =
      listPopupInformationFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 5);
      expect(respon[0].title, 'Program Unggulan Sapawarga');
      expect(respon[1].id, 6);
      expect(respon[1].title, 'Program Sapawarga');
    });
  });
}
