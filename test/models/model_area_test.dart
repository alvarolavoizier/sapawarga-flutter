import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/AreaModel.dart';

main() {
  group('Kabkota', () {
    test('is correctly generated from Kabkota Object', () {
      Kabkota mock = Kabkota(id: 1, name: 'bandung');
      final respon = kabkotaFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock.name, 'bandung');
      expect(mock.id, 1);
    });

    test('is correctly generated from Kabkota Array', () {
      Kabkota data1 = Kabkota(id: 1, name: 'bandung');
      Kabkota data2 = Kabkota(id: 2, name: 'jakarta');

      List<Kabkota> dataKabkota = List<Kabkota>();
      dataKabkota.add(data1);
      dataKabkota.add(data2);

      List<Kabkota> respon = listKabkotaFromJson(jsonEncode(dataKabkota));

      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].name, 'bandung');
      expect(respon[1].name, 'jakarta');
    });
  });

  group('Kecamatan', () {
    test('is correctly generated from Kecamatan Object', () {
      Kecamatan mock = Kecamatan(id: 1, name: 'sukasari');
      final respon = KecamatanFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock.name, 'sukasari');
      expect(mock.id, 1);
    });

    test('is correctly generated from Kecamatan Array', () {
      Kecamatan data1 = Kecamatan(id: 1, name: 'sukasari');
      Kecamatan data2 = Kecamatan(id: 2, name: 'coblong');

      List<Kecamatan> dataKecamatan = List<Kecamatan>();
      dataKecamatan.add(data1);
      dataKecamatan.add(data2);

      List<Kecamatan> respon = listKecamatanFromJson(jsonEncode(dataKecamatan));

      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].name, 'sukasari');
      expect(respon[1].name, 'coblong');
    });
  });

  group('Kelurahan', () {
    test('is correctly generated from Kelurahan Object', () {
      Kelurahan mock = Kelurahan(id: 1, name: 'gegerkalong');
      final respon = KelurahanFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock.name, 'gegerkalong');
      expect(mock.id, 1);
    });

    test('is correctly generated from Kelurahan Array', () {
      Kelurahan data1 = Kelurahan(id: 1, name: 'gegerkalong');
      Kelurahan data2 = Kelurahan(id: 2, name: 'setiabudi');

      List<Kelurahan> dataKelurahan = List<Kelurahan>();
      dataKelurahan.add(data1);
      dataKelurahan.add(data2);

      List<Kelurahan> respon = listKelurahanFromJson(jsonEncode(dataKelurahan));

      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].name, 'gegerkalong');
      expect(respon[1].name, 'setiabudi');
    });
  });
}
