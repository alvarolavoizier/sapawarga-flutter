import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/QnACommentModel.dart';
import 'package:sapawarga/models/UserModel.dart';

main() {
  group('QnACommentModel', () {
    test('is correctly generated from QnACommentModel Object', () {
      Map<String, dynamic> mock = {
        "id": 1,
        "question_id": 1,
        "text": "Komentar",
        "user": {
          "id": 1,
          "name": "Administrator",
          "photo_url_full": "https://d2o6nohz2yihhc.cloudfront.net/general/1571217770-QJXWOEw6FeNvLvm3kNYd4sev4RmrsD9J.jpg",
          "role_label": "Administrator"
        },
        "is_flagged": false,
        "created_at": 1575519233,
        "updated_at": 1575519233,
        "created_by": 1,
        "updated_by": 1
      };

      final response = qnaCommentModelFromJson(jsonEncode(mock));

      assert(response != null);
      expect(mock['id'], 1);
      expect(mock['text'], 'Komentar');
    });
  });

  test('is correctly generated from QnACommentModel Array', () {
    User userData = User(
        id: 2,
        name: "Admin staff Provinsi Jawa Barat",
        photoUrlFull:
        "https://d2o6nohz2yihhc.cloudfront.net/general/1573100814-q4QzHMxkGAaulgOJIjkfDKCxkijmanNv.jpg",
        roleLabel: "Staf Provinsi");

    QnACommentModel data1 = QnACommentModel(
        id: 1,
        questionId: 1,
        text: "Komentar",
        user: userData,
        isFlagged: false,
        createdAt: 1575519233,
        updatedAt: 1575519233,
        createdBy: 1,
        updatedBy: 1
    );

    QnACommentModel data2 = QnACommentModel(
        id: 2,
        questionId: 1,
        text: "OK",
        user: userData,
        isFlagged: false,
        createdAt: 1575519233,
        updatedAt: 1575519233,
        createdBy: 1,
        updatedBy: 1
    );

    List<QnACommentModel> mock = List<QnACommentModel>();
    mock.add(data1);
    mock.add(data2);

    List<QnACommentModel> response = listQnACommentModelFromJson(jsonEncode(mock));
    assert(response != null);
    expect(response.length, 2);
    expect(response[0].id, 1);
    expect(response[0].text, 'Komentar');
    expect(response[1].id, 2);
    expect(response[1].text, 'OK');

  });
}
