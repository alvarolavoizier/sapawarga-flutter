import 'dart:convert';

import 'package:sapawarga/models/MetaNotificationModel.dart';
import 'package:test/test.dart';

main() {
  group('Meta Notification', () {
    test('is correctly generated from MetaNotificationModel Array', () {
      Meta data1 = Meta(
        target: 'update',
        id: 121,
        url: 'https://www.google.com'
      );

      Meta data2 = Meta(
          target: 'berita',
          id: 122,
          url: 'https://www.kompas.com'
      );

      List<Meta> mock = List<Meta>();
      mock.add(data1);
      mock.add(data2);

      assert(mock != null);
      expect(mock.length, 2);
      expect(mock[0].id, 121);
      expect(mock[0].target, 'update');
      expect(mock[1].id, 122);
      expect(mock[1].target, 'berita');

      Meta data = metaNotificationFromJson(jsonEncode(data1));
      expect(data.target, 'update');
    });
  });

}