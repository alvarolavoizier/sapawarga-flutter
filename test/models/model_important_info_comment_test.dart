import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';
import 'package:sapawarga/models/MetaResponseModel.dart';
import 'package:sapawarga/models/UserModel.dart';

main() {
  group('ImportantInfoCommentModel', () {
    test('is correctly generated from ImportantInfoCommentModel Object', () {

      List<ItemImportantInfoComment> items = [];

      MetaResponseModel meta = MetaResponseModel(totalCount: 0, pageCount: 0, currentPage: 0, perPage: 0);


      Map<String, Object> mock = {
        "items": items,
        "_meta": meta,
      };

      final response = importantInfoCommentModelFromJson(jsonEncode(mock));

      assert(response != null);
      expect(response.items.length, 0);
      expect(response.meta.totalCount, 0);
    });

    test('is correctly generated from ItemImportantInfoCommentModel Object', () {

      User userData = User(
          id: 2,
          name: "Admin staff Provinsi Jawa Barat",
          photoUrlFull:
          "https://d2o6nohz2yihhc.cloudfront.net/general/1573100814-q4QzHMxkGAaulgOJIjkfDKCxkijmanNv.jpg",
          roleLabel: "Staf Provinsi");

      Map<String, Object> mock = {
        "id": 1,
        "news_important_id": 1,
        "text": "test",
        "user": userData,
        "created_at": 1111,
        "updated_at": 1111,
        "created_by": 1111,
        "updated_by": 1111,
      };

      final response = itemImportantInfoCommentFromJson(jsonEncode(mock));

      assert(response != null);
      expect(response.id, 1);
    });

    test('ItemImportantInfoCommentModel props', () {
      expect(ItemImportantInfoComment().props, [null]);
    });

  });

}
