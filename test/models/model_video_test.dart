import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/VideoModel.dart';

main() {
  group('VideoModel', () {
    test('is correctly generated from VideoModel Object', () {
      Map<String, Object> mock = {
        "id": 70,
        "title": "Video Baru Bro",
        "category_id": 23,
        "category": {"id": 23, "name": "Informasi"},
        "source": "youtube",
        "video_url": "https://www.youtube.com/watch?v=ygqjSBR-tz8",
        "kabkota_id": null,
        "kabkota": null,
        "total_likes": 0,
        "seq": 1,
        "status": 10,
        "status_label": "Aktif",
        "created_at": 1571759552,
        "updated_at": 1571759566,
        "created_by": 1
      };

      final respon = videoFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 70);
      expect(mock['title'], 'Video Baru Bro');
    });

    test('is correctly generated from VideoModel Array', () {
      Category categoryData = Category(id: 22, name: 'Informasi');

      VideoModel data1 = VideoModel(
          id: 70,
          title: "Video Baru Bro",
          categoryId: 23,
          category: categoryData,
          source: "youtube",
          videoUrl: "https://www.youtube.com/watch?v=ygqjSBR-tz8",
          kabkotaId: null,
          kabkota: null,
          totalLikes: 0,
          seq: 1,
          status: 10,
          statusLabel: "Aktif",
          createdAt: 1571759552,
          updatedAt: 1571759566,
          createdBy: 1);

      VideoModel data2 = VideoModel(
          id: 1,
          title: "Video",
          categoryId: 23,
          category: categoryData,
          source: "youtube",
          videoUrl: "https://www.youtube.com/watch?v=ygqjSBR-tz8",
          kabkotaId: null,
          kabkota: null,
          totalLikes: 0,
          seq: 1,
          status: 10,
          statusLabel: "Aktif",
          createdAt: 1571759552,
          updatedAt: 1571759566,
          createdBy: 1);

      List<VideoModel> mock = List<VideoModel>();
      mock.add(data1);
      mock.add(data2);

      List<VideoModel> respon = listVideoFromJson(jsonEncode(mock));
      final listToString = listVideoToJson(respon);
      assert(listToString != null);
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 70);
      expect(respon[0].title, 'Video Baru Bro');
      expect(respon[1].id, 1);
      expect(respon[1].title, 'Video');
    });
  });
}
