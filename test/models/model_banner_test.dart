import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/BannerModel.dart';

main() {
  group('Banner ', () {
    test('is correctly generated from BannerModel Object', () {
      Map<String, Object> mock = {
        "id": 13,
        "title": "banner pollinggg",
        "image_path": "general/1570595646-Nd2x4Pwlplh3RiTgusAjuG739aOifdMH.jpg",
        "image_path_url":
            "https://d2o6nohz2yihhc.cloudfront.net/general/1570595646-Nd2x4Pwlplh3RiTgusAjuG739aOifdMH.jpg",
        "type": "internal",
        "link_url": "",
        "internal_category": "polling",
        "internal_entity_id": 400,
        "internal_entity_name": "Polling Jabar",
        "status": 10,
        "status_label": "Aktif",
        "created_at": 1570595651,
        "updated_at": 1570595651,
        "created_by": 1
      };

      final respon = bannerFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 13);
      expect(mock['title'], 'banner pollinggg');
    });

    test('is correctly generated from Banner Array', () {
      BannerModel data1 = BannerModel(
          id: 13,
          title: 'banner pollinggg',
          imagePath: 'general/1570595646-Nd2x4Pwlplh3RiTgusAjuG739aOifdMH.jpg',
          imagePathUrl:
              'https://d2o6nohz2yihhc.cloudfront.net/general/1570595646-Nd2x4Pwlplh3RiTgusAjuG739aOifdMH.jpg',
          type: 'internal',
          linkUrl: '',
          internalCategory: 'polling',
          internalEntityId: 400,
          internalEntityName: 'Polling',
          status: 10,
          statusLabel: 'Aktif',
          createdAt: 1570595651,
          updatedAt: 1570595651,
          createdBy: 1);

      BannerModel data2 = BannerModel(
          id: 14,
          title: 'banner',
          imagePath: 'general/1570595646-Nd2x4Pwlplh3RiTgusAjuG739aOifdMH.jpg',
          imagePathUrl:
              'https://d2o6nohz2yihhc.cloudfront.net/general/1570595646-Nd2x4Pwlplh3RiTgusAjuG739aOifdMH.jpg',
          type: 'internal',
          linkUrl: '',
          internalCategory: 'polling',
          internalEntityId: 400,
          internalEntityName: 'Polling',
          status: 10,
          statusLabel: 'Aktif',
          createdAt: 1570595651,
          updatedAt: 1570595651,
          createdBy: 1);

      List<BannerModel> mock = List<BannerModel>();
      mock.add(data1);
      mock.add(data2);

      List<BannerModel> respon = listBannerFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 13);
      expect(respon[0].title, 'banner pollinggg');
      expect(respon[1].id, 14);
      expect(respon[1].title, 'banner');
    });
  });
}
