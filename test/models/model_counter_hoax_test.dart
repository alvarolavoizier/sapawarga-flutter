import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';

main() {
  group('CounterHoaxModel', () {
    test('is correctly generated from CounterHoaxModel Object', () {
      Map<String, Object> mock = {
        "id": 14,
        "title": "Test Ukuran Hoax",
        "content": "<p>Gaas</p>",
        "cover_path": "general/1572925302-9FS1vVjvksbOPgoJEBlQmRJkwbqFV03A.jpg",
        "cover_path_url":
            "https://d2o6nohz2yihhc.cloudfront.net/general/1572925302-9FS1vVjvksbOPgoJEBlQmRJkwbqFV03A.jpg",
        "source_date": "2019-11-05",
        "source_url": "",
        "category_id": 43,
        "category": {"id": 43, "name": "Berita"},
        "meta": null,
        "seq": null,
        "status": 10,
        "status_label": "Aktif",
        "created_at": 1572925316,
        "updated_at": 1572925321
      };

      final respon = counterHoaxFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 14);
      expect(mock['title'], 'Test Ukuran Hoax');
    });

    test('is correctly generated from CounterHoaxModel Array', () {
      Category categoryData = Category(id: 43, name: 'Berita');
      CounterHoaxModel data1 = CounterHoaxModel(
          id: 14,
          title: "Test Ukuran Hoax",
          content: "<p>Gaas</p>",
          coverPath: "general/1572925302-9FS1vVjvksbOPgoJEBlQmRJkwbqFV03A.jpg",
          coverPathUrl:
              "https://d2o6nohz2yihhc.cloudfront.net/general/1572925302-9FS1vVjvksbOPgoJEBlQmRJkwbqFV03A.jpg",
          sourceDate: DateTime.now(),
          sourceUrl: "",
          categoryId: 43,
          category: categoryData,
          meta: null,
          seq: null,
          status: 10,
          statusLabel: "Aktif",
          createdAt: 1572925316,
          updatedAt: 1572925321);

      CounterHoaxModel data2 = CounterHoaxModel(
          id: 22,
          title: "Test Ukuran Hoax1",
          content: "<p>Gaas</p>",
          coverPath: "general/1572925302-9FS1vVjvksbOPgoJEBlQmRJkwbqFV03A.jpg",
          coverPathUrl:
              "https://d2o6nohz2yihhc.cloudfront.net/general/1572925302-9FS1vVjvksbOPgoJEBlQmRJkwbqFV03A.jpg",
          sourceDate: DateTime.now(),
          sourceUrl: "",
          categoryId: 43,
          category: categoryData,
          meta: null,
          seq: null,
          status: 10,
          statusLabel: "Aktif",
          createdAt: 1572925316,
          updatedAt: 1572925321);

      List<CounterHoaxModel> mock = List<CounterHoaxModel>();
      mock.add(data1);
      mock.add(data2);

      List<CounterHoaxModel> respon = listCounterHoaxFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 14);
      expect(respon[0].title, 'Test Ukuran Hoax');
      expect(respon[1].id, 22);
      expect(respon[1].title, 'Test Ukuran Hoax1');
    });
  });
}
