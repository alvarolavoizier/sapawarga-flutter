import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/models/UserModel.dart';

main() {
  group('QnAModel', () {
    test('is correctly generated from QnAModel Object', () {
      Map<String, dynamic> mock = {
        "id": 1,
        "text": "Pertanyaan 1",
        "likes_count": 0,
        "comments_count": 11,
        "answer_id": null,
        "answer": null,
        "status": 10,
        "status_label": "Aktif",
        "created_at": 1575446621,
        "updated_at": 1575543707,
        "created_by": 26,
        "is_flagged": 0,
        "is_liked": false,
        "user": {
          "id": 26,
          "name": "Aldi Rohman",
          "photo_url_full":
              "https://d2o6nohz2yihhc.cloudfront.net/general/1573714163-ZzeGMWI9tlZ8ec1hBHtg49KNGdW3e599.jpg",
          "role_label": "RW"
        },
        "is_highlight": true,
      };

      final response = qnaModelFromJson(jsonEncode(mock));

      assert(response != null);
      expect(mock['id'], 1);
      expect(mock['text'], 'Pertanyaan 1');
    });
  });

  test('is correctly generated from QnAModel Array', () {
    User userData = User(
        id: 2,
        name: "Admin staff Provinsi Jawa Barat",
        photoUrlFull:
            "https://d2o6nohz2yihhc.cloudfront.net/general/1573100814-q4QzHMxkGAaulgOJIjkfDKCxkijmanNv.jpg",
        roleLabel: "Staf Provinsi");

    Answer answerData = Answer(
        id: 22,
        questionId: 22,
        text: "Komentar",
        user: userData,
        isFlagged: false,
        createdAt: 1575539881,
        updatedAt: 1575539881,
        createdBy: 2,
        updatedBy: 2);

    QnAModel data1 = QnAModel(
        id: 22,
        text: "Jarak antara kita",
        likesCount: 0,
        commentsCount: 4,
        answerId: 22,
        answer: answerData,
        status: 10,
        statusLabel: "Aktif",
        createdAt: 1575537289,
        updatedAt: 1575857295,
        createdBy: 186132,
        isFlagged: 0,
        isLiked: false,
        isHighlight: false,
        user: userData);

    QnAModel data2 = QnAModel(
        id: 19,
        text: "hallo hallo",
        likesCount: 0,
        commentsCount: 4,
        answerId: null,
        answer: null,
        status: 10,
        statusLabel: "Aktif",
        createdAt: 1575537289,
        updatedAt: 1575857295,
        createdBy: 186132,
        isFlagged: 0,
        isLiked: false,
        isHighlight: true,
        user: userData);

    List<QnAModel> mock = List<QnAModel>();
    mock.add(data1);
    mock.add(data2);

    List<QnAModel> response = listQnAModelFromJson(jsonEncode(mock));
    assert(response != null);
    expect(response.length, 2);
    expect(response[0].id, 22);
    expect(response[0].text, 'Jarak antara kita');
    expect(response[1].id, 19);
    expect(response[1].text, 'hallo hallo');

  });
}
