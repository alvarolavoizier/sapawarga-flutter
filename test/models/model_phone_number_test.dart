import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/enums/PhoneNumberTypeEnum.dart';
import 'package:sapawarga/models/PhoneNumberModel.dart';

main() {
  group('PhoneNumber', () {
    test('is correctly generated from PhoneNumberModel', () {

      PhoneNumberModel phoneNumberModel = PhoneNumberModel(
        type: PhoneNumberTypeEnum.phone, phoneNumber: '081231281238123'
      );

      Map<String, Object> mock = {
        'type': PhoneNumberTypeEnum.phone,
        'phone_number': '081231281238123',
      };

      PhoneNumberModel.fromMap(mock);
      phoneNumberModel.toJson();

      assert(phoneNumberModel != null);
      expect(phoneNumberModel.type, PhoneNumberTypeEnum.phone);
      expect(phoneNumberModel.phoneNumber, '081231281238123');
    });
  });

}