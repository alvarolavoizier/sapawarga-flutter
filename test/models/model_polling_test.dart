import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/PollingModel.dart';

main() {
  group('Polling', () {
    test('is correctly generated from PollingModel Object', () {
      Map<String, Object> mock = {
        "id": 563,
        "category_id": 74,
        "category": {"id": 74, "name": "Ekonomi"},
        "name": "Poling poling",
        "question": "Poling poling?",
        "description": "Poling poling",
        "excerpt": "Poling poling",
        "kabkota_id": null,
        "kabkota": null,
        "kec_id": null,
        "kecamatan": null,
        "kel_id": null,
        "kelurahan": null,
        "rw": null,
        "answers": [
          {
            "id": 1128,
            "body": "Ya",
            "created_at": 1571997511,
            "updated_at": 1571997511
          },
          {
            "id": 1129,
            "body": "Tidak",
            "created_at": 1571997511,
            "updated_at": 1571997511
          }
        ],
        "start_date": "2019-10-25",
        "end_date": "2019-10-26",
        "votes_count": 0,
        "meta": null,
        "status": 10,
        "status_label": "Dipublikasikan",
        "created_at": 1571997510,
        "updated_at": 1571997510
      };

      final respon = pollingFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 563);
      expect(mock['name'], 'Poling poling');
    });

    test('is correctly generated from PollingModel Array', () {
      Answer answers1 = Answer(
          id: 1126, body: 'ya', createdAt: 1571994782, updatedAt: 1571994782);
      Answer answers2 = Answer(
          id: 1127,
          body: 'tidak',
          createdAt: 1571994782,
          updatedAt: 1571994782);

      List<Answer> answers = List<Answer>();
      answers.add(answers1);
      answers.add(answers2);

      PollingModel data1 = PollingModel(
          id: 563,
          categoryId: 74,
          category: Category(id: 1, name: 'category'),
          name: "Poling poling",
          question: "Poling poling?",
          description: "Poling poling",
          excerpt: "Poling poling",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          answers: answers,
          startDate: DateTime.now(),
          endDate: DateTime.now(),
          votesCount: 0,
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          createdAt: 1571997510,
          updatedAt: 1571997510);

      PollingModel data2 = PollingModel(
          id: 563,
          categoryId: 74,
          category: Category(id: 1, name: 'category'),
          name: "Poling",
          question: "Poling poling?",
          description: "Poling poling",
          excerpt: "Poling poling",
          kabkotaId: null,
          kabkota: null,
          kecId: null,
          kecamatan: null,
          kelId: null,
          kelurahan: null,
          rw: null,
          answers: answers,
          startDate: DateTime.now(),
          endDate: DateTime.now(),
          votesCount: 0,
          meta: null,
          status: 10,
          statusLabel: "Dipublikasikan",
          createdAt: 1571997510,
          updatedAt: 1571997510);

      List<PollingModel> mock = List<PollingModel>();
      mock.add(data1);
      mock.add(data2);

      List<PollingModel> respon = listPollingFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, 563);
      expect(respon[0].name, 'Poling poling');
      expect(respon[1].id, 563);
      expect(respon[1].name, 'Poling');
    });
  });
}
