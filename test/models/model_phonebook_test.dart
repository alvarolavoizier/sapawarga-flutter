import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';

main() {
  group('Phonebook', () {
    test('is correctly generated from Phonebookmodel', () {

      Category category = Category(
        id: 1,
        type: CategoryType.PHONEBOOK,
        name: Name.LAYANAN,
        meta: null,
        status: 0,
        statusLabel: StatusLabel.AKTIF,
        createdAt: 0,
        updatedAt: 0
      );

      PhoneBookModel phoneBookModel = PhoneBookModel(
          id: 1,
          name: 'test',
          address: 'test',
          description: 'test',
          seq: 0,
          kabkotaId: 1,
          kecId: 1,
          kelId: 1,
          latitude: '0',
          longitude: '0',
          coverImagePath: '',
          phoneNumbers: null);

      Map<String, Object> mock = {
        'id': 1,
        'name': 'test',
        'address': 'test',
        'description': 'test',
        'seq': 0,
        'kabkota_id': 1,
        'kec_id': 1,
        'kel_id': 1,
        'latitude': '0',
        'longitude': '0',
        "cover_image_path": '',
        'cover_image_url': '',
        'phone_numbers': [
          {
            'type': 'phone',
            'phone_number': '081231281238123',
          },
        ],
        "category": {
          "id": 8,
          "type": "phonebook",
          "name": "Layanan",
          "meta": null,
          "status": 10,
          "status_label": "Aktif",
          "created_at": 1557803314,
          "updated_at": 1557803314,
        },
        "category_id": 8
      };

      var data = [
        {
          "pid": 1,
          "id": 32002,
          "name": "SatpolPP",
          "address": "SatpolPP",
          "description": "Trantibum",
          "seq": 1000,
          "kabkota_id": null,
          "kec_id": null,
          "kel_id": null,
          "latitude": null,
          "longitude": null,
          "cover_image_path": null,
          "cover_image_url": null,
          "phone_numbers":
              "[{\"type\":\"phone\",\"phone_number\":\"026554321\"}]",
          "category":
              "{\"id\":\"8\",\"type\":\"phonebook\",\"name\":\"Layanan\",\"meta\":\"null\",\"status\":\"10\",\"status_label\":\"Aktif\",\"created_at\":\"1557803314\",\"updated_at\":\"1557803314\"}",
          "category_id": 8
        }
      ];

      List<PhoneBookModel> list = data.isNotEmpty
          ? data.map((c) => PhoneBookModel.fromDatabaseMap(c)).toList()
          : [];

      PhoneBookModel.fromMap(mock);
      categoryFromJson(jsonEncode(mock));
      phoneBookModel.toJson();
      category.toJson();

      assert(phoneBookModel != null);
      expect(phoneBookModel.id, 1);
      expect(phoneBookModel.name, 'test');
      expect(list[0].name, 'SatpolPP');
    });
  });
}
