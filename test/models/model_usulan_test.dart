import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/UsulanModel.dart';

main() {
  group('Usulan', () {
    test('is correctly generated from Attachment Object', () {
      Map<String, Object> mock = {
        "type": 'test',
        "path": 'testtt',
        "url": 'testttt',
      };

      final respon = attachmentFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['type'], 'test');
      expect(mock['path'], 'testtt');
    });

    test('is correctly generated from Usulan Object', () {
      Map<String, Object> mock = {
        "id": 866,
        "author_id": 25,
        "author": {
          "id": 25,
          "name": "risa",
          "photo_url":
              "general/1571028191-JTXe4WoqkVcu09vzCGHd0v1QJRZLcKPQ.jpg",
          "photo_url_full":
              "https://d2o6nohz2yihhc.cloudfront.net/general/1571028191-JTXe4WoqkVcu09vzCGHd0v1QJRZLcKPQ.jpg",
          "role_label": "RW",
          "email": "risaa@gmail.com",
          "phone": "0812812122332",
          "address": "aaaa"
        },
        "category_id": 33,
        "category": {"id": 33, "name": "Keamanan"},
        "title": "aku boleh usul gak kaka",
        "description": "aku boleh usul gak kaka ?",
        "kabkota_id": 23,
        "kabkota": {"id": 23, "name": "KOTA BEKASI"},
        "kec_id": 449,
        "kecamatan": {"id": 449, "name": "PONDOKMELATI"},
        "kel_id": 6197,
        "kelurahan": {"id": 6197, "name": "JATIWARNA"},
        "likes_count": 0,
        "likes_users": [],
        "rw": null,
        "meta": null,
        "status": 10,
        "status_label": "Dipublikasikan",
        "approval_note": "bagus sekali dede",
        "attachments": null,
        "created_at": 1571995957,
        "updated_at": 1571995977,
        "approved_at": 1571995977,
        "submitted_at": 1571995957,
        "last_revised_at": null
      };

      final respon = usulanDetailModelFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 866);
      expect(mock['author_id'], 25);
    });

    test('is correctly generated from UsulanModel Array', () {
      Author author = Author(
          id: 35,
          name: 'User Kota Bandung',
          photoUrl: 'Pengguna',
          roleLabel: RoleLabel.PENGGUNA,
          email: 'user.bandung@demo.com',
          phone: '0857123456',
          photoUrlFull: '',
          address: 'Jl. Alamat Kota Bandung');

      Category category = Category(id: 1, name: 'Kesehatan');

      Category kabkota = Category(id: 22, name: 'KOTA BANDUNG');

      Category kecamatan = Category(id: 446, name: 'BANDUNG WETAN');

      Category kelurahan = Category(id: 6082, name: 'CICAHEUM');

      Category likeUser = Category(id: 123, name: 'angga');

      List<Category> likeUsersList = List<Category>();
      likeUsersList.add(likeUser);

      Attachment attachmentData = Attachment(type: 'photo', path: '', url: '');

      List<Attachment> listAttachment = [];
      listAttachment.add(attachmentData);

      UsulanModel data1 = UsulanModel(
          id: 13,
          authorId: 35,
          author: author,
          categoryId: 1,
          category: category,
          title: 'Listrik sering padam di kampung digital Ciletuh',
          description: 'mohon segera ditindak karena padam hampir seharian',
          kabkotaId: 22,
          kabkota: kabkota,
          kecId: 446,
          kecamatan: kecamatan,
          kelId: 6082,
          kelurahan: kelurahan,
          likesCount: 0,
          likesUsers: likeUsersList,
          rw: null,
          meta: null,
          status: 0,
          statusLabel: 'Draft',
          approvalNote: '',
          attachments: listAttachment,
          createdAt: 1570612013,
          updatedAt: 1570612013,
          approvedAt: 1570612013,
          submittedAt: 1570612013,
          lastRevisedAt: 1570612013);

      UsulanModel data2 = UsulanModel(
          id: 12,
          authorId: 35,
          author: author,
          categoryId: 1,
          category: category,
          title: 'Usulan mengundang grup sholawat ke Wonogiri',
          description: 'tes test test',
          kabkotaId: 22,
          kabkota: kabkota,
          kecId: 446,
          kecamatan: kecamatan,
          kelId: 6082,
          kelurahan: kelurahan,
          likesCount: 0,
          likesUsers: likeUsersList,
          rw: null,
          meta: null,
          status: 0,
          statusLabel: 'Draft',
          approvalNote: '',
          attachments: listAttachment,
          createdAt: 1570612013,
          updatedAt: 1570612013,
          approvedAt: 1570612013,
          submittedAt: 1570612013,
          lastRevisedAt: 1570612013);

      List<UsulanModel> mock = List<UsulanModel>();
      mock.add(data1);
      mock.add(data2);

      List<UsulanModel> respon = usulanModelFromJson(jsonEncode(mock));

      expect(respon.length, 2);
      expect(respon[0].id, 13);
      expect(respon[0].authorId, 35);
      expect(respon[0].categoryId, 1);
      expect(
          respon[0].title, 'Listrik sering padam di kampung digital Ciletuh');
      expect(respon[0].description,
          'mohon segera ditindak karena padam hampir seharian');
      expect(respon[0].kabkotaId, 22);
      expect(respon[0].kecId, 446);
      expect(respon[0].kelId, 6082);
      expect(respon[0].likesCount, 0);
      expect(respon[0].rw, null);
      expect(respon[0].meta, null);
      expect(respon[0].status, 0);
      expect(respon[0].approvalNote, '');
      expect(respon[0].createdAt, 1570612013);
      expect(respon[0].updatedAt, 1570612013);
      expect(respon[1].id, 12);
      expect(respon[1].authorId, 35);
      expect(respon[1].categoryId, 1);
      expect(respon[1].title, 'Usulan mengundang grup sholawat ke Wonogiri');
      expect(respon[1].description, 'tes test test');
      expect(respon[1].kabkotaId, 22);
      expect(respon[1].kecId, 446);
      expect(respon[1].kelId, 6082);
      expect(respon[1].likesCount, 0);
      expect(respon[1].rw, null);
      expect(respon[1].meta, null);
      expect(respon[1].status, 0);
      expect(respon[1].approvalNote, '');
      expect(respon[1].createdAt, 1570612013);
      expect(respon[1].updatedAt, 1570612013);
    });
  });
}
