# Sapawarga Flutter

## Setup Development

### Requirements

Flutter (Channel stable, v1.5.4). Installation guide: https://flutter.dev/docs/get-started/install/linux

Check your installation status using `flutter doctor` command.

```bash
$ flutter doctor

Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel stable, v1.5.4-hotfix.2, on Mac OS X 10.14.5 18F132, locale en-ID)

[✓] Android toolchain - develop for Android devices (Android SDK version 29.0.0)
[✓] iOS toolchain - develop for iOS devices (Xcode 10.2.1)
[✓] Android Studio (version 3.4)
[✓] VS Code (version 1.35.1)
[✓] Connected device (1 available)

No issues found!
```

### Install Dependency

```bash
flutter pub get
```

### Run DEBUG Mode

```bash
flutter run --flavor staging -t lib/MainStaging.dart
```

```bash
flutter run --flavor prod -t lib/Main.dart
```

### Run PROFILE Mode

```bash
flutter run --profile --flavor staging -t lib/MainStaging.dart
```

```bash
flutter run --profile --flavor prod -t lib/Main.dart
```

### Run RELEASE Mode

```bash
flutter run --release --flavor staging -t lib/MainStaging.dart
```

```bash
flutter run --release --flavor prod -t lib/Main.dart
```

### Build APK

### Production

```bash
flutter build apk --release --flavor prod -t lib/Main.dart
```

### Staging

```bash
flutter build apk --release --flavor prod -t lib/MainStaging.dart
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
