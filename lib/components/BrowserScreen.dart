import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';

class BrowserScreen extends StatefulWidget {
  final String url;
  final bool useInAppWebView;

  BrowserScreen({Key key, @required this.url, this.useInAppWebView = true});

  @override
  _BrowserScreenState createState() => _BrowserScreenState();
}

class _BrowserScreenState extends State<BrowserScreen> {
  WebViewController _webViewController;
  InAppWebViewController _inAppWebViewController;

  double progress = 0.0;
  bool showLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _exitWebView,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Sapawarga'),
        ),
        body: Column(
          children: <Widget>[
            widget.useInAppWebView && (progress != 1.0)
                ? LinearProgressIndicator(value: progress)
                : Container(),
            Expanded(
              child: Container(
                child: widget.useInAppWebView == false
                    ? _buildWebView()
                    : _buildInAppWebView(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildWebView() {
    return Stack(children: <Widget>[
      WebView(
          initialUrl: widget.url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController controller) {
            _webViewController = controller;
            setState(() {
              showLoading = true;
            });
          },
          onPageFinished: (url) {
            setState(() {
              showLoading = false;
            });
          }),
      showLoading ? Center(child: CircularProgressIndicator()) : Container()
    ]);
  }

  InAppWebView _buildInAppWebView() {
    return InAppWebView(
      initialUrl: widget.url,
      initialOptions: InAppWebViewWidgetOptions(
        inAppWebViewOptions: InAppWebViewOptions(
          debuggingEnabled: true,
        ),
      ),
      onWebViewCreated: (InAppWebViewController controller) {
        _inAppWebViewController = controller;
      },
      onProgressChanged: (InAppWebViewController controller, int progress) {
        setState(() {
          this.progress = progress / 100;
        });
      },
    );
  }

  Future<bool> _exitWebView() async {
    if (widget.useInAppWebView) {
      if (await _inAppWebViewController.canGoBack()) {
        await _inAppWebViewController.goBack();
        return Future.value(false);
      } else {
        return Future.value(true);
      }
    } else {
      if (await _webViewController.canGoBack()) {
        await _webViewController.goBack();
        return Future.value(false);
      } else {
        return Future.value(true);
      }
    }
  }
}
