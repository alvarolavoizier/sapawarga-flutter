import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;

openChromeSafariBrowser({String url}) async {
  await FlutterWebBrowser.openWebPage(
    url: url,
      androidToolbarColor: clr.Colors.blue
  );
}
