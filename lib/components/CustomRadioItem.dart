import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/RadioModel.dart';

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  RadioItem(this._item);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            height: 30.0,
            child: Center(
              child: Text(_item.buttonText,
                  style: TextStyle(
                    fontFamily: FontsFamily.sourceSansPro,
                    fontSize: 14.0,
                    color: _item.isSelected ? Colors.white : Colors.white,
                  )),
            ),
            decoration: BoxDecoration(
              color: _item.isSelected ? clr.Colors.blue : Colors.grey,
              border: Border.all(
                  width: 1.0,
                  color: _item.isSelected ? clr.Colors.blue : Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
          ),
          _item.text != null
              ? Container(
                  margin: EdgeInsets.only(left: 10.0),
                  child: Text(_item.text),
                )
              : Container()
        ],
      ),
    );
  }
}
