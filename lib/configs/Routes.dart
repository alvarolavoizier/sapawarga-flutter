import 'package:flutter/material.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/models/NewsDetailArgumentsModel.dart';
import 'package:sapawarga/screens/administrasi/ListAdministrasiScreen.dart';
import 'package:sapawarga/screens/changePassword/ChangePasswordScreen.dart';
import 'package:sapawarga/screens/counterHoax/CounterHoaxDetailScreen.dart';
import 'package:sapawarga/screens/counterHoax/CounterHoaxListScreen.dart';
import 'package:sapawarga/screens/esamsat/EsamsatListScreen.dart';
import 'package:sapawarga/screens/forgotPassword/ForgotPasswordScreen.dart';
import 'package:sapawarga/screens/importantInformation/ImportantInfoListScreen.dart';
import 'package:sapawarga/screens/main/account/submenuprofile/SubAdressScreen.dart';
import 'package:sapawarga/screens/main/account/submenuprofile/SubContactScreen.dart';
import 'package:sapawarga/screens/main/account/submenuprofile/SubProfileScreen.dart';
import 'package:sapawarga/screens/mission/MissionScreen.dart';
import 'package:sapawarga/screens/news/NewsDetailScreen.dart';
import 'package:sapawarga/screens/news/NewsListIndexScreen.dart';
import 'package:sapawarga/screens/notification/NotificationListScreen.dart';
import 'package:sapawarga/screens/phonebook/PhoneBookListScreen.dart';
import 'package:sapawarga/screens/polling/PollingDetailScreen.dart';
import 'package:sapawarga/screens/polling/PollingListScreen.dart';
import 'package:sapawarga/screens/qnaGovernor/QnAGovernorListScreen.dart';
import 'package:sapawarga/screens/report/ReportScreen.dart';
import 'package:sapawarga/screens/rwActivities/RWActivityScreen.dart';
import 'package:sapawarga/screens/service/ServiceScreen.dart';
import 'package:sapawarga/screens/survey/SurveyListScreen.dart';
import 'package:sapawarga/screens/usulan/UsulanScreen.dart';
import 'package:sapawarga/screens/broadcast/BroadcastDetailScreen.dart';

Route generateRoutes(RouteSettings settings) {
  // getting arguments passed
  final args = settings.arguments;

  switch (settings.name) {
    case NavigationConstrants.ForgotPassword:
      return buildRoute(settings, ForgotPasswordScreen());
    case NavigationConstrants.Service:
      return buildRoute(settings, ServiceScreen());
    case NavigationConstrants.Survey:
      return buildRoute(settings, SurveyListScreen());
    case NavigationConstrants.Polling:
      return buildRoute(settings, PollingListScreen());
    case NavigationConstrants.PollingDetail:
      return buildRoute(
          settings,
          PollingDetailScreen(
            record: args,
          ));
    case NavigationConstrants.Phonebook:
      return buildRoute(settings, PhoneBookListScreen());
    case NavigationConstrants.Esamsat:
      return buildRoute(settings, EsamsatListScreen());
    case NavigationConstrants.importanInformation:
      return buildRoute(settings, ImportantInfoListScreen());
    case NavigationConstrants.RWActivity:
      return buildRoute(settings, RWActivityScreen());
    case NavigationConstrants.QnAGovernor:
      return buildRoute(settings, QnAGovernorListScreen());
    case NavigationConstrants.Aspirasi:
      return buildRoute(settings, UsulanScreen());
    case NavigationConstrants.infoPKB:
      return buildRoute(
          settings,
          BrowserScreen(
            url: UrlThirdParty.urlInfoPKB,
            useInAppWebView: false,
          ));
    case NavigationConstrants.Lapor:
      return buildRoute(settings, ReportScreen());
    case NavigationConstrants.SaberHoax:
      return buildRoute(settings, CounterHoaxListScreen());
    case NavigationConstrants.SaberHoaxDetail:
      return buildRoute(settings, CounterHoaxDetailScreen(record: args));
    case NavigationConstrants.Pikobar:
      return buildRoute(
          settings,
          BrowserScreen(
            url: UrlThirdParty.urlPikobar,
          ));
    case NavigationConstrants.Browser:
      return buildRoute(
          settings,
          BrowserScreen(
            url: args,
          ));
    case NavigationConstrants.ChangePassword:
      return buildRoute(settings, ChangePasswordScreen(type: args));
    case NavigationConstrants.Mission:
      return buildRoute(settings, MissionScreen());
    case NavigationConstrants.BroadcastDetail:
      return buildRoute(
          settings,
          BroadcastDetailScreen(
            id: args,
          ));
    case NavigationConstrants.NewsIndex:
      return buildRoute(settings, NewsListIndexScreen(isIdKota: args));
    case NavigationConstrants.NewsDetail:
      NewsDetailArgumentsModel argumentsModel = args;
      return buildRoute(
          settings,
          NewsDetailScreen(
              newsId: argumentsModel.id, isIdKota: argumentsModel.isIdKota));
    case NavigationConstrants.NotificationList:
      return buildRoute(settings, NotificationListScreen());
    case NavigationConstrants.ImportantInfoList:
      return buildRoute(settings, ImportantInfoListScreen());
    case NavigationConstrants.AdministrationList:
      return buildRoute(settings, ListAdministrasiScreen());
    case NavigationConstrants.SubProfile:
      return buildRoute(settings, SubProfileScreen());
    case NavigationConstrants.SubContact:
      return buildRoute(settings, SubContactScreen());
      case NavigationConstrants.SubAddress:
      return buildRoute(settings, SubAdressScreen());
    default:
      return null;
  }
}

MaterialPageRoute buildRoute(RouteSettings settings, Widget builder) {
  return MaterialPageRoute(
    settings: settings,
    builder: (BuildContext context) => builder,
  );
}
