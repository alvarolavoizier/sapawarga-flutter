class Analytics {

  /// Don't change the values

  // User Properties
  static String USER_ID = 'id_user';
  static String USERNAME = 'username';
  static String NAME = 'name';
  static String ROLE = 'role';
  static String RT = 'rt';
  static String RW = 'rw';
  static String KELURAHAN = 'kelurahan';
  static String KECAMATAN = 'kecamatan';
  static String KABKOTA = 'kab_kota';

  // Screen Names
  static String LOGIN = 'Login';
  static String FORGOT_PASSWORD = 'Forgot Password';
  static String CHANGE_PASSWORD = 'Change Password';
  static String HOME = 'Home';
  static String MESSAGES = 'Message';
  static String HELP = 'Help';
  static String ACCOUNT = 'Account';
  static String PROPOSAL = 'Usulan';
  static String SURVEY = 'Survey';
  static String POLLING = 'Polling';
  static String PHONEBOOK = 'Phonebook';
  static String ESAMSAT = 'E-Samsat';
  static String ADMINISTRATION = 'Administration';
  static String NEWS = 'News';
  static String NOTIFICATIONS = 'Notification';
  static String RW_ACTIVITIES = 'Kegiatan RW';
  static String QNA = 'Tanya Jawab';
  static String IMPORTANT_INFO = 'Info Penting';
  static String SABER_HOAX = 'Jabar Saber Hoax';
  static String INFO_PKB = 'Info PKB';
  static String PIKOBAR = 'Info Corona';

  // Event OnBoarding
  static String EVENT_OPEN_WA_ADMIN_ONBOARDING = 'onboarding_open_wa_admin';
  static String EVENT_OPEN_TELP_ADMIN_ONBOARDING = 'onboarding_open_telp_admin';

  // Event Login
  static String EVENT_VIEW_LOGIN = 'login_view';
  static String EVENT_TAPPED_BUTTON_LOGIN = 'login_tapped_button_login';
  static String EVENT_TAPPED_NOT_REGISTER_LOGIN = 'login_tapped_not_registered';
  static String EVENT_VIEW_TERMS_OF_SERVICE_LOGIN = 'login_view_term_of_service';
  static String EVENT_VIEW_PRIVACY_POLICY_LOGIN = 'login_view_privacy_policy';
  static String EVENT_DOWNLOAD_GUIDEBOOK_LOGIN = 'login_download_guidebook';
  static String EVENT_OPEN_WA_ADMIN_LOGIN = 'login_open_wa_admin';
  static String EVENT_OPEN_TELP_ADMIN_LOGIN = 'login_open_telp_admin';
  static String EVENT_FAILED_LOGIN = 'login_failed';
  static String EVENT_SUCCESS_LOGIN = 'login_success';

  // Event Forgot Password
  static String EVENT_VIEW_FORGOT_PASSWORD = 'forgot_password_view';
  static String EVENT_TAPPED_BUTTON_NEXT_FORGOT_PASSWORD = 'forgot_password_tapped_button_next';
  static String EVENT_TAPPED_FORGOT_EMAIL_FORGOT_PASSWORD = 'forgot_password_tapped_forgot_email';
  static String EVENT_OPEN_WA_ADMIN_FORGOT_PASSWORD = 'forgot_password_open_wa_admin';
  static String EVENT_OPEN_TELP_ADMIN_FORGOT_PASSWORD = 'forgot_password_telp_admin';
  static String EVENT_FAILED_FORGOT_PASSWORD = 'forgot_password_failed';
  static String EVENT_SUCCESS_FORGOT_PASSWORD = 'forgot_password_success';

  // Event Change Password
  static String EVENT_VIEW_CHANGE_PASSWORD = 'change_password_view';
  static String EVENT_OPEN_WA_ADMIN_CHANGE_PASSWORD = 'change_password_open_wa_admin';
  static String EVENT_OPEN_TELP_ADMIN_CHANGE_PASSWORD = 'change_password_telp_admin';
  static String EVENT_FAILED_CHANGE_PASSWORD = 'change_password_failed';
  static String EVENT_SUCCESS_CHANGE_PASSWORD = 'change_password_success';

  // Event Polling
  static String EVENT_VIEW_LIST_POLLING = 'polling_view_list';
  static String EVENT_VIEW_DETAIL_POLLING = 'polling_view_detail';
  static String EVENT_ANSWER_POLLING = 'polling_answer';

  // Event Usulan
  static String EVENT_VIEW_LIST_GENERAL_USULAN = 'usulan_view_list_general';
  static String EVENT_VIEW_LIST_MY_USULAN = 'usulan_view_list_me';
  static String EVENT_VIEW_DETAIL_USULAN = 'usulan_view_detail';
  static String EVENT_VIEW_FORM_USULAN = 'usulan_view_form';
  static String EVENT_TAPPED_CATEGORY_USULAN = 'usulan_tapped_category';
  static String EVENT_CREATE_USULAN = 'usulan_create';
  static String EVENT_CREATE_USULAN_DRAFT = 'usulan_create_to_draft';
  static String EVENT_EDIT_USULAN = 'usulan_edit';
  static String EVENT_LIKE_USULAN = 'usulan_like';
  static String EVENT_UNLIKE_USULAN = 'usulan_unlike';

  // Event Survey
  static String EVENT_VIEW_LIST_SURVEY = 'survey_view_list';
  static String EVENT_DETAIL_SURVEY = 'survey_view_detail';

  // Event QNA
  static String EVENT_VIEW_LIST_QNA = 'qna_view_list_general';
  static String EVENT_VIEW_DETAIL_QNA = 'qna_view_detail';
  static String EVENT_VIEW_FORM_QNA = 'qna_view_form';
  static String EVENT_CREATE_QNA = 'qna_create';
  static String EVENT_LIKE_QNA = 'qna_like';
  static String EVENT_UNLIKE_QNA = 'qna_unlike';

  // Event News
  static String EVENT_VIEW_LIST_JABAR_NEWS = 'news_view_list_jabar';
  static String EVENT_VIEW_KABKOTA_NEWS = 'news_view_list_kabkota';
  static String EVENT_VIEW_DETAIL_NEWS = 'news_view_detail';
  static String EVENT_SHARE_NEWS = 'news_share';
  static String EVENT_VIEW_ORIGINAL_ARTICLE_NEWS = 'news_view_original_article';
  static String EVENT_LIKE_NEWS = 'news_like';
  static String EVENT_UNLIKE_NEWS = 'news_unlike';

  // Event Post RW
  static String EVENT_VIEW_LIST_GENERAL_KEGIATAN_RW =
      'kegiatan_rw_view_list_general';
  static String EVENT_VIEW_LIST_MY_KEGIATAN_RW = 'kegiatan_rw_view_list_me';
  static String EVENT_VIEW_DETAIL_KEGIATAN_RW = 'kegiatan_rw_view_detail';
  static String EVENT_CREATE_KEGIATAN_RW = 'kegiatan_rw_create';
  static String EVENT_COMMENT_KEGIATAN_RW = 'kegiatan_rw_comment';
  static String EVENT_LIKE_KEGIATAN_RW = 'kegiatan_rw_like';
  static String EVENT_UNLIKE_KEGIATAN_RW = 'kegiatan_rw_unlike';
  static String EVENT_SEARCH_KEGIATAN_RW = 'kegiatan_rw_search';
  static String EVENT_SHARE_KEGIATAN_RW = 'kegiatan_rw_share';

  // Event Saber Hoax
  static String EVENT_VIEW_LIST_SABER_HOAX = 'saber_hoax_view_list';
  static String EVENT_DETAIL_SABER_HOAX = 'saber_hoax_view_detail';
  static String EVENT_OPEN_WA_SABER_HOAX = 'saber_hoax_open_wa';

  // Event Broadcast
  static String EVENT_VIEW_LIST_BROADCAST = 'broadcast_view_list';
  static String EVENT_DETAIL_BROADCAST = 'broadcast_view_detail';

  // Event Help
  static String EVENT_VIEW_LIST_HELP = 'help_view_list';
  static String EVENT_OPEN_WA_ADMIN_HELP = 'help_open_wa_admin';
  static String EVENT_OPEN_TELP_ADMIN_HELP = 'help_open_telp_admin';
  static String EVENT_OPEN_EMAIL_ADMIN_HELP = 'help_open_email_admin';
  static String EVENT_OPEN_GUIDELINES_HELP = 'help_open_community_guidelines';

  // Event Account
  static String EVENT_DETAIL_ACCOUNT = 'account_view_detail';
  static String EVENT_EDIT_ACCOUNT = 'account_edit';

  // Event Humas Jabar
  static String EVENT_VIEW_LIST_HUMAS = 'humas_jabar_view_list';
  static String EVENT_VIEW_DETAIL_HUMAS = 'humas_jabar_detail';

  // Event Video Post
  static String EVENT_VIEW_DETAIL_JABAR_VIDEO_POST = 'video_post_detail_jabar';
  static String EVENT_VIEW_DETAIL_KABKOTA_VIDEO_POST =
      'video_post_detail_kabkota';

  // Event Notification
  static String EVENT_VIEW_LIST_NOTIFICATION = 'notification_view_list';
  static String EVENT_VIEW_DETAIL_NOTIFICATION = 'notification_view_detail';

  // Event Information Popup
  static String EVENT_VIEW_INFORMATION_POPUP = 'information_popup_view';
  static String EVENT_VIEW_DETAIL_INFORMATION_POPUP =
      'information_popup_view_detail';

  // Event Home
  static String EVENT_VIEW_DETAIL_BANNER = 'home_banner_view_detail';
  static String EVENT_TAPPED_BANNER = 'home_banner_';

  // Event Important Info
  static String EVENT_VIEW_LIST_IMPORTANT_INFO =
      'important_information_view_list';
  static String EVENT_VIEW_DETAIL_IMPORTANT_INFO = 'important_information_view_detail';
  static String EVENT_SHARE_IMPORTANT_INFO = 'important_information_share';
  static String EVENT_DOWNLOAD_ATTACHMENT_IMPORTANT_INFO = 'important_information_download_attach';
  static String EVENT_SEARCH_IMPORTANT_INFO = 'important_information_search';
  static String EVENT_FILTER_IMPORTANT_INFO = 'important_information_filter';
  static String EVENT_LIKE_IMPORTANT_INFO = 'important_information_like';
  static String EVENT_UNLIKE_IMPORTANT_INFO = 'important_information_unlike';
  static String EVENT_COMMENT_IMPORTANT_INFO = 'important_info_comment';

  // Event Phonebook
  static String EVENT_VIEW_LIST_PHONEBOOK = 'phonebook_view_list';
  static String EVENT_VIEW_DETAIL_PHONEBOOK = 'phonebook_view_detail';
  static String EVENT_TAPPED_MAP_PHONEBOOK = 'phonebook_tapped_map';
  static String EVENT_OPEN_TELP_PHONEBOOK = 'phonebook_open_telp';
  static String EVENT_OPEN_SMS_PHONEBOOK = 'phonebook_open_sms';

  // Event Info PKB
  static String EVENT_VIEW_INFO_PKB = 'info_pkb_view';

  // Event Info Corona
  static String EVENT_VIEW_INFO_CORONA = 'info_corona_view';

  // Event Announcement
  static String EVENT_VIEW_ANNOUNCEMENT = 'announcement_view';

  // Event Highlight Menu
  static String EVENT_VIEW_HIGHLIGHT_MENU = 'highlight_menu_view';
}
