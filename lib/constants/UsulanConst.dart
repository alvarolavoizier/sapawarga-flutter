import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/CategoryModel.dart';

class UsulanConst {
  static String getStringLabel(String label) {
    switch (label) {
      case Dictionary.dipublikasikan:
        return 'tag-green.png';
      case Dictionary.terkirim:
        return "tag-purple.png";
      case Dictionary.ditolak:
        return "tag-red.png";
      case Dictionary.draft:
        return "tag-orange.png";
      default:
        return "tag-orange.png";
    }
  }

  static int getDatePublish(int submitAt, int approvalAt, String label) {
    if (submitAt == null) {
      submitAt = 0;
    }

    if (approvalAt == null) {
      approvalAt = 0;
    }
    switch (label) {
      case Dictionary.dipublikasikan:
        return approvalAt;
      case Dictionary.terkirim:
        return submitAt;
      case Dictionary.ditolak:
        return approvalAt;
      case Dictionary.draft:
        return 0;
      default:
        return 0;
    }
  }

  static bool isLikeUsulan(List<Category> listLike, String myUsername) {
    for(int i=0;i<listLike.length;i++){
    }
    bool returnData;
    if (listLike.isNotEmpty) {
      if (listLike.length > 1) {
        returnData = false;
        listLike.forEach((like) {
          if (like.name == myUsername) {
            returnData = true;
          }
        });
      } else {
        if (listLike[0].name == myUsername) {
          returnData =  true;
        } else {
          returnData =  false;
        }
      }
    } else {
      returnData =  false;
    }

    return returnData;
  }

  static MaterialColor getColor(String label) {
    switch (label) {
      case Dictionary.dipublikasikan:
        return Colors.green;
      case Dictionary.terkirim:
        return Colors.deepPurple;
      case Dictionary.ditolak:
        return Colors.red;
      case Dictionary.draft:
        return Colors.orange;
      default:
        return Colors.orange;
    }
  }
}
