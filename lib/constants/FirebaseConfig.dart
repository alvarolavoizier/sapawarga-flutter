class FirebaseConfig {
  /// Firebase remote config parameter keys
  static const String highlightMenuKey = 'highlight_menu';
  static const String announcementKey = 'announcement';
  static const String helpContentKey = 'help_content';
  static const String callCenterNumbersKey = 'call_center_numbers';


  /// Firebase remote config default value
  static const String highlightMenuValue = '{"feature_name": "Verifikasi Bansos",'
      '"enabled":false,'
      '"cover-image":"https://firebasestorage.googleapis.com/v0/b/sapawarga-app.appspot.com/o/verifikasi-bantuan.png?alt=media&token=d0351b64-412c-4171-b12c-3163db365d64",'
      '"url":"https://quizzical-wing-910b8e.netlify.com/",'
      '"mode":"webview"}';
  
  static const String announcementValue = '{"name": "Bantuan Sosial","enabled":false,"title":"Info","content":"Telah tersedia versi terbaru aplikasi PIKOBAR. Silahkan unduh atau update melalui Play Store pada perangkat Anda.","action_url":"https://play.google.com/store/apps/details?id=id.go.jabarprov.pikobar"}';

  static const String callCenterNumbersValue = '["081212124023","081312227212","081312227214","081312227215"]';
}