import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/VideoModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class VideoRepository {
  Future<List<VideoModel>> fetchRecords({String kokabId = ''}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    String param =
        kokabId != null && kokabId.isNotEmpty ? 'kabkota_id=$kokabId&' : '';

    final response = await http
        .get('${EndPointPath.videos}?${param}limit=5',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      final list = listVideoFromJson(jsonEncode(data));

      if (kokabId != null && kokabId.isNotEmpty) {
        await Preferences.setVideoListKokab(listVideoToJson(list));
      } else {
        await Preferences.setVideoListJabar(listVideoToJson(list));
      }

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<List<VideoModel>> getVideosJabarLocal() async {
    final jsonData = await Preferences.getVideoListJabar();

    return listVideoFromJson(jsonData);
  }

  Future<bool> hasVideosJabarLocal() async {
    bool hasRecords = await Preferences.hasVideoListJabar();
    return hasRecords;
  }

  Future<void> deleteVideosJabarLocal() async {
    await Preferences.deleteVideoListJabar();
  }

  Future<List<VideoModel>> getVideosKokabLocal() async {
    final jsonData = await Preferences.getVideoListKokab();

    return listVideoFromJson(jsonData);
  }

  Future<bool> hasVideosKokabLocal() async {
    bool hasRecords = await Preferences.hasVideoListKokab();
    return hasRecords;
  }

  Future<void> deleteVideosKokabLocal() async {
    await Preferences.deleteVideoListKokab();
  }
}
