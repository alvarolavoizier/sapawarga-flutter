import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/UpdateAppModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';

class UpdateAppRepository {
  Future<bool> checkUpdate() async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.getVersion}',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      Map<String, dynamic> data = jsonDecode(response.body);

      UpdateAppModel updateApp = updateAppFromJson(jsonEncode(data));

      int mobileVersion = extractNumber(await getCurrentVersion());
      int serverVersion = extractNumber(updateApp.version);

      return (mobileVersion < serverVersion && updateApp.forceUpdate == true);
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<String> getCurrentVersion() async {
    String version = Dictionary.version;

    await PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      version = packageInfo.version != null
          ? packageInfo.version
          : Dictionary.version;
    });
    return version;
  }

  int extractNumber(String version) {
    return int.parse(version.replaceAll(RegExp("\\D+"),""));
  }
}
