import 'dart:convert';
import 'dart:io';

import 'package:meta/meta.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/enums/AttachmentType.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:sapawarga/models/RWActivityCommentModel.dart';
import 'package:sapawarga/models/RWActivityModel.dart';
import 'package:sapawarga/repositories/AttachmentRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:http/http.dart' as http;

class RWActivityRepository {
  //get data general RW Activities
  Future<RWActivityModel> fetchRecords({int page}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.rwActivities}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data general RW Activities

  Future<ItemRWActivity> getDetailRwActivities({int id}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.rwActivities}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = itemRWActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data my activities
  Future<RWActivityModel> fetchRecordsMyActivities({int page}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.rwMyActivities}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //search data general RW Activities
  Future<RWActivityModel> searchRecord(
      {@required String keyword,
      @required int page,
      bool isMyActivity = false}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get(
            '${isMyActivity ? EndPointPath.rwMyActivities : EndPointPath.rwActivities}?search=$keyword&page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<RWActivityModel> getRwActivityByAreas(
      {@required int kabKotaId, @required int page}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.rwActivities}?kabkota_id=$kabKotaId&page=$page',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //for send status like or not on activities
  Future<bool> sendLike({@required int id}) async {
    String token = await AuthRepository().getToken();

    var response = await http
        .post('${EndPointPath.rwActivities}/likes/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Duration(seconds: 10));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception(response.statusCode);
    }
  }

  //send new Rw activities
  Future<ItemRWActivity> postActivity(
      {@required List<File> image,
      @required String description,
      @required String tag}) async {
    String token = await AuthRepository().getToken();
    List<AddPhotoModel> responseImage = [];
    List attachJson;
    List<Map> dataAttach = [];

    for (int i = 0; i < image.length; i++) {
      responseImage.add(await AttachmentRepository()
          .addPhoto(image[i], AttachmentType.user_post_photo));
    }

    if (responseImage.isNotEmpty) {
      for (int i = 0; i < responseImage.length; i++) {
        Map requestDataPhoto = {'path': responseImage[i].data.path};
        dataAttach.add(requestDataPhoto);
      }
      attachJson = dataAttach;
    }

    Map requestData = {
      'text': description,
//      'image_path': responseImage.data.path,
      'images': attachJson,
      'tags': tag,
      'status': 10,
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post('${EndPointPath.rwActivities}',
            headers: await HttpHeaders.headers(token: token), body: requestBody)
        .timeout(Duration(seconds: 10));

    if (response.statusCode == 201) {
      var data = jsonDecode(response.body)['data'];

      ItemRWActivity resData = itemRWActivityFromJson(jsonEncode(data));

      return resData;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data comment from Activities
  Future<RWActivityCommentModel> fetchCommentRecords(
      {@required int id, int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.rwActivities}/$id/comments?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityCommentModelFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //for send comment on activities
  Future<ItemRwActivityComment> postComment(
      {@required int id, @required String text}) async {
    String token = await AuthRepository().getToken();

    Map requestData = {
      'user_post_id': id,
      'text': text,
      'status': 10,
    };

    var requestBody = json.encode(requestData);

    final response = await http
        .post('${EndPointPath.rwActivities}/$id/comments',
            headers: await HttpHeaders.headers(token: token), body: requestBody)
        .timeout(const Duration(seconds: 10));

    print(response.body);

    if (response.statusCode == 201) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = itemRWActivityCommentFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get category Areas
  Future<AreaModelCategory> getCategoryListArea(String name) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    if (name != null) {
      name = '&name=$name';
    } else {
      name = '';
    }

    final response = await http
        .get('${EndPointPath.categoryAreaList}?parent_id=1&all=true$name',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      var data = areaModelCategoryFromJson(response.body);
      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
