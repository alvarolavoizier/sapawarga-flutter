import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:sapawarga/configs/DBProvider.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/models/HumasJabarModel.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class HumasJabarRepository {
  Client client = Client();
  List<HumasJabarModel> listHumasJabar;
  //get data list humasjabar
  Future<void> fetchHumasJabarList() async {
    final response = await client.get(UrlThirdParty.newsHumasJabar);
    listHumasJabar = List<HumasJabarModel>();

    if (response.statusCode == 200) {
      await truncateLocal();
      Map jsonCodec = json.decode(response.body);
      for (int i = 0; i < jsonCodec.length; i++) {
        var humasjabar =
            HumasJabarModel.fromJson(json.decode(response.body)[i.toString()]);
        listHumasJabar.add(humasjabar);
      }

      listHumasJabar.forEach((humasjabar) async {
        var row = {
          'id': humasjabar.id,
          'postTitle': humasjabar.postTitle,
          'thumbnail': humasjabar.thumbnail,
          'slug': humasjabar.slug,
          'tglPublish': humasjabar.tglPublish,
        };
        await insertDatabase(row);
      });
    } else {
      throw Exception('failed to load post');
    }
  }
  //delete data from database
  Future<void> truncateLocal() async {
    Database db = await DBProvider.db.database;

    await db.rawDelete('Delete from HumasJabar');
  }
  //insert data to database
  Future<void> insertDatabase(Map<String, dynamic> row) async {
    Database db = await DBProvider.db.database;

    await db.insert('HumasJabar', row);
  }
  //get data list humasjabar from list Humasjabar
  Future<HumasJabarModel> getRecord(int id) async {
    Database db = await DBProvider.db.database;

    var res = await db.query('HumasJabar', where: 'id = ?', whereArgs: [id]);

    return res.isNotEmpty ? HumasJabarModel.fromDatabaseMap(res.first) : null;
  }

  Future<List<HumasJabarModel>> getRecordsDataHumasJabar(
      {bool forceRefresh = false}) async {
    // await truncateLocal();

    bool hasLocal = await hasLocalData();
    List<HumasJabarModel> localRecords;

    if (hasLocal == false || forceRefresh == true) {
      try {
        await fetchHumasJabarList();
      } catch (e) {
        localRecords = await getRecordsLocal();
      }
    }

    localRecords = await getRecordsLocal();

    return localRecords;
  }

  Future<List<HumasJabarModel>> getRecordsLocal() async {
    Database db = await DBProvider.db.database;

    var res = await db.rawQuery('SELECT * FROM HumasJabar');

    List<HumasJabarModel> list = res.isNotEmpty
        ? res.map((c) => HumasJabarModel.fromDatabaseMap(c)).toList()
        : [];

    return list;
  }

  Future<bool> hasLocalData() async {
    Database db = await DBProvider.db.database;

    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM HumasJabar'));
    return count > 0;
  }
}
