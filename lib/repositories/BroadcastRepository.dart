import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sapawarga/configs/DBProvider.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/BroadcastModel.dart';
import 'package:sapawarga/models/MessageModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sqflite/sqflite.dart';

class BroadcastRepository {
  //get data list message from server
  Future<void> fetchRecords(int page) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    final response = await http
        .get('${EndPointPath.messages}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      List<MessageModel> list = listMessageFromJson(jsonEncode(data));

      var totalCount =
      jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalCount));

      list.forEach((record) {
        insertToDatabase(record);
      });
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //Request for get detail message
  Future<BroadcastModel> getDetail(int id) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.broadcasts}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body)['data'];

      BroadcastModel record = broadcastFromJson(jsonEncode(data));

      return record;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<bool> deleteRecord(String id) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .delete('${EndPointPath.messages}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      return true;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //Request for delete data list message
  Future<bool> deleteListRecord(List<MessageModel> record) async {
    String token = await AuthRepository().getToken();

    List deleteListJson;
    List<String> dataDelete = [];
    if (record.isNotEmpty) {
      for (int i = 0; i < record.length; i++) {
        dataDelete.add(record[i].id);
      }
      deleteListJson = dataDelete;
    }

    Map requestData = {
      'ids': deleteListJson.toList(),
    };

    var requestBody = json.encode(requestData);

    final response = await http
        .post('${EndPointPath.messagesListDelete}',
            headers: await HttpHeaders.headers(token: token), body: requestBody)
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      return true;
    } else if (response.statusCode == 204) {
      for (int i = 0; i < dataDelete.length; i++) {
       await MultipleDelete(dataDelete[i]);
      }
      return true;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data list message from local db
  Future<List<MessageModel>> getRecords(
      {bool forceRefresh = false, int page = 1}) async {
    bool hasLocal = await hasLocalData();

    if (hasLocal == false || forceRefresh == true) {
      await fetchRecords(page);
    }

    List<MessageModel> localRecords = await getLocalData();

    return localRecords;
  }

  //insert data from server to database
  Future<void> insertToDatabase(MessageModel record) async {
    Database db = await DBProvider.db.database;
    try {
      await db.insert(
        'Messages',
        record.toJson(),
        conflictAlgorithm: ConflictAlgorithm.ignore,
      );
    } catch (e) {
      print(e.toString());
    }
  }

  Future<bool> hasLocalData() async {
    Database db = await DBProvider.db.database;

    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM Messages'));

    return count > 0;
  }

  Future<List<MessageModel>> getLocalData() async {
    Database db = await DBProvider.db.database;

    var res = await db.rawQuery('SELECT * FROM Messages ORDER BY message_id DESC');

    List<MessageModel> list =
        res.isNotEmpty ? res.map((c) => MessageModel.fromJson(c)).toList() : [];

    return list;
  }

  Future<int> hasUnreadData() async {
    Database db = await DBProvider.db.database;

    int count = Sqflite.firstIntValue(await db
        .rawQuery('SELECT COUNT(*) FROM Messages WHERE read_at IS NULL'));

    return count;
  }

  //Update read message
  Future<int> updateReadData(String id) async {
    Database db = await DBProvider.db.database;
    int readAt =
        (DateTime.now().toLocal().millisecondsSinceEpoch / 1000).round();
    return await db.update('Messages', {'read_at': readAt},
        where: 'id = ?', whereArgs: [id]);
  }

  Future<bool> delete(String id) async {
    bool status = await deleteRecord(id);
    if (status) {
      Database db = await DBProvider.db.database;
      return await db.delete('Messages', where: 'id = ?', whereArgs: [id]) > 0;
    } else {
      return false;
    }
  }

  //Delete Data list message
  Future<bool> MultipleDelete(String id) async {
    Database db = await DBProvider.db.database;
    return await db.delete('Messages', where: 'id = ?', whereArgs: [id]) > 0;
  }

  //Clear all data from db
  Future<void> clearLocalData() async {
    Database db = await DBProvider.db.database;

    await db.rawDelete('Delete from Messages');
  }
}
