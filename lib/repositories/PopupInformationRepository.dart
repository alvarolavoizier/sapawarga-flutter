import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:sapawarga/configs/DBProvider.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/PopupInformationLastShownModel.dart';
import 'package:sapawarga/models/PopupInformationModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sqflite/sqflite.dart';

class PopupInformationRepository {
  Future<List<PopupInformationModel>> fetchRecords() async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.popupInformation}',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      final list = listPopupInformationFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<void> setPopupInfoLastShown() async {
    Database db = await DBProvider.db.database;

    UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();

    PopupInformationLastShownModel lastShown = PopupInformationLastShownModel(
        id: userInfo.id, lastShown: DateTime.now());

    try {
      await db.insert('PopupInformation', lastShown.toJson(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<PopupInformationLastShownModel> getPopupInfoLastShown(int id) async {
    Database db = await DBProvider.db.database;

    List<Map> maps =
        await db.query('PopupInformation', where: 'id = ?', whereArgs: [id]);
    if (maps.isNotEmpty) {
      return PopupInformationLastShownModel.fromJson(maps.first);
    }
    return null;
  }

  Future<PopupInformationModel> hasShownPopupInformation() async {
    try {
      List<PopupInformationModel> records = await fetchRecords();

      if (records.isNotEmpty) {
        DateTime now = DateTime.now();
        DateTime endDate = records[0].endDate;

        if (now.isBefore(endDate)) {
          UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();

          PopupInformationLastShownModel popupLastShown =
              await getPopupInfoLastShown(userInfo.id);

          DateTime now = DateTime.now();
          DateTime lastShownDate =
              popupLastShown != null && popupLastShown.lastShown != null
                  ? popupLastShown.lastShown
                  : DateFormat("yyyy-MM-dd HH:mm:ss", "id_ID")
                      .parse("0000-00-00 00:00:00");

          var formatter = DateFormat('yyyy-MM-dd');
          String dateNow = formatter.format(now);
          String dateLastShown = formatter.format(lastShownDate);

          return dateNow == dateLastShown ? null : records[0];
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
