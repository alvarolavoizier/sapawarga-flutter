import 'dart:convert';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga/utilities/SharedPreferences.dart';

class CounterHoaxRepository {
  Future<List<CounterHoaxModel>> fetchRecords(int page) async {
    await Future.delayed(Duration(seconds: 1));

    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.counterHoax}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      var totalCount =
      jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalCount));

      final list = listCounterHoaxFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<CounterHoaxModel> getDetail(int id) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.counterHoax}/$id',
        headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body)['data'];

      CounterHoaxModel record = counterHoaxFromJson(jsonEncode(data));

      return record;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
