import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/SurveyModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class SurveyRepository {

  // get data list survey
  Future<List<SurveyModel>> fetchRecords(int page) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    final response = await http
        .get('${EndPointPath.survey}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      final list = listSurveyFromJson(jsonEncode(data));

      var totalUsulan =
      jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalUsulan));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get url link survey
  Future<String> getUrl(int id) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.survey}/$id',
        headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body)['data'];

      final survey = surveyFromJson(jsonEncode(data));

      return survey.externalUrl;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
