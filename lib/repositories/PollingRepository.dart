import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/PollingModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class PollingRepository {
  //for get data list polling
  Future<List<PollingModel>> fetchRecords(int page) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    final response = await http
        .get('${EndPointPath.polling}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      var totalUsulan =
          jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalUsulan));

      final list = listPollingFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //request for send vote polling
  Future<int> sendVote({@required int pollingId, @required int answer}) async {
    String token = await AuthRepository().getToken();

    Map requestData = {'id': answer};

    var requestBody = json.encode(requestData);

    final response = await http
        .put('${EndPointPath.polling}/$pollingId/vote',
            headers: await HttpHeaders.headers(token: token), body: requestBody)
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200 || response.statusCode == 422) {
      return response.statusCode;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //request for get vote status polling
  Future<bool> getVoteStatus({@required int pollingId}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.polling}/$pollingId/vote',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      return jsonDecode(response.body)['data']['is_voted'];
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //request for get detail polling
  Future<PollingModel> getDetail(int id) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.polling}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body)['data'];

      PollingModel polling = pollingFromJson(jsonEncode(data));

      return polling;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
