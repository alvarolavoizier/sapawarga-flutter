import 'dart:convert';

import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/BannerModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:http/http.dart' as http;

class BannerRepository {
  Future<List<BannerModel>> fetchRecords() async {
    await Future.delayed(Duration(seconds: 1));

    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.banner}?limit=5',
        headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      final list = listBannerFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
