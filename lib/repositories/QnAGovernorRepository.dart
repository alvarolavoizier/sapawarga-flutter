import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/models/QnACommentModel.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class QnAGovernorRepository {
  //get data list Qna
  Future<List<QnAModel>> fetchRecords({int page}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.qnaGovernor}?page=$page&limit=20',
        headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];
      int totalQnA =  jsonDecode(response.body)['data']['_meta']['totalCount'];

      await Preferences.setQnATotalCount(totalQnA);

      var list = listQnAModelFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  // get data to detail QNA
  Future<QnAModel> getDetail({int id}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.qnaGovernor}/$id',
        headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];

      return qnaModelFromJson(jsonEncode(data));
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  // get data comment QNA
  Future<List<QnACommentModel>> fetchComments(
      {int questionId, int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.qnaGovernor}/$questionId/comments',
        headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      var list = listQnACommentModelFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //send QNA
  Future<QnAModel> addQuestion({@required String question}) async {

    String token = await AuthRepository().getToken();

    Map requestData = {
      'text': question,
      'status': 10,
      'is_flagged': 0
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post('${EndPointPath.qnaGovernor}',
        headers: await HttpHeaders.headers(token: token), body: requestBody)
        .timeout(Duration(seconds: 10));

    if (response.statusCode == 201) {
      var data = jsonDecode(response.body)['data'];

      QnAModel resData = qnaModelFromJson(jsonEncode(data));

      QnAModel returnData = QnAModel(
        id: resData.id,
        text: resData.text,
        user: resData.user,
        answerId: resData.answerId,
        answer: resData.answer,
        isLiked: resData.isLiked,
        likesCount: resData.likesCount,
        commentsCount: resData.commentsCount,
        isFlagged: resData.isFlagged,
        statusLabel: resData.statusLabel,
        createdBy: resData.createdBy,
        createdAt: resData.createdAt,
        updatedAt: resData.updatedAt,
        isHighlight: true,
      );

      return returnData;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //send like QNA
  Future<bool> sendLike({@required int id}) async {
    String token = await AuthRepository().getToken();

    var response = await http
        .post('${EndPointPath.qnaGovernor}/likes/$id',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Duration(seconds: 10));

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
