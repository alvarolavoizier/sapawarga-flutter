import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:sapawarga/configs/DBProvider.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/NearByLocationsModel.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sqflite/sqflite.dart';

class PhoneBookRepository {

  //get data list phonebook
  Future<void> fetchRecords(int page, bool refresh) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.phoneBook}?page=$page&limit=20', headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }).timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> records = jsonDecode(response.body)['data']['items'];

      var totalCount =
      jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalCount));

      if (refresh) {
        await truncateLocal();
      }

      records.forEach((record) async {

        var row = {
          'id': record['id'],
          'name': record['name'],
          'address': record['address'],
          'description': record['description'],
          'seq': record['seq'],
          'kabkota_id': record['kabkota_id'],
          'kec_id': record['kec_id'],
          'kel_id': record['kel_id'],
          'latitude': record['latitude'],
          'longitude': record['longitude'],
          'cover_image_path': record['cover_image_url'],
//          'cover_image_url': record['cover_image_url'],
          'phone_numbers': jsonEncode(record['phone_numbers']),
//          'category': jsonEncode(record['category']),
//          'category_id': record['category_id'],
        };

        await insertDatabase(row);
      });
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data nearby phonbook
  Future<PhoneBookModel> getNearByLocationInstansi({String nameInstansi}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.phoneNearByLocation}?instansi=$nameInstansi',
        headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));
    if (response.statusCode == 200) {
      var dataPhoneBook = PhoneBookModel.fromMap(jsonDecode(response.body)['data']);
        return dataPhoneBook;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data from search records
  Future<void> searchRecords({@required String keyword, int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.phoneBook}?search=$keyword&page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> records = jsonDecode(response.body)['data']['items'];

      var totalCount =
      jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalCount));

      if (page <= 1) await truncateLocal();

      records.forEach((record) async {
        var row = {
          'id': record['id'],
          'name': record['name'],
          'address': record['address'],
          'description': record['description'],
          'seq': record['seq'],
          'kabkota_id': record['kabkota_id'],
          'kec_id': record['kec_id'],
          'kel_id': record['kel_id'],
          'latitude': record['latitude'],
          'longitude': record['longitude'],
          'cover_image_path': record['cover_image_url'],
//          'cover_image_url': record['cover_image_url'],
          'phone_numbers': jsonEncode(record['phone_numbers']),
//          'category': jsonEncode(record['category']),
//          'category_id': record['category_id'],
        };

        await insertDatabase(row);
      });
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data by filter phonebook
  Future<void> filterRecord({@required String filter}) async {
    String token = await AuthRepository().getToken();
    UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();

    String keyword;

    if (filter == '1') {
      keyword = "kabkota_id=" + userInfo.kabkotaId;
    } else if (filter == '2') {
      keyword = "kec_id=" + userInfo.kecId;
    } else if (filter == '3') {
      keyword = "kel_id=" + userInfo.kelId;
    } else {
      keyword = "";
    }

    final response = await http
        .get('${EndPointPath.phoneBook}?$keyword',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> records = jsonDecode(response.body)['data']['items'];

      var totalCount =
      jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalCount));

      // Jika berhasil, baru lakukan truncate/update local
      await truncateLocal();

      records.forEach((record) async {
        var row = {
          'id': record['id'],
          'name': record['name'],
          'address': record['address'],
          'description': record['description'],
          'seq': record['seq'],
          'kabkota_id': record['kabkota_id'],
          'kec_id': record['kec_id'],
          'kel_id': record['kel_id'],
          'latitude': record['latitude'],
          'longitude': record['longitude'],
          'cover_image_path': record['cover_image_url'],
//          'cover_image_url': record['cover_image_url'],
          'phone_numbers': jsonEncode(record['phone_numbers']),
//          'category': jsonEncode(record['category']),
//          'category_id': record['category_id'],
        };


        await insertDatabase(row);
      });
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data nearbylocation
  Future<List<NearByLocationsModel>> getNearByLocations(
      {@required double latitude, @required double longitude}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get(
            '${EndPointPath.phoneBook}?latitude=$latitude&longitude=$longitude',
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body)['data']['items'];

      final nearByLocations = nearByLocationsFromJson(json.encode(jsonData));

      return nearByLocations.isNotEmpty ? nearByLocations : [];
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<bool> hasLocalData() async {
    Database db = await DBProvider.db.database;

    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM PhoneBooks'));

    return count > 0;
  }

  //get data from database
  Future<List<PhoneBookModel>> getRecords(
      {bool forceRefresh = false,
      int page = 1,
      String keyword = '',
      String filter = ''}) async {
    // await truncateLocal();

    bool hasLocal = await hasLocalData();

    if (keyword.trim().isNotEmpty) {
      await searchRecords(keyword: keyword, page: page);
    } else if (filter.trim().isNotEmpty) {
      await filterRecord(filter: filter);
    } else {
      if (hasLocal == false || forceRefresh == true) {
        await fetchRecords(page, true);
      } else if (page > 1) {
        await fetchRecords(page, false);
      }
    }

    List<PhoneBookModel> localRecords = await getRecordsLocal(keyword: keyword);

    return localRecords;
  }

  //get data by keyword from database
  Future<List<PhoneBookModel>> getRecordsLocal({String keyword = ''}) async {
    Database db = await DBProvider.db.database;

    var res = await db.query('PhoneBooks',
        where: 'name LIKE ? OR phone_numbers LIKE ?',
        whereArgs: ['%$keyword%', '%$keyword%'],
        orderBy: null);

    List<PhoneBookModel> list = res.isNotEmpty
        ? res.map((c) => PhoneBookModel.fromDatabaseMap(c)).toList()
        : [];

    return list;
  }

  //delete data from database
  Future<void> truncateLocal() async {
    Database db = await DBProvider.db.database;

    await db.rawDelete('Delete from PhoneBooks');
  }

  //insett data list into database
  Future<void> insertDatabase(Map<String, dynamic> row) async {
    Database db = await DBProvider.db.database;
    try {
      await db.insert('PhoneBooks', row);
    } catch (e) {
      print(e.toString());
    }
  }

  //get data record
  Future<PhoneBookModel> getRecord(int id) async {
    Database db = await DBProvider.db.database;

    var res = await db.query('PhoneBooks', where: 'id = ?', whereArgs: [id]);

    return res.isNotEmpty ? PhoneBookModel.fromDatabaseMap(res.first) : null;
  }
}
