import 'dart:convert';

import 'package:device_apps/device_apps.dart';
import 'package:fancy_on_boarding/fancy_on_boarding.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:url_launcher/url_launcher.dart';

class OnBoardingData {
  static List<PageModel> onboardingList(BuildContext context) {
    return [
      PageModel(
          color: Colors.indigo,
          heroAssetPath: 'assets/images/onboarding/onboarding1.png',
          title: Text(Dictionary.titleOnboarding1,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w800,
                color: Colors.white,
                fontSize: 25.0,
              )),
          body: Padding(
            padding: EdgeInsets.all(10.0),
            child: Text(Dictionary.textOnboarding1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                )),
          ),
          iconAssetPath: 'assets/logo/logo-small.png'),
      PageModel(
          color: Colors.green,
          heroAssetPath: 'assets/images/onboarding/onboarding2.png',
          title: Text(Dictionary.titleOnboarding2,
              style: TextStyle(
                fontWeight: FontWeight.w800,
                color: Colors.white,
                fontSize: 25.0,
              )),
          body: Padding(
            padding: EdgeInsets.all(10.0),
            child: Text(Dictionary.textOnboarding2,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                )),
          ),
          iconAssetPath: 'assets/logo/logo-small.png'),
      PageModel(
        color: Colors.blue,
        heroAssetPath: 'assets/images/onboarding/onboarding3.png',
        title: Text(Dictionary.titleOnboarding3,
            style: TextStyle(
              fontWeight: FontWeight.w800,
              color: Colors.white,
              fontSize: 25.0,
            )),
        body: Padding(
          padding: EdgeInsets.all(10.0),
          child: Text(Dictionary.textOnboarding3,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
              )),
        ),
        iconAssetPath: 'assets/logo/logo-small.png',
      ),
      PageModel(
        color: Colors.grey[800],
        heroAssetPath: 'assets/images/onboarding/onboarding4.png',
        title: Text(Dictionary.titleOnboarding4,
            style: TextStyle(
              fontWeight: FontWeight.w800,
              color: Colors.white,
              fontSize: 25.0,
            )),
        body: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text(Dictionary.textOnboarding4,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                    )),
                SizedBox(height: Dimens.padding),

                FutureBuilder<RemoteConfig>(
                  future: _initializeRemoteConfig(),
                  builder: (BuildContext context, AsyncSnapshot<RemoteConfig> snapshot) {

                    List<dynamic> csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);

                    if (snapshot.hasData) {
                      final dataPhoneNumbers = snapshot.data.getString(
                          FirebaseConfig.callCenterNumbersKey);
                      if (dataPhoneNumbers != null) {
                        csPhoneNumbers = json.decode(dataPhoneNumbers);
                      }
                    }

                    return RaisedButton(
                        color: clr.Colors.blue,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        child: Text(Dictionary.tapHelp, style: TextStyle(fontSize: 18.0, color: Colors.white, fontWeight: FontWeight.bold)),
                        onPressed: () {
                          _openDialogContact(context, csPhoneNumbers);
                        });
                  },
                ),
              ],
            )
        ),
        iconAssetPath: 'assets/logo/logo-small.png',
      ),
    ];
  }

  static Future<RemoteConfig> _initializeRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey: FirebaseConfig.callCenterNumbersValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  static _openDialogContact(BuildContext context, List<dynamic> csPhoneNumbers) {
    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
          title: Dictionary.callCenterList,
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Container(
                  margin: index != 0 ? EdgeInsets.only(top: 10.0):null,
                  padding: EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
                  decoration: BoxDecoration(
                    color: Color(0xffebf8ff),
                    shape: BoxShape.rectangle,
                    border: Border.all(color: clr.Colors.blue),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Wrap(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 10.0),
                        child: Image.asset('${Environment.iconAssets}whatsapp.png', width: 20, height: 20, color: clr.Colors.green),
                      ),
                      Text(csPhoneNumbers[index], style: TextStyle(fontFamily: FontsFamily.sourceSansPro, fontSize: 16.0, fontWeight: FontWeight.w600, color: clr.Colors.green))
                    ],
                  ),
                ),
                onTap: (){
                  _launchWhatsApp(csPhoneNumbers[index]);
                  Navigator.of(context).pop();
                },
              );
            },
          ),
          buttonText: Dictionary.close.toUpperCase(),
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ));
  }

  static _launchWhatsApp(String csPhoneNumber) async {
    try {
      bool isInstalled = await DeviceApps.isAppInstalled('com.whatsapp');
      csPhoneNumber ??= Environment.csPhone;

      if (csPhoneNumber[0] == '0') {
        csPhoneNumber = csPhoneNumber.replaceFirst('0', '+62');
      }

      if (isInstalled) {
        String urlWhatsApp = 'https://wa.me/${csPhoneNumber}?text=${Uri.encodeFull(Dictionary.helpAdminWA)}%0A%0A';
        if (await canLaunch(urlWhatsApp)) {
          await launch(urlWhatsApp);

          await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_WA_ADMIN_ONBOARDING);
        } else {
          await launch('tel://${csPhoneNumber}');
          await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_ONBOARDING);
        }
      } else {
        await launch('tel://${csPhoneNumber}');
        await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_ONBOARDING);
      }
    } catch (_) {
      await launch('tel://${csPhoneNumber}');
      await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_ONBOARDING);
    }
  }
}
