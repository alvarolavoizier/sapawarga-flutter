import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/blocs/educations_list/Bloc.dart';
import 'package:sapawarga/blocs/jobs_list/Bloc.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/LocationPicker.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/EducationRepository.dart';
import 'package:sapawarga/repositories/JobRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Connection.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:sapawarga/constants/Colors.dart' as color;
import 'package:sapawarga/components/custom_dropdown.dart' as custom;

class ProfileEditScreen extends StatelessWidget {
  final UserInfoModel authUserInfo;
  final AuthProfileRepository authProfileRepository = AuthProfileRepository();
  final EducationRepository educationRepository = EducationRepository();
  final JobRepository jobRepository = JobRepository();

  ProfileEditScreen({@required this.authUserInfo});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AccountProfileEditBloc>(
          create: (context) => AccountProfileEditBloc(
              authProfileRepository: authProfileRepository),
        ),
        BlocProvider<EducationsListBloc>(
          create: (context) =>
              EducationsListBloc(educationRepository: educationRepository),
        ),
        BlocProvider<JoblistBloc>(
          create: (context) => JoblistBloc(jobRepository: jobRepository),
        ),
      ],
      child: ProfileEditForm(
        authUserInfoModel: authUserInfo,
      ),
    );
  }
}

class ProfileEditForm extends StatefulWidget {
  final UserInfoModel authUserInfoModel;

  const ProfileEditForm({Key key, this.authUserInfoModel}) : super(key: key);

  @override
  State<ProfileEditForm> createState() => _ProfileEditFormState();
}

class _ProfileEditFormState extends State<ProfileEditForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool isShowDialog = false;
  ProgressDialog loadingDialog;

  GoogleSignIn _googleSignIn;
  AccountProfileEditBloc _blocProfile;
  EducationsListBloc _blocEducationsList;
  JoblistBloc _blocJoblist;

  final _nameController = TextEditingController();
  final _birthDateController = TextEditingController();
  final _educationLevelIdController = TextEditingController();
  final _jobIdController = TextEditingController();
  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _kabkotaController = TextEditingController();
  final _kecamatanController = TextEditingController();
  final _kelurahanController = TextEditingController();
  final _rtController = TextEditingController();
  final _rwController = TextEditingController();
  final _addressController = TextEditingController();
  final _phoneController = TextEditingController();
  final _instagramController = TextEditingController();
  final _facebookController = TextEditingController();
  final _twitterController = TextEditingController();
  final _penggunaController = TextEditingController();

  String location = Dictionary.setYourLocation;
  String latitude;
  String longitude;

  DateTime _birthDate;

  @override
  void initState() {
    _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    _googleSignIn.signInSilently();

    _blocProfile = BlocProvider.of<AccountProfileEditBloc>(context);
    _blocEducationsList = BlocProvider.of<EducationsListBloc>(context);
    _blocJoblist = BlocProvider.of<JoblistBloc>(context);

    _blocEducationsList.add(EducationsLoad());
    _blocJoblist.add(JobsLoad());

    _nameController.text = widget.authUserInfoModel.name;
    _birthDateController.text = widget.authUserInfoModel.birthDate != null
        ? DateFormat('dd/MM/yyyy', 'id')
            .format(widget.authUserInfoModel.birthDate)
        : null;
    _educationLevelIdController.text =
        widget.authUserInfoModel.educationLevelId.toString();
    _jobIdController.text = widget.authUserInfoModel.jobTypeId.toString();
    _usernameController.text = widget.authUserInfoModel.username;
    _emailController.text = widget.authUserInfoModel.email;
    _kabkotaController.text = widget.authUserInfoModel.kabkota.name;
    _kecamatanController.text = widget.authUserInfoModel.kecamatan.name;
    _kelurahanController.text = widget.authUserInfoModel.kelurahan.name;
    _rtController.text = widget.authUserInfoModel.rt;
    _rwController.text = widget.authUserInfoModel.rw;
    _addressController.text = widget.authUserInfoModel.address;
    _phoneController.text = widget.authUserInfoModel.phone;
    _instagramController.text = widget.authUserInfoModel.instagram;
    _facebookController.text = widget.authUserInfoModel.facebook;
    _twitterController.text = widget.authUserInfoModel.twitter;
    _penggunaController.text = widget.authUserInfoModel.roleLabel;

    location = (widget.authUserInfoModel.lat != null &&
            widget.authUserInfoModel.lon != null)
        ? 'Latitude: ${widget.authUserInfoModel.lat}  Longitude: ${widget.authUserInfoModel.lon}'
        : Dictionary.setYourLocation;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        bloc: _blocProfile,
        listener: (context, state) {
          if (state is AccountProfileEditFailure) {
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.error,
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                    ));
          }

          if (state is AccountProfileEditValidationError) {
            if (state.errors.containsKey('email')) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => DialogTextOnly(
                        description: state.errors['email'][0].toString(),
                        buttonText: "OK",
                        onOkPressed: () {
                          Navigator.of(context).pop(); // To close the dialog
                        },
                      ));
            }
          }
        },
        child: BlocBuilder(
            bloc: _blocProfile,
            builder: (BuildContext context, AccountProfileEditState state) {
              if (state is AccountProfileEditUpdated) {
                hideLoading();
                Navigator.pop(context, Dictionary.successSaveProfile);
              }

              if (state is AccountProfileEditPhotoLoading) {
                _onWidgetDidBuild(() {
                  showLoading();
                });
              }

              if (state is AccountProfileEditPhotoUpdated) {
                hideLoading();
                _onWidgetDidBuild(() {
                  if (isShowDialog) {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(Dictionary.updatePhotoTitle),
                            content: Text(Dictionary.successSavePhoto),
                            actions: <Widget>[
                              FlatButton(
                                child: Text(Dictionary.ok),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                          );
                        });
                    isShowDialog = false;
                  }
                });
              }

              if (state is AccountProfileEditLoading) {
                _onWidgetDidBuild(() {
                  showLoading();
                });
              }

              return Scaffold(
                  appBar: AppBar(
                    elevation: 0,
                    title: Text(
                      'Profile',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  body: ListView(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 2.8,
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 125,
                                    height: 125,
                                    child: CircleAvatar(
                                      minRadius: 90,
                                      maxRadius: 150,
                                      backgroundImage: state
                                              is AccountProfileEditPhotoUpdated
                                          ? NetworkImage(state.image)
                                          : widget.authUserInfoModel.photoUrl !=
                                                  null
                                              ? NetworkImage(widget
                                                  .authUserInfoModel.photoUrl)
                                              : ExactAssetImage(
                                                  'assets/images/user.png'),
                                    ),
                                  ),
                                  Container(
                                    child: FlatButton(
                                      onPressed: () {
                                        _cameraBottomSheet(context);
                                      },
                                      child: Text(
                                        'Ganti Foto Profil',
                                        style: TextStyle(
                                            color: color.Colors.blue,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.0),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            child: Form(
                              key: _formKey,
                              autovalidate: _autoValidate,
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                        Dictionary.profile.toUpperCase(),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.0)),
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.name.toUpperCase(),
                                    Dictionary.placeHolderName,
                                    _nameController,
                                    Validations.nameValidation,
                                  ),
                                  SizedBox(height: 20),
                                  InkWell(
                                    child: buildTextField(
                                      Dictionary.birthDate.toUpperCase(),
                                      Dictionary.placeHolderBirthday,
                                      _birthDateController,
                                      null,
                                      null,
                                      null,
                                      false,
                                    ),
                                    onTap: showDatePicker,
                                  ),
                                  SizedBox(height: 20),
                                  BlocBuilder<EducationsListBloc,
                                      EducationsListState>(
                                    bloc: _blocEducationsList,
                                    builder: (context, state) {
                                      return state is EducationsLoaded
                                          ? buildDropdownField(
                                              Dictionary.education
                                                  .toUpperCase(),
                                              Dictionary.placeHolderEducation,
                                              state.record,
                                              _educationLevelIdController,
                                            )
                                          : state is EducationsFailure
                                              ? Text(state.error)
                                              : buildSkeleton(context);
                                    },
                                  ),
                                  SizedBox(height: 20),
                                  BlocBuilder<JoblistBloc, JoblistState>(
                                    bloc: _blocJoblist,
                                    builder: (context, state) {
                                      return state is JobsLoaded
                                          ? buildDropdownField(
                                              Dictionary.job.toUpperCase(),
                                              Dictionary.placeHolderJob,
                                              state.record,
                                              _jobIdController,
                                            )
                                          : state is JobsFailure
                                              ? Text(state.error)
                                              : buildSkeleton(context);
                                    },
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.username.toUpperCase(),
                                    Dictionary.placeHolderUsername,
                                    _usernameController,
                                    Validations.usernameValidation,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.role.toUpperCase(),
                                    '',
                                    _penggunaController,
                                    null,
                                    null,
                                    TextStyle(color: Colors.grey),
                                    false,
                                  ),
                                  SizedBox(height: 24),
                                  SizedBox(
                                    height: 15.0,
                                    child: Container(
                                      color: Colors.grey[200],
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                      Dictionary.contact.toUpperCase(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                      Dictionary.email.toUpperCase(),
                                      Dictionary.placeHolderEmail,
                                      _emailController,
                                      Validations.emailValidation,
                                      TextInputType.emailAddress),
                                  SizedBox(height: 20),
                                  buildTextField(
                                      Dictionary.noTelp.toUpperCase(),
                                      Dictionary.placeHolderNoTelp,
                                      _phoneController,
                                      Validations.phoneValidation,
                                      TextInputType.phone),
                                  SizedBox(height: 20),
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    alignment: Alignment.topLeft,
                                    child: Text(Dictionary.useYourEmail),
                                  ),
                                  SizedBox(height: 10),
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    child: ButtonTheme(
                                      minWidth:
                                          MediaQuery.of(context).size.width,
                                      height: 45.0,
                                      child: OutlineButton(
                                        child: Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: <Widget>[
                                            Image.asset(
                                              '${Environment.iconAssets}google.png',
                                              width: 24.0,
                                              height: 24.0,
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 10.0),
                                              child: Text(
                                                Dictionary.useGoogleAccount,
                                                style: TextStyle(
                                                    color: Colors.grey[800]),
                                              ),
                                            ),
                                          ],
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0)),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                          //Color of the border
                                          style: BorderStyle.solid,
                                          //Style of the border
                                          width: 1, //width of the border
                                        ),
                                        onPressed: _handleSignIn,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  SizedBox(
                                    height: 15.0,
                                    child: Container(
                                      color: Colors.grey[200],
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                      Dictionary.address.toUpperCase(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(
                                          left: 16.0, right: 16.0, top: 10.0),
                                      alignment: Alignment.topLeft,
                                      child: Text(location)),
                                  SizedBox(height: 10),
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    child: ButtonTheme(
                                      minWidth:
                                          MediaQuery.of(context).size.width,
                                      height: 45.0,
                                      child: OutlineButton(
                                        child: Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: <Widget>[
                                            Image.asset(
                                              '${Environment.iconAssets}location.png',
                                              width: 24.0,
                                              height: 24.0,
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 10.0),
                                              child: Text(
                                                Dictionary.setLocation,
                                                style: TextStyle(
                                                    color: Colors.grey[800]),
                                              ),
                                            ),
                                          ],
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0)),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                          //Color of the border
                                          style: BorderStyle.solid,
                                          //Style of the border
                                          width: 1, //width of the border
                                        ),
                                        onPressed: _handleLocation,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.fullAddress.toUpperCase(),
                                    Dictionary.placeHolderAddress,
                                    _addressController,
                                    Validations.addressValidation,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.kabkota.toUpperCase(),
                                    '',
                                    _kabkotaController,
                                    null,
                                    null,
                                    TextStyle(color: Colors.grey),
                                    false,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.kecamatan.toUpperCase(),
                                    '',
                                    _kecamatanController,
                                    null,
                                    null,
                                    TextStyle(color: Colors.grey),
                                    false,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.kelurahan.toUpperCase(),
                                    '',
                                    _kelurahanController,
                                    null,
                                    null,
                                    TextStyle(color: Colors.grey),
                                    false,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.rw.toUpperCase(),
                                    '',
                                    _rwController,
                                    null,
                                    TextInputType.number,
                                    TextStyle(color: Colors.grey),
                                    false,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.rt.toUpperCase(),
                                    Dictionary.placeHolderRt,
                                    _rtController,
                                    Validations.rtValidation,
                                    TextInputType.number,
                                  ),
                                  SizedBox(height: 20),
                                  SizedBox(
                                    height: 15.0,
                                    child: Container(
                                      color: Colors.grey[200],
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                      Dictionary.socmed.toUpperCase(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.twitter.toUpperCase(),
                                    Dictionary.placeHolderTwitter,
                                    _twitterController,
                                    Validations.twitterlidation,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.instagram.toUpperCase(),
                                    Dictionary.placeHolderInstagram,
                                    _instagramController,
                                    Validations.instagramValidation,
                                  ),
                                  SizedBox(height: 20),
                                  buildTextField(
                                    Dictionary.facebook.toUpperCase(),
                                    Dictionary.placeHolderFacebook,
                                    _facebookController,
                                    Validations.facebooklidation,
                                  ),
                                  SizedBox(height: 20),
                                  SizedBox(
                                    height: 15.0,
                                    child: Container(
                                      color: Colors.grey[200],
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Container(
                                    padding: EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(8.0),
                                      color: color.Colors.blue,
                                      child: MaterialButton(
                                        padding: EdgeInsets.all(0),
                                        minWidth:
                                            MediaQuery.of(context).size.width,
                                        child: Text(Dictionary.saveProfile,
                                            style: TextStyle(
                                                fontFamily:
                                                    FontsFamily.productSans,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 18.0)),
                                        onPressed: _onSaveProfileButtonPressed,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ));
            })
    );
  }

  Skeleton buildSkeleton(BuildContext context) {
    return Skeleton(
      child: Container(
        padding: EdgeInsets.only(left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 70,
              height: 30.0,
              color: Colors.grey[300],
            ),
            SizedBox(height: 5.0),
            Container(
                width: MediaQuery.of(context).size.width,
                height: 30.0,
                color: Colors.grey[300]),
          ],
        ),
      ),
    );
  }

  /// @params
  /// * [title] type String must not be null.
  /// * [hintText] type String must not be null.
  /// * [controller] type from class TextEditingController must not be null.
  /// * [validation] type from class Validation.
  /// * [textInputType] type from class TextInputType.
  /// * [textStyle] type from class TextStyle.
  /// * [isEdit] type bool.
  Widget buildTextField(
      String title, String hintText, TextEditingController controller,
      [validation,
      TextInputType textInputType,
      TextStyle textStyle,
      bool isEdit]) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
          ),
          TextFormField(
            style: textStyle != null
                ? TextStyle(color: Colors.grey)
                : TextStyle(color: Colors.black),
            enabled: isEdit != null ? false : true,
            validator: validation,
            controller: controller,
            decoration: InputDecoration(hintText: hintText),
            keyboardType:
                textInputType != null ? textInputType : TextInputType.text,
          )
        ],
      ),
    );
  }

  Widget buildDropdownField(String title, String hintText, List items,
      TextEditingController controller,
      [validation]) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
          ),
          custom.DropdownButton<String>(
            isExpanded: true,
            height: 320,
            hint: Text(hintText),
            items: items.map((item) {
              return custom.DropdownMenuItem(
                child: Text(item.title),
                value: item.id.toString(),
              );
            }).toList(),
            onChanged: (String value) {
              setState(() {
                controller.text = value;
              });
            },
            value: controller.text == 'null' ? null : controller.text,
          )
        ],
      ),
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  Future<void> _handleSignIn() async {
    try {
      bool isConnected =
          await Connection().checkConnection('https://www.google.com');
      if (isConnected) {
        try {
          await _googleSignIn.signIn();

          if (_googleSignIn.currentUser != null &&
              _googleSignIn.currentUser.email != null) {
            _emailController.text = _googleSignIn.currentUser.email;
            unawaited(_googleSignIn.disconnect());
          }
        } catch (error) {
          await showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: Dictionary.errorGetEmail,
                    buttonText: "OK",
                    onOkPressed: () {
                      Navigator.of(context).pop(); // To close the dialog
                    },
                  ));
        }
      }
    } catch (_) {
      await showDialog(
        context: context,
        builder: (BuildContext context) => DialogTextOnly(
          description: Dictionary.errorConnection,
          buttonText: "OK",
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ),
      );
    }
  }

  Future<void> _handleLocation() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    if (permission == PermissionStatus.granted) {
      final result = await Navigator.push(
          context, MaterialPageRoute(builder: (context) => LocationPicker()));

      setState(() {
        if (result != null) {
          LatLng latLng = result;
          latitude = '${latLng.latitude.toStringAsFixed(7)}';
          longitude = '${latLng.longitude.toStringAsFixed(7)}';
          location = 'Latitude : $latitude Longitude : $longitude';
        }
      });
    } else {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/map_pin.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionLocationMap,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.location]).then(_onStatusRequested);
                },
              )));
    }
  }

  _onSaveProfileButtonPressed() async {
    if (_formKey.currentState.validate()) {
      try {
        bool isConnected =
            await Connection().checkConnection('https://www.google.com');
        if (isConnected) {
          _blocProfile.add(
            AccountProfileEditSubmit(
              userInfoModel: UserInfoModel(
                name: _nameController.text,
                username: _usernameController.text,
                educationLevelId: _educationLevelIdController.text != 'null'
                    ? int.parse(_educationLevelIdController.text)
                    : null,
                jobTypeId: _jobIdController.text != 'null'
                    ? int.parse(_jobIdController.text)
                    : null,
                email: _emailController.text,
                address: _addressController.text,
                lat: latitude,
                lon: longitude,
                phone: _phoneController.text,
                rt: _rtController.text,
                facebook: _facebookController.text,
                instagram: _instagramController.text,
                twitter: _twitterController.text,
                birthDate: _birthDate,
              ),
            ),
          );

          await AnalyticsHelper.setLogEvent(Analytics.EVENT_EDIT_ACCOUNT);
        }
      } catch (_) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => DialogTextOnly(
                  description: Dictionary.errorConnection,
                  buttonText: "OK",
                  onOkPressed: () {
                    Navigator.of(context).pop(); // To close the dialog
                  },
                ));
      }
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void _cameraBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.camera),
                  title: Text(Dictionary.takePhoto),
                  onTap: () {
                    _permissionCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.image),
                  title: Text(Dictionary.takePhotoFromGallery),
                  onTap: () {
                    _permissionGallery();
                  },
                ),
              ],
            ),
          );
        });
  }

  void _permissionCamera() async {
    PermissionStatus permission =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/photo-camera.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionCamera,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.camera]).then(_onStatusRequestedCamera);
                },
              )));
    } else {
      await openCamera();
    }
  }

  void _permissionGallery() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionGalery,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.storage]).then(_onStatusRequested);
                },
              )));
    } else {
      await openGallery();
    }
  }

  void _onStatusRequested(
      Map<PermissionGroup, PermissionStatus> statuses) async {
    final statusStorage = statuses[PermissionGroup.storage];
    if (statusStorage == PermissionStatus.granted) {
      _permissionGallery();
    }

    final statusLocation = statuses[PermissionGroup.location];
    if (statusLocation == PermissionStatus.granted) {
      final result = await Navigator.push(
          context, MaterialPageRoute(builder: (context) => LocationPicker()));

      setState(() {
        if (result != null) {
          LatLng latLng = result;
          latitude = '${latLng.latitude.toStringAsFixed(7)}';
          longitude = '${latLng.longitude.toStringAsFixed(7)}';
          location = 'Latitude : $latitude Longitude : $longitude';
        }
      });
    }
  }

  void _onStatusRequestedCamera(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.camera];
    if (status == PermissionStatus.granted) {
      _permissionCamera();
    }
  }

  Future openCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 640, maxWidth: 640);
    File _image;

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        setState(() {
          _image = image;
          _blocProfile.add(AccountProfileEditPhotoSubmit(
              image: _image, userInfoModel: widget.authUserInfoModel));
        });
      }
    }

    isShowDialog = true;

    Navigator.of(context, rootNavigator: true).pop('dialog');
  }

  Future openGallery() async {
    File _image;
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 640, maxWidth: 640);

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        setState(() {
          _image = image;
          _blocProfile.add(AccountProfileEditPhotoSubmit(
              image: _image, userInfoModel: widget.authUserInfoModel));
        });
      }
    }

    isShowDialog = true;

    Navigator.of(context, rootNavigator: true).pop('dialog');
  }

  void hideLoading() {
    loadingDialog.hide().then((isHidden) {});
  }

// TODO: harus dibuat component supaya bisa di reuse
  void showLoading() {
    loadingDialog = ProgressDialog(context);
    loadingDialog.style(
        message: 'Silahkan Tunggu...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    loadingDialog.show();
  }

  void showDatePicker() {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      maxTime: DateTime(1999, 12, 30),
      minTime: DateTime(1945),
      onConfirm: (date) {
        _birthDateController.text = DateFormat('dd/MM/yyyy', 'id').format(date);
        _birthDate = date;
      },
      currentTime: DateTime.now(),
      locale: LocaleType.id,
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _usernameController.dispose();
    _emailController.dispose();
    _kabkotaController.dispose();
    _kecamatanController.dispose();
    _kelurahanController.dispose();
    _rtController.dispose();
    _rwController.dispose();
    _addressController.dispose();
    _phoneController.dispose();
    _instagramController.dispose();
    _facebookController.dispose();
    _twitterController.dispose();
    _penggunaController.dispose();

    _blocProfile.close();
    _blocEducationsList.close();
    _blocJoblist.close();

    super.dispose();
  }
}
