import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/OpenChromeSapariBrowser.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pedantic/pedantic.dart';
import 'dart:math' as math;

class VerificationSection extends StatefulWidget {
  final RemoteConfig remoteConfig;

  VerificationSection(this.remoteConfig);

  @override
  _VerificationSectionState createState() => _VerificationSectionState();
}

class _VerificationSectionState extends State<VerificationSection> {
  @override
  Widget build(BuildContext context) {
    return widget.remoteConfig != null ? _buildContent(widget.remoteConfig) : Container();
  }

  _buildContent(RemoteConfig remoteConfig) {

    Map<String, dynamic> data = json.decode(remoteConfig.getString(FirebaseConfig.highlightMenuKey));

    return data['enabled'] && data['cover-image'] != null ?

    CachedNetworkImage(
        imageUrl:data['cover-image'],
        imageBuilder: (context, imageProvider) =>
            Container (
                color: Color(0xFFE5E5E5),
                padding: EdgeInsets.only(top: 8.0),
                child:
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(Dimens.padding),
                  child: GestureDetector(
                    onTap: () async {

                      String feature_name = data['feature_name'];
                      unawaited(AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_HIGHLIGHT_MENU, <String, dynamic>{
                        'name': feature_name.substring(0, math.min(feature_name.length, 80))
                      }));

                      String url = await _userDataUrlAppend(data['url']);

                      switch (data['mode']) {
                        case 'webview' :
                          await Navigator.of(context).push(MaterialPageRoute(builder: (context) => BrowserScreen(url: url, useInAppWebView: false)));
                          break;
                        case 'inappwebview' :
                          await Navigator.of(context).push(MaterialPageRoute(builder: (context) => BrowserScreen(url: url, useInAppWebView: true)));
                          break;
                        case 'chrome' :
                          openChromeSafariBrowser(url: url);
                          break;
                        case 'redirect' :
                          _launchURL(url);
                          break;
                        default:
                          openChromeSafariBrowser(url: url);
                      }

                    },
                    child: Image(image: imageProvider, fit: BoxFit.fitWidth),
                  ),
                )),
        errorWidget: (context, url, error) =>
            Container())

     : Container();
  }



  Future<String> _userDataUrlAppend(String url) async {

    String token = await AuthRepository().getToken();
    UserInfoModel user = await AuthProfileRepository().getUserInfo();

    if (url == null) {
      return url;
    } else {
      Map<String, String> usrMap = {
        '_userToken_': '',
        '_userID_': ''
      };

      if (user != null) {
        usrMap = {
          '_userToken_': token,
          '_userID_': user.id.toString()
        };
      }

      usrMap.forEach((key, value) {
        url = url.replaceAll(key, value);
      });

      return url;
    }

  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
