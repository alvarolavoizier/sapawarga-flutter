import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/components/OpenChromeSapariBrowser.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:pedantic/pedantic.dart';
import 'dart:math' as math;

class Announcement extends StatefulWidget {
  final RemoteConfig remoteConfig;

  Announcement(this.remoteConfig);

  @override
  _AnnouncementState createState() => _AnnouncementState();
}

class _AnnouncementState extends State<Announcement> {
  Map<String, dynamic> dataAnnouncement;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.remoteConfig != null) {
      dataAnnouncement = json.decode(
          widget.remoteConfig.getString(FirebaseConfig.announcementKey));
    }

    return widget.remoteConfig != null && dataAnnouncement['enabled']
        ? Container(
            width: (MediaQuery.of(context).size.width),
            padding: EdgeInsets.all(10.0),
            margin: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                color: Color(0xFFF9EFD0),
                borderRadius: BorderRadius.circular(8.0)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                dataAnnouncement['title'] != null && dataAnnouncement['title'].toString().isNotEmpty ? Padding(
                  padding: EdgeInsets.only(bottom: 5.0),
                  child: Text(dataAnnouncement['title'], style: TextStyle(fontFamily: FontsFamily.productSans,
                    fontSize: 13.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[600])),
                ) : Container(),
                RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: dataAnnouncement['content'] != null
                              ? dataAnnouncement['content']
                              : '',
                          style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey[600],
                              fontFamily: FontsFamily.productSans),
                        ),
                        dataAnnouncement['action_url'].toString().isNotEmpty
                            ? TextSpan(
                                text: Dictionary.moreDetail,
                                style: TextStyle(
                                    color: clr.Colors.green,
                                    fontFamily: FontsFamily.productSans,
                                    fontWeight: FontWeight.bold),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () async {
                                    String url = await StringUtils.userDataUrlAppend(dataAnnouncement['action_url']);
                                    openChromeSafariBrowser(
                                        url: url);

                                    String name = dataAnnouncement['name'];
                                    unawaited(AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_ANNOUNCEMENT, <String, dynamic>{
                                      'name': name.substring(0, math.min(name.length, 80))
                                    }));
                                  })
                            : TextSpan(text: '')
                      ]),
                    ),
              ],
            ),
          )
        : Container();
  }
}
