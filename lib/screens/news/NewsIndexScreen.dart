import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/news_list/NewsListBloc.dart';
import 'package:sapawarga/blocs/news_list/NewsListState.dart';
import 'package:sapawarga/blocs/news_list/NewsListlEvent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';
import 'package:sapawarga/screens/news/NewsDetailScreen.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class NewsIndexScreen extends StatelessWidget {
  final NewsRepository newsRepository = NewsRepository();
  final bool isIdKota;
  final ScrollController scrollController;

  NewsIndexScreen({@required this.isIdKota, this.scrollController});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NewsListBloc>(
      create: (context) => NewsListBloc(newsRepository: newsRepository),
      child:
          _BeritaList(isIdKota: isIdKota, scrollController: scrollController),
    );
  }
}

class _BeritaList extends StatefulWidget {
  final bool isIdKota;
  final ScrollController scrollController;

  _BeritaList({@required this.isIdKota, this.scrollController});

  @override
  _BeritaListState createState() => _BeritaListState();
}

class _BeritaListState extends State<_BeritaList> {
  NewsListBloc _newsListBloc;
  String nameCity = "";
  int maxDatalength;
  int _page = 1;
  List<NewsModel> dataListModel = [];

  bool get _isIdKota => widget.isIdKota;

  @override
  void initState() {
    _initialize();
    _newsListBloc = BlocProvider.of<NewsListBloc>(context);
    _newsListBloc.add(NewsListLoad(isIdKota: _isIdKota, page: _page));
    widget.scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsListBloc, NewsListState>(
        bloc: _newsListBloc,
        builder: (context, state) {
          return Container(
              child: state is NewsListLoading
                  ? _buildLoading()
                  : state is NewsListLoaded
                      ? _buildContent(state)
                      : state is NewsListFailure
                          ? _buildFailure(state)
                          : Container());
        });
  }

  _buildContent(NewsListLoaded state) {
    if (state.isLoad) {
      _page++;
      maxDatalength = state.maxData;
      dataListModel.addAll(state.listNews);
      state.isLoad = false;
    }

    if (dataListModel.isNotEmpty) {
      return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: dataListModel.length + 1,
        itemBuilder: (BuildContext context, int index) {
          return index >= dataListModel.length
              ? maxDatalength != dataListModel.length
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                      child: Column(
                        children: <Widget>[
                          CupertinoActivityIndicator(),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(Dictionary.loadingData),
                        ],
                      ),
                    )
                  : Container()
              : GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NewsDetailScreen(
                                  newsId: dataListModel[index].id,
                                  isIdKota: _isIdKota,
                                )));
                  },
                  child: Card(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 5,
                            height: MediaQuery.of(context).size.height / 10,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: CachedNetworkImage(
                                imageUrl: dataListModel[index].coverPathUrl,
                                fit: BoxFit.fill,
                                placeholder: (context, url) => Center(
                                    heightFactor: 8.2,
                                    child: CupertinoActivityIndicator()),
                                errorWidget: (context, url, error) => Container(
                                    height: MediaQuery.of(context).size.height /
                                        3.3,
                                    color: Colors.grey[200],
                                    child: Image.asset(
                                        '${Environment.imageAssets}placeholder.png',
                                        fit: BoxFit.fitWidth)),
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                left: 15, top: 5, bottom: 5, right: 5),
                            width: MediaQuery.of(context).size.width - 175,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Text(
                                    dataListModel[index].title,
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.left,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 5.0),
                                  child: Row(
                                    children: <Widget>[
                                      Image.network(
                                        dataListModel[index].channel.iconUrl,
                                        width: 25.0,
                                        height: 25.0,
                                      ),
                                      Text(
                                        dataListModel[index].channel.name,
                                        style: TextStyle(
                                            fontSize: 12.0, color: Colors.grey),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ));
        },
      );
    } else {
      return Container();
    }
  }

  _buildFailure(NewsListFailure state) {
    return Container(child: Text(state.error));
  }

  _buildLoading() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        margin: EdgeInsets.only(left: 10.0, right: 5.0, bottom: 10.0),
        elevation: 3.0,
        child: ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 2,
            padding: const EdgeInsets.all(10.0),
            itemBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 100.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.all(5.0),
                      width: MediaQuery.of(context).size.width / 2,
                      child: Column(
                        children: <Widget>[
                          Container(
                              child: Skeleton(
                            height: 20.0,
                            width: MediaQuery.of(context).size.width,
                          )),
                          Container(
                            margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: <Widget>[
                                Skeleton(
                                  height: 35.0,
                                  width: 35.0,
                                ),
                                Skeleton(
                                  height: 25.0,
                                  width: 100.0,
                                  margin: 10.0,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Skeleton(
                          height: 300.0,
                          width: MediaQuery.of(context).size.width / 3),
                    )
                  ],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int dex) => Divider()),
      ),
    );
  }

  void _initialize() async {
    maxDatalength = await Preferences.getTotalCount();
  }

  void _scrollListener() {
    if (dataListModel.length != maxDatalength) {
      if (widget.scrollController.position.pixels ==
          widget.scrollController.position.maxScrollExtent) {
        _newsListBloc.add(NewsListLoad(isIdKota: _isIdKota, page: _page));
      }
    }
  }

  @override
  void dispose() {
    _newsListBloc.close();
    super.dispose();
  }
}
