import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/news_list_featured/NewsListFeaturedBloc.dart';
import 'package:sapawarga/blocs/news_list_featured/NewsListFeaturedState.dart';
import 'package:sapawarga/blocs/news_list_featured/NewsListlFeaturedEvent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';
import 'package:sapawarga/screens/news/NewsDetailScreen.dart';
import 'NewsListIndexScreen.dart';

class NewsListScreen extends StatelessWidget {
  final NewsRepository newsRepository = NewsRepository();
  final bool isIdKota;

  NewsListScreen({@required this.isIdKota});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NewsListFeaturedBloc>(
      create: (context) => NewsListFeaturedBloc(newsRepository: newsRepository),
      child: _BeritaList(
        isIdKota: isIdKota,
      ),
    );
  }
}

class _BeritaList extends StatefulWidget {
  final bool isIdKota;

  _BeritaList({@required this.isIdKota});

  @override
  _BeritaListState createState() => _BeritaListState();
}

class _BeritaListState extends State<_BeritaList> {
  NewsListFeaturedBloc _newsListBloc;
  String nameCity = "";

  bool get _isIdKota => widget.isIdKota;

  @override
  void initState() {
    _newsListBloc = BlocProvider.of<NewsListFeaturedBloc>(context);
    _newsListBloc.add(NewsListFeaturedLoad(isIdKota: _isIdKota));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsListFeaturedBloc, NewsListFeaturedState>(
        bloc: _newsListBloc,
        builder: (context, state) {
          return Container(
              child: state is NewsListFeaturedLoading
                  ? _buildLoading()
                  : state is NewsListFeatuedLoaded
                      ? _buildContent(state)
                      : state is NewsListFeaturedFailure
                          ? _buildFailure(state)
                          : Container());
        });
  }

  _buildContent(NewsListFeatuedLoaded state) {
    if (state.listtNews.isNotEmpty) {
      return Container(
        width: 325.0,
        child: Card(
          margin: EdgeInsets.only(left: 10.0, right: 5.0, bottom: 10.0),
          elevation: 3.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      _isIdKota ? state.cityName : Dictionary.titleNews,
                      style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 0.73),
                          fontSize: 14.0,
                          fontWeight: FontWeight.w600),
                    ),
                    TextButton(
                      title: Dictionary.viewAll,
                      textStyle: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.w600,
                          fontSize: 13.0),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => _isIdKota
                                    ? NewsListIndexScreen(isIdKota: true)
                                    : NewsListIndexScreen(isIdKota: false)));
                      },
                    ),
                  ],
                ),
              ),
              ListView.separated(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: state.listtNews.length,
                  padding: const EdgeInsets.only(bottom: 10.0),
                  itemBuilder: (BuildContext context, int index) {
                    return RaisedButton(
                      elevation: 0,
                      color: Colors.white,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NewsDetailScreen(
                                    newsId: state.listtNews[index].id,
                                    isIdKota: _isIdKota)));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 70,
                            height: 70,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: CachedNetworkImage(
                                imageUrl: state.listtNews[index].coverPathUrl,
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Center(
                                    heightFactor: 4.2,
                                    child: CupertinoActivityIndicator()),
                                errorWidget: (context, url, error) => Container(
                                    height: MediaQuery.of(context).size.height /
                                        3.3,
                                    color: Colors.grey[200],
                                    child: Image.asset(
                                        '${Environment.imageAssets}placeholder_square.png',
                                        fit: BoxFit.fitWidth)),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    state.listtNews[index].title,
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w600),
                                    textAlign: TextAlign.left,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 5.0),
                                    child: Row(
                                      children: <Widget>[
                                        Image.network(
                                          state
                                              .listtNews[index].channel.iconUrl,
                                          width: 25.0,
                                          height: 25.0,
                                        ),
                                        SizedBox(width: 3.0),
                                        Text(
                                          state.listtNews[index].channel.name,
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.grey),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int dex) =>
                      Divider()),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  _buildFailure(NewsListFeaturedFailure state) {
    return Container(child: Text(state.error));
  }

  _buildLoading() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        margin: EdgeInsets.only(left: 10.0, right: 5.0, bottom: 10.0),
        elevation: 3.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10.0),
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Skeleton(
                    height: 20.0,
                    width: MediaQuery.of(context).size.width / 4,
                    padding: 10.0,
                  ),
                  Skeleton(
                    height: 20.0,
                    width: MediaQuery.of(context).size.width / 4,
                    padding: 10.0,
                  ),
                ],
              ),
            ), //
            ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 3,
                padding: const EdgeInsets.all(10.0),
                itemBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    height: 100.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.all(5.0),
                          width: MediaQuery.of(context).size.width / 2,
                          child: Column(
                            children: <Widget>[
                              Container(
                                  child: Skeleton(
                                height: 20.0,
                                width: MediaQuery.of(context).size.width,
                              )),
                              Container(
                                margin: EdgeInsets.only(top: 5.0),
                                child: Row(
                                  children: <Widget>[
                                    Skeleton(
                                      height: 35.0,
                                      width: 35.0,
                                    ),
                                    Skeleton(
                                      height: 25.0,
                                      width: 100.0,
                                      margin: 10.0,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Skeleton(
                              height: 300.0,
                              width: MediaQuery.of(context).size.width / 3),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int dex) => Divider()),
          ],
        ),
      ),
    );
  }
}
