import 'dart:convert';
import 'dart:io';

import 'package:device_apps/device_apps.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/change_password/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/components/PasswordFormField.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/enums/ChangePasswordType.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;

class ChangePasswordScreen extends StatelessWidget {
  final ChangePasswordType type;

  ChangePasswordScreen({@required this.type}) : assert(type != null);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChangePasswordBloc>(
      create: (context) =>
          ChangePasswordBloc(authProfileRepository: AuthProfileRepository()),
      child: ChangePassword(type),
    );
  }
}

class ChangePassword extends StatefulWidget {
  final ChangePasswordType type;

  ChangePassword(this.type);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  ChangePasswordBloc _changePasswordBloc;
  AuthenticationBloc _authenticationBloc;
  final _formKey = GlobalKey<FormState>();
  final _oldPassController = TextEditingController();
  final _newPassController = TextEditingController();
  final _confirmPassController = TextEditingController();
  bool _autoValidate = false;
  bool _isLoading = false;

  List<dynamic> csPhoneNumbers;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.CHANGE_PASSWORD);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_CHANGE_PASSWORD);
    _changePasswordBloc = BlocProvider.of<ChangePasswordBloc>(context);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);

    super.initState();
  }

  Future<RemoteConfig> _initializeRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey: FirebaseConfig.callCenterNumbersValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ChangePasswordBloc, ChangePasswordState>(
      bloc: _changePasswordBloc,
      listener: (context, state) {
        if (state is ChangePasswordLoading) {
          _isLoading = true;
          blockCircleLoading(context: context);
        } else if (state is ChangePasswordSuccess) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }

          AnalyticsHelper.setLogEvent(Analytics.EVENT_SUCCESS_CHANGE_PASSWORD);

          showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: Dictionary.successChangePassword,
                    buttonText: "OK",
                    onOkPressed: () {
                      if (widget.type == ChangePasswordType.force) {
                        _authenticationBloc.add(LoggedOut());
                        Navigator.of(context, rootNavigator: true)
                            .pop(); // To close the dialog
                      } else {
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                      }
                    },
                  ));
        } else if (state is ChangePasswordFailure) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }

          AnalyticsHelper.setLogEvent(
              Analytics.EVENT_FAILED_CHANGE_PASSWORD,
              <String, dynamic>{
                'message': '${state.error.toString()}'
              });

          if (state.error.contains(Dictionary.errorUnauthorized)) {
            _authenticationBloc.add(LoggedOut());
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.error.toString(),
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context, rootNavigator: true)
                            .pop(); // To close the dialog
                      },
                    ));
          }
        } else if (state is ValidationError) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
          showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: state.errors['password'][0].toString(),
                    buttonText: "OK",
                    onOkPressed: () {
                      Navigator.of(context, rootNavigator: true)
                          .pop(); // To close the dialog
                    },
                  ));
        } else {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
        }
      },
      child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
        bloc: _changePasswordBloc,
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(Dictionary.changePassword),
              centerTitle: true,
            ),
            body: WillPopScope(
                onWillPop: _onBackPressed,
                child: Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 20.0),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 65.0),
                            child: SingleChildScrollView(
                              padding: EdgeInsets.only(bottom: 65.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  widget.type == ChangePasswordType.profile
                                      ? _currentPasswordField()
                                      : Container(),
                                  Text(
                                    Dictionary.labelPasswordNew,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  PasswordFormField(
                                    controller: _newPassController,
                                    showIcon: false,
                                    hasFloatingPlaceholder: false,
                                    hintText: Dictionary.hintPasswordNew,
                                    validator: (val) =>
                                        Validations.passwordValidation(val),
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  Text(
                                    Dictionary.labelPasswordNewConfirm,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  PasswordFormField(
                                    controller: _confirmPassController,
                                    showIcon: false,
                                    hasFloatingPlaceholder: false,
                                    hintText: Dictionary.hintPasswordNew,
                                    validator: (val) =>
                                        Validations.confirmPasswordValidation(
                                            _newPassController.text, val),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: RoundedButton(
                              title: Dictionary.save,
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.green,
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .button
                                  .copyWith(color: Colors.white),
                              onPressed: () {
                                _onSaveButtonPressed();
                              },
                            ),
                          )
                        ],
                      ),
                    ))),
          );
        },
      ),
    );
  }

  Widget _currentPasswordField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          Dictionary.labelPasswordOld,
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 5.0,
        ),
        PasswordFormField(
          controller: _oldPassController,
          showIcon: false,
          hasFloatingPlaceholder: false,
          hintText: Dictionary.hintPasswordOld,
          validator: (val) => Validations.passwordValidation(val),
        ),
        SizedBox(
          height: 10.0,
        ),
        FutureBuilder<RemoteConfig>(
          future: _initializeRemoteConfig(),
          builder: (BuildContext context, AsyncSnapshot<RemoteConfig> snapshot) {
            if (snapshot.hasData) {
              final dataPhoneNumbers = snapshot.data.getString(
                  FirebaseConfig.callCenterNumbersKey);
              if (dataPhoneNumbers != null) {
                csPhoneNumbers = json.decode(dataPhoneNumbers);
              }
            }

            return TextButton(
                title: Dictionary.forgotPassword,
                textStyle:
                TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
                onTap: _openDialogContact);
          },
        ),
        SizedBox(
          height: 30.0,
        ),
      ],
    );
  }

  _onSaveButtonPressed() {
    setState(() {
      _autoValidate = true;
    });

    if (_formKey.currentState.validate()) {
      _changePasswordBloc.add(SendChangedPassword(
          oldPass: widget.type == ChangePasswordType.profile
              ? _oldPassController.text
              : Environment.defaultPassword,
          newPass: _newPassController.text,
          confNewPass: _confirmPassController.text));
    } else {
      print("Validate Error");
    }
  }

  Future<bool> _onBackPressed() async {
    if (widget.type == ChangePasswordType.force) {
      await showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(Dictionary.confirmExitTitle),
              content: Text(Dictionary.confirmExit),
              actions: <Widget>[
                FlatButton(
                  child: Text(Dictionary.yes),
                  onPressed: () {
                    exit(0);
                  },
                ),
                FlatButton(
                  child: Text(Dictionary.cancel),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
      return false;
    } else {
      return true;
    }
  }

  _openDialogContact() {
    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
          title: Dictionary.callCenterList,
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Container(
                  margin: index != 0 ? EdgeInsets.only(top: 10.0):null,
                  padding: EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
                  decoration: BoxDecoration(
                    color: Color(0xffebf8ff),
                    shape: BoxShape.rectangle,
                    border: Border.all(color: clr.Colors.blue),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Wrap(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 10.0),
                        child: Image.asset('${Environment.iconAssets}whatsapp.png', width: 20, height: 20, color: clr.Colors.green),
                      ),
                      Text(csPhoneNumbers[index], style: TextStyle(fontFamily: FontsFamily.sourceSansPro, fontSize: 16.0, fontWeight: FontWeight.w600, color: clr.Colors.green))
                    ],
                  ),
                ),
                onTap: (){
                  _launchWhatsApp(csPhoneNumbers[index]);
                  Navigator.of(context).pop();
                },
              );
            },
          ),
          buttonText: Dictionary.close.toUpperCase(),
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ));
  }

  _launchWhatsApp(String csPhoneNumber) async {
    try {
      bool isInstalled = await DeviceApps.isAppInstalled('com.whatsapp');
      csPhoneNumber ??= Environment.csPhone;

      if (csPhoneNumber[0] == '0') {
        csPhoneNumber = csPhoneNumber.replaceFirst('0', '+62');
      }

      if (isInstalled) {
        String urlWhatsApp = 'https://wa.me/${csPhoneNumber}?text=${Uri.encodeFull(Dictionary.helpAdminWA)}%0A%0A';
        if (await canLaunch(urlWhatsApp)) {
          await launch(urlWhatsApp);

          await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_WA_ADMIN_CHANGE_PASSWORD);
        } else {
          await launch('tel://${csPhoneNumber}');
          await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_CHANGE_PASSWORD);
        }
      } else {
        await launch('tel://${csPhoneNumber}');
        await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_CHANGE_PASSWORD);
      }
    } catch (_) {
      await launch('tel://${csPhoneNumber}');
      await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_CHANGE_PASSWORD);
    }
  }

  @override
  void dispose() {
    _changePasswordBloc.close();
    _oldPassController.dispose();
    _newPassController.dispose();
    _confirmPassController.dispose();
    super.dispose();
  }
}
