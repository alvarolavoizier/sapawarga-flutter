import 'package:flutter/material.dart';
import 'package:sapawarga/screens/usulan/CategoryScreen.dart';

class AddUsulanScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Usulan"),
      ),
      body: CategoryScreen()
    );
  }
}

