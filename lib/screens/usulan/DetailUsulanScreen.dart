import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sapawarga/blocs/usulan/detailusulan/Bloc.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as prefix0;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/PopUpMenuChoiceUsulan.dart';
import 'package:sapawarga/constants/UsulanConst.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'EditUsulanScreen.dart';

class DetailUsulanScreen extends StatelessWidget {
  final UsulanRepository usulanRepository = UsulanRepository();

  final int idUsulan;
  final bool isUsulanSaya;

  DetailUsulanScreen({@required this.idUsulan, @required this.isUsulanSaya});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UsulanDetailBloc>(
      create: (context) => UsulanDetailBloc(usulanRepository: usulanRepository),
      child: DetailUsulan(idUsulan: idUsulan, isUsulanSaya: isUsulanSaya),
    );
  }
}

class DetailUsulan extends StatefulWidget {
  final int idUsulan;
  final bool isUsulanSaya;

  DetailUsulan({@required this.idUsulan, @required this.isUsulanSaya});

  @override
  _DetailUsulanState createState() => _DetailUsulanState();
}

class _DetailUsulanState extends State<DetailUsulan> {
  UsulanDetailBloc usulanDetailBloc;
  List<MasterCategoryModel> listCategoryUsulan = [];
  String note;
  ProgressDialog loadingDialog;
  String selectedCategoryUsulan;
  String titleUsulan;
  String descUsulan;
  List<AddPhotoModel> photoUsulan = [];
  String likeStat;
  UserInfoModel userInfo;
  int id;
  UsulanModel usulanModelDetail;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.PROPOSAL,
        screenClassOverride: 'UsulanScreen');

    usulanDetailBloc = BlocProvider.of<UsulanDetailBloc>(context);
    usulanDetailBloc.add(UsulanDetailLoad(usulanId: widget.idUsulan));
    getProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UsulanDetailBloc, UsulanDetailState>(
        bloc: usulanDetailBloc,
        builder: (context, state) {
          return Scaffold(
              body: Container(
            child: state is UsulanDetailLoading
                ? _buildLoading()
                : state is DeleteUsulanlLoading
                    ? _buildLoadingDeleteUsulan()
                    : state is UsulanDetailLoaded
                        ? _buildContent(state)
                        : state is DeleteUsulanLoaded
                            ? _buildDeleteContent(state)
                            : state is UsulanDetailFailure
                                ? _buildFailure(state)
                                : Container(),
          ));
        });
  }

  void choiceAction(String choice) {
    if (choice == PopUpMenuChoiceUsulan.EditUsulan) {
      var categoryUsulanModel;

      listCategoryUsulan.forEach((categorySelected) {
        if (categorySelected.name == selectedCategoryUsulan) {
          categoryUsulanModel = categorySelected;
        }
      });

      _navigateResult(context, categoryUsulanModel);
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(Dictionary.deleteUsulan),
              content: Text(Dictionary.confirmationDeleteUsulan),
              actions: <Widget>[
                FlatButton(
                  child: Text(Dictionary.cancel),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
                FlatButton(
                  child: Text(Dictionary.approve),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                    usulanDetailBloc
                        .add(DeleteUsulan(usulanId: widget.idUsulan));
                  },
                ),
              ],
            );
          });
    }
  }

  _buildLoadingDeleteUsulan() {
    _onWidgetDidBuild(() {
      showLoading();
    });
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  _buildDeleteContent(DeleteUsulanLoaded state) {
    hideLoading();
    Navigator.pop(context, Dictionary.successDeleteUsulan);
  }

  _buildContent(UsulanDetailLoaded state) {
    AnalyticsHelper.setLogEvent(
        Analytics.EVENT_VIEW_DETAIL_USULAN, <String, dynamic>{
      'id': state.usulanModel.id,
      'title': state.usulanModel.title,
      'category_id': state.usulanModel.category.id,
      'category_name': state.usulanModel.category.name,
    });

    photoUsulan.clear();
    usulanModelDetail = state.usulanModel;
    if (UsulanConst.isLikeUsulan(state.usulanModel.likesUsers, userInfo.name)) {
      likeStat = '${Environment.imageAssets}liked.png';
    } else {
      likeStat = '${Environment.imageAssets}like-empty.png';
    }

    id = state.usulanModel.id;
    listCategoryUsulan = state.categoryUsulan;
    note = state.usulanModel.approvalNote.toString();
    selectedCategoryUsulan = state.usulanModel.category.name;
    titleUsulan = state.usulanModel.title;
    descUsulan = state.usulanModel.description;
    if (state.usulanModel.attachments != null) {
      for (int i = 0; i < state.usulanModel.attachments.length; i++) {
        Data data = Data(
            path: state.usulanModel.attachments[i].path,
            url: state.usulanModel.attachments[i].url);
        photoUsulan.add(AddPhotoModel(data: data));
      }
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              state.usulanModel.statusLabel == Dictionary.ditolak &&
                          widget.isUsulanSaya ||
                      state.usulanModel.statusLabel == Dictionary.draft &&
                          widget.isUsulanSaya
                  ? AppBar(
                      title: Text(Dictionary.detailUsulan),
                      leading: IconButton(
                          icon: Icon(Icons.arrow_back, color: Colors.white),
                          onPressed: () {
                            Navigator.pop(context, usulanModelDetail);
                          }),
                    )
                  : AppBar(
                      title: Text(Dictionary.detailUsulan),
                      leading: IconButton(
                          icon: Icon(Icons.arrow_back, color: Colors.white),
                          onPressed: () {
                            Navigator.pop(context, usulanModelDetail);
                          }),
                    ),
              Container(
                margin: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 7),
                          width: 30,
                          height: 30,
                          child: CircleAvatar(
                            minRadius: 90,
                            maxRadius: 150,
                            backgroundImage:
                                state.usulanModel.author.photoUrlFull != null
                                    ? NetworkImage(
                                        state.usulanModel.author.photoUrlFull)
                                    : ExactAssetImage('assets/images/user.png'),
                          ),
                        ),
                        Text(
                          state.usulanModel.author.name != null
                              ? state.usulanModel.author.name
                              : Dictionary.anonim,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.black),
                        ),
                      ],
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 3.2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: CachedNetworkImage(
                              imageUrl: state.usulanModel.attachments != null
                                  ? state.usulanModel.attachments[0].url
                                  : "",
                              fit: BoxFit.fill,
                              placeholder: (context, url) => Center(
                                  heightFactor: 8.2,
                                  child: CupertinoActivityIndicator()),
                              errorWidget: (context, url, error) => Container(
                                  height:
                                      MediaQuery.of(context).size.height / 3.3,
                                  color: Colors.grey[200],
                                  child: Image.asset(
                                      '${Environment.imageAssets}placeholder.png',
                                      fit: BoxFit.fitWidth)),
                            ),
                          ),
                        ),
                        Positioned(
                          right: 10,
                          top: 20,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(6.0),
                            child: Container(
                              color: UsulanConst.getColor(
                                  state.usulanModel.statusLabel),
                              padding: EdgeInsets.all(8),
                              child: Text(
                                state.usulanModel.statusLabel,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    widget.isUsulanSaya
                        ? Container(
                            margin: EdgeInsets.only(top: 8, right: 8),
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              UsulanConst.getDatePublish(
                                          state.usulanModel.submittedAt,
                                          state.usulanModel.approvedAt,
                                          state.usulanModel.statusLabel) !=
                                      0
                                  ? unixTimeStampToDateTime(
                                      UsulanConst.getDatePublish(
                                          state.usulanModel.submittedAt,
                                          state.usulanModel.approvedAt,
                                          state.usulanModel.statusLabel))
                                  : "",
                              textAlign: TextAlign.right,
                              style:
                                  TextStyle(fontSize: 12.0, color: Colors.grey),
                            ),
                          )
                        : Container(
                            width: MediaQuery.of(context).size.width,
                            margin:
                                EdgeInsets.only(right: 10, top: 10, bottom: 10),
                            child: Stack(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        usulanDetailBloc.add(UsulanDetailLike(
                                            id: state.usulanModel.id));
                                        if (UsulanConst.isLikeUsulan(
                                            state.usulanModel.likesUsers,
                                            userInfo.name)) {
                                          updateLike(state.usulanModel, true);

                                          AnalyticsHelper.setLogEvent(
                                              Analytics.EVENT_UNLIKE_USULAN,
                                              <String, dynamic>{
                                                'id': state.usulanModel.id,
                                                'title': state.usulanModel.title
                                              });
                                        } else {
                                          updateLike(state.usulanModel, false);

                                          AnalyticsHelper.setLogEvent(
                                              Analytics.EVENT_LIKE_USULAN,
                                              <String, dynamic>{
                                                'id': state.usulanModel.id,
                                                'title': state.usulanModel.title
                                              });
                                        }
                                      },
                                      child: Image.asset(
                                        likeStat,
                                        width: 20.0,
                                        height: 20.0,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        state.usulanModel.likesCount.toString(),
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.black),
                                      ),
                                    ),
                                  ],
                                ),
                                Positioned(
                                  right: 0,
                                  child: Text(
                                    UsulanConst.getDatePublish(
                                                state.usulanModel.submittedAt,
                                                state.usulanModel.approvedAt,
                                                state
                                                    .usulanModel.statusLabel) !=
                                            0
                                        ? unixTimeStampToDateTime(
                                            UsulanConst.getDatePublish(
                                                state.usulanModel.submittedAt,
                                                state.usulanModel.approvedAt,
                                                state.usulanModel.statusLabel))
                                        : "",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        fontSize: 12.0, color: Colors.grey),
                                  ),
                                )
                              ],
                            ),
                          ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 5),
                      child: Text(
                        state.usulanModel.category.name,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.green,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        state.usulanModel.title.toString(),
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        state.usulanModel.description.toString(),
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 14.0, color: Colors.black),
                      ),
                    ),
                    state.usulanModel.statusLabel != Dictionary.draft &&
                            state.usulanModel.approvalNote != null
                        ? Container(
                            margin: EdgeInsets.only(top: 25),
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              'Catatan',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        : Container(),
                    state.usulanModel.statusLabel != Dictionary.draft &&
                            state.usulanModel.approvalNote != null
                        ? Container(
                            margin: EdgeInsets.only(top: 8),
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              state.usulanModel.approvalNote.toString(),
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        : Container(),
                    widget.isUsulanSaya
                        ? Container()
                        : Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 20),
                            child: Text(
                              state.usulanModel.kabkota.name +
                                  "," +
                                  state.usulanModel.kecamatan.name +
                                  "," +
                                  state.usulanModel.kelurahan.name,
                              style:
                                  TextStyle(fontSize: 12.0, color: Colors.grey),
                            ),
                          ),
                    state.usulanModel.statusLabel == Dictionary.ditolak &&
                                widget.isUsulanSaya ||
                            state.usulanModel.statusLabel == Dictionary.draft &&
                                widget.isUsulanSaya
                        ? Container(
                            height: MediaQuery.of(context).size.height / 3.3,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.center,
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(top: 10),
                                  child: RoundedButton(
                                    title: Dictionary.editUsulan,
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: prefix0.Colors.blue,
                                    textStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    onPressed: () {
                                      choiceAction(
                                          PopUpMenuChoiceUsulan.EditUsulan);
                                    },
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.only(top: 10),
                                  child: OutlineButton(
                                      borderSide:
                                          BorderSide(color: Colors.grey[600]),
                                      padding: EdgeInsets.all(12.0),
                                      child: Text(
                                        Dictionary.deleteUsulan,
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      onPressed: () {
                                        choiceAction(
                                            PopUpMenuChoiceUsulan.DeleteUsulan);
                                      },
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5.0))),
                                )
                              ],
                            ),
                          )
                        : Container()
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buildLoading() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            AppBar(
              title: Text(Dictionary.detailUsulan),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Skeleton(
                        margin: 10,
                        width: 30,
                        height: 30,
                        child: CircleAvatar(
                          minRadius: 20,
                          maxRadius: 20,
                        ),
                      ),
                      Skeleton(
                        margin: 3,
                        width: MediaQuery.of(context).size.width / 1.3,
                        height: 15,
                      ),
                    ],
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 3.2,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Skeleton(
                              height: 25,
                              width: MediaQuery.of(context).size.width,
                              margin: 5),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: <Widget>[
                        Skeleton(
                          width: 20,
                          height: 20,
                          margin: 4,
                        ),
                        Skeleton(
                          width: 40,
                          height: 20,
                        )
                      ],
                    ),
                  ),
                  Skeleton(
                    width: MediaQuery.of(context).size.width,
                    margin: 5,
                    height: 20,
                  ),
                  Skeleton(
                    width: MediaQuery.of(context).size.width,
                    margin: 5,
                    height: 20,
                  ),
                  Skeleton(
                    width: MediaQuery.of(context).size.width,
                    margin: 5,
                    height: 20,
                  ),
                  Skeleton(
                    width: MediaQuery.of(context).size.width,
                    margin: 4,
                    height: 10,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildFailure(UsulanDetailFailure state) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Dictionary.detailUsulan),
      ),
      body: ErrorContent(error: state.error),
    );
  }

  @override
  void dispose() {
    usulanDetailBloc.close();
    super.dispose();
  }

  void hideLoading() {
    loadingDialog.hide().then((isHidden) {
      print(isHidden);
    });
  }

  _navigateResult(
      BuildContext context, MasterCategoryModel categoryUsulanModel) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditUsulanScreen(
                categorySelected: categoryUsulanModel,
                listCategoryUsulanModel: listCategoryUsulan,
                titleUsulan: titleUsulan,
                descUsulan: descUsulan,
                photoUsulan: photoUsulan,
                id: id)));
    if (result == Dictionary.successSendUsulan) {
      await Preferences.setMyUsulanStat(true);
      usulanDetailBloc.add(UsulanDetailLoad(usulanId: widget.idUsulan));
    } else if (result == Dictionary.successSaveDraft) {
      usulanDetailBloc.add(UsulanDetailLoad(usulanId: widget.idUsulan));
      await Preferences.setMyUsulanStat(true);
    }
  }

  void getProfile() async {
    userInfo = await AuthProfileRepository().getUserInfo();
  }

  void updateLike(UsulanModel usulanModel, bool isLike) {
    if (isLike) {
      for (int i = 0; i < usulanModel.likesUsers.length; i++) {
        if (usulanModel.likesUsers[i].name == userInfo.name) {
          setState(() {
            usulanModel.likesUsers.removeAt(i);
            usulanModel.likesCount = usulanModel.likesCount - 1;
            usulanModelDetail = usulanModel;
          });
        }
      }
    } else {
      Category category = Category(id: 99, name: userInfo.name);

      setState(() {
        usulanModel.likesCount = usulanModel.likesCount + 1;
        usulanModel.likesUsers.add(category);
        usulanModelDetail = usulanModel;
      });
    }
  }

  Future<bool> _onWillPop() {
    Navigator.pop(context, usulanModelDetail);
    return Future.value();
  }

  void showLoading() {
    loadingDialog = ProgressDialog(context);
    loadingDialog.style(
        message: 'Loading...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    loadingDialog.show();
  }
}
