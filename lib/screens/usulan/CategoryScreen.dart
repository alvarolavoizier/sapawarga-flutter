import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/usulan/categoryusulan/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:sapawarga/screens/usulan/CreateUsulanScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';

class CategoryScreen extends StatelessWidget {
  final UsulanRepository usulanRepository = UsulanRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddCategoryUsulanBloc>(
      create: (context) =>
          AddCategoryUsulanBloc(usulanRepository: usulanRepository),
      child: AddUsulan(),
    );
  }
}

class AddUsulan extends StatefulWidget {
  @override
  _AddUsulanState createState() => _AddUsulanState();
}

class _AddUsulanState extends State<AddUsulan> {
  AddCategoryUsulanBloc _addUsulanBloc;
  int _page = 1;
  bool swap = false;
  MasterCategoryModel categoryUsulanModel, categoryUsulanModelLainyya;

  @override
  void initState() {
    _addUsulanBloc = BlocProvider.of<AddCategoryUsulanBloc>(context);
    _addUsulanBloc.add(AddCategoryUsulanLoad(page: _page));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AddCategoryUsulanBloc, AddCategoryUsulantState>(
        bloc: _addUsulanBloc,
        builder: (context, state) {
          return Container(
              child: state is AddCategoryUsulanLoading
                  ? _buildContentLoading()
                  : state is AddCategoryUsulanLoaded
                      ? state.records.isNotEmpty
                          ? _buildContent(state)
                          : EmptyData(message: Dictionary.empty)
                      : state is AddCategoryUsulanFailure
                          ? _buildContentFailure(state)
                          : Container());
        });
  }

  _buildContentLoading() {
    return Container(
      child: Container(
        margin: EdgeInsets.only(top: 10, left: 10, right: 10),
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.topLeft,
              width: MediaQuery.of(context).size.width,
              child: Skeleton(
                height: 20,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
              child: Row(
                children: <Widget>[
                  Skeleton(
                      width: MediaQuery.of(context).size.width / 1.2,
                      height: 30),
                  Skeleton(width: 30, height: 30, margin: 5),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 1.33,
              child: GridView.count(
                crossAxisCount: 3,
                children: List.generate(10, (index) {
                  return Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Column(
                      children: <Widget>[
                        Skeleton(
                            height: MediaQuery.of(context).size.height / 10,
                            width: MediaQuery.of(context).size.width,
                            margin: 5),
                        Skeleton(
                          height: 20,
                          width: MediaQuery.of(context).size.width,
                          margin: 5,
                        )
                      ],
                    ),
                  );
                }),
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildContent(AddCategoryUsulanLoaded state) {
    return state.records.isNotEmpty
        ? !swap
            ? SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          Dictionary.chooseCategory,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: TextField(
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 8),
                                  hintText: Dictionary.chooseCategory,
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.blue)),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 8,
                              child: RaisedButton(
                                onPressed: () {},
                                child: Icon(Icons.search, color: Colors.white),
                                color: Colors.blue,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        child: GridView.count(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          primary: true,
                          crossAxisCount: 3,
                          children:
                              List.generate(state.records.length, (index) {
                            return Container(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    swap = true;
                                    categoryUsulanModel = state.records[index];

                                    AnalyticsHelper.setLogEvent(
                                        Analytics.EVENT_TAPPED_CATEGORY_USULAN,
                                        <String, dynamic>{
                                          'id': state.records[index].id,
                                          'name': state.records[index].name
                                        });
                                  });
                                },
                                child: Column(
                                  children: <Widget>[
                                    CachedNetworkImage(
                                      imageUrl: "",
                                      fit: BoxFit.fill,
                                      placeholder: (context, url) => Center(
                                          child: CupertinoActivityIndicator()),
                                      errorWidget: (context, url, error) =>
                                          Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  10.5,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  3.3,
                                              color: Colors.grey[200],
                                              child: Image.asset(
                                                  '${Environment.imageAssets}placeholder.png',
                                                  fit: BoxFit.fitWidth)),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 5),
                                      child: Text(
                                        state.records[index].name,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 14.0,
                                        ),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }),
                        ),
                      )
                    ],
                  ),
                ),
              )
            : Container(
                child: CreateUsulanScreen(
                    categorySelected: categoryUsulanModel,
                    listCategoryUsulanModel: state.records),
              )
        : Container();
  }

  _buildContentFailure(AddCategoryUsulanFailure state) {
    return ErrorContent(error: state.error);
  }
}
