import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sapawarga/blocs/usulan/addusulan/AddUsulanBloc.dart';
import 'package:sapawarga/blocs/usulan/addusulan/AddUsulanEvent.dart';
import 'package:sapawarga/blocs/usulan/addusulan/AddUsulanState.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as prefix0;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:pedantic/pedantic.dart';
import 'package:sapawarga/components/custom_dropdown.dart' as custom;

final _formKey = GlobalKey<FormState>();

// ignore: must_be_immutable
class CreateUsulanScreen extends StatelessWidget {
  final UsulanRepository usulanRepository = UsulanRepository();
  final MasterCategoryModel categorySelected;
  final List<MasterCategoryModel> listCategoryUsulanModel;
  final String titleUsulan;
  final String descUsulan;
  List<AddPhotoModel> photoUsulan;
  final int id;
  Widget _form;

  CreateUsulanScreen(
      {this.categorySelected,
      this.listCategoryUsulanModel,
      this.titleUsulan,
      this.descUsulan,
      this.photoUsulan,
      this.id});

  Widget _createForm(BuildContext context) {
    return BlocProvider<AddUsulanBloc>(
        create: (context) => AddUsulanBloc(usulanRepository: usulanRepository),
        child: _CreateUsulan(
          category: categorySelected,
          listCategoryUsulanModel: listCategoryUsulanModel,
          titleUsulan: titleUsulan,
          descUsulan: descUsulan,
          photoUsulan: photoUsulan,
          id: id,
        ));
  }

  @override
  Widget build(BuildContext context) {
    if (_form == null) {
      // Create the form if it does not exist
      _form = _createForm(context); // Build the form
    }
    return _form;
  }
}

// ignore: must_be_immutable
class _CreateUsulan extends StatefulWidget {
  final MasterCategoryModel category;
  final List<MasterCategoryModel> listCategoryUsulanModel;
  final String titleUsulan;
  final String descUsulan;
  List<AddPhotoModel> photoUsulan;
  final int id;

  _CreateUsulan({
    this.category,
    this.listCategoryUsulanModel,
    this.titleUsulan,
    this.descUsulan,
    this.photoUsulan,
    this.id,
  });

  @override
  __CreateUsulanState createState() => __CreateUsulanState();
}

class __CreateUsulanState extends State<_CreateUsulan> {
  final _titleUsulanController = TextEditingController();
  final _descController = TextEditingController();
  List<AddPhotoModel> listPhoto = [];
  List<custom.DropdownMenuItem<String>> _dropDownMenuItems;
  AddUsulanBloc _addUsulanBloc;
  ProgressDialog loadingDialog;
  String _currentSelectedSpinner;
  bool _autoValidate = false;
  bool isShowDialog = false;
  bool isDeletePhoto = false;
  int statusBack;

  @override
  initState() {
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_FORM_USULAN);

    if (widget.titleUsulan != null) {
      _titleUsulanController.text = widget.titleUsulan;
    }

    if (widget.descUsulan != null) {
      _descController.text = widget.descUsulan;
    }

    if (widget.photoUsulan != null) {
      listPhoto.clear();
      listPhoto = widget.photoUsulan;
    }

    _addUsulanBloc = BlocProvider.of<AddUsulanBloc>(context);
    _dropDownMenuItems = getDropDownMenuItems();
    _currentSelectedSpinner = widget.category.name;
    super.initState();
  }

  List<custom.DropdownMenuItem<String>> getDropDownMenuItems() {
    List<custom.DropdownMenuItem<String>> items = List();

    for (int i = 0; i < widget.listCategoryUsulanModel.length; i++) {
      items.add(custom.DropdownMenuItem(
          value: widget.listCategoryUsulanModel[i].name,
          child: Text(
            widget.listCategoryUsulanModel[i].name,
            style: TextStyle(color: Colors.grey),
          )));
    }
    return items;
  }

  void changedDropDownItem(String selectedCategory) {
    setState(() {
      _currentSelectedSpinner = selectedCategory;
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() async {
      if (_descController.text.isNotEmpty ||
          _titleUsulanController.text.isNotEmpty) {
        return showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(Dictionary.aspiration),
                content: Text(Dictionary.confirmDraft),
                actions: <Widget>[
                  FlatButton(
                    child: Text(Dictionary.cancel),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                  ),
                  FlatButton(
                    child: Text(Dictionary.approve),
                    onPressed: () {
                      _onSubmitButtonPressed('0');
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            });
      } else {
        Navigator.pop(context);
        return false;
      }
    }

    return BlocBuilder(
        bloc: _addUsulanBloc,
        builder: (BuildContext context, AddUsulantState state) {
          if (state is AddUsulanUpdated) {
            hideLoading();
            if (statusBack != null) {
              if (statusBack == 0) {
                Navigator.pop(context, Dictionary.successSaveDraft);
              } else {
                Navigator.pop(context, Dictionary.successSendUsulan);
              }
            }
          }

          if (state is AddUsulanLoading) {
            _onWidgetDidBuild(() {
              showLoading();
            });
            isDeletePhoto = false;
          }

          if (state is AddUsulanPhotoDone) {
            hideLoading();

            if (!isDeletePhoto) {
              if (state.addPhotoModel.data != null) {
                if (!listPhoto.contains(state.addPhotoModel)) {
                  listPhoto.add(state.addPhotoModel);
                }
              }
            }
          }

          if (state is AddUsulanPhotoFailure) {
            hideLoading();
            Future.delayed(const Duration(milliseconds: 100), () {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(Dictionary.offline),
                ),
              );
            });
          }

          return WillPopScope(
            onWillPop: _onWillPop,
            child: SingleChildScrollView(
              child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      listPhoto.isNotEmpty
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              height: 100,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ListView.builder(
                                        shrinkWrap: true,
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        itemCount: listPhoto.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Container(
                                            margin: EdgeInsets.only(right: 8),
                                            child: Stack(
                                              children: <Widget>[
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  child: CachedNetworkImage(
                                                    height: 100,
                                                    width: 100,
                                                    fit: BoxFit.fill,
                                                    imageUrl: listPhoto[index]
                                                        .data
                                                        .url,
                                                    placeholder: (context,
                                                            url) =>
                                                        Center(
                                                            child:
                                                                CupertinoActivityIndicator()),
                                                    errorWidget: (context, url,
                                                            error) =>
                                                        Container(
                                                            height: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height /
                                                                9,
                                                            color:
                                                                Colors.red[200],
                                                            child: Image.asset(
                                                                '${Environment.imageAssets}placeholder.png',
                                                                fit: BoxFit
                                                                    .fitWidth)),
                                                  ),
                                                ),
                                                Positioned(
                                                  bottom: 0,
                                                  right: 0,
                                                  child: Container(
                                                    padding: EdgeInsets.all(5),
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          listPhoto
                                                              .removeAt(index);
                                                          widget.photoUsulan =
                                                              listPhoto;
                                                          isDeletePhoto = true;
                                                        });
                                                      },
                                                      child: Image.asset(
                                                          '${Environment.imageAssets}delete.png',
                                                          width: 20,
                                                          height: 20,
                                                          fit: BoxFit.fitWidth),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        }),
                                    Container(
                                      child: listPhoto.length < 5
                                          ? GestureDetector(
                                              onTap: () {
                                                _cameraBottomSheet(context);
                                              },
                                              child: Image.asset(
                                                  '${Environment.imageAssets}addphoto.png',
                                                  width: 25,
                                                  height: 25,
                                                  fit: BoxFit.fitWidth),
                                            )
                                          : Container(),
                                    )
                                  ],
                                ),
                              ),
                            )
                          : GestureDetector(
                              onTap: () {
                                _cameraBottomSheet(context);
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Container(
                                      color: Colors.grey,
                                      width:
                                          MediaQuery.of(context).size.width / 4,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              7,
                                      child: Container(
                                        child: Center(
                                          child: Image.asset(
                                              '${Environment.imageAssets}image.png',
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  9,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  9,
                                              fit: BoxFit.fitWidth),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width /
                                          1.5,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(right: 3),
                                            child: Image.asset(
                                              '${Environment.imageAssets}upload.png',
                                              height: 15,
                                              width: 20,
                                            ),
                                          ),
                                          Text(
                                            Dictionary.uploadImage,
                                            style: TextStyle(
                                              color: Colors.grey[600],
                                              fontSize: 16.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                      Container(
                        child: Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: Column(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(top: 14),
                                  width: MediaQuery.of(context).size.width,
                                  child: Text(
                                    Dictionary.category,
                                    style: TextStyle(
                                      color: Colors.grey[600],
                                      fontSize: 16.0,
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                              Container(
                                  margin: EdgeInsets.only(top: 10),
                                  padding: EdgeInsets.only(left: 10),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  width: MediaQuery.of(context).size.width,
                                  child: custom.DropdownButtonHideUnderline(
                                    child: custom.DropdownButton(
                                      isExpanded: true,
                                      height: 300,
                                      value: _currentSelectedSpinner,
                                      onChanged: changedDropDownItem,
                                      items: _dropDownMenuItems,
                                    ),
                                  )),
                              Container(
                                padding: EdgeInsets.only(
                                    right: 10.0, top: 14, bottom: 10),
                                width: MediaQuery.of(context).size.width,
                                child: Text(Dictionary.addTitleUsulan,
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontSize: 16.0)),
                              ),
                              TextFormField(
                                validator: Validations.titleUsulanValidation,
                                controller: _titleUsulanController,
                                style: TextStyle(color: Colors.grey[600]),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 14),
                                  hintText: Dictionary.typeUsulan,
                                  hintStyle: TextStyle(color: Colors.grey),
                                  border: OutlineInputBorder(),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    right: 10.0, top: 14, bottom: 10),
                                width: MediaQuery.of(context).size.width,
                                child: Text(Dictionary.desc,
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontSize: 16.0)),
                              ),
                              TextFormField(
                                style: TextStyle(color: Colors.grey[600]),
                                validator: Validations.descUsulanValidation,
                                keyboardType: TextInputType.multiline,
                                maxLines: 10,
                                controller: _descController,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 12),
                                  hintText: "",
                                  border: OutlineInputBorder(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          widget.id == null
                              ? Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.2,
                                  margin: EdgeInsets.only(top: 10),
                                  padding: EdgeInsets.only(right: 7),
                                  child: OutlineButton(
                                      borderSide:
                                          BorderSide(color: Colors.grey[600]),
                                      padding: EdgeInsets.all(12.0),
                                      child: Text(
                                        Dictionary.saveDraft,
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      onPressed: () {
                                        // status 0 = draft
                                        _onSubmitButtonPressed('0');
                                      },
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5.0))),
                                )
                              : Container(),
                          widget.id == null
                              ? Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.2,
                                  margin: EdgeInsets.only(top: 10),
                                  padding: EdgeInsets.only(left: 7),
                                  child: RoundedButton(
                                    title: Dictionary.send,
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: prefix0.Colors.blue,
                                    textStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    onPressed: () {
                                      //status 5 = send but confirmation waiting
                                      _onSubmitButtonPressed('5');
                                    },
                                  ),
                                )
                              : Container(),
                          widget.id != null
                              ? Container(
                                  alignment: Alignment.center,
                                  width:
                                      MediaQuery.of(context).size.width / 1.1,
                                  margin: EdgeInsets.only(top: 10),
                                  padding: EdgeInsets.only(left: 7),
                                  child: RoundedButton(
                                    title: Dictionary.editUsulan,
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: Colors.blue,
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .button
                                        .copyWith(color: Colors.white),
                                    onPressed: () {
                                      _popUpConfirmationSendEdit();
                                    },
                                  ),
                                )
                              : Container()
                        ],
                      )
                    ],
                  )),
            ),
          );
        });
  }

  _onSubmitButtonPressed(String status, [bool isEdit]) {
    String selectedIdCategory;
    String selectedNameCategory;
    if (_formKey.currentState.validate()) {
      for (int i = 0; i < widget.listCategoryUsulanModel.length; i++) {
        if (widget.listCategoryUsulanModel[i].name == _currentSelectedSpinner) {
          selectedIdCategory = widget.listCategoryUsulanModel[i].id.toString();
          selectedNameCategory =
              widget.listCategoryUsulanModel[i].name.toString();
        }
      }

      setState(() {
        statusBack = int.parse(status);
      });

      _addUsulanBloc.add(AddUsulanSubmit(
          title: _titleUsulanController.text,
          description: _descController.text,
          status: status,
          categoryId: selectedIdCategory,
          attachment: listPhoto,
          id: widget.id));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }

    AnalyticsHelper.setLogEvent(
        int.parse(status) == 5 && isEdit == null
            ? Analytics.EVENT_CREATE_USULAN
            : isEdit == true
                ? Analytics.EVENT_EDIT_USULAN
                : Analytics.EVENT_CREATE_USULAN_DRAFT,
        <String, dynamic>{
          'title': _titleUsulanController.text,
          'description': _descController.text,
          'status': status,
          'category_id': selectedIdCategory,
          'category_name': selectedNameCategory,
        });
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  void hideLoading() {
    loadingDialog.hide().then((isHidden) {
      print(isHidden);
    });
  }

  Future openCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 640, maxWidth: 640);
    File _image;

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        setState(() {
          _image = image;
          _addUsulanBloc.add(AddPhotoSubmit(image: _image));
        });
      }
    }

    isShowDialog = true;
    Navigator.of(context, rootNavigator: true).pop('dialog');
  }

  Future openGallery() async {
    File _image;
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 640, maxWidth: 640);

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        setState(() {
          _image = image;
          _addUsulanBloc.add(AddPhotoSubmit(image: _image));
        });
      }
    }

    isShowDialog = true;
    Navigator.of(context, rootNavigator: true).pop('dialog');
  }

  void _cameraBottomSheet(context) {
    if (listPhoto.length < 5) {
      showModalBottomSheet(
          context: context,
          builder: (BuildContext bc) {
            return Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.camera),
                    title: Text(Dictionary.takePhoto),
                    onTap: () {
                      _permissionCamera();
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.image),
                    title: Text(Dictionary.takePhotoFromGallery),
                    onTap: () {
                      _permissionGallery();
                    },
                  ),
                ],
              ),
            );
          });
    } else {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('Anda hanya dapat menyertakan maksimal 5 foto saja'),
        ),
      );
    }
  }

  void _popUpConfirmationSendEdit() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(Dictionary.confirm),
            content: Text(Dictionary.confirmSendEditUsulan),
            actions: <Widget>[
              FlatButton(
                child: Text(Dictionary.cancel),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text(Dictionary.approve),
                onPressed: () {
                  _onSubmitButtonPressed('5', true);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  void _permissionCamera() async {
    PermissionStatus permission =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/photo-camera.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionCamera,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.camera]).then(_onStatusRequestedCamera);
                },
              )));
    } else {
      await openCamera();
    }
  }

  void _permissionGallery() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionGalery,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.storage]).then(_onStatusRequested);
                },
              )));
    } else {
      await openGallery();
    }
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      _permissionGallery();
    }
  }

  void _onStatusRequestedCamera(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.camera];
    if (status == PermissionStatus.granted) {
      _permissionCamera();
    }
  }

  void showLoading() {
    loadingDialog = ProgressDialog(context);
    loadingDialog.style(
        message: 'Loading...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    loadingDialog.show();
  }

  @override
  void dispose() {
    _titleUsulanController.dispose();
    _descController.dispose();
    _addUsulanBloc.close();
    super.dispose();
  }
}
