import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sapawarga/blocs/polling_detail/Bloc.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/PollingModel.dart';
import 'package:sapawarga/repositories/PollingRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';

class PollingDetailScreen extends StatelessWidget {
  final PollingRepository pollingRepository = PollingRepository();
  final PollingModel record;

  PollingDetailScreen({this.record});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PollingDetailBloc>(
      create: (context) =>
          PollingDetailBloc(pollingRepository: pollingRepository),
      child: PollingDetail(record: record),
    );
  }
}

class PollingDetail extends StatefulWidget {
  final PollingModel record;

  PollingDetail({this.record});

  @override
  _PollingDetailState createState() => _PollingDetailState();
}

class _PollingDetailState extends State<PollingDetail> {
  PollingDetailBloc _pollingDetailBloc;
  int _rgProgramming = -1;
  int _selectedValue;
  String _selectedText;

  final List<Answer> _programmingList = [];

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.POLLING,
        screenClassOverride: 'PollingScreen');
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_DETAIL_POLLING,
        <String, dynamic>{'id': widget.record.id, 'name': widget.record.name});

    for (var answer in widget.record.answers) {
      _programmingList.add(Answer(id: answer.id, body: answer.body));
    }

    _pollingDetailBloc = BlocProvider.of<PollingDetailBloc>(context);
    _pollingDetailBloc.add(PollingDetailLoad(record: widget.record));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Dictionary.polling),
      ),
      body: _buildContent(),
    );
  }

  _buildContent() {
    return BlocListener<PollingDetailBloc, PollingDetailState>(
      listener: (context, state) {
        if (state is PollingDetailVoted) {
          if (state.status == 200 || state.status == 422) {
            Fluttertoast.showToast(
                msg: state.status == 200
                    ? Dictionary.pollingVotedSuccessMessage
                    : Dictionary.pollingHasVotedMessage,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.TOP,
                backgroundColor: Colors.green,
                textColor: Colors.white);
            Navigator.of(context).pop();
          }
        }
      },
      child: BlocBuilder<PollingDetailBloc, PollingDetailState>(
        builder: (context, state) {
          return state is PollingDetailLoaded
              ? _buildCard()
              : state is PollingDetailFailure
                  ? ErrorContent(error: state.error)
                  : Container();
        },
      ),
    );
  }

  Widget _buildCard() {
    return Card(
      margin: EdgeInsets.all(10.0),
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            Text(widget.record.question,
                style: TextStyle(color: Colors.black, fontSize: 15.0)),
            SizedBox(height: 15.0),
            _buildRadioButton(),
            SizedBox(height: 15.0),
            Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                  child: Text(Dictionary.send.toUpperCase()),
                  onPressed: () {
                    _sendVote();
                  }),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildRadioButton() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _programmingList
          .map((programming) => RadioListTile(
                title: Text(programming.body),
                value: programming.id,
                groupValue: _rgProgramming,
                dense: true,
                onChanged: (value) {
                  setState(() {
                    _rgProgramming = value;
                    _selectedValue = programming.id;
                    _selectedText = programming.body;
                  });
                },
              ))
          .toList(),
    );
  }

  void _sendVote() {
    if (_selectedValue == null) {
      Fluttertoast.showToast(
          msg: "Silakan pilih jawaban Anda",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      _pollingDetailBloc.add(PollingDetailSendVote(
          pollingId: widget.record.id, answerId: _selectedValue));

      AnalyticsHelper.setLogEvent(
          Analytics.EVENT_ANSWER_POLLING, <String, dynamic>{
        'id': widget.record.id,
        'name': widget.record.name,
        'id_answer': _selectedValue,
        'title_answer': _selectedText,
      });
    }
  }

  @override
  void dispose() {
    _pollingDetailBloc.close();
    super.dispose();
  }
}
