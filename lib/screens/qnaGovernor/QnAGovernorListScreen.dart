import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/qna_governor/Bloc.dart';
import 'package:sapawarga/components/Bubble.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/HelpQnaModel.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';
import 'package:sapawarga/screens/qnaGovernor/QnAGovernorAddScreen.dart';
import 'package:sapawarga/screens/qnaGovernor/QnAGovernorDetailScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/CustomPageRoute.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';

class QnAGovernorListScreen extends StatefulWidget {
  @override
  _QnAGovernorListScreenState createState() => _QnAGovernorListScreenState();
}

class _QnAGovernorListScreenState extends State<QnAGovernorListScreen> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  RefreshController _mainRefreshController = RefreshController();
  ScrollController _scrollController = ScrollController();
  List<QnAModel> _qnaList = List<QnAModel>();
  List<HelpQnaModel> qnaHelpModel = [];

  bool helpScreen;
  bool isLikeTooltip = false;

  QnAGovernorListBloc _listBloc;
  AuthenticationBloc _authenticationBloc;
  String animationThumbsUp = 'unlike';
  int _maxDataLength;
  int _page = 1;
  int indeksShowTooltip = 0;
  int current = 0;
  final GlobalKey _showcaseOne = GlobalKey();
  final GlobalKey _showcaseLast = GlobalKey();
  BuildContext showcaseContext;

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.QNA);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_QNA);

    _initialize();
    qnaHelpModel.add(HelpQnaModel(Dictionary.titleHelpQna1,
        '${Environment.imageAssets}helpqna1.jpg', Dictionary.deschelpQna1));
    qnaHelpModel.add(HelpQnaModel(Dictionary.titleHelpQna2,
        '${Environment.imageAssets}helpqna2.jpg', Dictionary.deschelpQna2));
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  _initialize() async {
    helpScreen = await Preferences.hasShowHelpQna();
    setState(() {
      helpScreen;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ShowCaseWidget(
      onFinish: () async {
        await Preferences.setShowcaseQna(true);
      },
      builder: Builder(
        builder: (context) {
          showcaseContext = context;
          return Scaffold(
            key: scaffoldKey,
            appBar: CustomAppBar()
                .DefaultAppBar(title: Dictionary.qnaGovernor, actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.info),
                  onPressed: () async {
                    if (helpScreen) {
                      await Preferences.setShowcaseQna(false);
                      await Preferences.setShowcaseQna2(false);

                      bool hasShown = await Preferences.hasShowcaseQna();

                      if (hasShown == null || hasShown == false) {
                        ShowCaseWidget.of(showcaseContext)
                            .startShowCase([_showcaseOne, _showcaseLast]);
                      }
                    }
                  })
            ]),
            body: Container(
              child: BlocProvider<QnAGovernorListBloc>(
                create: (context) =>
                    _listBloc = QnAGovernorListBloc(QnAGovernorRepository())
                      ..add(QnAGovernorListLoad(_page)),
                child: BlocListener<QnAGovernorListBloc, QnAGovernorListState>(
                  bloc: _listBloc,
                  listener: (context, state) {
                    if (state is QnAGovernorListLoaded) {
                      _page = _page + 1;
                      _qnaList.addAll(state.records);
                      _maxDataLength = state.maxLength;
                    } else if (state is QnAGovernorListFailure) {
                      if (state.error.contains(Dictionary.errorUnauthorized)) {
                        _authenticationBloc.add(LoggedOut());
                        Navigator.of(context).pop();
                      }
                    }
                  },
                  child: BlocBuilder<QnAGovernorListBloc, QnAGovernorListState>(
                      bloc: _listBloc,
                      builder: (context, state) => SmartRefresher(
                          controller: _mainRefreshController,
                          enablePullDown: true,
                          header: WaterDropMaterialHeader(),
                          onRefresh: () async {
                            _page = 1;
                            _qnaList.clear();
                            _listBloc.add(QnAGovernorListLoad(1));
                            _mainRefreshController.refreshCompleted();
                          },
                          child: state is QnAGovernorListLoading
                              ? _buildLoading()
                              : state is QnAGovernorListLoaded
                                  ? state.records.isNotEmpty
                                      ? _buildContent(state)
                                      : EmptyData(
                                          message: Dictionary.emptyDataQnA)
                                  : state is QnAGovernorListFailure
                                      ? ErrorContent(error: state.error)
                                      : _buildLoading())),
                ),
              ),
            ),
            floatingActionButton: _buildShowcase(
                key: _showcaseOne,
                context: context,
                widgets: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(bottom: 5),
                        alignment: Alignment.topLeft,
                        child: Text(
                          Dictionary.titleShowCaseQna2,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        Dictionary.showCaseRW1,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  )
                ],
                onOkTap: () {
                  ShowCaseWidget.of(context).completed(_showcaseOne);
                },
                child: helpScreen
                    ? FloatingActionButton.extended(
                        backgroundColor: clr.Colors.blue,
                        onPressed: _openCreateQnA,
                        icon: Icon(Icons.add),
                        label: Text(Dictionary.addQuestion),
                      )
                    : Container()),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
          );
        },
      ),
    );
  }

  ListView _buildLoading() {
    return ListView.separated(
      padding: EdgeInsets.only(bottom: 80.0),
      itemCount: 3,
      separatorBuilder: (context, index) => Container(
          width: MediaQuery.of(context).size.width,
          height: 1.0,
          color: Colors.grey[300]),
      itemBuilder: (context, index) {
        return Skeleton(
          child: Container(
            padding:
                EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  contentPadding: EdgeInsets.all(0.0),
                  leading: Container(
                    width: 40.0,
                    height: 40.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Container(
                          width: 40.0, height: 40.0, color: Colors.grey[300]),
                    ),
                  ),
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          height: 20.0,
                          color: Colors.grey[300]),
                      SizedBox(height: 2.0),
                      Container(
                          width: 50.0, height: 15.0, color: Colors.grey[300]),
                    ],
                  ),
                  trailing: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 6.0),
                          decoration: BoxDecoration(
                              color: clr.Colors.blue,
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Container(
                              width: 20.0,
                              height: 18.0,
                              color: Colors.grey[300]),
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 5.0),
                            width: 20.0,
                            height: 18.0,
                            color: Colors.grey[300]),
                      ],
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 56.0),
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                Container(
                    margin: EdgeInsets.only(left: 56.0, top: 2.0),
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                Container(
                    margin: EdgeInsets.only(left: 56.0, top: 2.0),
                    width: MediaQuery.of(context).size.width - 200,
                    height: 20.0,
                    color: Colors.grey[300]),
                Container(
                    margin: EdgeInsets.only(left: 56.0, top: 10.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                            width: 20.0, height: 20.0, color: Colors.grey[300]),
                        Container(
                            margin: EdgeInsets.only(left: 5.0),
                            width: 70.0,
                            height: 20.0,
                            color: Colors.grey[300]),
                        Container(
                            margin: EdgeInsets.only(left: 5.0),
                            width: MediaQuery.of(context).size.width - 200,
                            height: 15.0,
                            color: Colors.grey[300])
                      ],
                    )),
              ],
            ),
          ),
        );
      },
    );
  }

  _buildContent(QnAGovernorListLoaded state) {
    return Stack(
      children: <Widget>[
        ListView.separated(
          controller: _scrollController,
          padding: EdgeInsets.only(bottom: 80.0),
          itemCount: _qnaList.length + 1,
          separatorBuilder: (context, index) => Container(
              width: MediaQuery.of(context).size.width,
              height: 1.0,
              color: Colors.grey[300]),
          itemBuilder: (context, index) {
            return index >= _qnaList.length
                ? _maxDataLength != _qnaList.length
                    ? Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Column(
                          children: <Widget>[
                            CupertinoActivityIndicator(),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(Dictionary.loadingData),
                          ],
                        ),
                      )
                    : Container()
                : _buildItem(context, index);
          },
        ),
        !helpScreen ? helpQna() : Container()
      ],
    );
  }

  _buildItem(BuildContext context, int index) {
    bool isLiked = _qnaList[index].isLiked;

    if (isLiked) {
      animationThumbsUp = 'like';
    } else {
      animationThumbsUp = 'unlike';
    }

    if (_qnaList[index].isFlagged == 0 && !isLikeTooltip) {
      indeksShowTooltip = index;
      isLikeTooltip = true;
    }

    return GestureDetector(
      child: Container(
          width: MediaQuery.of(context).size.width,
          color: _qnaList[index].isHighlight
              ? Color(0xFFF8FFF8)
              : _qnaList[index].isFlagged > 0
                  ? Color(0xFFFFFAFA)
                  : Colors.grey[50],
          padding:
              EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: CachedNetworkImage(
                      imageUrl: _qnaList[index].user.photoUrlFull != null
                          ? _qnaList[index].user.photoUrlFull.toString()
                          : '',
                      fit: BoxFit.fill,
                      placeholder: (context, url) => Container(
                          color: Colors.grey[200],
                          child: Center(child: CupertinoActivityIndicator())),
                      errorWidget: (context, url, error) => Container(
                          color: Colors.grey[200],
                          child: Image.asset(
                              '${Environment.imageAssets}user.png',
                              fit: BoxFit.cover)),
                    ),
                  ),
                ),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _qnaList[index].user.name,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.productSans),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      unixTimeStampToTimeAgo(_qnaList[index].createdAt),
                      style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                    )
                  ],
                ),
                trailing: _qnaList[index].isFlagged == 0
                    ? GestureDetector(
                        child: index == indeksShowTooltip
                            ? _buildShowcase2(
                                key: _showcaseLast,
                                context: context,
                                widgets: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.only(bottom: 5),
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          Dictionary.titleShowCaseQna1,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      Text(
                                        Dictionary.showCaseRW2,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ],
                                  )
                                ],
                                onOkTap: () {
                                  ShowCaseWidget.of(context)
                                      .completed(_showcaseLast);
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 6.0),
                                      margin: EdgeInsets.only(top: 6),
                                      decoration: BoxDecoration(
                                          color: clr.Colors.blue,
                                          borderRadius:
                                              BorderRadius.circular(5.0)),
                                      child: Text(
                                          '${_qnaList[index].likesCount}',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                    Container(
                                      width: 30,
                                      height: 30,
                                      child: FlareActor(
                                        '${Environment.flareAssets}thumbs_up.flr',
                                        alignment: Alignment.center,
                                        fit: BoxFit.contain,
                                        animation: animationThumbsUp,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 6.0),
                                    margin: EdgeInsets.only(top: 6),
                                    decoration: BoxDecoration(
                                        color: clr.Colors.blue,
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    child: Text('${_qnaList[index].likesCount}',
                                        style: TextStyle(color: Colors.white)),
                                  ),
                                  Container(
                                    width: 30,
                                    height: 30,
                                    child: FlareActor(
                                      '${Environment.flareAssets}thumbs_up.flr',
                                      alignment: Alignment.center,
                                      fit: BoxFit.contain,
                                      animation: animationThumbsUp,
                                    ),
                                  )
                                ],
                              ),
                        onTap: _qnaList[index].isFlagged == 0
                            ? () {
                                _listBloc.add(
                                    QnAGovernorListLike(_qnaList[index].id));
                                if (isLiked) {
                                  _updateLike(
                                      _qnaList, _qnaList[index].id, true);
                                } else {
                                  _updateLike(
                                      _qnaList, _qnaList[index].id, false);
                                }
                              }
                            : null,
                      )
                    : SizedBox(height: 15.0),
              ),
              Container(
                  margin: EdgeInsets.only(left: 56.0),
                  child: Text(
                    '${_qnaList[index].text}',
                    style: TextStyle(fontSize: 15.0),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  )),
              _qnaList[index].answer != null
                  ? Container(
                      margin: EdgeInsets.only(left: 56.0, top: 10.0),
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        runSpacing: 5.0,
                        children: <Widget>[
                          Image.asset(
                            '${Environment.iconAssets}comment.png',
                            width: 20,
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 5.0),
                              child: Text(
                                Dictionary.governor,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: FontsFamily.sourceSansPro),
                                maxLines: 1,
                                overflow: TextOverflow.clip,
                              )),
                          Container(
                              margin: EdgeInsets.only(left: 5.0),
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                _qnaList[index].answer.text,
                                style: TextStyle(
                                    fontSize: 12.0,
                                    fontFamily: FontsFamily.sourceSansPro,
                                    color: Colors.grey[600]),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ))
                        ],
                      ))
                  : _qnaList[index].commentsCount > 0
                      ? Container(
                          margin: EdgeInsets.only(left: 56.0, top: 10.0),
                          child: Text(
                              '${_qnaList[index].commentsCount} Komentar',
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontFamily: FontsFamily.sourceSansPro,
                                  color: Colors.grey[600])))
                      : Container(),
              _qnaList[index].isFlagged != 0
                  ? Container(
                      margin: EdgeInsets.only(left: 56, top: 15.0),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.warning, color: Colors.red),
                          Container(
                            margin: EdgeInsets.only(left: 5),
                            width: MediaQuery.of(context).size.width - 120,
                            child: Text(
                              Dictionary.contentNotPermitted,
                              style:
                                  TextStyle(fontSize: 15.0, color: Colors.red),
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ))
                  : SizedBox(height: 15.0),
            ],
          )),
      onTap: () {
        _openDetailQnA(_qnaList[index].id);
      },
    );
  }

  _updateLike(List<QnAModel> records, int id, bool isLike) {
    for (int i = 0; i < records.length; i++) {
      if (records[i].id == id) {
        if (isLike) {
          if (records[i].isLiked) {
            setState(() {
              records[i].isLiked = false;
              records[i].likesCount = records[i].likesCount - 1;
            });
          }
        } else {
          setState(() {
            records[i].likesCount = records[i].likesCount + 1;
            records[i].isLiked = true;
          });
        }
      }
    }
  }

  _openCreateQnA() async {
    final result = await Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => QnAGovernorAddScreen()))
        as QnAModel;

    if (result != null) {
      _qnaList.insert(0, result);

      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                'Buat Pertanyaan',
                style: TextStyle(fontSize: 14.0),
              )),
          backgroundColor: clr.Colors.blue,
          duration: Duration(seconds: 2),
        ),
      );
    }
  }

  _openDetailQnA(int id) async {
    final result = await Navigator.of(context)
        .push(SizedPageRoute(QnAGovernorDetailScreen(id))) as QnAModel;
    if (result != null) {
      for (int i = 0; i < _qnaList.length; i++) {
        if (_qnaList[i].id == result.id) {
          setState(() {
            _qnaList[i] = result;
          });
        }
      }
    }
  }

  void _scrollListener() {
    if (_qnaList.length < _maxDataLength) {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _listBloc.add(QnAGovernorListLoad(_page));
      }
    }
  }

  Showcase _buildShowcase(
      {GlobalKey key,
      BuildContext context,
      List<Widget> widgets,
      Widget child,
      GestureTapCallback onOkTap}) {
    return Showcase.withWidget(
      shapeBorder: CircleBorder(),
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      key: key,
      disposeOnTap: false,
      container: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Bubble(
          nipLocation: NipLocation.BOTTOM,
          color: Colors.white,
          child: Container(
            width: MediaQuery.of(context).size.width - 50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  children: widgets,
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                    child: RoundedButton(
                        minWidth: 5,
                        height: 30,
                        color: clr.Colors.blue,
                        title: Dictionary.next,
                        onPressed: onOkTap,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)))
              ],
            ),
          ),
        ),
      ),
      child: child,
    );
  }

  Showcase _buildShowcase2(
      {GlobalKey key,
      BuildContext context,
      List<Widget> widgets,
      Widget child,
      GestureTapCallback onOkTap}) {
    return Showcase.withWidget(
      shapeBorder:  RoundedRectangleBorder(),
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      key: key,
      disposeOnTap: false,
      container: Container(
        margin: EdgeInsets.only(top: 10.0, left: 20),
        child: Bubble(
          nipLocation: NipLocation.TOP_RIGHT,
          color: Colors.white,
          child: Container(
            width: MediaQuery.of(context).size.width - 50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  children: widgets,
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                    child: RoundedButton(
                        minWidth: 5,
                        height: 30,
                        color: clr.Colors.blue,
                        title: Dictionary.next,
                        onPressed: onOkTap,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)))
              ],
            ),
          ),
        ),
      ),
      child: child,
    );
  }

  Widget helpQna() {
    CarouselSlider basicSlider;
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
      child: Container(
          alignment: Alignment.bottomCenter,
          color: Colors.black12.withOpacity(0.5),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Card(
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Stack(
                children: <Widget>[
                  basicSlider = CarouselSlider(
                      initialPage: 0,
                      height: 450,
                      enableInfiniteScroll: false,
                      viewportFraction: 1.0,
                      onPageChanged: (record) {
                        setState(() {
                          current = record;
                        });
                      },
                      items: qnaHelpModel.map((record) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                                margin: EdgeInsets.all(10),
                                child: Stack(
                                  children: <Widget>[
                                    Positioned(
                                      top: 0,
                                      left: 0,
                                      right: 0,
                                      child: Container(
                                        margin: EdgeInsets.all(10),
                                        alignment: Alignment.center,
                                        child: Text(
                                          record.title,
                                          style: TextStyle(
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Image.asset(
                                            record.assetImage,
                                            fit: BoxFit.cover,
                                            width: 170,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            record.desc,
                                            style: TextStyle(fontSize: 16.0),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ));
                          },
                        );
                      }).toList()),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: map<Widget>(qnaHelpModel, (index, record) {
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: current == index
                                      ? clr.Colors.blue
                                      : Colors.grey),
                            );
                          }),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: RoundedButton(
                            title: qnaHelpModel.length - 1 == current
                                ? Dictionary.buttonStartQna
                                : Dictionary.buttonNext,
                            borderRadius: BorderRadius.circular(5.0),
                            color: clr.Colors.blue,
                            textStyle: Theme.of(context)
                                .textTheme
                                .button
                                .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0),
                            onPressed: () async {
                              if (qnaHelpModel.length - 1 == current) {
                                await Preferences.setShowHelpQna(true);
                                bool hasShown =
                                    await Preferences.hasShowcaseQna();

                                if (hasShown == null || hasShown == false) {
                                  Future.delayed(
                                      Duration(milliseconds: 200),
                                      () => ShowCaseWidget.of(showcaseContext)
                                          .startShowCase(
                                              [_showcaseOne, _showcaseLast]));
                                }

                                setState(() {
                                  helpScreen = true;
                                });
                              } else {
                                await basicSlider.nextPage(
                                    duration: Duration(milliseconds: 300),
                                    curve: Curves.linear);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ))),
    );
  }

  @override
  void dispose() {
    _listBloc.close();
    _mainRefreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }
}
