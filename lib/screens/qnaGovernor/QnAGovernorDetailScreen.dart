import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/qna_governor/Bloc.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';

class QnAGovernorDetailScreen extends StatefulWidget {
  final int id;

  QnAGovernorDetailScreen(this.id);

  @override
  _QnAGovernorDetailScreenState createState() =>
      _QnAGovernorDetailScreenState();
}

class _QnAGovernorDetailScreenState extends State<QnAGovernorDetailScreen> {
  final _textMessage = TextEditingController();
  QnAGovernorDetailBloc _qnAGovernorDetailBloc;
  QnAGovernorCommentListBloc _qnAGovernorCommentListBloc;
  QnAModel _qnAModel;

  String animationThumbsUp = 'unlike';

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen('Tanya Jawab',
        screenClassOverride: 'TanyaJawabScreen');
    _textMessage.addListener((() {
      setState(() {});
    }));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar().DefaultAppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () {
                  Navigator.pop(context, _qnAModel);
                }),
            title: Dictionary.detail),
        body: MultiBlocProvider(
          providers: [
            BlocProvider<QnAGovernorDetailBloc>(
                create: (context) => _qnAGovernorDetailBloc =
                    QnAGovernorDetailBloc(QnAGovernorRepository())
                      ..add(QnAGovernorDetailLoad(widget.id))),
            BlocProvider<QnAGovernorCommentListBloc>(
                create: (context) => _qnAGovernorCommentListBloc =
                    QnAGovernorCommentListBloc(QnAGovernorRepository())),
          ],
          child: MultiBlocListener(
            listeners: [
              BlocListener<QnAGovernorDetailBloc, QnAGovernorDetailState>(
                  bloc: _qnAGovernorDetailBloc,
                  listener: (context, state) {
                    if (state is QnAGovernorDetailLoaded) {
                      _qnAGovernorCommentListBloc.add(
                          QnAGovernorCommentListLoad(
                              questionId: widget.id, page: 1));
                    }
                  })
            ],
            child: WillPopScope(
              onWillPop: _onWillPop,
              child: BlocBuilder<QnAGovernorDetailBloc, QnAGovernorDetailState>(
                bloc: _qnAGovernorDetailBloc,
                builder: (context, state) => state is QnAGovernorDetailLoading
                    ? _buildQuestionLoading()
                    : state is QnAGovernorDetailLoaded
                        ? _buildContent(state)
                        : state is QnAGovernorDetailFailure
                            ? ErrorContent(error: state.error)
                            : Container(),
              ),
            ),
          ),
        ));
  }

  Container _buildContent(QnAGovernorDetailLoaded state) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: ListView(
              children: <Widget>[
                _buildQuestion(state),
                Divider(thickness: 2.0),
                _buildComment(),
              ],
            ),
          ),

          /** Input field & send button **/
          /*Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(top: BorderSide(color: Colors.grey[300]))),
              child: Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 50),
                    child: TextField(
                        controller: _textMessage,
                        autofocus: false,
                        maxLines: 5,
                        minLines: 1,
                        maxLength: 255,
                        style: TextStyle(color: Colors.black, fontSize: 16.0),
                        decoration: InputDecoration(
                            hintText: 'Tulis pesan ...',
                            counterText: "",
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(
                                left: Dimens.contentPadding,
                                top: Dimens.contentPadding,
                                bottom: Dimens.contentPadding))),
                  ),
                  Positioned(
                      right: 15.0,
                      bottom: 15.0,
                      child: _textMessage.text.trim().isNotEmpty
                          ? GestureDetector(
                              child: Icon(Icons.send, color: clr.Colors.blue),
                              onTap: () {
                                setState(() {
                                  _textMessage.clear();
                                });
                              })
                          : Icon(Icons.send, color: clr.Colors.grey))
                ],
              )),*/
        ],
      ),
    );
  }

  _buildQuestionLoading() {
    return Skeleton(
        child: Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: Dimens.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.all(0.0),
            leading: Container(
              width: 40.0,
              height: 40.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Container(
                    width: 40.0, height: 40.0, color: Colors.grey[300]),
              ),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                SizedBox(height: 2.0),
                Container(width: 50.0, height: 15.0, color: Colors.grey[300]),
              ],
            ),
            trailing: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 6.0),
                    decoration: BoxDecoration(
                        color: clr.Colors.blue,
                        borderRadius: BorderRadius.circular(5.0)),
                    child: Container(
                        width: 20.0, height: 18.0, color: Colors.grey[300]),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 5.0),
                      width: 20.0,
                      height: 18.0,
                      color: Colors.grey[300]),
                ],
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              height: 20.0,
              color: Colors.grey[300]),
          Container(
              margin: EdgeInsets.only(top: 2.0),
              width: MediaQuery.of(context).size.width,
              height: 20.0,
              color: Colors.grey[300]),
          Container(
              margin: EdgeInsets.only(top: 2.0),
              width: MediaQuery.of(context).size.width - 200,
              height: 20.0,
              color: Colors.grey[300]),
        ],
      ),
    ));
  }

  _buildQuestion(QnAGovernorDetailLoaded state) {
    AnalyticsHelper.setLogEvent(
        Analytics.EVENT_VIEW_DETAIL_QNA, <String, dynamic>{
      'id': state.record.id,
      'title': state.record.text,
      'is_flag': state.record.isFlagged
    });

    _qnAModel = state.record;
    bool isLiked = state.record.isLiked;

    if (isLiked) {
      animationThumbsUp = 'like';
    } else {
      animationThumbsUp = 'unlike';
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: Dimens.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.all(0.0),
            leading: Container(
              width: 40.0,
              height: 40.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: CachedNetworkImage(
                  imageUrl: state.record.user.photoUrlFull != null
                      ? state.record.user.photoUrlFull
                      : '',
                  fit: BoxFit.fill,
                  placeholder: (context, url) => Container(
                      color: Colors.grey[200],
                      child: Center(child: CupertinoActivityIndicator())),
                  errorWidget: (context, url, error) => Container(
                      color: Colors.grey[200],
                      child: Image.asset('${Environment.imageAssets}user.png',
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  state.record.user.name,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: FontsFamily.productSans),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  unixTimeStampToTimeAgo(state.record.createdAt),
                  style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                )
              ],
            ),
            trailing: state.record.isFlagged == 0
                ? GestureDetector(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 6.0),
                          margin: EdgeInsets.only(top: 6),
                          decoration: BoxDecoration(
                              color: clr.Colors.blue,
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Text('${state.record.likesCount}',
                              style: TextStyle(color: Colors.white)),
                        ),
                        Container(
                          width: 30,
                          height: 30,
                          child: FlareActor(
                            '${Environment.flareAssets}thumbs_up.flr',
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            animation: animationThumbsUp,
                          ),
                        )
                      ],
                    ),
                    onTap: state.record.isFlagged == 0
                        ? () {
                            _qnAGovernorDetailBloc
                                .add(QnAGovernorDetailLike(state.record.id));
                            if (isLiked) {
                              _updateLike(state.record, true);
                            } else {
                              _updateLike(state.record, false);
                            }
                          }
                        : null,
                  )
                : SizedBox(height: 15.0),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            child: Text(
              state.record.text,
              style: TextStyle(fontSize: 15.0),
            ),
          ),
          state.record.isFlagged != 0
              ? Container(
                  margin: EdgeInsets.only(top: 15.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.warning, color: Colors.red),
                      Container(
                        margin: EdgeInsets.only(left: 5),
                        width: MediaQuery.of(context).size.width - 65,
                        child: Text(
                          Dictionary.contentNotPermitted,
                          style: TextStyle(fontSize: 15.0, color: Colors.red),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ))
              : Container(),
        ],
      ),
    );
  }

  _buildComment() {
    return BlocBuilder<QnAGovernorCommentListBloc, QnAGovernorCommentListState>(
      bloc: _qnAGovernorCommentListBloc,
      builder: (context, state) => state is QnAGovernorCommentListLoaded
          ? _buildCommentList(state)
          : state is QnAGovernorCommentListFailure
              ? Text(state.error)
              : Container(),
    );
  }

  Container _buildCommentList(QnAGovernorCommentListLoaded state) {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: state.records.length,
        separatorBuilder: (context, index) => Container(
            width: MediaQuery.of(context).size.width,
            height: 1.0,
            color: Colors.grey[300]),
        itemBuilder: (context, index) {
          return Container(
            padding:
                EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  contentPadding: EdgeInsets.all(0.0),
                  leading: Container(
                    width: 40.0,
                    height: 40.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: CachedNetworkImage(
                        imageUrl: state.records[index].user.photoUrlFull != null
                            ? state.records[index].user.photoUrlFull
                            : '',
                        fit: BoxFit.fill,
                        placeholder: (context, url) => Container(
                            color: Colors.grey[200],
                            child: Center(child: CupertinoActivityIndicator())),
                        errorWidget: (context, url, error) => Container(
                            color: Colors.grey[200],
                            child: Image.asset(
                                '${Environment.imageAssets}user.png',
                                fit: BoxFit.cover)),
                      ),
                    ),
                  ),
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Wrap(
                        children: <Widget>[
                          Text(
                            state.records[index].user.roleLabel == 'staffProv'
                                ? Dictionary.governorJabar
                                : state.records[index].user.name,
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontsFamily.productSans),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(width: 5.0),
                          if (state.records[index].user.roleLabel ==
                              'staffProv')
                            Icon(
                              Icons.check_circle,
                              color: clr.Colors.blue,
                              size: 18.0,
                            )
                        ],
                      ),
                      Text(
                        unixTimeStampToTimeAgo(state.records[index].createdAt),
                        style:
                            TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                      )
                    ],
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 56.0),
                    child: Text(
                      state.records[index].text,
                      style: TextStyle(fontSize: 15.0),
                    )),
              ],
            ),
          );
        },
      ),
    );
  }

  _updateLike(QnAModel record, bool isLike) {
    if (isLike) {
      if (record.isLiked) {
        setState(() {
          record.isLiked = false;
          record.likesCount = record.likesCount - 1;
          _qnAModel = record;
        });
      }
    } else {
      setState(() {
        record.isLiked = true;
        record.likesCount = record.likesCount + 1;
        _qnAModel = record;
      });
    }
  }

  Future<bool> _onWillPop() {
    Navigator.pop(context, _qnAModel);
    return Future.value();
  }
}
