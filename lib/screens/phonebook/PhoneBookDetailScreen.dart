import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/blocs/phonebook_detail/Bloc.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/enums/PhoneNumberTypeEnum.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/screens/phonebook/ShowLocationsScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pedantic/pedantic.dart';

class PhoneBookDetailScreen extends StatelessWidget {
  final PhoneBookModel record;

  PhoneBookDetailScreen({Key key, @required this.record}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PhoneBookDetailBloc>(
        create: (context) => PhoneBookDetailBloc(),
        child: PhoneBookDetail(
          record: record,
        ));
  }
}

class PhoneBookDetail extends StatefulWidget {
  final PhoneBookModel record;

  PhoneBookDetail({Key key, @required this.record}) : super(key: key);

  @override
  _PhoneBookDetailState createState() => _PhoneBookDetailState();
}

class _PhoneBookDetailState extends State<PhoneBookDetail> {
  PhoneBookModel get _record => widget.record;

  PhoneBookDetailBloc _phoneBookDetailBloc;
  bool statConnect;

  @override
  void initState() {
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_DETAIL_PHONEBOOK,
        <String, dynamic>{'id': _record.id, 'title': _record.name});

    Future.delayed(Duration(milliseconds: 100), () => checkConnection().then((isConnect) {
      if (isConnect != null && isConnect) {
        setState(() {
          statConnect = true;
        });
      } else {
        setState(() {
          statConnect = false;
        });
      }
    }));

    _phoneBookDetailBloc = BlocProvider.of<PhoneBookDetailBloc>(context);
    _phoneBookDetailBloc.add(PhoneBookDetailLoad(record: _record));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (statConnect == null || statConnect) {
      Widget coverImageSection(
          BuildContext context, PhoneBookDetailState state) {
        Widget _loader = Container(
            height: 240,
            child: Center(
              child: CircularProgressIndicator(),
            ));

          if (state is PhoneBookDetailLoaded) {
            return _record.coverImageUrl != null
                ? CachedNetworkImage(
              imageUrl: _record.coverImageUrl,
              placeholder: (context, url) => _loader,
              errorWidget: (context, url, error) => Icon(Icons.error),
            )
                : Container(
                padding: EdgeInsets.only(left: 60.0, right: 60.0, top: 30.0),
                child: Image.asset('assets/logo/logo.png'));
          } else {
            return Container();
          }

      }

      Widget mainSection(BuildContext context, PhoneBookDetailState state) {
        if (state is PhoneBookDetailLoaded) {
          String name = _record.name.replaceAll(RegExp("\\s+"), " ");

          return Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(name ?? '',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
//                  _record.latitude != null && _record.longitude != null ?  GestureDetector(
//                      child: Icon(Icons.map),
//                      onTap: (){
//                        _openMaps();
//                      },
//                    ):Container()
                  ],
                ),
                _record.phoneNumbers != null && _record.phoneNumbers.isNotEmpty
                    ? ListView.separated(
                        separatorBuilder: (context, index) => Divider(),
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _record.phoneNumbers.length,
                        itemBuilder: (context, position) {
                          return ListTile(
                            contentPadding: EdgeInsets.all(0),
                            trailing: _record.phoneNumbers[position].type ==
                                    PhoneNumberTypeEnum.phone
                                ? Icon(Icons.call)
                                : Icon(Icons.message),
                            title: Text(
                              _record.phoneNumbers[position].phoneNumber,
                              style: TextStyle(fontSize: 16.0),
                            ),
                            onTap: () => _launchURL(
                                _record.phoneNumbers[position].type,
                                _record.phoneNumbers[position].phoneNumber),
                          );
                        })
                    : Container(),
              ],
            ),
          );
        } else {
          return Container();
        }
      }

      Widget addressSection(BuildContext context, PhoneBookDetailState state) {
        if (state is PhoneBookDetailLoaded) {
          return _record.address != null
              ? Card(
                  child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        Dictionary.address,
                        style: TextStyle(fontSize: 20.0),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        _record.address,
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ],
                  ),
                ))
              : Container();
        } else {
          return Container();
        }
      }

      return BlocBuilder<PhoneBookDetailBloc, PhoneBookDetailState>(
          bloc: _phoneBookDetailBloc,
          builder: (context, state) {
            return Scaffold(
                appBar: AppBar(
                    title: Text(state is PhoneBookDetailLoaded
                        ? _record.name.trim().replaceAll(RegExp("\\s+"), " ") ??
                            'Detail Nomor Penting'
                        : 'Detail Nomor Penting')),
                body: ListView(children: [
                  coverImageSection(context, state),
                  mainSection(context, state),
                  addressSection(context, state)
                ]));
          });
    } else {
      return Scaffold(
          appBar: AppBar(title: Text('Detail Nomor Penting')),
          body: ErrorContent(error: Dictionary.errorConnection));
    }
  }

  _launchURL(PhoneNumberTypeEnum type, String phoneNumber) async {
    String url;

    if (type == PhoneNumberTypeEnum.phone) {
      url = 'tel:$phoneNumber';

      await AnalyticsHelper.setLogEvent(
          Analytics.EVENT_OPEN_TELP_PHONEBOOK, <String, dynamic>{
        'id': _record.id,
        'title': _record.name,
        'telp': phoneNumber
      });
    } else {
      url = 'sms:$phoneNumber';

      await AnalyticsHelper.setLogEvent(
          Analytics.EVENT_OPEN_SMS_PHONEBOOK, <String, dynamic>{
        'id': _record.id,
        'title': _record.name,
        'telp': phoneNumber
      });
    }

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void dispose() {
    super.dispose();
    _phoneBookDetailBloc.close();
  }

  // ignore: missing_return
  Future<bool> checkConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  void _openMaps() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    if (permission == PermissionStatus.granted) {
      unawaited(Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShowLocationsScreen(_record),
        ),
      ));
    } else {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
            image: Image.asset(
              'assets/icons/map_pin.png',
              fit: BoxFit.contain,
              color: Colors.white,
            ),
            description: Dictionary.permissionPhoneBookMap,
            onOkPressed: () {
              Navigator.of(context).pop();
              PermissionHandler().requestPermissions(
                  [PermissionGroup.location]).then(_onStatusRequested);
            },
          )));
    }
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.location];
    if (status == PermissionStatus.granted) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShowLocationsScreen(_record),
        ),
      );
    }
  }


}
