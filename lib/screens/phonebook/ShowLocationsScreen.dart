import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/screens/phonebook/PhoneBookDetailScreen.dart';

class ShowLocationsScreen extends StatelessWidget {
  final PhoneBookModel _phoneBookModel;

  ShowLocationsScreen(this._phoneBookModel);

  @override
  Widget build(BuildContext context) {
    return ShowLocations(_phoneBookModel);
  }
}

class ShowLocations extends StatefulWidget {
  final PhoneBookModel _phoneBookModel;

  ShowLocations(this._phoneBookModel);

  @override
  _ShowLocationsState createState() => _ShowLocationsState();
}

class _ShowLocationsState extends State<ShowLocations> {
  Completer<GoogleMapController> _controller = Completer();

  LatLng _center;

  double _latitude;
  double _longitude;

  BitmapDescriptor myIcon;

  Set<Marker> _markers = Set();

  @override
  void initState() {
    _center = LatLng(double.tryParse(widget._phoneBookModel.latitude),
        double.tryParse(widget._phoneBookModel.longitude));
    _checkPermission();
    super.initState();
  }

  void _onMapCreated(GoogleMapController controller) async {
    LatLng currentLocation = LatLng(_latitude, _longitude);

    await controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: currentLocation, zoom: 15.5),
      ),
    );

    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(milliseconds: 500), () async {
      _markers.add(Marker(
          markerId: MarkerId(widget._phoneBookModel.id.toString()),
          icon: BitmapDescriptor.fromBytes(
              await _markerIcon(widget._phoneBookModel.category.name.toString())),
          draggable: false,
          infoWindow: InfoWindow(
              title: widget._phoneBookModel.name,
              snippet: widget._phoneBookModel.address,
              onTap: () {
                _onTapItem(
                    context,
                    PhoneBookModel(
                        id: widget._phoneBookModel.id,
                        name: widget._phoneBookModel.name,
                        address: widget._phoneBookModel.address,
                        description: widget._phoneBookModel.description,
                        latitude: widget._phoneBookModel.latitude,
                        longitude: widget._phoneBookModel.longitude,
                        phoneNumbers: widget._phoneBookModel.phoneNumbers));
              }),
          position: LatLng(double.tryParse(widget._phoneBookModel.latitude),
              double.tryParse(widget._phoneBookModel.longitude))));
    });

    return Scaffold(
        appBar: AppBar(
          title: Text(Dictionary.nearbyLocation),
        ),
        body: buildGoogleMap());
  }

  GoogleMap buildGoogleMap() {
    return GoogleMap(
      myLocationEnabled: true,
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 15.0,
      ),
      markers: _markers,
    );
  }

  Future<Uint8List> _markerIcon(String category) async {
    String uriIcon;
    switch (category.toLowerCase()) {
      case 'kesehatan':
        uriIcon = 'assets/icons/kesehatan.png';
        break;
      case 'ekonomi':
        uriIcon = 'assets/icons/ekonomi.png';
        break;
      case 'keamanan':
        uriIcon = 'assets/icons/keamanan.png';
        break;
      case 'transportasi':
        uriIcon = 'assets/icons/transport.png';
        break;
      case 'sosial':
        uriIcon = 'assets/icons/sosial.png';
        break;
      case 'layanan':
        uriIcon = 'assets/icons/pelayanan.png';
        break;
      default:
        uriIcon = 'assets/icons/pelayanan.png';
        break;
    }

    Uint8List markerIcon = await getBytesFromAsset(uriIcon, 100);

    return markerIcon;
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  void _onTapItem(BuildContext context, PhoneBookModel record) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PhoneBookDetailScreen(record: record),
      ),
    );
  }

  void _onStatusRequested(
      Map<PermissionGroup, PermissionStatus> statuses) async {
    final status = statuses[PermissionGroup.location];
    if (status == PermissionStatus.granted) {
      final GoogleMapController controller = await _controller.future;
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      LatLng currentLocation = LatLng(position.latitude, position.longitude);
      _latitude = position.latitude;
      _longitude = position.longitude;

      await controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: currentLocation, zoom: 15.5),
        ),
      );
    }
  }

  void _checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);

    if (permission == PermissionStatus.granted) {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      _latitude = position.latitude;
      _longitude = position.longitude;

      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/map_pin.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionPhoneBookMap,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.location]).then(_onStatusRequested);
                },
              )));
    }
  }
}
