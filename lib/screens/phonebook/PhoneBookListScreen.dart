import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/phonebook_list/Bloc.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/PhoneBookFilter.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/repositories/PhoneBookRepository.dart';
import 'package:sapawarga/screens/phonebook/ListViewPhoneBooks.dart';
import 'package:sapawarga/screens/phonebook/NearByLocationsScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class PhoneBookListScreen extends StatelessWidget {
  final PhoneBookRepository phoneBookRepository = PhoneBookRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PhoneBookListBloc>(
      create: (BuildContext context) =>
          PhoneBookListBloc(phoneBookRepository: phoneBookRepository),
      child: PhoneBookList(),
    );
  }
}

class PhoneBookList extends StatefulWidget {
  @override
  _PhoneBookList createState() => _PhoneBookList();
}

class _PhoneBookList extends State<PhoneBookList>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  bool _isSearch = false;
  final _searchController = TextEditingController();
  Timer _debounce;
  bool _hasChange = false;
  String _selectedFilter = "0";
  var containerWidth = 40.0;
  final _nodeOne = FocusNode();
  ScrollController _scrollController = ScrollController();
  int _page = 1;
  PhoneBookListBloc _phoneBookListBloc;
  AuthenticationBloc _authenticationBloc;
  int maxDatalength;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.PHONEBOOK);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_PHONEBOOK);

    _initialize();
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );
    _phoneBookListBloc = BlocProvider.of<PhoneBookListBloc>(context);
    _phoneBookListBloc.add(PhoneBookListLoad(page: _page, isFirstLoad: true));
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _searchController.addListener((() {
      _onSearchChanged();
    }));
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhoneBookListBloc, PhoneBookListState>(
        bloc: _phoneBookListBloc,
        builder: (context, state) {
          if (state is PhoneBookListLoaded) {
            _updatePage(state.records);
            maxDatalength = state.maxData;
          }

          if (state is PhoneBookListFailure) {
            if (state.error.contains(Dictionary.errorUnauthorized)) {
              _authenticationBloc.add(LoggedOut());
              Navigator.of(context).pop();
            }
          }

          return Scaffold(
              appBar: _buildAppBar(),
              body: RefreshIndicator(
                  key: _refreshIndicatorKey,
                  child: Container(
                      child: state is PhoneBookListLoading
                          ? Center(child: _buildLoading())
                          : state is PhoneBookListLoaded
                              ? state.records.isNotEmpty
                                  ? ListViewPhoneBooks(
                                      records: state.records,
                                      scrollController: _scrollController,
                                      maxDataLength: maxDatalength,
                                    )
                                  : EmptyData(
                                      message: Dictionary.emptyDataPhoneBook)
                              : state is PhoneBookListFailure
                                  ? ErrorContent(error: state.error)
                                  : Container()),
                  onRefresh: _refresh),
              floatingActionButton: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  elevation: 5.0,
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 15.0, horizontal: 5.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset(
                          'assets/icons/filter.png',
                          width: 24.0,
                          height: 24.0,
                          color: Colors.green,
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 5.0),
                            child: Text(Dictionary.filter,
                                style: TextStyle(
                                    color: Colors.green, fontSize: 16.0)))
                      ],
                    ),
                  ),
                  color: Colors.grey[50],
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => PhoneBookFilter(
                            selectedFilter: _selectedFilter,
                            onSubmit: (val) {
                              if (val != _selectedFilter) {
                                if (val != "0") {
                                  _phoneBookListBloc
                                      .add(PhoneBookListFilter(keyword: val));
                                }
                              }
                              _selectedFilter = val;
                            }));
                  }));
        });
  }

  AppBar _buildAppBar() {
    return AppBar(title: _titleBar(), actions: <Widget>[
      // action button
      Visibility(
        visible: !_isSearch,
        child: IconButton(
            icon: Icon(Icons.map),
            onPressed: () {
              _openMaps();
            }),
      ),

      RotationTransition(
        turns: Tween(begin: 0.0, end: 1.0).animate(_animationController),
        child: IconButton(
          icon: _isSearch ? Icon(Icons.close) : Icon(Icons.search),
          onPressed: () {
            _searchPressed();
          },
        ),
      ),
      // action button
    ]);
  }

  Widget _buildLoading() {
    return ListView.builder(
        itemCount: 10,
        padding: EdgeInsets.all(6),
        itemBuilder: (context, index) {
          return Card(
              child: Padding(
            padding: EdgeInsets.all(10),
            child: ListTile(
              leading: Skeleton(
                child: Icon(FontAwesomeIcons.building),
              ),
              title: Skeleton(
                width: MediaQuery.of(context).size.width / 4,
                height: 20,
              ),
            ),
          ));
        });
  }

  Widget _titleBar() {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.centerRight,
          child: AnimatedOpacity(
            opacity: _isSearch ? 1.0 : 0.0,
            duration: Duration(milliseconds: 500),
            child: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              width: containerWidth,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: TextField(
                controller: _searchController,
                autofocus: false,
                focusNode: _nodeOne,
                textInputAction: TextInputAction.go,
                style: TextStyle(color: Colors.black, fontSize: 16.0),
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0)),
              ),
            ),
          ),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Visibility(
                visible: !_isSearch, child: Text(Dictionary.phoneBook))),
      ],
    );
  }

  void _initialize() async {
    _page = await Preferences.getPhoneBookPage() != null
        ? await Preferences.getPhoneBookPage()
        : 1;
    maxDatalength = await Preferences.getTotalCount() != null
        ? await Preferences.getTotalCount()
        : 0;
  }

  void _updatePage(List<PhoneBookModel> records) async {
    _page = (records.length / 20).round() + 1;
    if (_page == 1 && records.length < 20) {
      _page = 0;
    }
    await Preferences.setPhoneBookPage(_page);
  }

  void _searchPressed() {
    return setState(() {
      _isSearch = !_isSearch;
      _animationController.forward(from: 0.0);
      _showSearch();
    });
  }

  void _showSearch() {
    if (!_isSearch) {
      if (_hasChange) {
        _hasChange = false;
        _refresh();
      }
      containerWidth = 50.0;
      FocusScope.of(context).unfocus();
    } else {
      containerWidth = MediaQuery.of(context).size.width;
      FocusScope.of(context).requestFocus(_nodeOne);
    }
    _searchController.clear();
  }

  void _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (_searchController.text.trim().isNotEmpty) {
        _hasChange = true;
        _phoneBookListBloc.add(PhoneBookListSearch(
            keyword: _searchController.text, page: 1, isFirstLoad: true));
      }
    });
  }

  void _openMaps() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    if (permission == PermissionStatus.granted) {
      unawaited(Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => NearByLocationsScreen(),
        ),
      ));

      await AnalyticsHelper.setLogEvent(Analytics.EVENT_TAPPED_MAP_PHONEBOOK);
    } else {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/map_pin.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionPhoneBookMap,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.location]).then(_onStatusRequested);
                },
              )));
    }
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.location];
    if (status == PermissionStatus.granted) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => NearByLocationsScreen(),
        ),
      );
    }
  }

  void _scrollListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (_isSearch) {
        if (_searchController.text.trim().isNotEmpty) {
          _phoneBookListBloc.add(PhoneBookListSearch(
              keyword: _searchController.text, page: _page));
        }
      } else {
        _phoneBookListBloc.add(PhoneBookListLoad(page: _page));
      }
    }
  }

  Future<void> _refresh() async {
    _phoneBookListBloc.add(PhoneBookListRefresh());
    _page = 1;
    await Preferences.setPhoneBookPage(1);
  }

  @override
  void dispose() {
    _searchController.removeListener(_onSearchChanged);
    _searchController.dispose();
    _phoneBookListBloc.close();
    _animationController.dispose();
    _scrollController.dispose();
    super.dispose();
  }
}
