import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/screens/phonebook/PhoneBookDetailScreen.dart';

class ListViewPhoneBooks extends StatelessWidget {
  final List<PhoneBookModel> records;
  final ScrollController scrollController;
  final int maxDataLength;

  ListViewPhoneBooks({Key key, @required this.records, this.scrollController, this.maxDataLength})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ListTile _cardTile(PhoneBookModel record) {
      return ListTile(
        leading: Icon(FontAwesomeIcons.building),
        title: Text(record.name.trim().replaceAll(RegExp("\\s+"), " ")),
        onTap: () => _onTapItem(context, record),
      );
    }

    Widget _card(PhoneBookModel record) {
      return Card(
          child: Padding(
        padding: EdgeInsets.all(10),
        child: _cardTile(record),
      ));
    }

    return ListView.builder(
        controller: scrollController,
        padding: EdgeInsets.all(6),
        itemCount: records.length + 1,
        itemBuilder: (context, position) {
          if (position == records.length) {
            if (records.length > 15 && maxDataLength != records.length) {
              return Padding(
                padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: Column(
                  children: <Widget>[
                    CupertinoActivityIndicator(),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(Dictionary.loadingData),
                  ],
                ),
              );
            } else {
              return Container();
            }
          }
          return _card(records[position]);
        });
  }

  void _onTapItem(BuildContext context, PhoneBookModel record) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PhoneBookDetailScreen(record: record),
      ),
    );
  }
}
