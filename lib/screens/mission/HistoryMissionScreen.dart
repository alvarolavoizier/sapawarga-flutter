import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationBloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_list_history/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'package:sapawarga/screens/mission/DetailMissionScreen.dart';

class HistoryMissionScreen extends StatefulWidget {
  @override
  _HistoryMissionScreenState createState() => _HistoryMissionScreenState();
}

class _HistoryMissionScreenState extends State<HistoryMissionScreen> {
  GamificationsListHistoryBloc gamificationsListHistoryBloc;
  List<ItemOnProgressModel> _gamificationList = List<ItemOnProgressModel>();
  RefreshController _mainRefreshController = RefreshController();
  AuthenticationBloc _authenticationBloc;
  ScrollController _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  int _pageCount = 0;
  int _currentPage = 1;

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _scrollController.addListener(() {
      _onScroll();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocProvider<GamificationsListHistoryBloc>(
        create: (context) =>
            gamificationsListHistoryBloc = GamificationsListHistoryBloc(
                gamificationsRepository: GamificationsRepository())
              ..add(GamificationsHistoryListLoad(page: _currentPage)),
        child: BlocListener<GamificationsListHistoryBloc,
            GamificationsListHistoryState>(
          bloc: gamificationsListHistoryBloc,
          listener: (context, state) {
            if (state is GamificationsListHistoryLoaded) {
              _pageCount = state.records.meta.pageCount;
              _currentPage = state.records.meta.currentPage + 1;
              _gamificationList.addAll(state.records.items);
              _gamificationList = _gamificationList.toSet().toList();
              setState(() {});
            } else if (state is GamificationsListHistoryFailure) {
              if (state.error.contains(Dictionary.errorUnauthorized)) {
                _authenticationBloc.add(LoggedOut());
                Navigator.of(context).pop();
              }
            }
          },
          child: BlocBuilder<GamificationsListHistoryBloc,
              GamificationsListHistoryState>(
            bloc: gamificationsListHistoryBloc,
            builder: (context, state) => SmartRefresher(
                controller: _mainRefreshController,
                enablePullDown: true,
                header: WaterDropMaterialHeader(),
                onRefresh: () async {
                  _currentPage = 1;
                  _gamificationList.clear();
                  gamificationsListHistoryBloc
                      .add(GamificationsHistoryListLoad(page: _currentPage));
                  _mainRefreshController.refreshCompleted();
                },
                child: state is GamificationsListHistoryLoading
                    ? _buildLoading()
                    : state is GamificationsListHistoryLoaded
                        ? state.records.items.isNotEmpty
                            ? _buildContent(state)
                            : EmptyData(message: Dictionary.emptyDataHistory)
                        : state is GamificationsListHistoryFailure
                            ? ErrorContent(error: state.error)
                            : _buildLoading()),
          ),
        ),
      ),
    );
  }

  _buildContent(GamificationsListHistoryLoaded state) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: state.records.items.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailMissionScreen(
                          itemOnProgressModel: state.records.items[index],
                          title: Dictionary.detailHistoryMission,
                        )));
          },
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Container(
              padding: EdgeInsets.only(top: 10, right: 15, left: 15, bottom: 15),
              child: Row(
                children: <Widget>[
                  _buildButtonColumn('icon_kegiatan_rw_white.png'),
                  Container(
                    width: MediaQuery.of(context).size.width - 100,
                    padding: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Text(state.records.items[index].gamification.title,
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                  maxLines: 2,
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis),
                            ),
                            state.records.items[index].totalUserHit ==
                                    state.records.items[index].gamification
                                        .totalHit
                                ? Icon(
                                    Icons.check_circle,
                                    color: clr.Colors.green,
                                    size: 30,
                                  )
                                : Container()
                          ],
                        ),
                        Text(
                            state.records.items[index].totalUserHit
                                    .toString() +
                                ' dari ' +
                                state.records.items[index].gamification.totalHit
                                    .toString() +
                                ' misi',
                            style: TextStyle(
                                fontSize: 14.0, color: Colors.grey[600]),
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        gamificationsListHistoryBloc
            .add(GamificationsHistoryListLoad(page: _currentPage));
      }
    }
  }

  _buildButtonColumn(String iconPath) {
    return Column(
      children: [
        Container(
          width: 55,
          height: 55,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              blurRadius: 6.0,
              color: Colors.black.withOpacity(.2),
              offset: Offset(3.0, 5.0),
            ),
          ], borderRadius: BorderRadius.circular(12.0), color: clr.Colors.blue),
          child: IconButton(
            color: Theme.of(context).textTheme.body1.color,
            icon: Image.asset('${Environment.iconAssets}' + iconPath,
                height: 25, fit: BoxFit.fitWidth, width: 30),
            onPressed: () {},
          ),
        ),
      ],
    );
  }

  _buildLoading() {
    return ListView.builder(
      padding: EdgeInsets.only(bottom: 80.0, top: 10),
      itemCount: 4,
      itemBuilder: (context, index) => Skeleton(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding:
              EdgeInsets.fromLTRB(Dimens.padding, 4.0, Dimens.padding, 4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: Container(
                      height: MediaQuery.of(context).size.height / 5.8,
                      color: Colors.grey[300]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
