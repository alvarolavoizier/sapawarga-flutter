import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationBloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_list_onprogress/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Expandable.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/models/TimelineObjectModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'package:sapawarga/screens/mission/DetailMissionScreen.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

class OnProgressMissionScreen extends StatefulWidget {
  @override
  _OnProgressMissionScreenState createState() =>
      _OnProgressMissionScreenState();
}

class _OnProgressMissionScreenState extends State<OnProgressMissionScreen> {
  GamificationsListOnProgressBloc gamificationsListOnProgressBloc;
  List<ItemOnProgressModel> _gamificationOnProgressList =
      List<ItemOnProgressModel>();
  RefreshController _mainRefreshController = RefreshController();
  AuthenticationBloc _authenticationBloc;
  ScrollController _scrollController = ScrollController();
  List<TimelineModel> timelineList = [];
  final _scrollThreshold = 200.0;
  int _pageCount = 0;
  int _currentPage = 1;
  List<TimelineObjectModel> listTimelineObject = [];
  List<Widget> pages = [];
  ExpandableController expandableController;

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _scrollController.addListener(() {
      _onScroll();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocProvider<GamificationsListOnProgressBloc>(
        create: (context) =>
            gamificationsListOnProgressBloc = GamificationsListOnProgressBloc(
                gamificationsRepository: GamificationsRepository())
              ..add(GamificationsOnProgressListLoad(page: _currentPage)),
        child: BlocListener<GamificationsListOnProgressBloc,
            GamificationsListOnProgressState>(
          bloc: gamificationsListOnProgressBloc,
          listener: (context, state) {
            if (state is GamificationsListOnProgressLoaded) {
              _pageCount = state.records.meta.pageCount;
              _currentPage = state.records.meta.currentPage + 1;
              _gamificationOnProgressList.addAll(state.records.items);
              _gamificationOnProgressList =
                  _gamificationOnProgressList.toSet().toList();
              setState(() {});
            } else if (state is GamificationsListOnProgressFailure) {
              if (state.error.contains(Dictionary.errorUnauthorized)) {
                _authenticationBloc.add(LoggedOut());
                Navigator.of(context).pop();
              }
            }

//            else if (state is GamificationsDetailProgressLoaded) {
//              List<TimelineModel> timelineListProgress = [];
//              if (state.records.items.isNotEmpty) {
//                for (int i = 0; i < state.records.items[0].totalHit; i++) {
//                  if (i < state.records.items.length) {
//                    timelineListProgress.add(timelineConmponent(
//                        getNameSubmission(state.records.items[i].objectEvent) +
//                            (i + 1).toString(),
//                        state.records.items[i].createdAt.toString(),
//                        Icons.check,
//                        Colors.green));
//                  } else if (i == state.records.items.length) {
//                    timelineListProgress.add(timelineConmponent(
//                        '', '', FontAwesomeIcons.solidIdBadge, Colors.grey));
//                  } else {
//                    timelineListProgress.add(
//                        timelineConmponent('', '', Icons.check, Colors.grey));
//                  }
//                }
//              }
//
//              setState(() {
//                timelineList = timelineListProgress;
//              });
//            }
          },
          child: BlocBuilder<GamificationsListOnProgressBloc,
              GamificationsListOnProgressState>(
            bloc: gamificationsListOnProgressBloc,
            builder: (context, state) => SmartRefresher(
                controller: _mainRefreshController,
                enablePullDown: true,
                header: WaterDropMaterialHeader(),
                onRefresh: () async {
                  _currentPage = 1;
                  _gamificationOnProgressList.clear();
                  gamificationsListOnProgressBloc
                      .add(GamificationsOnProgressListLoad(page: _currentPage));
                  _mainRefreshController.refreshCompleted();
                },
                child: state is GamificationsListOnProgressLoading
                    ? _buildLoading()
                        : state is GamificationsListOnProgressLoaded
                            ? state.records.items.isNotEmpty
                                ? _buildContent(state)
                                : EmptyData(
                                    message: Dictionary.emptyDataOnProgress)
                            : state is GamificationsListOnProgressFailure
                                ? ErrorContent(error: state.error)
                                    : _buildLoading()),
          ),
        ),
      ),
    );
  }

  _buildContent(GamificationsListOnProgressLoaded state) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: _gamificationOnProgressList.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailMissionScreen(
                          itemOnProgressModel: state.records.items[index],
                      title: Dictionary.detailOnProgressMission,
                        )));
          },
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
//                ExpandablePanel(
//                    controller: controllerExpandable(
//                        _gamificationOnProgressList[index].gamification.id),
//                    header: Row(
//                      children: <Widget>[
//                        _buildButtonColumn('icon_kegiatan_rw_white.png'),
//                        Container(
//                          padding:
//                              EdgeInsets.only(left: 15, top: 10, bottom: 10),
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Text(
//                                  _gamificationOnProgressList[index]
//                                      .gamification
//                                      .title,
//                                  style: TextStyle(
//                                      fontSize: 16.0,
//                                      fontWeight: FontWeight.bold,
//                                      color: Colors.black),
//                                  maxLines: 1,
//                                  textAlign: TextAlign.left,
//                                  overflow: TextOverflow.ellipsis),
//                              SizedBox(
//                                height: 10,
//                              ),
//                              Text(
//                                  _gamificationOnProgressList[index]
//                                      .gamification
//                                      .description,
//                                  style: TextStyle(
//                                      fontSize: 14.0, color: Colors.grey[600]),
//                                  maxLines: 1,
//                                  textAlign: TextAlign.left,
//                                  overflow: TextOverflow.ellipsis),
//                              Container(
//                                margin: EdgeInsets.only(top: 15),
//                                width: MediaQuery.of(context).size.width - 150,
//                                child: MySeparator(
//                                  color: Colors.grey[600],
//                                ),
//                              )
//                            ],
//                          ),
//                        ),
//                      ],
//                    ),
//                    expanded: Container(),
//                    collapsed: Timeline(
//                        children: timelineList,
//                        position: TimelinePosition.Left,
//                        lineColor: Colors.green,
//                        shrinkWrap: true,
//                        physics: NeverScrollableScrollPhysics())
////                      ScreenProgress(listTimelineObject: listTimelineObject),
//                    ),
                  Row(
                    children: <Widget>[
                      _buildButtonColumn('icon_kegiatan_rw_white.png'),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(left: 15, top: 8, bottom: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                  _gamificationOnProgressList[index]
                                      .gamification
                                      .title,
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                  maxLines: 1,
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                  _gamificationOnProgressList[index].totalUserHit
                                      .toString() +
                                      ' dari ' +
                                      _gamificationOnProgressList[index].gamification.totalHit
                                          .toString()+' misi',
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.grey[600]),
                                  maxLines: 1,
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        gamificationsListOnProgressBloc
            .add(GamificationsOnProgressListLoad(page: _currentPage));
      }
    }
  }

  _buildButtonColumn(String iconPath) {
    return Column(
      children: [
        Container(
          width: 55,
          height: 55,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              blurRadius: 6.0,
              color: Colors.black.withOpacity(.2),
              offset: Offset(3.0, 5.0),
            ),
          ], borderRadius: BorderRadius.circular(12.0), color: clr.Colors.blue),
          child: IconButton(
            color: Theme.of(context).textTheme.body1.color,
            icon: Image.asset('${Environment.iconAssets}' + iconPath,
                height: 25, fit: BoxFit.fitWidth, width: 30),
            onPressed: () {

            },
          ),
        ),
      ],
    );
  }

  timelineModel(TimelinePosition position) => Timeline.builder(
      itemBuilder: centerTimelineBuilder,
      itemCount: listTimelineObject.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      position: position);

  TimelineModel centerTimelineBuilder(BuildContext context, int i) {
    final itemTimeline = listTimelineObject[i];
    return TimelineModel(
        Container(
          margin: EdgeInsets.only(top: 10, bottom: 30),
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Text(
                  itemTimeline.title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                width: double.infinity,
                child: Text(
                  itemTimeline.desc,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                ),
              )
            ],
          ),
        ),
        position:
            i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
        iconBackground: itemTimeline.color,
        icon: Icon(itemTimeline.iconData, color: Colors.white));
  }

  String getNameSubmission(String labelName) {
    if (labelName == 'news_view_detail') {
      return 'baca berita ';
    } else {
      return 'posting kegiatan';
    }
  }

  _buildLoading() {
    return ListView.builder(
      padding: EdgeInsets.only(bottom: 80.0, top: 10),
      itemCount: 4,
      itemBuilder: (context, index) => Skeleton(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding:
              EdgeInsets.fromLTRB(Dimens.padding, 4.0, Dimens.padding, 4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: Container(
                      height: MediaQuery.of(context).size.height / 5.8,
                      color: Colors.grey[300]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
