import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationBloc.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationEvent.dart';
import 'package:sapawarga/blocs/gamifications/gamification_detail/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/models/GamificationsModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'package:sapawarga/utilities/FormatDate.dart';

class DetailNewMissionScreen extends StatefulWidget {
  final int id;

  DetailNewMissionScreen({this.id});

  @override
  _DetailNewMissionScreenState createState() => _DetailNewMissionScreenState();
}

class _DetailNewMissionScreenState extends State<DetailNewMissionScreen> {
  GamificationsDetailBloc _gamificationsDetailBloc;
  RefreshController _mainRefreshController = RefreshController();
  ItemGamification gamificationModel;
  AuthenticationBloc _authenticationBloc;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar().DefaultAppBar(title: Dictionary.overviewMission),
      body: Container(
        child: BlocProvider<GamificationsDetailBloc>(
          create: (context) => _gamificationsDetailBloc =
              GamificationsDetailBloc(
                  gamificationsRepository: GamificationsRepository())..add(GamificationsDetailLoad(id: widget.id)),
          child:
              BlocListener<GamificationsDetailBloc, GamificationsDetailState>(
            bloc: _gamificationsDetailBloc,
            listener: (context, state) {
              if (state is GamificationsDetailLoaded) {
                gamificationModel = state.records;
                setState(() {});
              } else if (state is GamificationsDetailFailure) {
                if (state.error.contains(Dictionary.errorUnauthorized)) {
                  _authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                }
              } else if (state is GamificationsTakeMissionlLoading) {
                _isLoading = true;
                blockCircleLoading(context: context);
              } else if (state is GamificationsTakeMissionLoaded) {
                if (_isLoading) {
                  _isLoading = false;
                  Navigator.of(context, rootNavigator: true).pop();
                }

                Navigator.pop(context, state.isTakeMission);
              } else if (state is GamificationsTakeMissionFailure) {
                if (_isLoading) {
                  _isLoading = false;
                  Navigator.of(context, rootNavigator: true).pop();
                }
                if (state.error.contains(Dictionary.errorUnauthorized)) {
                  _authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                } else {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: state.error.toString(),
                            buttonText: "OK",
                            onOkPressed: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop(); // To close the dialog
                            },
                          ));
                }
              }
            },
            child:
                BlocBuilder<GamificationsDetailBloc, GamificationsDetailState>(
              bloc: _gamificationsDetailBloc,
              builder: (context, state) => SmartRefresher(
                  controller: _mainRefreshController,
                  enablePullDown: true,
                  header: WaterDropMaterialHeader(),
                  onRefresh: () async {
                    _gamificationsDetailBloc
                        .add(GamificationsDetailLoad(id: widget.id));
                    _mainRefreshController.refreshCompleted();
                  },
                  child: state is GamificationsDetailLoading
                      ? _buildLoading()
                      : state is GamificationsDetailLoaded
                          ? state.records != null
                              ? _buildContent(state)
                              : EmptyData(
                                  message: Dictionary.emptyDataRWActivity)
                          : state is GamificationsDetailFailure
                              ? ErrorContent(error: state.error)
                              : _buildLoading()),
            ),
          ),
        ),
      ),
    );
  }

  _buildLoading() {
    return Skeleton(
        child: Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          padding:
              EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: Container(
                      height: MediaQuery.of(context).size.height / 4.8,
                      color: Colors.grey[300]),
                ),
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: 20.0,
                  color: Colors.grey[300]),
              Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: MediaQuery.of(context).size.width,
                  height: 20.0,
                  color: Colors.grey[300]),
              Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: 200,
                  height: 20.0,
                  color: Colors.grey[300])
            ],
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: Container(
              margin: EdgeInsets.all(10.0),
              width: 200,
              height: 40.0,
              color: Colors.grey[300]),
        )
      ],
    ));
  }

  _buildContent(GamificationsDetailLoaded state) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(12),
          child: Column(
            children: <Widget>[
              Container(
                  height: 150.0,
                  padding: EdgeInsets.all(20),
                  alignment: Alignment.topCenter,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: clr.Colors.gradientBlueToPurple),
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text(
                          gamificationModel.title,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 19.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(right: 5),
                              child: Image.asset(
                                  '${Environment.iconAssets}icon_time.png',
                                  height: 20,
                                  fit: BoxFit.fitWidth),
                            ),
                            Text(
                              'Berakhir : ' +
                                  unixTimeStampToDateWithoutMultiplication(
                                      gamificationModel
                                          .endDate.millisecondsSinceEpoch),
                              maxLines: 3,
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                  fontSize: 17.0, color: Colors.white),
                            )
                          ],
                        ),
                      )
                    ],
                  )),
              Container(
                padding: EdgeInsets.only(bottom: 10, top: 18),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  gamificationModel.description,
                  overflow: TextOverflow.clip,
                  style: TextStyle(fontSize: 16.0, color: Colors.black),
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ),
        Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.all(10),
              child: RoundedButton(
                title: Dictionary.startMission,
                borderRadius: BorderRadius.circular(5.0),
                color: clr.Colors.blue,
                textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
                onPressed: () {
                  _gamificationsDetailBloc
                      .add(GamificationsTakeMission(id: widget.id));
                },
              ),
            ))
      ],
    );
  }
}
