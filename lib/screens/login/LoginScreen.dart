import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/login/Bloc.dart';
import 'package:sapawarga/blocs/update_app/Bloc.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/DialogUpdateApp.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/repositories/UpdateAppRepository.dart';
import 'package:sapawarga/screens/login/LoginForm.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';

class LoginScreen extends StatefulWidget {
  final AuthRepository authRepository;

  LoginScreen({Key key, @required this.authRepository})
      : assert(authRepository != null),
        super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  AuthRepository get _authRepository => widget.authRepository;
  UpdateAppBloc _updateAppBloc;

  String _versionText = Dictionary.version;

  @override
  void initState() {

    AnalyticsHelper.setCurrentScreen(Analytics.LOGIN);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LOGIN);

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        _versionText = packageInfo.version != null
            ? packageInfo.version
            : Dictionary.version;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UpdateAppBloc>(
      create: (context) => _updateAppBloc =
          UpdateAppBloc(updateAppRepository: UpdateAppRepository())
            ..add(CheckUpdate()),
      child: BlocListener<UpdateAppBloc, UpdateAppState>(
        bloc: _updateAppBloc,
        listener: (context, state) {
          if (state is UpdateAppRequired) {
            showDialog(
                context: context,
                builder: (context) => WillPopScope(
                    onWillPop: () {
                      return;
                    },
                    child: DialogUpdateApp()),
                barrierDismissible: false);
          }
        },
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: MultiBlocProvider(
            providers: [
              BlocProvider<LoginBloc>(
                create: (BuildContext context) => LoginBloc(
                  authenticationBloc:
                      BlocProvider.of<AuthenticationBloc>(context),
                  authRepository: _authRepository,
                ),
              )
            ],
            child: Stack(
              children: <Widget>[
                // Login Form
                Container(
                    child: Padding(
                  padding:
                      const EdgeInsets.only(left: 20.0, right: 20.0, top: 80.0),
                  child: LoginForm(),
                )),
                // Footer
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextButton(
                            padding: EdgeInsets.all(5.0),
                            title: Dictionary.termOfService,
                            textStyle: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Colors.blue),
                            onTap: () {
                              AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_TERMS_OF_SERVICE_LOGIN);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BrowserScreen(
                                          url: UrlThirdParty.termOfService,
                                        )),
                              );
                            },
                          ),
                          Text(' | '),
                          TextButton(
                            padding: EdgeInsets.all(5.0),
                            title: Dictionary.privacyPolicy,
                            textStyle: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Colors.blue),
                            onTap: () {
                              AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_PRIVACY_POLICY_LOGIN);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BrowserScreen(
                                          url: UrlThirdParty.privacyPolicy,
                                        )
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),

                    // TODO: Don't delete this code. It's possible to be used again
                    /*GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 5.0, bottom: 10.0),
                        child: Text(
                          Dictionary.downloadGuideBook,
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                      onTap: _downloadGuideBook,
                    ),*/


                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 1.5,
                      color: Color.fromRGBO(180, 180, 180, 0.5),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50.0,
                      color: Colors.white,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Version ' + _versionText,
                                style: TextStyle(fontSize: 12.0)),
                            Text(Dictionary.copyRight,
                                style: TextStyle(fontSize: 12.0)),
                          ],
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _downloadGuideBook() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionDownloadManualBook,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.storage]).then(_onStatusRequested);
                },
              )));
    } else {

      unawaited(Fluttertoast.showToast(
          msg: Dictionary.downloadingFile,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0));

      try {
        await FlutterDownloader.enqueue(
          url: UrlThirdParty.manualBook,
          savedDir: Environment.downloadStorage,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
          true, // click on notification to open downloaded file (for Android)
        );
      } catch (e) {
        String dir = (await getExternalStorageDirectory()).path + '/download';
        await FlutterDownloader.enqueue(
          url: UrlThirdParty.manualBook,
          savedDir: dir,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
          true, // click on notification to open downloaded file (for Android)
        );
      }


      await AnalyticsHelper.setLogEvent(Analytics.EVENT_DOWNLOAD_GUIDEBOOK_LOGIN);
    }
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      _downloadGuideBook();
    }
  }

  @override
  void dispose() {
    _updateAppBloc.close();
    super.dispose();
  }
}
