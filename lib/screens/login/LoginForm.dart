import 'dart:convert';

import 'package:device_apps/device_apps.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/login/Bloc.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/components/PasswordFormField.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  LoginBloc _loginBloc;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  String token;
  List<dynamic> csPhoneNumbers;

  @override
  void initState() {
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _firebaseMessaging.getToken().then((token) {
      this.token = token;
    });
    csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);
    super.initState();
  }

  Future<RemoteConfig> _initializeRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey: FirebaseConfig.callCenterNumbersValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    _onLoginButtonPressed() {
      AnalyticsHelper.setLogEvent(Analytics.EVENT_TAPPED_BUTTON_LOGIN);

      FocusScope.of(context).requestFocus(FocusNode()); // Hide Keyboard
      if (_formKey.currentState.validate()) {
        _loginBloc.add(LoginButtonPressed(
          username: _usernameController.text,
          password: _passwordController.text,
          fcmToken: token,
        ));
      } else {
        print("Validate Error");
      }
    }

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          AnalyticsHelper.setLogEvent(
              Analytics.EVENT_FAILED_LOGIN, <String, dynamic>{
            'username': '${_usernameController.text}',
            'message': '${state.error.toString()}'
          });
          showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: state.error.toString(),
                    buttonText: "OK",
                    onOkPressed: () {
                      Navigator.of(context).pop(); // To close the dialog
                    },
                  ));
          Scaffold.of(context).hideCurrentSnackBar();
        } else if (state is LoginLoading) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Container(
                    margin: EdgeInsets.only(left: 15.0),
                    child: Text(Dictionary.loadingLogin),
                  )
                ],
              ),
              duration: Duration(seconds: 10),
            ),
          );
        } else if (state is LoginValidationError) {
          if (state.errors.containsKey('password') &&
              state.errors['password'][0].toString().contains('salah')) {
            AnalyticsHelper.setLogEvent(
                Analytics.EVENT_FAILED_LOGIN, <String, dynamic>{
              'username': '${_usernameController.text}',
              'message': '${state.errors['password'][0].toString()}'
            });
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.errors['password'][0].toString(),
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                    ));
            Scaffold.of(context).hideCurrentSnackBar();
          } else if (state.errors.containsKey('status')) {
            AnalyticsHelper.setLogEvent(
                Analytics.EVENT_FAILED_LOGIN, <String, dynamic>{
              'username': '${_usernameController.text}',
              'message': Dictionary.errorStatusUnActive
            });
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.errors['status'][0].toString(),
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                    ));
            Scaffold.of(context).hideCurrentSnackBar();
          }
        } else {
          Scaffold.of(context).hideCurrentSnackBar();
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        bloc: _loginBloc,
        builder: (
          BuildContext context,
          LoginState state,
        ) {
          return Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: 80.0,
                  child: Image.asset(
                    'assets/logo/logo.png',
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: 20.0),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          controller: _usernameController,
                          obscureText: false,
                          decoration: InputDecoration(
                              icon: Icon(Icons.person),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 14),
                              labelText: Dictionary.labelUsername,
                              errorText: (state is LoginValidationError &&
                                      state.errors.containsKey('username'))
                                  ? state.errors['username'][0]
                                  : null,
                              border: OutlineInputBorder()),
                          validator: (val) =>
                              Validations.usernameValidation(val),
                        ),
                        SizedBox(height: 15.0),
                        PasswordFormField(
                          controller: _passwordController,
                          labelText: Dictionary.labelPassword,
                          errorText: (state is LoginValidationError &&
                                  state.errors.containsKey('password') &&
                                  !state.errors['password'][0]
                                      .toString()
                                      .contains('salah'))
                              ? state.errors['password'][0]
                              : null,
                          validator: (val) =>
                              Validations.passwordValidation(val),
                        ),
                        SizedBox(height: Dimens.padding),
                        RoundedButton(
                          title: Dictionary.login,
                          borderRadius: BorderRadius.circular(5.0),
                          color: clr.Colors.blue,
                          textStyle: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                          onPressed: state is! LoginLoading
                              ? _onLoginButtonPressed
                              : null,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.0),
                    child: Text('Belum Punya Akun? Lupa Kata Sandi?')),
                SizedBox(height: Dimens.dialogRadius),
                FutureBuilder<RemoteConfig>(
                  future: _initializeRemoteConfig(),
                  builder: (BuildContext context, AsyncSnapshot<RemoteConfig> snapshot) {
                    if (snapshot.hasData) {
                      final dataPhoneNumbers = snapshot.data.getString(
                          FirebaseConfig.callCenterNumbersKey);
                      if (dataPhoneNumbers != null) {
                        csPhoneNumbers = json.decode(dataPhoneNumbers);
                      }
                    }

                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 4.0),
                      child: ButtonTheme(
                          minWidth: MediaQuery.of(context).size.width,
                          height: 45.0,
                          child: OutlineButton(
                              color: Colors.green[600],
                              borderSide: BorderSide(color: Colors.green[600]),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                              child: Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    '${Environment.iconAssets}whatsapp.png',
                                    width: 24.0,
                                    height: 24.0,
                                    color: Colors.green[600],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(Dictionary.contactUs,
                                        style: TextStyle(
                                            color: Colors.green[600],
                                            fontWeight: FontWeight.bold)),
                                  )
                                ],
                              ),
                              onPressed: _openDialogContact)),
                    );
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  _openDialogContact() {
    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
          title: Dictionary.callCenterList,
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Container(
                  margin: index != 0 ? EdgeInsets.only(top: 10.0):null,
                  padding: EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
                  decoration: BoxDecoration(
                    color: Color(0xffebf8ff),
                    shape: BoxShape.rectangle,
                    border: Border.all(color: clr.Colors.blue),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Wrap(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 10.0),
                        child: Image.asset('${Environment.iconAssets}whatsapp.png', width: 20, height: 20, color: clr.Colors.green),
                      ),
                      Text(csPhoneNumbers[index], style: TextStyle(fontFamily: FontsFamily.sourceSansPro, fontSize: 16.0, fontWeight: FontWeight.w600, color: clr.Colors.green))
                    ],
                  ),
                ),
                onTap: (){
                  _launchWhatsApp(csPhoneNumbers[index]);
                  Navigator.of(context).pop();
                },
              );
            },
          ),
          buttonText: Dictionary.close.toUpperCase(),
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ));
  }

  _launchWhatsApp(String csPhoneNumber) async {
    try {
      bool isInstalled = await DeviceApps.isAppInstalled('com.whatsapp');
      csPhoneNumber ??= Environment.csPhone;

      if (csPhoneNumber[0] == '0') {
        csPhoneNumber = csPhoneNumber.replaceFirst('0', '+62');
      }

      if (isInstalled) {
        String urlWhatsApp = 'https://wa.me/${csPhoneNumber}?text=${Uri.encodeFull(Dictionary.helpAdminWA)}%0A%0A';
        if (await canLaunch(urlWhatsApp)) {
          await launch(urlWhatsApp);

          await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_WA_ADMIN_LOGIN);
        } else {
          await launch('tel://${csPhoneNumber}');
          await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_LOGIN);
        }
      } else {
        await launch('tel://${csPhoneNumber}');
        await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_LOGIN);
      }
    } catch (_) {
      await launch('tel://${csPhoneNumber}');
      await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_LOGIN);
    }
  }

  @override
  void dispose() {
    _loginBloc.close();
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
