import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/important_info/important_info_search/Bloc.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';

import 'ImportantInfoDetailScreen.dart';

class ImportantInfoSearchScreen extends StatefulWidget {
  @override
  _ImportantInfoSearchScreenState createState() =>
      _ImportantInfoSearchScreenState();
}

class _ImportantInfoSearchScreenState extends State<ImportantInfoSearchScreen> {
  final _searchController = TextEditingController();
  ScrollController _scrollController = ScrollController();

  ImportantInfoSearchBloc _importantInfoSearchBloc;

  List<ImportantInfoModel> _resultList = List<ImportantInfoModel>();

  Timer _debounce;
  int _maxDataLength;
  int _page = 1;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.IMPORTANT_INFO);

    _searchController.addListener((() {
      _onSearchChanged();
    }));

    _scrollController.addListener(() {
      _scrollListener();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.SearchAppBar(context, _searchController),
      body: Container(
        child: BlocProvider<ImportantInfoSearchBloc>(
          create: (context) => _importantInfoSearchBloc =
              ImportantInfoSearchBloc(ImportantInfoRepository()),
          child: BlocListener<ImportantInfoSearchBloc, ImportantInfoSearchState>(
            bloc: _importantInfoSearchBloc,
            listener: (context, state) {
              if (state is ImportantInfoSearchLoaded) {
                _page = _page + 1;
                _resultList.addAll(state.records);
                _maxDataLength = state.maxLength;
                _resultList = _resultList.toSet().toList();
              }
            },
            child: BlocBuilder<ImportantInfoSearchBloc, ImportantInfoSearchState>(
              bloc: _importantInfoSearchBloc,
              builder: (context, state) => state is ImportantInfoSearchLoading
                  ? _buildLoading()
                  : state is ImportantInfoSearchLoaded
                      ? state.records.isNotEmpty
                          ? _buildContent(state)
                          : EmptyData(message: Dictionary.emptyDataImportantInfo)
                      : state is ImportantInfoSearchFailure
                          ? ErrorContent(error: state.error)
                          : Container(),
            ),
          ),
        ),
      ),
    );
  }

  _buildLoading() {
    return SingleChildScrollView(
      child: Container(
        child: ListView.separated(
            padding: EdgeInsets.symmetric(horizontal: Dimens.padding),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            separatorBuilder: (context, index) => Container(
                width: MediaQuery.of(context).size.width,
                height: 1.0,
                color: Colors.grey[300]),
            itemCount: 4,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: Skeleton(
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 70,
                        height: 70,
                        decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.grey[100], width: 1.0),
                            borderRadius: BorderRadius.circular(5.0),
                            color: Colors.grey[300]),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width - 120,
                        height: 70.0,
                        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              height: 20.0,
                              margin: EdgeInsets.symmetric(
                                  horizontal: Dimens.padding),
                              color: Colors.grey[300],
                            ),
                            Container(
                              width: 150.0,
                              height: 20.0,
                              margin: EdgeInsets.symmetric(
                                  horizontal: Dimens.padding),
                              color: Colors.grey[300],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }

  _buildContent(ImportantInfoSearchLoaded state) {
    return ListView.separated(
        controller: _scrollController,
        padding: EdgeInsets.symmetric(horizontal: Dimens.padding),
        separatorBuilder: (context, index) => Container(
            width: MediaQuery.of(context).size.width,
            height: 1.0,
            color: Colors.grey[300]),
        itemCount: _resultList.length + 1,
        itemBuilder: (context, index) {
          return index >= _resultList.length
              ? _maxDataLength != _resultList.length
              ? Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
            child: Column(
              children: <Widget>[
                CupertinoActivityIndicator(),
                SizedBox(
                  height: 5.0,
                ),
                Text(Dictionary.loadingData),
              ],
            ),
          )
              : Container()
              : _buildItem(context, index);
        });
  }

  Container _buildItem(BuildContext context, int index) {
    return Container(
          margin: EdgeInsets.only(bottom: 10.0, top: 10.0),
          child: FlatButton(
            padding: EdgeInsets.all(0.0),
            onPressed: () {
              _openDetail(_resultList[index]);
            },
            child: Row(
              children: <Widget>[
                Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[100], width: 1.0),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: _resultList[index].imagePathUrl != null
                        ? CachedNetworkImage(
                            imageUrl: _resultList[index].imagePathUrl,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Center(
                                heightFactor: 4.2,
                                child: CupertinoActivityIndicator()),
                            errorWidget: (context, url, error) => Container(
                                height:
                                    MediaQuery.of(context).size.height / 3.3,
                                color: Colors.grey[200],
                                child: Image.asset(
                                    '${Environment.imageAssets}placeholder_square.png',
                                    fit: BoxFit.fitWidth)),
                          )
                        : Image.asset(
                            '${Environment.imageAssets}placeholder.png',
                            fit: BoxFit.fitWidth),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 120,
                  height: 70.0,
                  margin: EdgeInsets.only(left: Dimens.padding),
                  padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(_resultList[index].title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 14.0,
                              fontFamily: FontsFamily.productSans,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey[800])),
                      Text(_resultList[index].category.name,
                          style: TextStyle(
                              fontSize: 12.0,
                              fontFamily: FontsFamily.productSans,
                              color: Colors.grey[600])),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
  }

  _openDetail(ImportantInfoModel record) {
    Navigator.of(context).pop();
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ImportantInfoDetailScreen(id: record.id)));
  }

  void _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (_searchController.text.trim().isNotEmpty) {
        _resultList.clear();
        _page = 1;
        _importantInfoSearchBloc
            .add(ImportantInfoSearch(_searchController.text, _page));
        AnalyticsHelper.setLogEvent(Analytics.EVENT_SEARCH_IMPORTANT_INFO,  <String, dynamic>{
          'keyword': '${_searchController.text}',
        });
      }
    });
  }

  void _scrollListener() {
    if (_resultList.length < _maxDataLength) {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _importantInfoSearchBloc.add(ImportantInfoSearch(_searchController.text, _page));
      }
    }
  }

  @override
  void dispose() {
    _searchController.dispose();
    _importantInfoSearchBloc.close();
    _scrollController.dispose();
    super.dispose();
  }
}
