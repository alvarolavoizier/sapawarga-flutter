import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:html/dom.dart' as dom;
import 'package:path_provider/path_provider.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/important_info/important_info_comment/Bloc.dart';
import 'package:sapawarga/blocs/important_info/important_info_detail/Bloc.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/HeroImagePreviewScreen.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import 'package:sapawarga/screens/importantInformation/ImportantInfoDetailComment.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class ImportantInfoDetailScreen extends StatefulWidget {
  final int id;

  ImportantInfoDetailScreen({this.id});

  @override
  _ImportantInfoDetailScreenState createState() =>
      _ImportantInfoDetailScreenState();
}

class _ImportantInfoDetailScreenState extends State<ImportantInfoDetailScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  RefreshController _mainRefreshController = RefreshController();
  ScrollController _scrollController = ScrollController();

  ImportantInfoDetailBloc _importantInfoDetailBloc;
  ImportantInfoCommentListBloc _listCommentBloc;

  bool _isLoaded = false;
  int _id;
  String _title;
  String _sourceUrl;
  String _likeState = '${Environment.imageAssets}like-empty.png';

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.IMPORTANT_INFO);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            title: Text(Dictionary.importantInfo),
            actions: <Widget>[
              _isLoaded
                  ? Container(
                      margin: EdgeInsets.only(right: 10.0),
                      child: IconButton(
                        icon: Icon(Icons.share),
                        onPressed: () {
                          Share.share(
                              '$_title\nBaca Selengkapnya: $_sourceUrl\n\n_dibagikan dari Sapawarga App_');

                          AnalyticsHelper.setLogEvent(
                              Analytics.EVENT_SHARE_IMPORTANT_INFO,
                              <String, dynamic>{'id': _id, 'title': _title});
                        },
                      ))
                  : Container()
            ]),
        body: MultiBlocProvider(
          providers: [
            BlocProvider<ImportantInfoDetailBloc>(
                create: (context) =>
                    _importantInfoDetailBloc = ImportantInfoDetailBloc(
                        importantInfoRepository: ImportantInfoRepository())
                      ..add(ImportantInfoDetailLoad(id: widget.id))),
            BlocProvider<ImportantInfoCommentListBloc>(
                create: (context) => _listCommentBloc =
                    ImportantInfoCommentListBloc(ImportantInfoRepository())),
          ],
          child:
              BlocListener<ImportantInfoDetailBloc, ImportantInfoDetailState>(
            listener: (context, state) {
              if (state is ImportantInfoDetailLoaded) {
                _isLoaded = true;
                _id = state.record.id;
                _title = state.record.title;
                _sourceUrl = state.record.publicSourceUrl;
                setState(() {});

                _listCommentBloc
                    .add(ImportantInfoCommentListLoad(widget.id, 1));
              }
            },
            child: WillPopScope(
          onWillPop: () {
             Navigator.pop(context, true);
          },
              child: SmartRefresher(
                controller: _mainRefreshController,
                enablePullDown: true,
                header: WaterDropMaterialHeader(),
                onRefresh: () async {
                  _isLoaded = false;
                  setState(() {});
                  _importantInfoDetailBloc
                      .add(ImportantInfoDetailLoad(id: widget.id));
                  _mainRefreshController.refreshCompleted();
                },
                child: ListView(
                  controller: _scrollController,
                  children: <Widget>[
                    BlocBuilder<ImportantInfoDetailBloc,
                            ImportantInfoDetailState>(
                        builder: (context, state) =>
                            state is ImportantInfoDetailLoading
                                ? _buildLoading()
                                : state is ImportantInfoDetailLoaded
                                    ? _buildContent(state)
                                    : state is ImportantInfoDetailFailure
                                        ? ErrorContent(error: state.error)
                                        : Container())
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  _buildLoading() {
    return Container(
      margin: EdgeInsets.only(
          left: Dimens.padding, right: Dimens.padding, top: 10.0),
      child: Skeleton(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 20.0,
              color: Colors.grey[300],
            ),
            Container(
              margin: EdgeInsets.only(top: 2.0),
              width: MediaQuery.of(context).size.width / 1.5,
              height: 20.0,
              color: Colors.grey[300],
            ),
            SizedBox(height: 5.0),
            Container(
              width: 100,
              height: 15.0,
              color: Colors.grey[300],
            ),
            SizedBox(height: 5.0),
            Container(
              width: 150,
              height: 15.0,
              color: Colors.grey[300],
            ),
            SizedBox(height: 15.0),
            Container(
              height: 180,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: Colors.grey[300]),
            ),
            SizedBox(height: 15.0),
            Container(
              height: 20.0,
              color: Colors.grey[300],
            ),
            Container(
              margin: EdgeInsets.only(top: 2.0),
              height: 20.0,
              color: Colors.grey[300],
            ),
            Container(
              margin: EdgeInsets.only(top: 2.0),
              height: 20.0,
              color: Colors.grey[300],
            ),
            Container(
              margin: EdgeInsets.only(top: 2.0),
              height: 20.0,
              color: Colors.grey[300],
            ),
            Container(
              margin: EdgeInsets.only(top: 2.0),
              width: MediaQuery.of(context).size.width / 1.5,
              height: 20.0,
              color: Colors.grey[300],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 1.0,
              color: Colors.grey,
              margin: EdgeInsets.only(top: 25.0, bottom: 10.0),
            ),
          ],
        ),
      ),
    );
  }

  _buildContent(ImportantInfoDetailLoaded state) {
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_DETAIL_IMPORTANT_INFO,
        <String, dynamic>{'id': state.record.id, 'title': state.record.title});

    bool isLiked = state.record.isLiked != null ? state.record.isLiked : false;

    if (isLiked) {
      _likeState = '${Environment.imageAssets}liked.png';
    } else {
      _likeState = '${Environment.imageAssets}like-empty.png';
    }

    return Container(
      margin: EdgeInsets.only(left: Dimens.padding, right: Dimens.padding),
      padding: EdgeInsets.only(top: 10.0, bottom: 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(state.record.title,
              style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: FontsFamily.productSans,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[800])),
          state.record.imagePathUrl != null
              ? Container(
                  margin: EdgeInsets.only(top: 15.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(shape: BoxShape.circle),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      child: GestureDetector(
                        child: Hero(
                          tag: Dictionary.heroImageTag,
                          child: CachedNetworkImage(
                            imageUrl: state.record.imagePathUrl,
                            placeholder: (context, url) => Center(
                                heightFactor: 9,
                                child: CupertinoActivityIndicator()),
                            errorWidget: (context, url, error) {
                              print(error.toString());
                              return Container();
                            },
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => HeroImagePreview(
                                      Dictionary.heroImageTag,imageUrl: 
                                      state.record.imagePathUrl,)));
                        },
                      )))
              : Container(),
          Container(
            margin: EdgeInsets.only(top: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Text(state.record.category.name,
                          style: TextStyle(
                              fontSize: 12.0,
                              fontFamily: FontsFamily.productSans,
                              color: Colors.grey)),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Text('·',
                            style: TextStyle(
                                fontSize: 12.0,
                                fontFamily: FontsFamily.productSans,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey)),
                      ),
                      Text(
                          unixTimeStampToDateWithoutDay(state.record.createdAt),
                          style: TextStyle(
                              fontSize: 12.0,
                              fontFamily: FontsFamily.productSans,
                              color: Colors.grey)),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        child: Image.asset(
                          _likeState,
                          width: 18.0,
                          height: 18.0,
                        ),
                        onTap: () {
                          _importantInfoDetailBloc.add(ImportantInfoDetailLike(
                              importantInfoId: state.record.id));
                          if (isLiked) {
                            _updateLike(state.record, true);
                            AnalyticsHelper.setLogEvent(
                                Analytics.EVENT_LIKE_IMPORTANT_INFO,
                                <String, dynamic>{
                                  'id': state.record.id,
                                  'title': state.record.title
                                });
                          } else {
                            _updateLike(state.record, false);
                            AnalyticsHelper.setLogEvent(
                                Analytics.EVENT_UNLIKE_IMPORTANT_INFO,
                                <String, dynamic>{
                                  'id': state.record.id,
                                  'title': state.record.title
                                });
                          }
                        },
                      ),
                      Container(
                        height: 18,
                        margin: EdgeInsets.only(left: 10.0),
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            color: Colors.black.withOpacity(0.7)),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              '${Environment.iconAssets}eye.png',
                              height: 12.0,
                            ),
                            Container(
                                height: 15.0,
                                margin: EdgeInsets.only(left: 3.0),
                                child: Text('${state.record.totalViewers}',
                                    style: TextStyle(
                                        fontSize: 11.0, color: Colors.white)))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20.0),
          Html(
              data: state.record.content,
              defaultTextStyle: TextStyle(
                  color: Colors.grey[800],
                  fontSize: 14.0,
                  fontFamily: FontsFamily.productSans),
              customTextAlign: (dom.Node node) {
                return TextAlign.justify;
              },
              onLinkTap: (url) {
                _launchURL(url);
              },
              customTextStyle: (dom.Node node, TextStyle baseStyle) {
                if (node is dom.Element) {
                  switch (node.localName) {
                    case "p":
                      return baseStyle.merge(TextStyle(height: 1.3));
                  }
                }
                return baseStyle;
              }),
          Container(
            margin: EdgeInsets.only(left: 2.0, top: Dimens.padding),
            child: GestureDetector(
              child: Text(
                state.record.sourceUrl,
                style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: 14.0,
                    fontFamily: FontsFamily.productSans,
                    decoration: TextDecoration.underline),
              ),
              onTap: () {
                _launchURL(state.record.sourceUrl);
              },
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 1.0,
            color: Colors.grey,
            margin: EdgeInsets.only(top: 25.0, bottom: 16.0),
          ),
          state.record.attachments != null &&
                  state.record.attachments.isNotEmpty
              ? _buildAttachments(state)
              : Container(),
          ImportantInfoDetailComment(
              _scaffoldKey, widget.id, _listCommentBloc, _scrollController)
        ],
      ),
    );
  }

  _buildAttachments(ImportantInfoDetailLoaded state) {
    return Container(
      margin: EdgeInsets.only(bottom: 30.0),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: state.record.attachments.length,
        itemBuilder: (ctx, idx) => Wrap(
          alignment: WrapAlignment.spaceBetween,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width - 180,
              child: Text(state.record.attachments[idx].name,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: FontsFamily.productSans,
                      color: Colors.grey[800])),
            ),
            ButtonTheme(
              minWidth: 129.0,
              height: 34.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: RaisedButton(
                highlightElevation: 5,
                child: Text(Dictionary.downloadAttachment,
                    style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: FontsFamily.productSans,
                        color: Colors.white)),
                onPressed: () {
                  _downloadAttachment(state.record.attachments[idx].name,
                      state.record.attachments[idx].fileUrl);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _updateLike(ImportantInfoModel record, bool isLike) {
    if (isLike) {
      if (record.isLiked) {
        setState(() {
          record.isLiked = false;
          record.likesCount = record.likesCount - 1;
        });
      }
    } else {
      setState(() {
        record.isLiked = true;
        record.likesCount = record.likesCount + 1;
      });
    }
  }

  void _downloadAttachment(String name, String url) async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionDownloadAttachment,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.storage]).then((val) {
                    _onStatusRequested(val, name, url);
                  });
                },
              )));
    } else {

      unawaited(Fluttertoast.showToast(
          msg: Dictionary.downloadingFile,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0));

      try {
        await FlutterDownloader.enqueue(
          url: url,
          savedDir: Environment.downloadStorage,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
          true, // click on notification to open downloaded file (for Android)
        );
      } catch (e) {
        String dir = (await getExternalStorageDirectory()).path + '/download';
        await FlutterDownloader.enqueue(
          url: url,
          savedDir: dir,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
          true, // click on notification to open downloaded file (for Android)
        );
      }


      await AnalyticsHelper.setLogEvent(
          Analytics.EVENT_DOWNLOAD_ATTACHMENT_IMPORTANT_INFO, <String, dynamic>{
        'important_info_id': _id,
        'attachment_name': name,
      });
    }
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses,
      String name, String url) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      _downloadAttachment(name, url);
    }
  }

  @override
  void dispose() {
    _importantInfoDetailBloc.close();
    _listCommentBloc.close();
    _mainRefreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }
}
