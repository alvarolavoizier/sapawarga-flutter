import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:sapawarga/blocs/counter_hoax/Bloc.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:url_launcher/url_launcher.dart';

class CounterHoaxDetailScreen extends StatelessWidget {
  final CounterHoaxModel record;

  CounterHoaxDetailScreen({@required this.record}) : assert(record != null);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterHoaxDetailBloc>(
      create: (context) => CounterHoaxDetailBloc(),
      child: CounterHoaxDetail(record),
    );
  }
}

class CounterHoaxDetail extends StatefulWidget {
  final CounterHoaxModel record;

  CounterHoaxDetail(this.record);

  @override
  _CounterHoaxDetailState createState() => _CounterHoaxDetailState();
}

class _CounterHoaxDetailState extends State<CounterHoaxDetail> {
  CounterHoaxDetailBloc _counterHoaxDetailBloc;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.SABER_HOAX);
    AnalyticsHelper.setLogEvent(
        Analytics.EVENT_DETAIL_SABER_HOAX, <String, dynamic>{
      'id': widget.record.id,
      'title': widget.record.title
    });

    _counterHoaxDetailBloc = BlocProvider.of<CounterHoaxDetailBloc>(context);
    _counterHoaxDetailBloc.add(CounterHoaxDetailLoad(record: widget.record));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(Dictionary.saberHoax), centerTitle: true),
        body: BlocBuilder<CounterHoaxDetailBloc, CounterHoaxDetailState>(
          bloc: _counterHoaxDetailBloc,
          builder: (context, state) {
            return state is CounterHoaxDetailLoading
                ? _buildLoading()
                : state is CounterHoaxDetailLoaded
                    ? _buildContent(state)
                    : Container();
          },
        ));
  }

  Widget _buildLoading() {
    return SingleChildScrollView(
      child: Skeleton(
        child: Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
                child: Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: 20.0,
                        color: Colors.grey[300]),
                    SizedBox(height: 5.0),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: 20.0,
                        color: Colors.grey[300]),
                    SizedBox(height: 10.0),
                  ],
                )),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 3.3,
              color: Colors.grey[300],
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
              child: Column(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _listTextShimmer(),
                  ),
                  SizedBox(height: 10.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _listTextShimmer(),
                  ),
                  SizedBox(height: 10.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _listTextShimmer(),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildContent(CounterHoaxDetailLoaded state) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
            child: Text(
                '[${state.counterHoaxModel.category.name.toUpperCase()}]  ${state.counterHoaxModel.title}',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold)),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: CachedNetworkImage(
                imageUrl: state.counterHoaxModel.coverPathUrl,
                fit: BoxFit.fill,
                placeholder: (context, url) => Center(
                    heightFactor: 10.2, child: CupertinoActivityIndicator()),
                errorWidget: (context, url, error) => Container(
                    height: MediaQuery.of(context).size.height / 3.3,
                    color: Colors.grey[200],
                    child: Image.asset(
                        '${Environment.imageAssets}placeholder.png',
                        fit: BoxFit.fitWidth))),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
            child: Html(
                data: state.counterHoaxModel.content.replaceAll('\n', '</br>'),
                defaultTextStyle:
                    TextStyle(color: Colors.black, fontSize: 15.0),
                customTextAlign: (dom.Node node) {
                  return TextAlign.justify;
                },
                onLinkTap: _launchURL),
          )
        ],
      ),
    );
  }

  List<Widget> _listTextShimmer() {
    List<Widget> list = List();
    for (int i = 0; i < 4; i++) {
      list.add(Container(
          width: i == 3 ? 100.0 : MediaQuery.of(context).size.width,
          height: 20.0,
          color: Colors.grey[300]));
      list.add(SizedBox(height: 5.0));
    }
    return list;
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
