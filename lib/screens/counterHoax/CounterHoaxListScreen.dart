import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/counter_hoax/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';
import 'package:sapawarga/repositories/CounterHoaxRepository.dart';
import 'package:sapawarga/screens/counterHoax/CounterHoaxDetailScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:url_launcher/url_launcher.dart';

class CounterHoaxListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterHoaxListBloc>(
      create: (context) =>
          CounterHoaxListBloc(counterHoaxRepository: CounterHoaxRepository()),
      child: CounterHoaxList(),
    );
  }
}

class CounterHoaxList extends StatefulWidget {
  @override
  _CounterHoaxListState createState() => _CounterHoaxListState();
}

class _CounterHoaxListState extends State<CounterHoaxList> {
  final RefreshController _mainRefreshController = RefreshController();
  final RefreshController _listRefreshController = RefreshController();
  ScrollController _scrollController = ScrollController();
  int maxDatalength;
  int _page = 1;
  List<CounterHoaxModel> dataListModel = [];

  CounterHoaxListBloc _counterHoaxListBloc;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.SABER_HOAX);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_SABER_HOAX);

    _initialize();
    _counterHoaxListBloc = BlocProvider.of<CounterHoaxListBloc>(context);
    _counterHoaxListBloc.add(CounterHoaxListLoad(page: _page));
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Dictionary.saberHoax),
        centerTitle: true,
      ),
      body: SmartRefresher(
          controller: _mainRefreshController,
          enablePullDown: true,
          header: WaterDropMaterialHeader(),
          onRefresh: () async {
            _counterHoaxListBloc.add(CounterHoaxListRefresh());
            _mainRefreshController.refreshCompleted();
          },
          child: _buildContent()),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _launchWhatsApp();

          AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_WA_SABER_HOAX);
        },
        child: Image.asset(
          '${Environment.iconAssets}whatsapp.png',
          color: Colors.white,
          width: 24.0,
          height: 24.0,
        ),
        backgroundColor: Colors.green,
      ),
    );
  }

  _buildContent() {
    return BlocBuilder<CounterHoaxListBloc, CounterHoaxListState>(
      bloc: _counterHoaxListBloc,
      builder: (context, state) {
        return state is CounterHoaxListLoading
            ? _buildItemLoading()
            : state is CounterHoaxListLoaded
                ? _buildItemContent(state)
                : state is CounterHoaxListFailure
                    ? ErrorContent(error: state.error)
                    : Container();
      },
    );
  }

  _buildItemLoading() {
    return Container(
      child: Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.all(15.0),
          child: Text(
            'Berita Terverifikasi',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 50.0),
          child: ListView.builder(
            padding: EdgeInsets.only(bottom: 15.0),
            itemCount: 3,
            itemBuilder: (context, pos) {
              return Card(
                elevation: 5.0,
                margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 15.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Skeleton(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 4.5,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Skeleton(
                                  width:
                                      MediaQuery.of(context).size.width - 180.0,
                                  height: 20.0,
                                ),
                                SizedBox(height: 5.0),
                                Skeleton(
                                  width: 100.0,
                                  height: 20.0,
                                ),
                              ],
                            ),
                            Skeleton(
                              width: 100.0,
                              height: 20.0,
                            ),
                          ],
                        ))
                  ],
                ),
              );
            },
          ),
        )
      ]),
    );
  }

  _buildItemContent(CounterHoaxListLoaded state) {
    if (state.isLoad) {
      _page++;
      maxDatalength = state.maxLengthData;
      dataListModel.addAll(state.records);
      state.isLoad = false;
    }

    return Container(
      child: Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.all(15.0),
          child: Text(
            'Berita Terverifikasi',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 50.0),
          child: dataListModel.isNotEmpty
              ? SmartRefresher(
                  controller: _listRefreshController,
                  enablePullDown: true,
                  header: WaterDropMaterialHeader(),
                  onRefresh: () async {
                    _page = 1;
                    dataListModel.clear();
                    _counterHoaxListBloc.add(CounterHoaxListRefresh());
                    _listRefreshController.refreshCompleted();
                  },
                  child: ListView.builder(
                    padding: EdgeInsets.only(bottom: 15.0),
                    itemCount: dataListModel.length + 1,
                    controller: _scrollController,
                    itemBuilder: (context, position) {
                      return position >= dataListModel.length
                          ? maxDatalength != dataListModel.length
                              ? Padding(
                                  padding: const EdgeInsets.only(
                                      top: 20.0, bottom: 20.0),
                                  child: Column(
                                    children: <Widget>[
                                      CupertinoActivityIndicator(),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(Dictionary.loadingData),
                                    ],
                                  ),
                                )
                              : Container()
                          : GestureDetector(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                elevation: 5.0,
                                margin:
                                    EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 15.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    AspectRatio(
                                      aspectRatio: 16 / 9,
                                      child: CachedNetworkImage(
                                          imageUrl: dataListModel[position]
                                              .coverPathUrl,
                                          imageBuilder: (context,
                                                  imageProvider) =>
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  5.0),
                                                          topRight:
                                                              Radius.circular(
                                                                  5.0)),
                                                  image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                          placeholder: (context, url) => Center(
                                              heightFactor: 10.2,
                                              child:
                                                  CupertinoActivityIndicator()),
                                          errorWidget: (context, url, error) =>
                                              Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey[200],
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    5.0),
                                                            topRight: Radius
                                                                .circular(5.0)),
                                                  ),
                                                  child: Image.asset(
                                                      '${Environment.imageAssets}placeholder.png',
                                                      fit: BoxFit.fitWidth))),
                                    ),
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.all(15.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  180.0,
                                              child: Text(
                                                StringUtils.capitalizeWord(
                                                    dataListModel[position]
                                                        .title),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.black),
                                              ),
                                            ),
                                            Container(
                                              width: 110.0,
                                              child: Text(
                                                dataListModel[position]
                                                    .category
                                                    .name
                                                    .toUpperCase(),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                textAlign: TextAlign.end,
                                                style: TextStyle(
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.bold,
                                                    color: _categoryColor(
                                                        dataListModel[position]
                                                            .category
                                                            .name)),
                                              ),
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              ),
                              onTap: () {
                                _openDetail(dataListModel[position]);
                              },
                            );
                    },
                  ),
                )
              : EmptyData(
                  message: Dictionary.emptyDataSaberHoax, center: false),
        )
      ]),
    );
  }

  _categoryColor(String category) {
    Color color = Colors.black;
    String _category = category.toLowerCase();
    if (_category.contains('klarifikasi')) {
      color = Colors.black;
    } else if (_category.contains('disinformasi')) {
      color = Colors.amber;
    } else if (_category.contains('misinformasi')) {
      color = Colors.blue;
    }
    return color;
  }

  _launchWhatsApp() async {
    String urlWhatsApp =
        'https://wa.me/${Environment.saberHoaxPhone}?text=${Uri.encodeFull(Dictionary.reportSaberHoax)}%0A%0A';
    List<String> recipients = ['${Environment.saberHoaxPhone}'];
    String message = Dictionary.reportSaberHoax;
    if (await canLaunch(urlWhatsApp)) {
      await launch(urlWhatsApp);
    } else {
      await FlutterSms.sendSMS(message: message, recipients: recipients)
          .catchError((onError) {
        debugPrint(onError);
      });
    }
  }

  _openDetail(CounterHoaxModel record) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CounterHoaxDetailScreen(record: record)));
  }

  void _initialize() async {
    maxDatalength = await Preferences.getTotalCount();
  }

  void _scrollListener() {
    if (dataListModel.length != maxDatalength) {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _counterHoaxListBloc.add(CounterHoaxListLoad(page: _page));
      }
    }
  }

  @override
  void dispose() {
    _mainRefreshController.dispose();
    _listRefreshController.dispose();
    _scrollController.dispose();
    _counterHoaxListBloc.close();
    super.dispose();
  }
}
