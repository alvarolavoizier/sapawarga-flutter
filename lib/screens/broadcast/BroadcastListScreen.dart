import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:html/parser.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/broadcast/broadcast_list/Bloc.dart';
import 'package:sapawarga/blocs/message_badge/Bloc.dart';
import 'package:sapawarga/components/DialogConfirmation.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as prefix0;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/MessageModel.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class BroadcastListScreen extends StatelessWidget {
  final BroadcastRepository broadcastRepository = BroadcastRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BroadcastListBloc>(
      create: (BuildContext context) =>
          BroadcastListBloc(broadcastRepository: broadcastRepository),
      child: BroadcastList(),
    );
  }
}

class BroadcastList extends StatefulWidget {
  @override
  _BroadcastListState createState() => _BroadcastListState();
}

class _BroadcastListState extends State<BroadcastList> {
  final RefreshController _mainRefreshController = RefreshController();

  BroadcastListBloc _broadcastListBloc;
  AuthenticationBloc _authenticationBloc;
  MessageBadgeBloc _messageBadgeBloc;
  List<bool> inputs = [];
  List<MessageModel> listDeleteData = [];
  bool isDelete = false;
  ScrollController _scrollController = ScrollController();
  int _page = 1;
  int maxDatalength;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.MESSAGES);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_BROADCAST);

    _initialize();
    _broadcastListBloc = BlocProvider.of<BroadcastListBloc>(context);
    _broadcastListBloc.add(BroadcastListLoad(page: _page));
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _messageBadgeBloc = BlocProvider.of<MessageBadgeBloc>(context);
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<BroadcastListBloc, BroadcastListState>(
            bloc: _broadcastListBloc,
            listener: (context, state) {
              if (state is BroadcastListFailure) {
                if (state.error.contains(Dictionary.errorUnauthorized)) {
                  _authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                }
              }

              if (state is BroadcastListLoaded) {
                _messageBadgeBloc.add(CheckMessageBadge());
                _updatePage(state.records);
                maxDatalength = state.maxData;
              }
            }),
        BlocListener<MessageBadgeBloc, MessageBadgeState>(
            bloc: _messageBadgeBloc,
            listener: (context, state) {
              if (state is MessageBadgeShow) {
                if (state.fromNotification) {
                  _broadcastListBloc.add(BroadcastListLoad(page: 1));
                }
              }
            }),
      ],
      child: BlocBuilder<BroadcastListBloc, BroadcastListState>(
        bloc: _broadcastListBloc,
        builder: (context, state) {
          return Scaffold(
              appBar: AppBar(
                title: ListTile(
                  title: Text(
                    Dictionary.message,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
              bottomNavigationBar: isDelete
                  ? Container(
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.only(right: 25),
                      width: MediaQuery.of(context).size.width,
                      height: 50,
                      child: GestureDetector(
                        onTap: () {
                          if (listDeleteData.isNotEmpty) {
                            _confirmDeleteList(listDeleteData);
                          }
                        },
                        child: Text(Dictionary.delete,
                            style: TextStyle(
                                color: prefix0.Colors.blue,
                                fontSize: 17,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  : Container(height: 0, width: 0),
              body: SmartRefresher(
                  controller: _mainRefreshController,
                  enablePullDown: true,
                  header: WaterDropMaterialHeader(),
                  onRefresh: () async {
                    _broadcastListBloc.add(BroadcastListLoad(page: 1));
                    isDelete = false;
                    listDeleteData.clear();
                    inputs.clear();
                    _mainRefreshController.refreshCompleted();
                  },
                  child: state is BroadcastListLoading
                      ? _buildLoading()
                      : state is BroadcastListLoaded
                          ? state.records.isNotEmpty
                              ? _buildContent(state)
                              : EmptyData(message: Dictionary.emptyDataMessages)
                          : state is BroadcastListFailure
                              ? ErrorContent(error: state.error)
                              : _buildLoading()));
        },
      ),
    );
  }

  _buildLoading() {
    listDeleteData.clear();
    inputs.clear();
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) => Card(
          margin: EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
          elevation: 0.2,
          child: Container(
            margin: EdgeInsets.all(15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(right: 10.0, top: 5.0),
                    child: Skeleton(
                      width: 24.0,
                      height: 24.0,
                    )),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Skeleton(
                          width: MediaQuery.of(context).size.width,
                          height: 20.0),
                      Skeleton(
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 20.0,
                            margin: EdgeInsets.only(top: 10.0, bottom: 5.0),
                            color: Colors.grey[300]),
                      ),
                      Skeleton(
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 20.0,
                            margin: EdgeInsets.only(bottom: 5.0),
                            color: Colors.grey[300]),
                      ),
                      Skeleton(
                        child: Container(
                            width: MediaQuery.of(context).size.width - 170,
                            height: 20.0,
                            margin: EdgeInsets.only(bottom: 15.0),
                            color: Colors.grey[300]),
                      ),
                      Skeleton(
                          width: MediaQuery.of(context).size.width - 250,
                          height: 15.0),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  void ItemChange(bool val, int index, MessageModel messageModel) {
    setState(() {
      if (val) {
        if (!listDeleteData.contains(messageModel)) {
          listDeleteData.add(messageModel);
        }
      } else {
        if (listDeleteData.contains(messageModel)) {
          for (int i = 0; i < listDeleteData.length; i++) {
            if (listDeleteData[i].id == messageModel.id) {
              listDeleteData.removeAt(i);
            }
          }
        }
      }
      inputs[index] = val;
    });
  }

  _buildContent(BroadcastListLoaded state) {
    if (state.records.isNotEmpty) {
      for (int i = 0; i < state.records.length; i++) {
        inputs.add(false);
      }
    }

    if (state.isDeleteSuccess != null && state.isDeleteSuccess) {
      Future.delayed(const Duration(milliseconds: 100), () {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(Dictionary.successDelete),
        ));
      });
      state.isDeleteSuccess = false;
    }

    return ListView.builder(
          controller: _scrollController,
          padding: EdgeInsets.only(bottom: 10.0),
          itemCount: state.records.length + 1,
          itemBuilder: (context, index) {
            if (index == state.records.length) {
              if (state.records.length > 15 &&
                  maxDatalength != state.records.length) {
                return Padding(
                  padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: Column(
                    children: <Widget>[
                      CupertinoActivityIndicator(),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(Dictionary.loadingData),
                    ],
                  ),
                );
              } else {
                return Container();
              }
            }

            return GestureDetector(
              child: Card(
                  color: state.records[index].readAt == null
                      ? Color(0xFFFFF9EE)
                      : null,
                  margin: EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
                  elevation: 0.2,
                  child: Container(
                    margin: EdgeInsets.all(15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(right: 10.0, top: 5.0),
                            child: Image.asset(
                              '${Environment.iconAssets}broadcast.png',
                              width: 24.0,
                              height: 24.0,
                            )),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                state.records[index].title,
                                style: state.records[index].readAt == null
                                    ? TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold)
                                    : TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.grey[700]),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8.0, bottom: 15.0),
                                child: Text(
                                  _parseHtmlString(
                                      state.records[index].content),
                                  maxLines: 3,
                                  overflow: TextOverflow.clip,
                                  style: state.records[index].readAt == null
                                      ? TextStyle(
                                          fontSize: 15.0, color: Colors.black)
                                      : TextStyle(
                                          fontSize: 15.0,
                                          color: Colors.grey[700]),
                                ),
                              ),
                              Text(
                                unixTimeStampToDateTime(
                                    state.records[index].updatedAt),
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                        isDelete
                            ? Container(
                                child: Checkbox(
                                value: inputs[index],
                                onChanged: (bool val) {
                                  ItemChange(val, index, state.records[index]);
                                },
                              ))
                            : Container(),
                      ],
                    ),
                  )),
              onTap: () {
                _openDetail(state.records[index]);
              },
              onLongPress: () {
                setState(() {
                  isDelete = true;
                });
              },
            );
          },
    );
  }

  _openDetail(MessageModel record) async {
    await _broadcastListBloc.add(BroadcastListTap(id: record.id));

    await _messageBadgeBloc.add(CheckMessageBadge());
    listDeleteData.clear();
    inputs.clear();
    isDelete = false;

    await Navigator.pushNamed(context, NavigationConstrants.BroadcastDetail,
        arguments: record.messageId);
  }

  _confirmDeleteList(List<MessageModel> record) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return DialogConfirmation(
          title: Dictionary.deleteMessage,
          body: Dictionary.confirmDeleteMessage,
          buttonOkText: Dictionary.delete,
          onOkPressed: () async {
            await _broadcastListBloc
                .add(BroadcastMultipleDelete(record: record));
            await _messageBadgeBloc.add(CheckMessageBadge());
            await Navigator.of(context).pop();
            setState(() {
              isDelete = false;
            });
          },
        );
      },
    );
  }

  String _parseHtmlString(String htmlString) {
    var document = parse(htmlString);

    String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }

  void _scrollListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      _broadcastListBloc.add(BroadcastListLoad(page: _page));
    }
  }

  void _initialize() async {
    _page = await Preferences.getBroadcastPage() != null
        ? await Preferences.getBroadcastPage()
        : 1;
    maxDatalength = await Preferences.getTotalCount() != null
        ? await Preferences.getTotalCount()
        : 0;
  }

  void _updatePage(List<MessageModel> records) async {
    _page = (records.length / 20).round() + 1;
    if (_page == 1 && records.length < 20) {
      _page = 0;
    }
    await Preferences.setBroadcastPage(_page);
  }

  @override
  void dispose() {
    _mainRefreshController.dispose();
    _scrollController.dispose();
    _broadcastListBloc.close();
    super.dispose();
  }
}
