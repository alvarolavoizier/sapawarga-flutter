import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:html/dom.dart' as dom;
import 'package:pedantic/pedantic.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/broadcast/broadcast_detail/bloc.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NewsDetailArgumentsModel.dart';
import 'package:sapawarga/models/PollingModel.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';
import 'package:sapawarga/repositories/PollingRepository.dart';
import 'package:sapawarga/repositories/SurveyRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;

class BroadcastDetailScreen extends StatefulWidget {
  final int id;

  BroadcastDetailScreen({@required this.id}) : assert(id != null);

  @override
  _BroadcastDetailScreenState createState() => _BroadcastDetailScreenState();
}

class _BroadcastDetailScreenState extends State<BroadcastDetailScreen> {
  BroadcastDetailBloc _broadcastDetailBloc;
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.MESSAGES);

    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(Dictionary.message)),
      body: BlocProvider<BroadcastDetailBloc>(
        create: (context) =>
            _broadcastDetailBloc = BroadcastDetailBloc(BroadcastRepository())
              ..add(BroadcastDetailLoad(id: widget.id)),
        child: BlocListener<BroadcastDetailBloc, BroadcastDetailState>(
          bloc: _broadcastDetailBloc,
          listener: (context, state) {
            if (state is BroadcastDetailLoaded) {
              AnalyticsHelper.setLogEvent(
                  Analytics.EVENT_DETAIL_BROADCAST, <String, dynamic>{
                'id': state.record.id,
                'title': state.record.title.length > 90
                    ? '${state.record.title.substring(0, 90)}...'
                    : state.record.title
              });
            }

            if (state is BroadcastDetailFailure) {
              if (state.error.contains(Dictionary.errorUnauthorized)) {
                _authenticationBloc.add(LoggedOut());
                Navigator.of(context).pop();
              }
            }
          },
          child: BlocBuilder<BroadcastDetailBloc, BroadcastDetailState>(
              bloc: _broadcastDetailBloc,
              builder: (context, state) => state is BroadcastDetailLoading
                  ? _buildLoading()
                  : state is BroadcastDetailLoaded
                      ? _buildContent(state)
                      : state is BroadcastDetailFailure
                          ? ErrorContent(error: state.error)
                          : _buildLoading()),
        ),
      ),
    );
  }

  _buildLoading() {
    return Container(
      padding: EdgeInsets.all(Dimens.padding),
      child: Skeleton(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 150,
              height: 18,
              color: Colors.grey[300],
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.5,
              height: 20,
              margin: EdgeInsets.only(top: 5),
              color: Colors.grey[300],
            ),
            Container(
              width: 100,
              height: 15,
              margin: EdgeInsets.only(top: 5),
              color: Colors.grey[300],
            ),
            Container(
              width: 100,
              height: 15,
              margin: EdgeInsets.only(top: 2),
              color: Colors.grey[300],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 18,
              margin: EdgeInsets.only(top: Dimens.padding),
              color: Colors.grey[300],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 18,
              margin: EdgeInsets.only(top: 2),
              color: Colors.grey[300],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 18,
              margin: EdgeInsets.only(top: 2),
              color: Colors.grey[300],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 18,
              margin: EdgeInsets.only(top: 2),
              color: Colors.grey[300],
            ),
            Container(
              width: MediaQuery.of(context).size.width / 2,
              height: 18,
              margin: EdgeInsets.only(top: 2),
              color: Colors.grey[300],
            ),
            SizedBox(
              height: Dimens.sbHeight,
            ),
          ],
        ),
      ),
    );
  }

  _buildContent(BroadcastDetailLoaded state) {
    return ListView(
      padding: EdgeInsets.all(Dimens.padding),
      children: <Widget>[
        _buildText(
            Text(
              'Dari: ${state.record.author.name}',
              style: TextStyle(
                  fontSize: 12.0,
                  color: Colors.black,
                  fontWeight: FontWeight.w600),
            ),
            context),
        _buildText(
            Text(
              state.record.title,
              style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            context),
        _buildText(
            Text(
              state.record.category.name,
              style: TextStyle(
                  fontSize: 12.0,
                  color: Colors.black,
                  fontWeight: FontWeight.w600),
            ),
            context),
        _buildText(
            Text(
              unixTimeStampToDate(state.record.updatedAt),
              style: TextStyle(fontSize: 12.0, color: Colors.grey),
            ),
            context),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(top: 6, bottom: Dimens.padding),
          child: Html(
              data: state.record.description,
              defaultTextStyle: TextStyle(
                  color: Colors.grey[800],
                  fontSize: 14.0,
                  fontFamily: FontsFamily.productSans),
              customTextAlign: (dom.Node node) {
                return TextAlign.justify;
              },
              onLinkTap: (url) {
                _launchURL(url);
              },
              customTextStyle: (dom.Node node, TextStyle baseStyle) {
                if (node is dom.Element) {
                  switch (node.localName) {
                    case "p":
                      return baseStyle.merge(TextStyle(height: 1.3));
                  }
                }
                return baseStyle;
              }),
        ),
        _openLink(state),
        SizedBox(
          height: Dimens.sbHeight,
        ),
      ],
    );
  }

  _openLink(BroadcastDetailLoaded state) {
    if (state.record.type != null) {
      if (state.record.type == 'external') {
        return RaisedButton(
            onPressed: () {
              _launchURL(state.record.linkUrl);
            },
            child: Text('Buka Tautan'),
          color: clr.Colors.blue,
          textColor: Colors.white,
        );
      } else if (state.record.type == 'internal') {
        switch (state.record.internalObjectType) {
          case 'survey':
            return RaisedButton(
              onPressed: () {
                _openDetailSurvey(state.record.internalObjectId);
              },
              child: Text('Buka Survei'),
              color: clr.Colors.blue,
              textColor: Colors.white,
            );
          case 'polling':
            return RaisedButton(
              onPressed: () {
                _openDetailPolling(state.record.internalObjectId);
              },
              child: Text('Buka Poling'),
              color: clr.Colors.blue,
              textColor: Colors.white,
            );
          case 'news':
            return RaisedButton(
              onPressed: () {
                _openDetailNews(state.record.internalObjectId);
              },
              child: Text('Buka Berita'),
              color: clr.Colors.blue,
              textColor: Colors.white,
            );
          default:
            return Container();
        }
      }
    } else {
      return Container();
    }
  }

  _buildText(Text text, context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(bottom: 10.0),
      child: text,
    );
  }

  _openDetailSurvey(int id) async {
    try {
      if (id != null) {
        String externalUrl = await SurveyRepository().getUrl(id);
        await Navigator.pushNamed(context, NavigationConstrants.Browser,
            arguments: externalUrl);
      } else {
        await Navigator.pushNamed(context, NavigationConstrants.Survey);
      }
    } catch (e) {
      unawaited(Fluttertoast.showToast(
          msg: CustomException.onConnectionException(e.toString()),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          backgroundColor: Colors.red,
          textColor: Colors.white));
    }
  }

  _openDetailPolling(int id) async {
    PollingRepository _pollingRepository = PollingRepository();

    try {
      if (id != null) {
        bool isVoted = await _pollingRepository.getVoteStatus(pollingId: id);

        if (!isVoted) {
          PollingModel record = await _pollingRepository.getDetail(id);
          await Navigator.pushNamed(context, NavigationConstrants.PollingDetail,
              arguments: record);
        } else {
          unawaited(Fluttertoast.showToast(
              msg: Dictionary.pollingHasVotedMessage,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              backgroundColor: Colors.blue,
              textColor: Colors.white));
        }
      } else {
        await Navigator.pushNamed(context, NavigationConstrants.Polling);
      }
    } catch (e) {
      unawaited(Fluttertoast.showToast(
          msg: CustomException.onConnectionException(e.toString()),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          backgroundColor: Colors.red,
          textColor: Colors.white));
    }
  }

  _openDetailNews(int id) {
    if (id != null) {
      NewsDetailArgumentsModel argumentsModel =
          NewsDetailArgumentsModel(id: id, isIdKota: false);
      Navigator.pushNamed(context, NavigationConstrants.NewsDetail,
          arguments: argumentsModel);
    } else {
      Navigator.pushNamed(context, NavigationConstrants.NewsIndex,
          arguments: false);
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
