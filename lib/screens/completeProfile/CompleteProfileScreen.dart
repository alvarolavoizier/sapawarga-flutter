import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:pedantic/pedantic.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/educations_list/Bloc.dart';
import 'package:sapawarga/blocs/force_change_profile/Bloc.dart';
import 'package:sapawarga/blocs/jobs_list/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/EducationRepository.dart';
import 'package:sapawarga/repositories/JobRepository.dart';
import 'package:sapawarga/utilities/Connection.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:sapawarga/components/custom_dropdown.dart' as custom;

class CompleteProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AccountProfileEditBloc>(
            create: (context) => AccountProfileEditBloc(
                authProfileRepository: AuthProfileRepository())),
        BlocProvider<EducationsListBloc>(
          create: (context) =>
              EducationsListBloc(educationRepository: EducationRepository()),
        ),
        BlocProvider<JoblistBloc>(
          create: (context) => JoblistBloc(jobRepository: JobRepository()),
        ),
      ],
      child: CompleteProfile(),
    );
  }
}

class CompleteProfile extends StatefulWidget {
  @override
  _CompleteProfileState createState() => _CompleteProfileState();
}

class _CompleteProfileState extends State<CompleteProfile> {
  GoogleSignIn _googleSignIn;
  AccountProfileEditBloc _accountProfileEditBloc;
  ForceChangeProfileBloc _forceChangeProfileBloc;
  AuthenticationBloc _authenticationBloc;
  EducationsListBloc _educationsListBloc;
  JoblistBloc _joblistBloc;

  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _addressController = TextEditingController();
  final _educationLevelIdController = TextEditingController();
  final _jobIdController = TextEditingController();
  bool _autoValidate = false;
  bool _isLoading = false;

  @override
  void initState() {
    _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    _googleSignIn.signInSilently();
    _accountProfileEditBloc = BlocProvider.of<AccountProfileEditBloc>(context);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _forceChangeProfileBloc = BlocProvider.of<ForceChangeProfileBloc>(context);
    _educationsListBloc = BlocProvider.of<EducationsListBloc>(context);
    _joblistBloc = BlocProvider.of<JoblistBloc>(context);

    _educationsListBloc.add(EducationsLoad());
    _joblistBloc.add(JobsLoad());

    _educationLevelIdController.text = 'null';
    _jobIdController.text = 'null';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AccountProfileEditBloc, AccountProfileEditState>(
      bloc: _accountProfileEditBloc,
      listener: (context, state) {
        if (state is AccountProfileEditLoading) {
          _isLoading = true;
          blockCircleLoading(context: context);
        } else if (state is AccountProfileEditUpdated) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
          showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: Dictionary.successChangeProfile,
                    buttonText: "OK",
                    onOkPressed: () {
                      _forceChangeProfileBloc.add(CheckForceChangeProfile());
                      Navigator.of(context, rootNavigator: true)
                          .pop(); // To close the dialog
                    },
                  ));
        } else if (state is AccountProfileEditFailure) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
          if (state.error.contains(Dictionary.errorUnauthorized)) {
            _authenticationBloc.add(LoggedOut());
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.error.toString(),
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context, rootNavigator: true)
                            .pop(); // To close the dialog
                      },
                    ));
          }
        } else if (state is AccountProfileEditValidationError) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
          showDialog(
              context: context,
              builder: (BuildContext context) => DialogWidgetContent(
                    child: Column(
                      children: <Widget>[
                        state.errors['name'] != null
                            ? Text(state.errors['name'][0].toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(fontSize: 16.0))
                            : Container(),
                        state.errors['email'] != null
                            ? Text(state.errors['email'][0].toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(fontSize: 16.0))
                            : Container(),
                        state.errors['phone'] != null
                            ? Text(state.errors['phone'][0].toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(fontSize: 16.0))
                            : Container(),
                        state.errors['address'] != null
                            ? Text(state.errors['address'][0].toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(fontSize: 16.0))
                            : Container(),
                      ],
                    ),
                    buttonText: "OK",
                    onOkPressed: () {
                      Navigator.of(context, rootNavigator: true)
                          .pop(); // To close the dialog
                    },
                  ));
        } else {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
        }
      },
      child: BlocBuilder<AccountProfileEditBloc, AccountProfileEditState>(
        bloc: _accountProfileEditBloc,
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(Dictionary.completeYourProfile),
              centerTitle: true,
            ),
            body: WillPopScope(
                onWillPop: () async {
                  await showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(Dictionary.confirmExitTitle),
                          content: Text(Dictionary.confirmExit),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(Dictionary.yes),
                              onPressed: () {
                                exit(0);
                              },
                            ),
                            FlatButton(
                              child: Text(Dictionary.cancel),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        );
                      });
                  return false;
                },
                child: Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 20.0),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 65.0),
                            child: SingleChildScrollView(
                              padding: EdgeInsets.only(bottom: 65.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  TextFormField(
                                    controller: _nameController,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 12, vertical: 14),
                                        labelText: Dictionary.labelName,
                                        border: OutlineInputBorder()),
                                    validator: (val) =>
                                        Validations.nameValidation(val),
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  TextFormField(
                                    controller: _emailController,
                                    keyboardType: TextInputType.emailAddress,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 12, vertical: 14),
                                        labelText: Dictionary.labelEmail,
                                        border: OutlineInputBorder()),
                                    validator: (val) =>
                                        Validations.emailValidation(val),
                                    onTap: _handleSignIn,
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  BlocBuilder<EducationsListBloc,
                                      EducationsListState>(
                                    bloc: _educationsListBloc,
                                    builder: (context, state) {
                                      return state is EducationsLoaded
                                          ? buildDropdownField(
                                        Dictionary.education,
                                        Dictionary.placeHolderEducation,
                                        state.record,
                                        _educationLevelIdController,
                                      )
                                          : state is EducationsFailure
                                          ? Text(state.error)
                                          : buildSkeleton(context);
                                    },
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  BlocBuilder<JoblistBloc, JoblistState>(
                                    bloc: _joblistBloc,
                                    builder: (context, state) {
                                      return state is JobsLoaded
                                          ? buildDropdownField(
                                        Dictionary.job,
                                        Dictionary.placeHolderJob,
                                        state.record,
                                        _jobIdController,
                                      )
                                          : state is JobsFailure
                                          ? Text(state.error)
                                          : buildSkeleton(context);
                                    },
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  TextFormField(
                                    controller: _phoneController,
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 12, vertical: 14),
                                        labelText: Dictionary.labelPhone,
                                        border: OutlineInputBorder()),
                                    validator: (val) =>
                                        Validations.phoneValidation(val),
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  TextFormField(
                                    controller: _addressController,
                                    keyboardType: TextInputType.multiline,
                                    minLines: 1,
                                    maxLines: 3,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 12, vertical: 14),
                                        labelText: Dictionary.labelAddress,
                                        border: OutlineInputBorder()),
                                    validator: (val) =>
                                        Validations.addressValidation(val),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: RoundedButton(
                              title: Dictionary.save,
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.green,
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .button
                                  .copyWith(color: Colors.white),
                              onPressed: () {
                                _onSaveButtonPressed();
                              },
                            ),
                          )
                        ],
                      ),
                    ))),
          );
        },
      ),
    );
  }

  Widget buildDropdownField(String title, String hintText, List items,
      TextEditingController controller,
      [validation]) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
            side: BorderSide(width: 1.0, style: BorderStyle.solid, color: Colors.grey[500]),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
        ),
      child: custom.DropdownButtonHideUnderline(
            child: custom.DropdownButton<String>(
              isExpanded: true,
              height: 320,
              hint: Text(hintText),
              items: items.map((item) {
                return custom.DropdownMenuItem(
                  child: Text(item.title),
                  value: item.id.toString(),
                );
              }).toList(),
              onChanged: (String value) {
                setState(() {
                  controller.text = value;
                });
              },
              value: controller.text == 'null' ? null : controller.text,
            ),
          )
    );
  }

  Skeleton buildSkeleton(BuildContext context) {
    return Skeleton(
      child: Container(
        padding: EdgeInsets.only(left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 70,
              height: 30.0,
              color: Colors.grey[300],
            ),
            SizedBox(height: 5.0),
            Container(
                width: MediaQuery.of(context).size.width,
                height: 30.0,
                color: Colors.grey[300]),
          ],
        ),
      ),
    );
  }

  Future<void> _handleSignIn() async {
    if (_emailController.text.trim().isEmpty) {
      try {
        bool isConnected =
            await Connection().checkConnection('https://www.google.com');
        if (isConnected) {
          try {
            await _googleSignIn.signIn();

            if (_googleSignIn.currentUser.email != null) {
              _emailController.text = _googleSignIn.currentUser.email;
              unawaited(_googleSignIn.disconnect());
            }
          } catch (error) {
            print(error);
          }
        }
      } catch (_) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => DialogTextOnly(
                  description: Dictionary.errorConnection,
                  buttonText: "OK",
                  onOkPressed: () {
                    Navigator.of(context).pop(); // To close the dialog
                  },
                ));
      }
    }
  }

  _onSaveButtonPressed() {
    FocusScope.of(context).requestFocus(FocusNode()); // Hide Keyboard
    setState(() {
      _autoValidate = true;
    });

    if (_formKey.currentState.validate()) {
      _accountProfileEditBloc.add(CompleteProfileSubmit(
          name: _nameController.text,
          email: _emailController.text,
          phone: _phoneController.text,
          address: _addressController.text,
          jobId: _jobIdController.text,
          educationId: _educationLevelIdController.text
      ));
    } else {
      print("Validate Error");
    }
  }

  @override
  void dispose() {
    _accountProfileEditBloc.close();
    _nameController.dispose();
    _emailController.dispose();
    _phoneController.dispose();
    _addressController.dispose();
    super.dispose();
  }
}
