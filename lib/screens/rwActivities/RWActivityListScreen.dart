import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/rw_activities/Bloc.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/Bubble.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/ReadMoreText.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/RWActivityModel.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';
import 'package:sapawarga/screens/rwActivities/RWActivityCreateScreen.dart';
import 'package:sapawarga/screens/rwActivities/RWActivityDetailScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:sapawarga/utilities/CustomPageRoute.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';
import 'package:http/http.dart' as http;

import 'RWActivityScreen.dart';

// ignore: must_be_immutable
class RWActivityListScreen extends StatefulWidget {
  bool tooltipLoad;
  RWActivityScreenState rwActivityScreenState;

  RWActivityListScreen({this.tooltipLoad, this.rwActivityScreenState});

  @override
  _RWActivityListScreenState createState() => _RWActivityListScreenState();
}

class _RWActivityListScreenState extends State<RWActivityListScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  RefreshController _mainRefreshController = RefreshController();
  ScrollController _scrollController = ScrollController();
  List<ItemRWActivity> _rwActivityList = List<ItemRWActivity>();

  final GlobalKey _showcaseOne = GlobalKey();
  final GlobalKey _showcaseTwo = GlobalKey();

  RWActivityListBloc _listBloc;
  AuthenticationBloc _authenticationBloc;

  String _likeState = '${Environment.imageAssets}like-empty.png';

  BuildContext showcaseContext;
  Timer _debounce;
  bool _isLoaded = false;
  int _pageCount = 0;
  int _currentPage = 1;
  final _scrollThreshold = 200.0;
  List<int> currentIndexPage = [];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.RW_ACTIVITIES);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_GENERAL_KEGIATAN_RW);
    currentIndexPage.clear();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    widget.rwActivityScreenState.isFilterKab = false;
    widget.rwActivityScreenState.itemKabKotaCategoryModel = null;
    widget.rwActivityScreenState.isGeneral = true;

    widget.rwActivityScreenState.searchController.addListener((() {
      _onSearchChanged();
    }));

    _scrollController.addListener(() {
      _onScroll();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.rwActivityScreenState.toolTipLoad == true) {
      Future.delayed(
          Duration(milliseconds: 200),
          () => ShowCaseWidget.of(showcaseContext)
              .startShowCase([_showcaseOne, _showcaseTwo]));

      setState(() {
        widget.tooltipLoad = false;
        widget.rwActivityScreenState.toolTipLoad = false;
      });
    }

    if (widget.rwActivityScreenState.isFilterKab == true) {
      currentIndexPage.clear();
      _onSearchByAreasChanged();
      widget.rwActivityScreenState.isFilterKab = false;
    }

    if (!widget.rwActivityScreenState.isSearch &&
        !widget.rwActivityScreenState.isFilterKab) {
      if (widget.rwActivityScreenState.hasChange) {
        widget.rwActivityScreenState.hasChange = false;
        if (widget.rwActivityScreenState.isGeneral && _listBloc != null) {
          refresh();
        }
      }
    }

    return ShowCaseWidget(
      onFinish: () async {
        await Preferences.setShowHelpRwActivity(true);
      },
      builder: Builder(
        builder: (context) {
          showcaseContext = context;
          return Scaffold(
            key: _scaffoldKey,
            body: Container(
                child: BlocProvider<RWActivityListBloc>(
              create: (context) {
                if (!widget.rwActivityScreenState.isSearch) {
                  return _listBloc = RWActivityListBloc(RWActivityRepository())
                    ..add(RWActivityListLoad(_currentPage));
                } else {
                  if (widget.rwActivityScreenState.searchController.text
                      .trim()
                      .isEmpty) {
                    return _listBloc =
                        RWActivityListBloc(RWActivityRepository())
                          ..add(RWActivityListLoad(_currentPage));
                  } else if (widget.rwActivityScreenState.isFilterKab) {
                    return _listBloc =
                        RWActivityListBloc(RWActivityRepository())
                          ..add(RWActivitySearchByAreas(
                              widget.rwActivityScreenState
                                  .itemKabKotaCategoryModel.id,
                              _currentPage));
                  } else {
                    return _listBloc =
                        RWActivityListBloc(RWActivityRepository())
                          ..add(RWActivityListSearch(
                              widget
                                  .rwActivityScreenState.searchController.text,
                              _currentPage));
                  }
                }
              },
              child: _buildBlocListener(),
            )),
            floatingActionButton: _isLoaded
                ? _buildShowcase(
                    key: _showcaseOne,
                    context: context,
                    widgets: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(bottom: 5),
                            alignment: Alignment.topLeft,
                            child: Text(
                              Dictionary.titleShowCaseRwActivity,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text(
                            Dictionary.showCaseQna1,
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      )
                    ],
                    onOkTap: () {
                      ShowCaseWidget.of(context).completed(_showcaseOne);
                    },
                    child: FloatingActionButton.extended(
                      backgroundColor: clr.Colors.blue,
                      onPressed: _openCreatePost,
                      icon: Icon(Icons.add),
                      label: Text(Dictionary.postActivity),
                    ))
                : null,
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
          );
        },
      ),
    );
  }

  _buildBlocListener() {
    return BlocListener<RWActivityListBloc, RWActivityListState>(
      bloc: _listBloc,
      listener: (context, state) {
        if (state is RWActivityListLoaded) {
          _pageCount = state.records.meta.pageCount;
          _currentPage = state.records.meta.currentPage + 1;
          _rwActivityList.addAll(state.records.items);
          _updateIndeksSlider(state.records.items);
          _rwActivityList = _rwActivityList.toSet().toList();
          _isLoaded = true;
          setState(() {});
        } else if (state is RWActivityListFailure) {
          if (state.error.contains(Dictionary.errorUnauthorized)) {
            _authenticationBloc.add(LoggedOut());
            Navigator.of(context).pop();
          }
        }
      },
      child: BlocBuilder<RWActivityListBloc, RWActivityListState>(
        bloc: _listBloc,
        builder: (context, state) => SmartRefresher(
            controller: _mainRefreshController,
            enablePullDown: true,
            header: WaterDropMaterialHeader(),
            onRefresh: refresh,
            child: state is RWActivityListLoading
                ? _buildLoading()
                : state is RWActivityListLoaded
                    ? state.records.items.isNotEmpty
                        ? _buildContent(state)
                        : EmptyData(message: Dictionary.emptyDataRWActivity)
                    : state is RWActivityListFailure
                        ? ErrorContent(error: state.error)
                        : _buildLoading()),
      ),
    );
  }

  Showcase _buildShowcase(
      {GlobalKey key,
      BuildContext context,
      List<Widget> widgets,
      Widget child,
      GestureTapCallback onOkTap}) {
    return Showcase.withWidget(
      shapeBorder: CircleBorder(),
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      key: key,
      disposeOnTap: false,
      container: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Bubble(
          nipLocation: NipLocation.BOTTOM,
          color: Colors.white,
          child: Container(
            width: MediaQuery.of(context).size.width - 50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  children: widgets,
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                    child: RoundedButton(
                        minWidth: 5,
                        height: 30,
                        color: clr.Colors.blue,
                        title: Dictionary.next,
                        onPressed: onOkTap,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)))
              ],
            ),
          ),
        ),
      ),
      child: child,
    );
  }

  Showcase _buildShowcase2(
      {GlobalKey key,
      BuildContext context,
      List<Widget> widgets,
      Widget child,
      GestureTapCallback onOkTap}) {
    return Showcase.withWidget(
      shapeBorder: RoundedRectangleBorder(),
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      key: key,
      disposeOnTap: false,
      container: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Bubble(
          nipLocation: NipLocation.TOP_LEFT,
          color: Colors.white,
          child: Container(
            width: MediaQuery.of(context).size.width - 50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  children: widgets,
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                    child: RoundedButton(
                        minWidth: 5,
                        height: 30,
                        color: clr.Colors.blue,
                        title: Dictionary.next,
                        onPressed: onOkTap,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)))
              ],
            ),
          ),
        ),
      ),
      child: child,
    );
  }

  _buildLoading() {
    return ListView.separated(
      padding: EdgeInsets.only(bottom: 80.0),
      itemCount: 2,
      separatorBuilder: (context, index) => Container(
          width: MediaQuery.of(context).size.width,
          height: 1.0,
          color: Colors.grey[300]),
      itemBuilder: (context, index) => Skeleton(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding:
              EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Container(
                        width: 40.0, height: 40.0, color: Colors.grey[300]),
                  ),
                ),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width / 2,
                        height: 20.0,
                        color: Colors.grey[300]),
                    SizedBox(height: 2.0),
                    Container(
                        width: 50.0, height: 15.0, color: Colors.grey[300]),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),
                  child: Container(
                      height: MediaQuery.of(context).size.height / 3.5,
                      color: Colors.grey[300]),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 20,
                            height: 20,
                            color: Colors.grey[300],
                          ),
                          SizedBox(width: 5.0),
                          Container(
                              width: MediaQuery.of(context).size.width / 4,
                              height: 20.0,
                              color: Colors.grey[300]),
                        ],
                      ),
                    ),
                    Container(
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 20,
                            height: 20,
                            color: Colors.grey[300],
                          ),
                          SizedBox(width: 5.0),
                          Container(
                              width: MediaQuery.of(context).size.width / 4,
                              height: 20.0,
                              color: Colors.grey[300]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 10.0),
                  width: MediaQuery.of(context).size.width,
                  height: 20.0,
                  color: Colors.grey[300]),
              Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: MediaQuery.of(context).size.width,
                  height: 20.0,
                  color: Colors.grey[300]),
              Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: MediaQuery.of(context).size.width / 1.5,
                  height: 20.0,
                  color: Colors.grey[300]),
            ],
          ),
        ),
      ),
    );
  }

  _buildContent(RWActivityListLoaded state) {
    return ListView(
      controller: _scrollController,
      children: <Widget>[
        widget.rwActivityScreenState.itemKabKotaCategoryModel != null
            ? Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          StringUtils.capitalizeWord(widget
                              .rwActivityScreenState
                              .itemKabKotaCategoryModel
                              .name),
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: FontsFamily.productSans),
                          maxLines: 1,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.close, color: Colors.grey),
                        onPressed: () {
                          setState(() {
                            widget.rwActivityScreenState.isFilterKab = false;
                            widget.rwActivityScreenState
                                .itemKabKotaCategoryModel = null;
                            _currentPage = 1;
                          });
                          refresh();
                        },
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5.0, bottom: 5),
                    color: Colors.grey[300],
                    height: 1.5,
                    width: MediaQuery.of(context).size.width,
                  )
                ],
              )
            : Container(),
        ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.only(bottom: 80.0),
            itemCount: state.records.meta.totalCount == _rwActivityList.length
                ? _rwActivityList.length
                : _rwActivityList.length + 1,
            separatorBuilder: (context, index) => Container(
                width: MediaQuery.of(context).size.width,
                height: 1.0,
                color: Colors.grey[300]),
            itemBuilder: (context, index) => index >= _rwActivityList.length
                ? Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: Column(
                      children: <Widget>[
                        CupertinoActivityIndicator(),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(Dictionary.loadingData),
                      ],
                    ),
                  )
                : _buildItem(context, index))
      ],
    );
  }

  GestureDetector _buildItem(BuildContext context, int index) {
    bool isLiked = _rwActivityList[index].isLiked;

    if (isLiked) {
      _likeState = '${Environment.imageAssets}liked.png';
    } else {
      _likeState = '${Environment.imageAssets}like-empty.png';
    }

    return GestureDetector(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding:
            EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              leading: Container(
                width: 40.0,
                height: 40.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),
                  child: CachedNetworkImage(
                    imageUrl: _rwActivityList[index].user.photoUrlFull,
                    fit: BoxFit.fill,
                    placeholder: (context, url) => Container(
                        color: Colors.grey[200],
                        child: Center(child: CupertinoActivityIndicator())),
                    errorWidget: (context, url, error) => Container(
                        color: Colors.grey[200],
                        child: Image.asset('${Environment.imageAssets}user.png',
                            fit: BoxFit.cover)),
                  ),
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _rwActivityList[index].user.name,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: FontsFamily.productSans),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text('${Dictionary.RW} ' +
                    StringUtils.capitalizeWord(
                          _rwActivityList[index].user.rw +
                          ', ' +
                          _rwActivityList[index].user.kelurahan.toLowerCase() +
                          ', ' +
                          _rwActivityList[index].user.kecamatan.toLowerCase() +
                          ', ' +
                          _rwActivityList[index].user.kabkota.toLowerCase(),
                    ),
                    style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                  )
                ],
              ),
            ),
            _rwActivityList[index].status == 0 ? _pendingMarker() : Container(),
            CarouselSlider(
                initialPage: 0,
                enableInfiniteScroll: false,
                viewportFraction: 1.0,
                onPageChanged: (record) {
                  setState(() {
                    currentIndexPage[index] = record;
                  });
                },
                items: _rwActivityList[index].images.map((record) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: CachedNetworkImage(
                        useOldImageOnUrlChange: true,
                        imageUrl: record.url,
                        fit: BoxFit.fitWidth,
                        placeholder: (context, url) => Center(
                            heightFactor: 8.2,
                            child: CupertinoActivityIndicator()),
                        errorWidget: (context, url, error) => Container(
                            height: MediaQuery.of(context).size.height / 3.3,
                            color: Colors.grey[200],
                            child: Image.asset(
                                '${Environment.imageAssets}placeholder.png',
                                fit: BoxFit.fitWidth)),
                      ),
                    ),
                  );
                }).toList()),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: map<Widget>(_rwActivityList[index].images,
                  (indexImage, record) {
                return Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: currentIndexPage.isNotEmpty ? currentIndexPage[index] == indexImage
                          ? clr.Colors.blue
                          : Colors.grey :Colors.grey),
                );
              }),
            ),
            Container(
              child: index == 0
                  ? _buildShowcase2(
                      key: _showcaseTwo,
                      context: context,
                      widgets: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(bottom: 5),
                              alignment: Alignment.topLeft,
                              child: Text(
                                Dictionary.titleShowCaseRwActivity2,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.topLeft,
                              child: Text(
                                Dictionary.showCaseQna2,
                                style: TextStyle(color: Colors.grey),
                              ),
                            )
                          ],
                        )
                      ],
                      onOkTap: () {
                        ShowCaseWidget.of(context).completed(_showcaseTwo);
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 2.0),
                        child: Row(
                          children: <Widget>[
                            GestureDetector(
                              child: Container(
                                width: 24,
                                height: 24,
                                child: Image.asset(
                                  _likeState,
                                ),
                              ),
                              onTap: () {
                                _listBloc.add(RWActivityListLike(
                                    _rwActivityList[index].id));
                                if (isLiked) {
                                  _updateLike(_rwActivityList,
                                      _rwActivityList[index].id, true);
                                } else {
                                  _updateLike(_rwActivityList,
                                      _rwActivityList[index].id, false);
                                }
                              },
                            ),
                            SizedBox(width: 5.0),
                            Text(
                              _rwActivityList[index].likesCount > 0
                                  ? '${_rwActivityList[index].likesCount} ' +
                                  Dictionary.likes
                                  : Dictionary.likes,
                              style: TextStyle(fontSize: 15.0),
                            ),
                            SizedBox(width: 10.0),
                            Container(
                              margin: EdgeInsets.only(right: 2.0),
                              child: Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    '${Environment.iconAssets}comments.png',
                                    width: 24.0,
                                    height: 24.0,
                                  ),
                                  _rwActivityList[index].commentsCount > 0
                                      ? SizedBox(width: 5.0)
                                      : Container(),
                                  Text(
                                    _rwActivityList[index].commentsCount > 0
                                        ? '${_rwActivityList[index].commentsCount} ' +
                                        Dictionary.comment
                                        : ' ' + Dictionary.comment,
                                    style: TextStyle(fontSize: 15.0),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(width: 10.0),
                            GestureDetector(
                              child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                          '${Environment.iconAssets}share.png',
                                          height: 24.0),
                                      SizedBox(width: 5.0),
                                      Text(
                                        Dictionary.share,
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                    ],
                                  )),
                              onTap: () {
                                _shareImage(
                                    _rwActivityList[index].id,
                                    _rwActivityList[index].images.isNotEmpty ? _rwActivityList[index].images[0].url:_rwActivityList[index].imagePathFull,
                                    _rwActivityList[index].text);
                              },
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.only(left: 2.0),
                      child: Row(
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                              width: 24,
                              height: 24,
                              child: Image.asset(
                                _likeState,
                              ),
                            ),
                            onTap: () {
                              _listBloc.add(RWActivityListLike(
                                  _rwActivityList[index].id));
                              if (isLiked) {
                                _updateLike(_rwActivityList,
                                    _rwActivityList[index].id, true);
                              } else {
                                _updateLike(_rwActivityList,
                                    _rwActivityList[index].id, false);
                              }
                            },
                          ),
                          SizedBox(width: 5.0),
                          Text(
                            _rwActivityList[index].likesCount > 0
                                ? '${_rwActivityList[index].likesCount} ' +
                                    Dictionary.likes
                                : Dictionary.likes,
                            style: TextStyle(fontSize: 15.0),
                          ),
                          SizedBox(width: 10.0),
                          Container(
                            margin: EdgeInsets.only(right: 2.0),
                            child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '${Environment.iconAssets}comments.png',
                                  width: 24.0,
                                  height: 24.0,
                                ),
                                _rwActivityList[index].commentsCount > 0
                                    ? SizedBox(width: 5.0)
                                    : Container(),
                                Text(
                                  _rwActivityList[index].commentsCount > 0
                                      ? '${_rwActivityList[index].commentsCount} ' +
                                          Dictionary.comment
                                      : ' ' + Dictionary.comment,
                                  style: TextStyle(fontSize: 15.0),
                                )
                              ],
                            ),
                          ),
                          SizedBox(width: 10.0),
                          GestureDetector(
                            child: Container(
                                child: Row(
                              children: <Widget>[
                                Image.asset(
                                    '${Environment.iconAssets}share.png',
                                    height: 24.0),
                                SizedBox(width: 5.0),
                                Text(
                                  Dictionary.share,
                                  style: TextStyle(fontSize: 15.0),
                                ),
                              ],
                            )),
                            onTap: () {
                              _shareImage(
                                  _rwActivityList[index].id,
                                  _rwActivityList[index].imagePathFull,
                                  _rwActivityList[index].text);
                            },
                          ),
                        ],
                      ),
                    ),
            ),
            _rwActivityList[index].status == 0
                ? Container(
                    margin: EdgeInsets.only(top: 15.0),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          '${Environment.iconAssets}warning_rw.png',
                          width: 18.0,
                          height: 18.0,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          width: MediaQuery.of(context).size.width - 120,
                          child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                text:
                                    Dictionary.contentNotPermittedRWActivities,
                                style: TextStyle(
                                    fontSize: 15.0, color: Colors.red),
                              ),
                              TextSpan(
                                  text: Dictionary.readMore,
                                  style: TextStyle(
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => BrowserScreen(
                                            url: UrlThirdParty
                                                .urlCommunityGuideLine,
                                          ),
                                        ),
                                      );
                                    })
                            ]),
                          ),
                        )
                      ],
                    ))
                : SizedBox(height: 15.0),
            Container(
                margin: EdgeInsets.only(top: 10.0),
                child: ReadMoreText(_rwActivityList[index].text,
                    style: TextStyle(fontSize: 15.0),
                    trimLines: 3,
                    colorClickableText: Colors.grey[600],
                    trimMode: TrimMode.Line,
                    trimCollapsedText: '... Selengkapnya')),
            _rwActivityList[index].lastComment != null
                ? Container(
                    margin: EdgeInsets.only(top: 5.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _rwActivityList[index].lastComment.user.roleLabel !=
                                'pimpinan'
                            ? Expanded(
                                child: RichText(
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: _rwActivityList[index]
                                            .lastComment
                                            .user
                                            .name,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey[900]),
                                      ),
                                      TextSpan(
                                        text: ' ' +
                                            _rwActivityList[index]
                                                .lastComment
                                                .text,
                                        style:
                                            TextStyle(color: Colors.grey[900]),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : Expanded(
                                child: Wrap(
                                  children: <Widget>[
                                    Text(
                                        _rwActivityList[index]
                                            .lastComment
                                            .user
                                            .name,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey[900])),
                                    Icon(
                                      Icons.check_circle,
                                      color: clr.Colors.blue,
                                      size: 18.0,
                                    ),
                                    Text(
                                        ' ' +
                                            _rwActivityList[index]
                                                .lastComment
                                                .text,
                                        maxLines: 3,
                                        softWrap: true,
                                        overflow: TextOverflow.ellipsis,
                                        style:
                                            TextStyle(color: Colors.grey[900]))
                                  ],
                                ),
                              )
                      ],
                    ))
                : Container(),
            SizedBox(height: 5),
            Text(
              unixTimeStampToTimeAgo(_rwActivityList[index].createdAt),
              style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
            ),
          ],
        ),
      ),
      onTap: () {
        Navigator.of(context).push(SizedPageRoute(
            RWActivityDetailScreen(_rwActivityList[index], null, null)));
      },
    );
  }

  Stack _pendingMarker() {
    return Stack(
      children: <Widget>[
        Container(
            height: 30,
            child: Image.asset(
                '${Environment.imageAssets}ribbon-silhouette-leading.png',
                color: Colors.amber)),
        Container(
          margin: EdgeInsets.only(left: 70.0, top: 0.18),
          width: 30,
          height: 24.75,
          color: Colors.amber,
        ),
        Container(
            margin: EdgeInsets.only(left: 85),
            height: 30,
            child: Image.asset(
                '${Environment.imageAssets}ribbon-silhouette-trailing.png',
                color: Colors.amber)),
        Container(
          margin: EdgeInsets.only(left: 10, top: 2),
          child: Text(
            Dictionary.postRWActivitiesRejected,
            style: TextStyle(color: Colors.black),
          ),
        ),
      ],
    );
  }

  _updateLike(List<ItemRWActivity> records, int id, bool isLike) {
    for (int i = 0; i < records.length; i++) {
      if (records[i].id == id) {
        if (isLike) {
          if (records[i].isLiked) {
            setState(() {
              records[i].isLiked = false;
              records[i].likesCount = records[i].likesCount - 1;
            });
          }
        } else {
          setState(() {
            records[i].likesCount = records[i].likesCount + 1;
            records[i].isLiked = true;
          });
        }
      }
    }
  }

  _updateIndeksSlider(List<ItemRWActivity> records) {
    for (int i = 0; i < records.length; i++) {
      currentIndexPage.add(0);
    }
  }

  Future<void> _shareImage(int id, String url, String text) async {
    try {
      final response = await http.get(url);
      await Share.file(Dictionary.rwActivities, 'kegiatan_rw.jpg',
          response.bodyBytes, 'image/jpg',
          text: '$text ${Dictionary.sharedFrom}');

      await AnalyticsHelper.setLogEvent(Analytics.EVENT_SHARE_KEGIATAN_RW,
          <String, dynamic>{'id': id, 'image': url});
    } catch (e) {
      print('error: $e');
    }
  }

  void _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (widget.rwActivityScreenState.searchController.text
          .trim()
          .isNotEmpty) {
        if (widget.rwActivityScreenState.isGeneral) {
          widget.rwActivityScreenState.hasChange = true;
          _rwActivityList.clear();
          _currentPage = 1;
          _listBloc.add(RWActivityListSearch(
              widget.rwActivityScreenState.searchController.text,
              _currentPage));
          AnalyticsHelper.setLogEvent(
              Analytics.EVENT_SEARCH_KEGIATAN_RW, <String, dynamic>{
            'keyword': '${widget.rwActivityScreenState.searchController.text}',
          });
        }
      }
    });
  }

  void _onSearchByAreasChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (widget.rwActivityScreenState.itemKabKotaCategoryModel != null) {
        if (widget.rwActivityScreenState.isGeneral) {
          _rwActivityList.clear();
          _currentPage = 1;
          _listBloc.add(RWActivitySearchByAreas(
              widget.rwActivityScreenState.itemKabKotaCategoryModel.id,
              _currentPage));
        }
      }
    });
  }

  Future<void> refresh() async {
    currentIndexPage.clear();
    widget.rwActivityScreenState.itemKabKotaCategoryModel = null;
    widget.rwActivityScreenState.isFilterKab = false;
    _currentPage = 1;
    _rwActivityList.clear();
    _listBloc.add(RWActivityListLoad(_currentPage));
    _mainRefreshController.refreshCompleted();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        if (widget.rwActivityScreenState.isSearch) {
          if (widget.rwActivityScreenState.searchController.text
              .trim()
              .isNotEmpty) {
            _listBloc.add(RWActivityListSearch(
                widget.rwActivityScreenState.searchController.text,
                _currentPage));
          }
        } else if (widget.rwActivityScreenState.itemKabKotaCategoryModel !=
            null) {
          _listBloc.add(RWActivitySearchByAreas(
              widget.rwActivityScreenState.itemKabKotaCategoryModel.id,
              _currentPage));
        } else {
          _listBloc.add(RWActivityListLoad(_currentPage));
        }
      }
    }
  }

  void _openCreatePost() async {
//    File _image = await ImagePickerHelper(context).openDialog();

    final result = await Navigator.of(context)
        .push(FadedPageRoute(RWActivityCreateScreen())) as ItemRWActivity;

    if (result != null) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                'Kegiatan berhasil diposting',
                style: TextStyle(fontSize: 14.0),
              )),
          backgroundColor: clr.Colors.blue,
          duration: Duration(seconds: 2),
        ),
      );

      await _mainRefreshController.requestRefresh();
    }
  }

  void _permission() async {
    PermissionStatus permissionStorage = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    PermissionStatus permissionCamera =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (permissionStorage != PermissionStatus.granted) {
      await showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionRWActivities,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler()
                      .requestPermissions([PermissionGroup.storage]).then(
                          _onStatusRequestedStorage);
                },
              ));
    } else {
      if (permissionCamera != PermissionStatus.granted) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => DialogRequestPermission(
                  image: Image.asset(
                    'assets/icons/photo-camera.png',
                    fit: BoxFit.contain,
                    color: Colors.white,
                  ),
                  description: Dictionary.permissionRWActivities,
                  onOkPressed: () {
                    Navigator.of(context).pop();
                    PermissionHandler()
                        .requestPermissions([PermissionGroup.camera]).then(
                            _onStatusRequestedCamera);
                  },
                ));
      } else {
        _openCreatePost();
      }
    }
  }

  void _onStatusRequestedStorage(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      PermissionHandler().requestPermissions([PermissionGroup.camera]).then(
          _onStatusRequestedCamera);
    }
  }

  void _onStatusRequestedCamera(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.camera];
    if (status == PermissionStatus.granted) {
      _permission();
    }
  }

  @override
  void dispose() {
    widget.rwActivityScreenState.searchController
        .removeListener(_onSearchChanged);
    _mainRefreshController.dispose();
    _scrollController.dispose();
    _listBloc.close();
    super.dispose();
  }
}
