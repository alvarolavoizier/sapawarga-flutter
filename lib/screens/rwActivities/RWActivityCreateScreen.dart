import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/rw_activities/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/repositories/MasterCategoryRepository.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';
import 'package:pedantic/pedantic.dart';

class RWActivityCreateScreen extends StatefulWidget {
  @override
  _RWActivityCreateScreenState createState() => _RWActivityCreateScreenState();
}

class _RWActivityCreateScreenState extends State<RWActivityCreateScreen> {
  final _textMessage = TextEditingController();
  List<File> listImage = [];
  String tag = '';
  List<MasterCategoryModel> lisCategoryModel = List<MasterCategoryModel>();
  RWActivityAddBloc _addBloc;
  AuthenticationBloc _authenticationBloc;

  bool _isLoading = false;
  bool isDeletePhoto = false;
  List <bool> check = [];



  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _textMessage.addListener((() {
      setState(() {});
    }));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: BlocProvider<RWActivityAddBloc>(
        create: (context) {
          return _addBloc = RWActivityAddBloc(
              RWActivityRepository(), MasterCategoryRepository())
            ..add(RWActivityCategoryLoad());
        },
        child: BlocListener<RWActivityAddBloc, RWActivityAddState>(
          bloc: _addBloc,
          listener: (context, state) {
            if (state is RWActivityAddLoading) {
              _isLoading = true;
              blockCircleLoading(context: context);
            } else if (state is RWActivityAdded) {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }

              Navigator.pop(context, state.record);
            } else if (state is RWActivityAddFailure) {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }
              if (state.error.contains(Dictionary.errorUnauthorized)) {
                _authenticationBloc.add(LoggedOut());
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              } else {
                if (_textMessage.text.length < 10) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: Dictionary.errorLengthTextDescription,
                            buttonText: "OK",
                            onOkPressed: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop(); // To close the dialog
                            },
                          ));
                } else {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: state.error.toString(),
                            buttonText: "OK",
                            onOkPressed: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop(); // To close the dialog
                            },
                          ));
                }
              }
            } else if (state is RWActivityCategoryAdded) {
              if (state.records != null) {
                lisCategoryModel.addAll(state.records);
                lisCategoryModel = lisCategoryModel.toSet().toList();
                for(int i=0;i<state.records.length;i++){
                  check.add(false);
                }
              }
              setState(() {});
            } else if (state is RWActivityCategoryFailure) {
              if (state.error.contains(Dictionary.errorUnauthorized)) {
                _authenticationBloc.add(LoggedOut());
                Navigator.of(context).pop();
              }
            }
          },
          child: Container(
              color: Colors.grey[100],
              child: Scaffold(
                resizeToAvoidBottomPadding: false,
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  title: Text(Dictionary.rwActivities,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis),
                  titleSpacing: 0.0,
                ),
                body: SingleChildScrollView(
                  child: Container(
                      alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              listImage.isNotEmpty
                                  ? Container(
                                width: MediaQuery.of(context).size.width,
                                height: 120,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    children: <Widget>[
                                      ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                          const NeverScrollableScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          itemCount: listImage.length,
                                          itemBuilder:
                                              (BuildContext context,
                                              int index) {
                                            return Container(
                                              margin:
                                              EdgeInsets.only(right: 8),
                                              child: Stack(
                                                children: <Widget>[
                                                  ClipRRect(
                                                    borderRadius:
                                                    BorderRadius
                                                        .circular(5),
                                                    child: Container(
                                                        width: 120,
                                                        height:
                                                        MediaQuery.of(
                                                            context)
                                                            .size
                                                            .height,
                                                        child: Image.file(
                                                            listImage[
                                                            index],
                                                            fit: BoxFit
                                                                .fill)),
                                                  ),
                                                  Positioned(
                                                    bottom: 0,
                                                    right: 0,
                                                    child: Container(
                                                      padding:
                                                      EdgeInsets.all(5),
                                                      child:
                                                      GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            listImage
                                                                .removeAt(
                                                                index);
                                                            isDeletePhoto =
                                                            true;
                                                          });
                                                        },
                                                        child: Image.asset(
                                                            '${Environment.imageAssets}delete_blue.png',
                                                            width: 20,
                                                            height: 20,
                                                            fit: BoxFit
                                                                .fitWidth),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            );
                                          }),
                                      Container(
                                        child: listImage.length < 5
                                            ? GestureDetector(
                                          onTap: () {
                                            _cameraBottomSheet(
                                                context);
                                          },
                                          child: Image.asset(
                                              '${Environment.imageAssets}addphoto.png',
                                              width: 25,
                                              height: 25,
                                              fit: BoxFit.fitWidth),
                                        )
                                            : Container(),
                                      )
                                    ],
                                  ),
                                ),
                              )
                                  : GestureDetector(
                                onTap: () {
                                  _cameraBottomSheet(context);
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      border:
                                      Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5))),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Container(
                                        color: Colors.grey,
                                        width: MediaQuery.of(context)
                                            .size
                                            .width /
                                            4,
                                        height: MediaQuery.of(context)
                                            .size
                                            .height /
                                            7,
                                        child: Container(
                                          child: Center(
                                            child: Image.asset(
                                                '${Environment.imageAssets}image.png',
                                                width:
                                                MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                    9,
                                                height:
                                                MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                    9,
                                                fit: BoxFit.fitWidth),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context)
                                            .size
                                            .width /
                                            1.5,
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              margin:
                                              EdgeInsets.only(right: 3),
                                              child: Image.asset(
                                                '${Environment.imageAssets}upload.png',
                                                height: 15,
                                                width: 20,
                                              ),
                                            ),
                                            Text(
                                              Dictionary.uploadImage,
                                              style: TextStyle(
                                                color: Colors.grey[600],
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: Dimens.padding, bottom: Dimens.padding),
                                alignment: Alignment.topLeft,
                                width: MediaQuery.of(context).size.width,
                                child: Text('Deskripsi',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Container(
                                padding: EdgeInsets.only(right: 35),
                                child: TextField(
                                    controller: _textMessage,
                                    autofocus: false,
                                    maxLines: 5,
                                    minLines: 1,
                                    maxLength: 255,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16.0),
                                    decoration: InputDecoration(
                                        hintText: Dictionary.hintDescription,
                                        counterText: "",
                                        border: InputBorder.none,
                                        contentPadding: EdgeInsets.all(10.0))),
                              ),
                              Container(
                                color: clr.Colors.grey,
                                width: MediaQuery.of(context).size.width,
                                height: 1,
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: Dimens.padding, bottom: Dimens.padding),
                                alignment: Alignment.topLeft,
                                width: MediaQuery.of(context).size.width,
                                child: Text(Dictionary.category,
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold)),
                              ),
                              BlocBuilder<RWActivityAddBloc, RWActivityAddState>(
                                  bloc: _addBloc,
                                  builder: (context, state) => Container(
                                    child: state is RWActivityCategoryLoading
                                        ? _buildLoadingCategory()
                                        : state is RWActivityCategoryAdded
                                        ? state.records.isNotEmpty
                                        ? _buildContentCategory()
                                        : EmptyData(
                                        message: Dictionary
                                            .emptyDataRWActivity)
                                        : state is RWActivityCategoryFailure
                                        ? ErrorContent(
                                        error: state.error)
                                        : _buildContentCategory(),
                                  )),
                            ],
                          ),
                          RoundedButton(
                            title: Dictionary.send,
                            borderRadius: BorderRadius.circular(5.0),
                            color: clr.Colors.blue,
                            textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                            onPressed: () {
                              if (listImage.isEmpty) {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        DialogTextOnly(
                                          description:
                                          Dictionary.fotoRwActivitiesIsEmpty,
                                          buttonText: "OK",
                                          onOkPressed: () {
                                            Navigator.of(context,
                                                rootNavigator: true)
                                                .pop(); // To close the dialog
                                          },
                                        ));
                              } else if (_textMessage.text.isEmpty) {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        DialogTextOnly(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          description:
                                          Dictionary.descRwActivitiesIsEmpty,
                                          buttonText: "OK",
                                          onOkPressed: () {
                                            Navigator.of(context,
                                                rootNavigator: true)
                                                .pop(); // To close the dialog
                                          },
                                        ));
                              }
                              else if (listImage.isNotEmpty /*&& tag != null*/) {
                                _addBloc.add(
                                  RWActivityAdd(
                                      image: listImage,
                                      description: _textMessage.text.trim(),
                                      tags: tag),
                                );
                              }
                            },
                          )
                        ],
                      )),
                ),
              )),
        ),
      ),
    );
  }

  _buildLoadingCategory() {
    return Container(
      margin: EdgeInsets.only(top: 5.0, bottom: 5),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(25.0),
        child: Container(
            height: MediaQuery.of(context).size.height / 2.8,
            color: Colors.grey[300]),
      ),
    );
  }

  _buildContentCategory() {

    if(lisCategoryModel.isNotEmpty){
      return Container(
        margin: EdgeInsets.only(bottom: 5),
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: lisCategoryModel.length,
          itemBuilder: (context, index){
            return CheckboxListTile(
              title: Text(lisCategoryModel[index].name),
              value: check[index],
              activeColor: clr.Colors.blue,
              checkColor: Colors.white,
              onChanged: (bool value) {
                setState(() {
                  check[index] = value;
                  if(value){
                    tag += lisCategoryModel[index].name+', ';
                  }else{
                    tag =   tag.replaceAll(lisCategoryModel[index].name+', ', '');
                  }
                });
              },
            );
          },
        ),
      );
    } else {
      return Container();
    }
  }

  void _cameraBottomSheet(context) {
    if (listImage.length < 5) {
      showModalBottomSheet(
          context: context,
          builder: (BuildContext bc) {
            return Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.camera),
                    title: Text(Dictionary.takePhoto),
                    onTap: () {
                      _permissionCamera();
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.image),
                    title: Text(Dictionary.takePhotoFromGallery),
                    onTap: () {
                      _permissionGallery();
                    },
                  ),
                ],
              ),
            );
          });
    } else {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(Dictionary.maxPhotoLength),
        ),
      );
    }
  }

  void _permissionGallery() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionGalery,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.storage]).then(_onStatusRequested);
                },
              )));
    } else {
      await openGallery();
    }
  }

  String concertDataListTag(data) {
    return jsonEncode(data)
        .replaceAll('[', '')
        .replaceAll(']', '')
        .replaceAll(',', ', ')
        .replaceAll('"', '');
  }

  Future openGallery() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 640, maxWidth: 640);

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        setState(() {
          listImage.add(image);
        });
        Navigator.pop(context);
      }
    }
  }

  Future openCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 640, maxWidth: 640);

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        setState(() {
          listImage.add(image);
        });
        Navigator.pop(context);
      }
    }
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      _permissionGallery();
    }
  }

  void _onStatusRequestedCamera(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.camera];
    if (status == PermissionStatus.granted) {
      _permissionCamera();
    }
  }

  void _permissionCamera() async {
    PermissionStatus permission =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/photo-camera.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionCamera,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.camera]).then(_onStatusRequestedCamera);
                },
              )));
    } else {
      await openCamera();
    }
  }

  @override
  void dispose() {
    _addBloc.close();
    _textMessage.dispose();
    super.dispose();
  }
}
