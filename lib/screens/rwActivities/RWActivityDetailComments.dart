import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/rw_activities/rw_activity_comment/Bloc.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Colors.dart' as prefix0;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/RWActivityCommentModel.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:sapawarga/utilities/FormatDate.dart';

class RWActivityDetailComments extends StatefulWidget {
  final RWActivityCommentListBloc commentListBloc;
  final List<ItemRwActivityComment> listComments;
  final int itemCount;

  RWActivityDetailComments(
      this.commentListBloc, this.listComments, this.itemCount);

  @override
  _RWActivityDetailCommentsState createState() =>
      _RWActivityDetailCommentsState();
}

class _RWActivityDetailCommentsState extends State<RWActivityDetailComments> {
  RWActivityCommentListBloc get _listBloc => widget.commentListBloc;

  List<ItemRwActivityComment> get _listComments => widget.listComments;

  int get _itemCount => widget.itemCount;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<RWActivityCommentListBloc, RWActivityCommentListState>(
        bloc: _listBloc,
        builder: (context, state) => state is RWActivityCommentListLoading
            ? _buildLoading()
            : state is RWActivityCommentListLoaded
                ? _listComments.isNotEmpty
                    ? _buildContent(state)
                    : _buildNoComment()
                : state is RWActivityCommentListFailure
                    ? _buildError(state.error)
                    : _buildLoading(),
      ),
    );
  }

  Container _buildNoComment() {
    return Container(
        alignment: Alignment.topCenter,
        margin: EdgeInsets.only(top: 10.0),
        child: Text(
          Dictionary.emptyComments,
          style:
              TextStyle(fontStyle: FontStyle.italic, color: Colors.grey[700]),
        ));
  }

  Container _buildError(String error) {
    return Container(
        alignment: Alignment.topCenter,
        margin: EdgeInsets.only(top: 10.0),
        child: Text(
          error,
          style:
              TextStyle(fontStyle: FontStyle.italic, color: Colors.grey[700]),
        ));
  }

  ListView _buildLoading() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 2,
      itemBuilder: (context, index) {
        return Skeleton(
          child: Container(
            padding:
                EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  contentPadding: EdgeInsets.all(0.0),
                  leading: Container(
                    width: 40.0,
                    height: 40.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(25.0),
                      child: Container(
                          width: 40.0, height: 40.0, color: Colors.grey[300]),
                    ),
                  ),
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          height: 20.0,
                          color: Colors.grey[300]),
                      SizedBox(height: 2.0),
                      Container(
                          width: 50.0, height: 15.0, color: Colors.grey[300]),
                    ],
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 56.0),
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                Container(
                    margin: EdgeInsets.only(left: 56.0, top: 2.0),
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                Container(
                    margin: EdgeInsets.only(left: 56.0, top: 2.0),
                    width: MediaQuery.of(context).size.width / 1.5,
                    height: 20.0,
                    color: Colors.grey[300]),
              ],
            ),
          ),
        );
      },
    );
  }

  ListView _buildContent(RWActivityCommentListLoaded state) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: _itemCount == _listComments.length
            ? _listComments.length
            : _listComments.length + 1,
        itemBuilder: (context, index) => index >= _listComments.length
            ? Padding(
                padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: Column(
                  children: <Widget>[
                    CupertinoActivityIndicator(),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(Dictionary.loadingData),
                  ],
                ),
              )
            : _buildItem(index));
  }

  Container _buildItem(int index) {
    return Container(
      padding: EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.all(0.0),
            leading: Container(
              width: 40.0,
              height: 40.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: CachedNetworkImage(
                  imageUrl: _listComments[index].user.photoUrlFull,
                  fit: BoxFit.fill,
                  placeholder: (context, url) => Container(
                      color: Colors.grey[200],
                      child: Center(child: CupertinoActivityIndicator())),
                  errorWidget: (context, url, error) => Container(
                      color: Colors.grey[200],
                      child: Image.asset('${Environment.imageAssets}user.png',
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Wrap(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              _listComments[index].user.name,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: FontsFamily.productSans),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            if (_listComments[index].user.roleLabel ==
                                'pimpinan')
                              Icon(
                                Icons.check_circle,
                                color: prefix0.Colors.blue,
                                size: 18.0,
                              )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
//                Text(
//                  unixTimeStampToTimeAgo(_listComments[index].createdAt),
//                  style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
//                )
                SizedBox(height: 2.0),

                _listComments[index].user.name == Dictionary.nameBuAtalia
                    ? Text(
                  Dictionary.titleLeadOfPKK,
                  style:
                  TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                )
                    : _listComments[index].user.name ==
                    Dictionary.nameRidwanKamil
                    ? Text(
                  Dictionary.titleGovernor,
                  style: TextStyle(
                      fontSize: 14.0, color: Colors.grey[600]),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                )
                    :_listComments[index].user.roleLabel ==
                    Dictionary.nameRoleStaffProv
                    ? Text(
                  Dictionary.titleStaffProv,
                  style: TextStyle(
                      fontSize: 14.0, color: Colors.grey[600]),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                )
                    : Text(
                  StringUtils.capitalizeWord('RW ' +
                      _listComments[index].user.rw +
                      ', ' +
                      _listComments[index].user.kelurahan +
                      ', ' +
                      _listComments[index].user.kabkota),
                  style: TextStyle(
                      fontSize: 14.0, color: Colors.grey[600]),
                )
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 56.0),
              child: Text(
                _listComments[index].text,
                style: TextStyle(fontSize: 15.0),
              )),
          Container(
            margin: EdgeInsets.only(left: 56.0, top:2),
            child: Text(
              unixTimeStampToTimeAgo(
                  _listComments[index].createdAt),
              style: TextStyle(
                  fontSize: 12.0, color: Colors.grey[600]),
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
