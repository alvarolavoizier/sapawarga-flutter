import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/survey_list/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/models/SurveyModel.dart';
import 'package:sapawarga/repositories/SurveyRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:shimmer/shimmer.dart';

class SurveyListScreen extends StatefulWidget {
  @override
  _SurveyListScreenState createState() => _SurveyListScreenState();
}

class _SurveyListScreenState extends State<SurveyListScreen> {
  final RefreshController _mainRefreshController = RefreshController();
  ScrollController _scrollController = ScrollController();
  int maxDatalength;
  int _page = 1;
  List<SurveyModel> dataListModel = [];

  SurveyListBloc _surveyListBloc;
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.SURVEY,
        screenClassOverride: 'SurveiScreen');
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_SURVEY);

    _initialize();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SurveyListBloc>(
      create: (BuildContext context) =>
          _surveyListBloc = SurveyListBloc(surveyRepository: SurveyRepository())
            ..add(SurveyListLoad(page: _page)),
      child: BlocListener<SurveyListBloc, SurveyListState>(
        listener: (context, state) {
          if (state is SurveyListFailure) {
            if (state.error.contains(Dictionary.errorUnauthorized)) {
              _authenticationBloc.add(LoggedOut());
              Navigator.of(context).pop();
            }
          }
        },
        child: BlocBuilder<SurveyListBloc, SurveyListState>(
            bloc: _surveyListBloc,
            builder: (context, state) {
              return Scaffold(
                  appBar: AppBar(
                    title: Text(Dictionary.survey),
                  ),
                  body: SmartRefresher(
                      controller: _mainRefreshController,
                      enablePullDown: true,
                      header: WaterDropMaterialHeader(),
                      onRefresh: () async {
                        _page = 1;
                        dataListModel.clear();
                        _surveyListBloc.add(SurveyListLoad(page: _page));
                        _mainRefreshController.refreshCompleted();
                      },
                      child: state is SurveyListLoading
                          ? _buildLoading()
                          : state is SurveyListLoaded
                              ? state.records.isNotEmpty
                                  ? _buildContent(state)
                                  : EmptyData(
                                      message: Dictionary.emptyDataSurvey)
                              : state is SurveyListFailure
                                  ? ErrorContent(error: state.error)
                                  : _buildLoading()));
            }),
      ),
    );
  }

  _buildLoading() {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) => Card(
        margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
        child: Container(
            margin: EdgeInsets.all(15.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width - 175,
                          height: 20.0,
                          color: Colors.grey[300]),
                      SizedBox(height: 5.0),
                      Container(
                          width: MediaQuery.of(context).size.width - 175,
                          height: 20.0,
                          color: Colors.grey[300]),
                      SizedBox(height: 5.0),
                      Container(
                          width: 100.0, height: 20.0, color: Colors.grey[300]),
                    ],
                  ),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.white,
                  child: Container(
                    width: 100.0,
                    height: 40.0,
                    color: Colors.grey[300],
                  ),
                )
              ],
            )),
      ),
    );
  }

  _buildContent(SurveyListLoaded state) {
    if (state.isLoad) {
      _page++;
      maxDatalength = state.maxData;
      dataListModel.addAll(state.records);
      state.isLoad = false;
    }
    return Container(
      child: ListView.builder(
          itemCount: dataListModel.length + 1,
          controller: _scrollController,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return index >= dataListModel.length
                ? maxDatalength != dataListModel.length
                    ? Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Column(
                          children: <Widget>[
                            CupertinoActivityIndicator(),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(Dictionary.loadingData),
                          ],
                        ),
                      )
                    : Container()
                : Card(
                    margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                    child: Container(
                        margin: EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width - 175,
                              child: Text(dataListModel[index].title,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.black)),
                            ),
                            Container(
                              width: 100.0,
                              child: RaisedButton(
                                color: Colors.green,
                                textColor: Colors.white,
                                child:
                                    Text(Dictionary.fillSurvey.toUpperCase()),
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, NavigationConstrants.Browser,
                                      arguments:
                                          dataListModel[index].externalUrl);

                                  AnalyticsHelper.setLogEvent(
                                      Analytics.EVENT_DETAIL_SURVEY,
                                      <String, dynamic>{
                                        'id': dataListModel[index].id,
                                        'title': dataListModel[index].title
                                      });
                                },
                              ),
                            )
                          ],
                        )),
                  );
          }),
    );
  }

  void _initialize() async {
    maxDatalength = await Preferences.getTotalCount();
  }

  void _scrollListener() {
    if (dataListModel.length != maxDatalength) {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _surveyListBloc.add(SurveyListLoad(page: _page));
      }
    }
  }

  @override
  void dispose() {
    _surveyListBloc.close();
    _scrollController.dispose();
    _mainRefreshController.dispose();
    super.dispose();
  }
}
