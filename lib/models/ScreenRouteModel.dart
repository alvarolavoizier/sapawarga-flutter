import 'package:firebase_analytics/firebase_analytics.dart';

class ScreenRouteModel {
  FirebaseAnalytics analytics;
  Object arguments;

  ScreenRouteModel(this.analytics, {this.arguments});
}