import 'dart:convert';

import 'PhoneNumberModel.dart';

Category categoryFromJson(String str) => Category.fromMap(json.decode(str));

class PhoneBookModel {
  final int id;
  final String name;
  final String address;
  final String description;
  final int seq;
  final int kabkotaId;
  final int kecId;
  final int kelId;
  final String latitude;
  final String longitude;
  final String coverImagePath;
  final String coverImageUrl;
  final int category_id;
  final List<PhoneNumberModel> phoneNumbers;
  final Category category;

  PhoneBookModel(
      {this.id,
      this.name,
      this.address,
      this.description,
      this.seq,
      this.kabkotaId,
      this.kecId,
      this.kelId,
      this.latitude,
      this.longitude,
      this.coverImagePath,
      this.coverImageUrl,
      this.phoneNumbers,
      this.category_id,
      this.category});

  factory PhoneBookModel.fromMap(Map<String, dynamic> json) {
    List<PhoneNumberModel> phoneNumbers = json['phone_numbers']
        .map((value) => PhoneNumberModel.fromMap(value))
        .toList()
        .cast<PhoneNumberModel>();


//    Category category = Category(
//      id: json['category']['id'],
//      type:  categoryTypeValues.map[json['category']['type']],
//      name: nameValues.map[json['category']['name']],
//      meta: json['category']['meta'],
//      status: json['category']['status'],
//      statusLabel: statusLabelValues.map[json['category']['status_label']],
//      createdAt: json['category']['created_at'],
//      updatedAt: json['category']['updated_at'],
//    );

    PhoneBookModel item = PhoneBookModel(
        id: json['id'],
        name: json['name'],
        address: json['address'],
        description: json['description'],
        seq: json['seq'],
        kabkotaId: json['kabkota_id'],
        kecId: json['kec_id'],
        kelId: json['kel_id'],
        latitude: json['latitude'],
        longitude: json['longitude'],
        coverImagePath: json['cover_image_path'],
        coverImageUrl: json['cover_image_url'],
//        category_id: json['category_id'],
        phoneNumbers: phoneNumbers,
//        category: category
    );

    return item;
  }

  factory PhoneBookModel.fromDatabaseMap(Map<String, dynamic> row) {
    List<PhoneNumberModel> phoneNumbers = jsonDecode(row['phone_numbers'])
        .map((value) => PhoneNumberModel.fromMap(value))
        .toList()
        .cast<PhoneNumberModel>();

//
//    final categoryJson = json.decode(row['category']);
//
//    Category category = Category(
//      id: int.parse(categoryJson['id'].toString()),
//      type:  categoryTypeValues.map[categoryJson['type']],
//      name: nameValues.map[categoryJson['name']],
//      meta: categoryJson['meta'],
//      status: int.parse(categoryJson['status'].toString()),
//      statusLabel: statusLabelValues.map[categoryJson['status_label']],
//      createdAt: int.parse(categoryJson['created_at'].toString()),
//      updatedAt: int.parse(categoryJson['updated_at'].toString()),
//    );

    PhoneBookModel item = PhoneBookModel(
        id: row['id'],
        name: row['name'],
        address: row['address'],
        description: row['description'],
        seq: row['seq'],
        kabkotaId: row['kabkota_id'],
        kecId: row['kec_id'],
        kelId: row['kel_id'],
        latitude: row['latitude'],
        longitude: row['longitude'],
        coverImagePath: row['cover_image_url'],
        coverImageUrl: row['cover_image_path'],
//        category_id: row['category_id'],
        phoneNumbers: phoneNumbers,
//        category: category
    );

    return item;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'address': address,
        'description': description,
      };
}

class Category {
  int id;
  CategoryType type;
  Name name;
  dynamic meta;
  int status;
  StatusLabel statusLabel;
  int createdAt;
  int updatedAt;

  Category({
    this.id,
    this.type,
    this.name,
    this.meta,
    this.status,
    this.statusLabel,
    this.createdAt,
    this.updatedAt,
  });

  factory Category.fromMap(Map<String, dynamic> json) {
    return Category(
      id: json["id"],
      type: categoryTypeValues.map[json["type"]],
      name: nameValues.map[json["name"]],
      meta: json["meta"],
      status: json["status"],
      statusLabel: statusLabelValues.map[json["status_label"]],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": categoryTypeValues.reverse[type],
        "name": nameValues.reverse[name],
        "meta": meta,
        "status": status,
        "status_label": statusLabelValues.reverse[statusLabel],
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}

enum Name { SOSIAL, LAYANAN, KESEHATAN }

enum CategoryType { PHONEBOOK }

final categoryTypeValues = EnumValues({"phonebook": CategoryType.PHONEBOOK});

final nameValues = EnumValues({
  "Kesehatan": Name.KESEHATAN,
  "Layanan": Name.LAYANAN,
  "Sosial": Name.SOSIAL
});

enum StatusLabel { AKTIF }

final statusLabelValues = EnumValues({"Aktif": StatusLabel.AKTIF});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => MapEntry(v, k));
    }
    return reverseMap;
  }
}
