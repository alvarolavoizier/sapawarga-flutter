class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;
  final dynamic value;

  RadioModel(this.isSelected, this.buttonText, this.value, {this.text});
}