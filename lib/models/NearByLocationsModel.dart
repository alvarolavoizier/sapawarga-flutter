import 'dart:convert';

import 'PhoneNumberModel.dart';

List<NearByLocationsModel> nearByLocationsFromJson(String str) =>
    List<NearByLocationsModel>.from(
        json.decode(str).map((x) => NearByLocationsModel.fromJson(x)));

class NearByLocationsModel {
  String id;
  String category;
  String name;
  dynamic description;
  String address;
  List<PhoneNumberModel> phoneNumbers;
  String latitude;
  String longitude;
  String distance;

  NearByLocationsModel({
    this.id,
    this.category,
    this.name,
    this.description,
    this.address,
    this.phoneNumbers,
    this.latitude,
    this.longitude,
    this.distance,
  });

  factory NearByLocationsModel.fromJson(Map<String, dynamic> json) =>
      NearByLocationsModel(
        id: json["id"],
        category: json["category"],
        name: json["name"],
        description: json["description"],
        address: json["address"],
        phoneNumbers: json["phone_numbers"] == null ? null : List<PhoneNumberModel>.from(json["phone_numbers"].map((x) => PhoneNumberModel.fromMap(x))),
        latitude: json["latitude"],
        longitude: json["longitude"],
        distance: json["distance"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category": category,
        "name": name,
        "description": description,
        "address": address,
        "phone_numbers": phoneNumbers == null ? null : List<dynamic>.from(phoneNumbers.map((x) => x.toJson())),
        "latitude": latitude,
        "longitude": longitude,
        "distance": distance,
      };
}
