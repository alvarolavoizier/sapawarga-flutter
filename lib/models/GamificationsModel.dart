import 'dart:convert';
import 'package:sapawarga/models/MetaResponseModel.dart';

GamificationModel gamificationFromJson(String str) => GamificationModel.fromJson(json.decode(str));
ItemGamification itemGamificationFromJson(String str) => ItemGamification.fromJson(json.decode(str));

class GamificationModel {
  List<ItemGamification> items;
  MetaResponseModel meta;

  GamificationModel({
    this.items,
    this.meta,
  });

  factory GamificationModel.fromJson(Map<String, dynamic> json) => GamificationModel(
    items: List<ItemGamification>.from(json["items"].map((x) => ItemGamification.fromJson(x))),
    meta: MetaResponseModel.fromJson(json["_meta"]),
  );

  Map<String, dynamic> toJson() => {
    "items": List<dynamic>.from(items.map((x) => x.toJson())),
    "_meta": meta.toJson(),
  };
}

class ItemGamification {
  int id;
  String title;
  String titleBadge;
  String description;
  String objectType;
  String objectEvent;
  int totalHit;
  String imageBadgePathUrl;
  DateTime startDate;
  DateTime endDate;
  int status;
  String statusLabel;
  int createdAt;
  int updatedAt;
  int createdBy;

  ItemGamification({
    this.id,
    this.title,
    this.titleBadge,
    this.description,
    this.objectType,
    this.objectEvent,
    this.totalHit,
    this.imageBadgePathUrl,
    this.startDate,
    this.endDate,
    this.status,
    this.statusLabel,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
  });

  factory ItemGamification.fromJson(Map<String, dynamic> json) => ItemGamification(
    id: json["id"],
    title: json["title"],
    titleBadge: json["title_badge"],
    description: json["description"],
    objectType: json["object_type"],
    objectEvent: json["object_event"],
    totalHit: json["total_hit"],
    imageBadgePathUrl: json["image_badge_path_url"],
    startDate: DateTime.parse(json["start_date"]),
    endDate: DateTime.parse(json["end_date"]),
    status: json["status"],
    statusLabel: json["status_label"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    createdBy: json["created_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "title_badge": titleBadge,
    "description": description,
    "object_type": objectType,
    "object_event": objectEvent,
    "total_hit": totalHit,
    "image_badge_path_url": imageBadgePathUrl,
    "start_date": "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
    "end_date": "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
    "status": status,
    "status_label": statusLabel,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "created_by": createdBy,
  };
}
