import 'dart:convert';

import 'package:sapawarga/models/CategoryModel.dart';

List<PollingModel> listPollingFromJson(String str) => List<PollingModel>.from(
    json.decode(str).map((x) => PollingModel.fromJson(x)));

PollingModel pollingFromJson(String str) =>
    PollingModel.fromJson(json.decode(str));

class PollingModel {
  int id;
  int categoryId;
  Category category;
  String name;
  String question;
  String description;
  String excerpt;
  dynamic kabkotaId;
  dynamic kabkota;
  dynamic kecId;
  dynamic kecamatan;
  dynamic kelId;
  dynamic kelurahan;
  dynamic rw;
  List<Answer> answers;
  DateTime startDate;
  DateTime endDate;
  int votesCount;
  dynamic meta;
  int status;
  String statusLabel;
  int createdAt;
  int updatedAt;

  PollingModel({
    this.id,
    this.categoryId,
    this.category,
    this.name,
    this.question,
    this.description,
    this.excerpt,
    this.kabkotaId,
    this.kabkota,
    this.kecId,
    this.kecamatan,
    this.kelId,
    this.kelurahan,
    this.rw,
    this.answers,
    this.startDate,
    this.endDate,
    this.votesCount,
    this.meta,
    this.status,
    this.statusLabel,
    this.createdAt,
    this.updatedAt,
  });

  factory PollingModel.fromJson(Map<String, dynamic> json) => PollingModel(
        id: json["id"],
        categoryId: json["category_id"],
        category: Category.fromJson(json["category"]),
        name: json["name"],
        question: json["question"],
        description: json["description"],
        excerpt: json["excerpt"],
        kabkotaId: json["kabkota_id"],
        kabkota: json["kabkota"],
        kecId: json["kec_id"],
        kecamatan: json["kecamatan"],
        kelId: json["kel_id"],
        kelurahan: json["kelurahan"],
        rw: json["rw"],
        answers:
            List<Answer>.from(json["answers"].map((x) => Answer.fromJson(x))),
        startDate: DateTime.parse(json["start_date"]),
        endDate: DateTime.parse(json["end_date"]),
        votesCount: json["votes_count"],
        meta: json["meta"],
        status: json["status"],
        statusLabel: json["status_label"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category_id": categoryId,
        "category": category.toJson(),
        "name": name,
        "question": question,
        "description": description,
        "excerpt": excerpt,
        "kabkota_id": kabkotaId,
        "kabkota": kabkota,
        "kec_id": kecId,
        "kecamatan": kecamatan,
        "kel_id": kelId,
        "kelurahan": kelurahan,
        "rw": rw,
        "answers": List<dynamic>.from(answers.map((x) => x.toJson())),
        "start_date":
            "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
        "end_date":
            "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
        "votes_count": votesCount,
        "meta": meta,
        "status": status,
        "status_label": statusLabel,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}

class Answer {
  int id;
  String body;
  int createdAt;
  int updatedAt;

  Answer({
    this.id,
    this.body,
    this.createdAt,
    this.updatedAt,
  });

  factory Answer.fromJson(Map<String, dynamic> json) => Answer(
        id: json["id"],
        body: json["body"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "body": body,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}