import 'package:flutter/cupertino.dart';
import 'package:timeline_list/timeline.dart';

class TimelineObjectModel {
  String title;
  String desc;
  IconData iconData;
  Color color;
  TimelinePosition timelinePosition;

  TimelineObjectModel(
      {this.title,
      this.desc,
      this.iconData,
      this.color,
      this.timelinePosition});
}
