enum CategoryType {
  news_important,
  phonebook,
  broadcast,
  polling,
  survey,
  aspirasi,
  video,
  user_post
}

class CategoryTypes {
  static String getString(CategoryType type) {
    switch (type) {
      case CategoryType.news_important:
        return 'news_important';

      case CategoryType.phonebook:
        return 'phonebook';

      case CategoryType.broadcast:
        return 'broadcast';

      case CategoryType.polling:
        return 'polling:';

      case CategoryType.survey:
        return 'survey';

      case CategoryType.aspirasi:
        return 'aspirasi';

      case CategoryType.video:
        return 'video';
      case CategoryType.user_post:
        return 'user_post';

      default:
        return null;
    }
  }
}
