import 'package:sapawarga/models/CategoryModel.dart';

class ToggleHelper {

  static bool isLike(List<Category> listLike, int userId) {
    bool returnData;
    if (listLike.isNotEmpty) {
      if (listLike.length > 1) {
        returnData = false;
        listLike.forEach((like) {
          if (like.id == userId) {
            returnData = true;
          }
        });
      } else {
        if (listLike[0].id == userId) {
          returnData =  true;
        } else {
          returnData =  false;
        }
      }
    } else {
      returnData =  false;
    }

    return returnData;
  }

}