import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/UserInfoModel.dart';

class AnalyticsHelper {
  static Future<void> setCurrentScreen(String screenName,
      {String screenClassOverride}) async {
    final FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: screenClassOverride != null
          ? screenClassOverride
          : '${screenName.replaceAll(RegExp("\\s+"), "")}Screen',
    );
  }

  static Future<void> setUserProperty(UserInfoModel userInfo) async {
    final FirebaseAnalytics analytics = FirebaseAnalytics();
    if (userInfo != null &&
        userInfo.username != null &&
        userInfo.username.isNotEmpty) {
      await analytics.setUserProperty(
          name: Analytics.USER_ID, value: '${userInfo.id}');
      await analytics.setUserProperty(
          name: Analytics.USERNAME, value: '${userInfo.username}');
      await analytics.setUserProperty(
          name: Analytics.NAME, value: '${userInfo.name}');
      await analytics.setUserProperty(
          name: Analytics.ROLE, value: '${userInfo.roleLabel}');
      await analytics.setUserProperty(
          name: Analytics.RT, value: '${userInfo.rt}');
      await analytics.setUserProperty(
          name: Analytics.RW, value: '${userInfo.rw}');
      await analytics.setUserProperty(
          name: Analytics.KELURAHAN, value: '${userInfo.kelurahan.name}');
      await analytics.setUserProperty(
          name: Analytics.KECAMATAN, value: '${userInfo.kecamatan.name}');
      await analytics.setUserProperty(
          name: Analytics.KABKOTA, value: '${userInfo.kabkota.name}');
    } else {
      await analytics.setUserProperty(
          name: Analytics.USER_ID, value: Dictionary.unknown);
      await analytics.setUserProperty(
          name: Analytics.USERNAME, value: Dictionary.unknown);
      await analytics.setUserProperty(name: Analytics.NAME, value: Dictionary.unknown);
      await analytics.setUserProperty(name: Analytics.ROLE, value: Dictionary.unknown);
      await analytics.setUserProperty(name: Analytics.RT, value: Dictionary.unknown);
      await analytics.setUserProperty(name: Analytics.RW, value: Dictionary.unknown);
      await analytics.setUserProperty(
          name: Analytics.KELURAHAN, value: Dictionary.unknown);
      await analytics.setUserProperty(
          name: Analytics.KECAMATAN, value: Dictionary.unknown);
      await analytics.setUserProperty(
          name: Analytics.KABKOTA, value: Dictionary.unknown);
    }
  }

  static Future<void> setLogEvent(String eventName,
      [Map<String, dynamic> source]) async {
    final FirebaseAnalytics analytics = FirebaseAnalytics();

    await analytics.logEvent(
      name: eventName,
      parameters: source,
    );
  }
}
