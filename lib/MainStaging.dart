import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/components/LoadingScreen.dart';
import 'package:sapawarga/configs/FlavorConfig.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/screens/login/LoginScreen.dart';
import 'package:sapawarga/screens/main/MainScreen.dart';
import 'package:sapawarga/screens/onBoarding/Onboarding.dart';
import 'package:sapawarga/screens/splashScreen/SplashScreen.dart';
import 'package:sapawarga/constants/Colors.dart' as color;

import 'configs/Routes.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() async {
  FlavorConfig(
      flavor: Flavor.STAGING,
      color: Colors.red,
      values: FlavorValues(
          baseUrl: Environment.apiStaging,
          databaseName: Environment.databaseNameStaging,
          apiStorage: Environment.apiStagingStorage));
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final authRepository = AuthRepository();

  await FlutterDownloader.initialize();

  runApp(BlocProvider<AuthenticationBloc>(
    create: (context) =>
        AuthenticationBloc(authRepository: authRepository)..add(AppStarted()),
    child: App(authRepository: authRepository),
  ));
}

class App extends StatefulWidget {
  final AuthRepository authRepository;

  App({Key key, @required AuthRepository authRepository})
      : assert(authRepository != null),
        authRepository = authRepository,
        super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  @override
  void initState() {
    _createDirectory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: color.Colors.blue));

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      title: '${Dictionary.appName} Staging',
      theme: ThemeData(
          primaryColor: color.Colors.blue,
          primaryColorBrightness: Brightness.dark,
          fontFamily: FontsFamily.sourceSansPro),
      debugShowCheckedModeBanner: false,
      home: BlocListener<AuthenticationBloc, AuthenticationState>(
        bloc: BlocProvider.of<AuthenticationBloc>(context),
        listener: (context, state) {
          if (state is AuthenticationUnauthenticated) {
            if (!state.hasOnBoarding) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => OnBoardingScreen()));
            }
          }
        },
        child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          bloc: BlocProvider.of<AuthenticationBloc>(context),
          builder: (context, state) {
            if (state is AuthenticationAuthenticated) {
              return MainScreen();
            } else if (state is AuthenticationUnauthenticated) {
              return LoginScreen(
                authRepository: widget.authRepository,
              );
            } else if (state is AuthenticationLoading) {
              return LoadingScreen();
            } else {
              return SplashScreen();
            }
          },
        ),
      ),
      onGenerateRoute: generateRoutes,
      navigatorKey: NavigationConstrants.navKey,
    );
  }

  _createDirectory() async {
    if (Platform.isAndroid) {
      String localPath =
          (await getExternalStorageDirectory()).path + '/download';
      final publicDownloadDir = Directory(Environment.downloadStorage);
      final savedDir = Directory(localPath);
      bool hasExistedPublicDownloadDir = await publicDownloadDir.exists();
      bool hasExistedSavedDir = await savedDir.exists();
      if (!hasExistedPublicDownloadDir) {
        await publicDownloadDir.create();
      }
      if (!hasExistedSavedDir) {
        await savedDir.create();
      }
    }
  }
}
