import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GamifationDetailEvent extends Equatable {
  GamifationDetailEvent([List props = const <dynamic>[]]);
}

class GamificationsDetailLoad extends GamifationDetailEvent {
  final int id;

  GamificationsDetailLoad({@required this.id}) : super([id]);

  @override
  String toString() {
    return 'Event GamificationsDetailLoad';
  }

  @override
  List<Object> get props => [id];
}

class GamificationsTakeMission extends GamifationDetailEvent {
  final int id;

  GamificationsTakeMission({@required this.id}) : super([id]);

  @override
  String toString() {
    return 'Event GamificationsTakeMission';
  }

  @override
  List<Object> get props => [id];
}
