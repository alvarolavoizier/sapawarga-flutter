import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GamifationsMyBadgeEvent extends Equatable {
  GamifationsMyBadgeEvent([List props = const <dynamic>[]]);
}

class GamificationsMyBadgeLoad extends GamifationsMyBadgeEvent {
  final int page;

  GamificationsMyBadgeLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event GamificationsMyBadgeLoad';
  }

  @override
  List<Object> get props => [page];
}
