import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/GamificationDetailOnProgressModel.dart';

@immutable
abstract class GamificationsDetailMissionState extends Equatable {
  GamificationsDetailMissionState([List props = const <dynamic>[]]);
}

class GamificationsDetailMissionInitial extends GamificationsDetailMissionState {
  @override
  String toString() => 'State GamificationsDetailMissionInitial';

  @override
  List<Object> get props => [];
}

class GamificationsDetailMissionLoading extends GamificationsDetailMissionState {
  @override
  String toString() {
    return 'State GamificationsDetailMissionLoading';
  }

  @override
  List<Object> get props => [];
}


class GamificationsDetailMissionLoaded extends GamificationsDetailMissionState {
  final GamificationDetailOnProgressModel records;

  GamificationsDetailMissionLoaded({@required this.records}) : super([records]);

  @override
  String toString() {
    return 'State GamificationsDetailMissionLoaded';
  }

  @override
  List<Object> get props => [records];
}

class GamificationsDetailMissionFailure extends GamificationsDetailMissionState {
  final String error;

  GamificationsDetailMissionFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State GamificationsDetailMissionFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
