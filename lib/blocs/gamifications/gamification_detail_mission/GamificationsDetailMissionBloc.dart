import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GamificationDetailOnProgressModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'Bloc.dart';

class GamificationsDetailMissionBloc extends Bloc<
    GamifationsDetailMissionEvent, GamificationsDetailMissionState> {
  final GamificationsRepository gamificationsRepository;

  GamificationsDetailMissionBloc({@required this.gamificationsRepository});

  @override
  GamificationsDetailMissionState get initialState =>
      GamificationsDetailMissionInitial();

  @override
  Stream<GamificationsDetailMissionState> mapEventToState(
    GamifationsDetailMissionEvent event,
  ) async* {
    if (event is GamificationsDetailMissionLoad) {
      yield GamificationsDetailMissionLoading();

      try {
        GamificationDetailOnProgressModel records =
            await gamificationsRepository.getDetailMission(
                id: event.id);
        yield GamificationsDetailMissionLoaded(records: records);
      } catch (e) {
        yield GamificationsDetailMissionFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
