import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GamificationsModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'Bloc.dart';


class GamificationsListBloc extends Bloc<GamifationsListEvent, GamificationsListState> {
  final GamificationsRepository gamificationsRepository;

  GamificationsListBloc({@required this.gamificationsRepository}):assert(gamificationsRepository != null);

  @override
  GamificationsListState get initialState => GamificationsListInitial();

  @override
  Stream<GamificationsListState> mapEventToState(
    GamifationsListEvent event,
  ) async* {
    if (event is GamificationsNewMissionListLoad) {
      if (event.page == 1) yield GamificationsListLoading();

      try {
        GamificationModel records = await gamificationsRepository.getNewMission(page: event.page, status: 10);
        yield GamificationsListLoaded(records: records);
      } catch (e) {
        yield GamificationsListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
