import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GamifationsListEvent extends Equatable {
  GamifationsListEvent([List props = const <dynamic>[]]);
}

class GamificationsNewMissionListLoad extends GamifationsListEvent {
  final int page;

  GamificationsNewMissionListLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event GamificationsListLoad';
  }

  @override
  List<Object> get props => [page];
}
