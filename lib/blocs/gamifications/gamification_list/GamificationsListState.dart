import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/GamificationsModel.dart';

@immutable
abstract class GamificationsListState extends Equatable {
  GamificationsListState([List props = const <dynamic>[]]);
}

class GamificationsListInitial extends GamificationsListState {
  @override
  String toString() => 'State GamificationsListInitial';

  @override
  List<Object> get props => [];
}

class GamificationsListLoading extends GamificationsListState {
  @override
  String toString() {
    return 'State GamificationsListLoading';
  }

  @override
  List<Object> get props => [];
}


class GamificationsListLoaded extends GamificationsListState {
  final GamificationModel records;

  GamificationsListLoaded({@required this.records}) : super([records]);

  @override
  String toString() {
    return 'State GamificationsListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class GamificationsListFailure extends GamificationsListState {
  final String error;

  GamificationsListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State GamificationsListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
