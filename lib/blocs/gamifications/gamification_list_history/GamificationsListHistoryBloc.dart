import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'Bloc.dart';

class GamificationsListHistoryBloc extends Bloc<
    GamifationsListHistoryEvent, GamificationsListHistoryState> {
  final GamificationsRepository gamificationsRepository;

  GamificationsListHistoryBloc({@required this.gamificationsRepository});

  @override
  GamificationsListHistoryState get initialState =>
      GamificationsListHistoryInitial();

  @override
  Stream<GamificationsListHistoryState> mapEventToState(
    GamifationsListHistoryEvent event,
  ) async* {
    if (event is GamificationsHistoryListLoad) {
      if (event.page == 1) yield GamificationsListHistoryLoading();

      try {
        GamificationOnprogressModel records = await gamificationsRepository
            .getMissionHistory(page: event.page);
        yield GamificationsListHistoryLoaded(records: records);
      } catch (e) {
        yield GamificationsListHistoryFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
