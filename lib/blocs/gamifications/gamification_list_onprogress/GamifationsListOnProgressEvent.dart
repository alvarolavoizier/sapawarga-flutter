import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GamifationsListOnProgressEvent extends Equatable {
  GamifationsListOnProgressEvent([List props = const <dynamic>[]]);
}

class GamificationsOnProgressListLoad extends GamifationsListOnProgressEvent {
  final int page;

  GamificationsOnProgressListLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event GamificationsOnProgressListLoad';
  }

  @override
  List<Object> get props => [page];
}
