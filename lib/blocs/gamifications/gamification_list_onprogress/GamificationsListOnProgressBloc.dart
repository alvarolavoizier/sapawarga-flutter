import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'Bloc.dart';

class GamificationsListOnProgressBloc extends Bloc<
    GamifationsListOnProgressEvent, GamificationsListOnProgressState> {
  final GamificationsRepository gamificationsRepository;

  GamificationsListOnProgressBloc({@required this.gamificationsRepository});

  @override
  GamificationsListOnProgressState get initialState =>
      GamificationsListOnProgressInitial();

  @override
  Stream<GamificationsListOnProgressState> mapEventToState(
    GamifationsListOnProgressEvent event,
  ) async* {
    if (event is GamificationsOnProgressListLoad) {
      if (event.page == 1) yield GamificationsListOnProgressLoading();

      try {
        GamificationOnprogressModel records = await gamificationsRepository
            .getMissionOnProgress(page: event.page);
        yield GamificationsListOnProgressLoaded(records: records);
      } catch (e) {
        yield GamificationsListOnProgressFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
