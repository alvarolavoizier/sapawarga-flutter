import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'NewsListState.dart';
import 'NewsListlEvent.dart';

class NewsListBloc extends Bloc<NewsListEvent, NewsListState> {

  final NewsRepository newsRepository;


  NewsListBloc({@required this.newsRepository}):assert(newsRepository != null);

  @override
  NewsListState get initialState => NewsListInitial();

  @override
  Stream<NewsListState> mapEventToState(NewsListEvent event) async* {
    if (event is NewsListLoad) {
      if(event.page == 1) yield NewsListLoading();

      try {
        List<NewsModel> listNews = await newsRepository.getNews(isKotaId:event.isIdKota, page: event.page);
        int maxDatalength = await Preferences.getTotalCount();
        try {
          String cityName = await newsRepository.getCityName();
          yield NewsListLoaded(listNews: listNews, cityName:cityName, maxData: maxDatalength, isLoad: true);
        } catch (_) {
          yield NewsListLoaded(listNews: [], maxData: maxDatalength, isLoad: true);
        }

      } catch (e) {
        yield NewsListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
