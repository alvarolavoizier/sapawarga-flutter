import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/BroadcastModel.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';
import 'bloc.dart';

class BroadcastDetailBloc extends Bloc<BroadcastDetailEvent, BroadcastDetailState> {
  BroadcastRepository broadcastRepository;

  BroadcastDetailBloc(this.broadcastRepository);

  @override
  BroadcastDetailState get initialState => InitialBroadcastDetailState();

  @override
  Stream<BroadcastDetailState> mapEventToState(
    BroadcastDetailEvent event,
  ) async* {
    if (event is BroadcastDetailLoad) {
      yield BroadcastDetailLoading();

      try {

        BroadcastModel record = await broadcastRepository.getDetail(
            event.id);

        yield BroadcastDetailLoaded(record: record);
      } catch (e) {
        yield BroadcastDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }

    }
  }
}
