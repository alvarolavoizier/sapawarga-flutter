import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/HumasJabarModel.dart';

abstract class HumasJabarListState extends Equatable {
  HumasJabarListState([List props = const []]);
}

class HumasJabarlInitial extends HumasJabarListState {
  @override
  String toString() => 'State HumasJabarListInitial';

  @override
  List<Object> get props => [];
}

class HumasJabarLoading extends HumasJabarListState {
  @override
  String toString() => 'State HumasJabarListLoading';

  @override
  List<Object> get props => [];
}

class HumasJabarLoaded extends HumasJabarListState {
  final List<HumasJabarModel> records;

  HumasJabarLoaded({@required this.records}) : super([records]);

  @override
  String toString() => 'State HumasJabarListLoaded';

  @override
  List<Object> get props => [records];
}

class HumasJabarFailure extends HumasJabarListState {
  final String error;

  HumasJabarFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'State HumasJabarListFailure';

  @override
  List<Object> get props => [error];
}
