import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PopupInformationEvent extends Equatable {
  PopupInformationEvent([List props = const <dynamic>[]]);
}

class CheckPopupInformation extends PopupInformationEvent {
  @override
  String toString() {
    return 'Event CheckPopupInformation';
  }

  @override
  List<Object> get props => [];
}
