import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/PollingModel.dart';
import 'package:sapawarga/repositories/PollingRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import './Bloc.dart';

class PollingListBloc extends Bloc<PollingListEvent, PollingListState> {

  final PollingRepository pollingRepository;


  PollingListBloc({@required this.pollingRepository}):assert(pollingRepository != null);

  @override
  PollingListState get initialState => PollingListInitial();

  @override
  Stream<PollingListState> mapEventToState(
    PollingListEvent event,
  ) async* {
    if (event is PollingListLoad) {
      yield PollingListLoading();

      try {
        List<PollingModel> records = await pollingRepository.fetchRecords(event.page);
        int maxDatalength = await Preferences.getTotalCount();
        yield PollingListLoaded(records: records, maxData: maxDatalength, isLoad: true);
      } catch (e) {
        print(e.toString());
        yield PollingListFailure(error: CustomException.onConnectionException(e.toString()));
      }

    }
  }

}
