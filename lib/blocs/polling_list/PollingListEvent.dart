import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PollingListEvent extends Equatable {
  PollingListEvent([List props = const <dynamic>[]]);
}

class PollingListLoad extends PollingListEvent {
  final int pollingId;
  final int page;

  PollingListLoad({this.pollingId, this.page});

  @override
  String toString() {
    return 'Event PollingListLoad';
  }

  @override
  List<Object> get props => [pollingId];
}
