import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/UserInfoModel.dart';

abstract class AccountProfileEditEvent extends Equatable {
  AccountProfileEditEvent([List props = const []]);
}

class AccountProfileEditSubmit extends AccountProfileEditEvent {
  final UserInfoModel userInfoModel;

  AccountProfileEditSubmit({@required this.userInfoModel}):assert(userInfoModel != null);

  @override
  String toString() => 'Event AccountProfileEditSubmit';

  @override
  List<Object> get props => [];
}

class AccountProfileEditPhotoSubmit extends AccountProfileEditEvent {
  final image;
  final UserInfoModel userInfoModel;

  AccountProfileEditPhotoSubmit({@required this.image, this.userInfoModel})
      : super([image, userInfoModel]);

  @override
  String toString() => 'Event AccountProfileEditPhotoSubmit';

  @override
  List<Object> get props => [];
}

class CompleteProfileSubmit extends AccountProfileEditEvent {
  final String name;
  final String email;
  final String phone;
  final String address;
  final String jobId;
  final String educationId;

  CompleteProfileSubmit({@required this.name, @required this.email, @required this.phone, @required this.address, this.jobId, this.educationId}):assert((name != null) && (email != null) && (phone != null) && (address != null));

  @override
  String toString() {
    return 'Event CompleteProfileSubmit';
  }

  @override
  List<Object> get props => [];
}
