import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GeneralTitleModel.dart';
import 'package:sapawarga/repositories/EducationRepository.dart';

import 'Bloc.dart';

class EducationsListBloc
    extends Bloc<EducationsListEvent, EducationsListState> {
  EducationRepository educationRepository = EducationRepository();

  EducationsListBloc({@required this.educationRepository})
      : assert(educationRepository != null);

  @override
  EducationsListState get initialState => InitialEducationsListState();

  @override
  Stream<EducationsListState> mapEventToState(
    EducationsListEvent event,
  ) async* {
    if (event is EducationsLoad) {
      yield EducationsListLoading();

      try {
        List<GeneralTitleModel> record =
            await educationRepository.fetchEducations();

        yield EducationsLoaded(record: record);
      } catch (e) {
        yield EducationsFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
