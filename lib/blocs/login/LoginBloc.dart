import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/login/Bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository authRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.authRepository,
    @required this.authenticationBloc,
  })  : assert(authRepository != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
        final token = await authRepository.authenticate(
          username: event.username,
          password: event.password,
          fcmToken: event.fcmToken,
        );

        authenticationBloc.add(LoggedIn(token: token));
        yield LoginInitial();
      } on ValidationException catch (error) {
        yield LoginValidationError(errors: error.errors);
      } catch (e) {
        yield LoginFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
