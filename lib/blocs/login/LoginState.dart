import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginState extends Equatable {
  LoginState([List props = const <dynamic>[]]);
}

class LoginInitial extends LoginState {
  @override
  String toString() => 'LoginInitial';

  @override
  List<Object> get props => [];
}

class LoginLoading extends LoginState {
  @override
  String toString() => 'LoginLoading';

  @override
  List<Object> get props => [];
}

class LoginFailure extends LoginState {
  final String error;

  LoginFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'LoginFailure { error: $error }';

  @override
  List<Object> get props => [error];
}

class LoginValidationError extends LoginState {
  final Map<String, dynamic> errors;

  LoginValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() => 'LoginValidationError';

  @override
  List<Object> get props => [errors];
}
