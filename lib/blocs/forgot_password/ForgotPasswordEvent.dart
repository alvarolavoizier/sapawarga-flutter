import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ForgotPasswordEvent extends Equatable {
  ForgotPasswordEvent([List props = const <dynamic>[]]);
}

class RequestForgotPassword extends ForgotPasswordEvent {
  final String email;

  RequestForgotPassword({@required this.email}) : assert(email != null);

  @override
  String toString() {
    return 'SendRequestForgotPassword{email: $email}';
  }

  @override
  List<Object> get props => [email];
}
