import 'package:equatable/equatable.dart';

abstract class QnAGovernorListEvent extends Equatable {
  const QnAGovernorListEvent([List props = const <dynamic>[]]);
}

class QnAGovernorListLoad extends QnAGovernorListEvent {
  final int page;

  QnAGovernorListLoad(this.page):super([page]);

  @override
  String toString() {
    return 'Event QnAGovernorListLoad{page: $page}';
  }

  @override
  List<Object> get props => [page];

}

class QnAGovernorListLike extends QnAGovernorListEvent {
  final int id;

  QnAGovernorListLike(this.id):super([id]);

  @override
  String toString() {
    return 'Event QnAGovernorListLike{id: $id}';
  }

  @override
  List<Object> get props => [id];

}