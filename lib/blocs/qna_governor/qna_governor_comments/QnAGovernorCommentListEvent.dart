import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class QnAGovernorCommentListEvent extends Equatable {
  const QnAGovernorCommentListEvent([List props = const <dynamic>[]]);
}

class QnAGovernorCommentListLoad extends QnAGovernorCommentListEvent {
  final int questionId;
  final int page;

  QnAGovernorCommentListLoad({@required this.questionId, @required this.page})
      : super([questionId, page]);

  @override
  String toString() {
    return 'Event QnAGovernorCommentListLoad{questionId: $questionId, page: $page}';
  }

  @override
  List<Object> get props => [questionId, page];
}
