import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/QnACommentModel.dart';

abstract class QnAGovernorCommentListState extends Equatable {
  const QnAGovernorCommentListState([List props = const <dynamic>[]]);
}

class QnAGovernorCommentListInitial extends QnAGovernorCommentListState {
  @override
  List<Object> get props => [];
}

class QnAGovernorCommentListLoading extends QnAGovernorCommentListState {
  @override
  String toString() {
    return 'State QnAGovernorCommentListLoading';
  }

  @override
  List<Object> get props => [];
}

class QnAGovernorCommentListLoaded extends QnAGovernorCommentListState {
  final List<QnACommentModel> records;

  QnAGovernorCommentListLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State QnAGovernorCommentListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class QnAGovernorCommentListFailure extends QnAGovernorCommentListState {
  final String error;

  QnAGovernorCommentListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State QnAGovernorCommentListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}