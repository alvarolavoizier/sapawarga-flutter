import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/QnAModel.dart';

abstract class QnAGovernorDetailState extends Equatable {
  const QnAGovernorDetailState([List props = const <dynamic>[]]);
}

class QnAGovernorDetailInitial extends QnAGovernorDetailState {
  @override
  String toString() {
    return 'QnAGovernorDetailInitial';
  }

  @override
  List<Object> get props => [];
}

class QnAGovernorDetailLoading extends QnAGovernorDetailState {
  @override
  String toString() {
    return 'State QnAGovernorDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class QnAGovernorDetailLoaded extends QnAGovernorDetailState {
  final QnAModel record;

  QnAGovernorDetailLoaded(this.record) : super([record]);

  @override
  String toString() {
    return 'State QnAGovernorDetailLoaded';
  }

  @override
  List<Object> get props => [record];
}

class QnAGovernorDetailFailure extends QnAGovernorDetailState {
  final String error;

  QnAGovernorDetailFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State QnAGovernorDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
