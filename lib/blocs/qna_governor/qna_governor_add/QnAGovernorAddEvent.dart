import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class QnAGovernorAddEvent extends Equatable {
  const QnAGovernorAddEvent([List props = const <dynamic>[]]);
}

class QnAGovernorAdd extends QnAGovernorAddEvent {
  final String question;

  QnAGovernorAdd({@required this.question}) : super([question]);

  @override
  String toString() {
    return 'Event QnAGovernorAdd{question: $question}';
  }

  @override
  List<Object> get props => [question];
}
