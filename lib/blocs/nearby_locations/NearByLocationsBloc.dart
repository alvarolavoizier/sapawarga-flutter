import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:sapawarga/blocs/nearby_locations/Bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NearByLocationsModel.dart';
import 'package:sapawarga/repositories/PhoneBookRepository.dart';

class NearByLocationsBloc
    extends Bloc<NearByLocationsEvent, NearByLocationsState> {
  final PhoneBookRepository phoneBookRepository;

  NearByLocationsBloc({this.phoneBookRepository}):assert(phoneBookRepository != null);

  @override
  NearByLocationsState get initialState => NearByLocationsInitial();

  @override
  Stream<NearByLocationsState> mapEventToState(
    NearByLocationsEvent event,
  ) async* {
    if (event is NearByLocationsLoad) {
      yield NearByLocationsLoading();

      try {
        List<NearByLocationsModel> records =
            await phoneBookRepository.getNearByLocations(
                latitude: event.latitude, longitude: event.longitude);

        yield NearByLocationsLoaded(records: records);
      } catch (e) {
        yield NearByLocationsFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
