import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';
import 'package:sapawarga/repositories/CounterHoaxRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import './Bloc.dart';

class CounterHoaxListBloc extends Bloc<CounterHoaxListEvent, CounterHoaxListState> {
  CounterHoaxRepository counterHoaxRepository;

  CounterHoaxListBloc({@required this.counterHoaxRepository}):assert(counterHoaxRepository != null);

  @override
  CounterHoaxListState get initialState => CounterHoaxListInitial();

  @override
  Stream<CounterHoaxListState> mapEventToState(
    CounterHoaxListEvent event,
  ) async* {
    if (event is CounterHoaxListLoad) {
     if(event.page == 1) yield CounterHoaxListLoading();
      try {
        List<CounterHoaxModel> records = await counterHoaxRepository.fetchRecords(event.page);
        int maxDatalength = await Preferences.getTotalCount();
        yield CounterHoaxListLoaded(records: records, maxLengthData: maxDatalength, isLoad: true);
      } catch (e) {
        yield CounterHoaxListFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is CounterHoaxListRefresh) {
      yield CounterHoaxListLoading();
      try {
        List<CounterHoaxModel> records = await counterHoaxRepository.fetchRecords(1);
        int maxDatalength = await Preferences.getTotalCount();

        yield CounterHoaxListLoaded(records: records, maxLengthData: maxDatalength, isLoad: true);
      } catch (e) {
        yield CounterHoaxListFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
