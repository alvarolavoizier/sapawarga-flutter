import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CounterHoaxListEvent extends Equatable {
  CounterHoaxListEvent([List props = const <dynamic>[]]);
}

class CounterHoaxListLoad extends CounterHoaxListEvent {
  final int page;

  CounterHoaxListLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event ConterHoaxListLoad';
  }

  @override
  List<Object> get props => [];
}

class CounterHoaxListRefresh extends CounterHoaxListEvent {
  @override
  String toString() {
    return 'Event ConterHoaxListRefresh';
  }

  @override
  List<Object> get props => [];
}
