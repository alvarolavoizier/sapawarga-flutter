import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/VideoModel.dart';

@immutable
abstract class VideoListJabarState extends Equatable {
  VideoListJabarState([List props = const <dynamic>[]]);
}

class VideoListJabarInitial extends VideoListJabarState {
  @override
  List<Object> get props => [];
}

class VideoListJabarLoading extends VideoListJabarState {
  @override
  String toString() {
    return 'State VideoListJabarLoading';
  }

  @override
  List<Object> get props => [];
}

class VideoListJabarLoaded extends VideoListJabarState {
  final List<VideoModel> records;

  VideoListJabarLoaded({@required this.records}) : assert(records != null);

  @override
  String toString() {
    return 'Stete VideoListJabarLoaded';
  }

  @override
  List<Object> get props => [records];
}

class VideoListJabarFailure extends VideoListJabarState {
  final String error;

  VideoListJabarFailure({this.error});

  @override
  String toString() {
    return 'State VideoListJabarFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
