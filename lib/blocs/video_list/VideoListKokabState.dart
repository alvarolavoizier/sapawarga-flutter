import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/VideoModel.dart';

@immutable
abstract class VideoListKokabState extends Equatable {
  VideoListKokabState([List props = const <dynamic>[]]);
}

class VideoListKokabInitial extends VideoListKokabState {
  @override
  List<Object> get props => [];
}

class VideoListKokabLoading extends VideoListKokabState {
  @override
  String toString() {
    return 'State VideoListKokabLoading';
  }

  @override
  List<Object> get props => [];
}

class VideoListKokabLoaded extends VideoListKokabState {
  final List<VideoModel> records;
  final String kokab;

  VideoListKokabLoaded({@required this.records, this.kokab}):assert(records != null);

  @override
  String toString() {
    return 'Stete VideoListKokabLoaded';
  }

  @override
  List<Object> get props => [records, kokab];
}

class VideoListKokabFailure extends VideoListKokabState {
  final String error;

  VideoListKokabFailure({this.error});

  @override
  String toString() {
    return 'State VideoListKokabFailure{error: $error}';
  }

  List<Object> get props => [error];
}
