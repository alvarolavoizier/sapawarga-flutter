import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class VideoListKokabEvent extends Equatable {
  VideoListKokabEvent([List props = const <dynamic>[]]);
}

class VideoListKokabLoad extends VideoListKokabEvent {
  @override
  String toString() {
    return 'Event VideoListKokabLoad';
  }

  @override
  List<Object> get props => [];
}
