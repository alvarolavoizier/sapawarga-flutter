import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/VideoModel.dart';
import 'package:sapawarga/repositories/VideoRepository.dart';

import './Bloc.dart';

class VideoListJabarBloc extends Bloc<VideoListJabarEvent, VideoListJabarState> {
  final VideoRepository videoRepository;

  VideoListJabarBloc({@required this.videoRepository}):assert(videoRepository != null);

  @override
  VideoListJabarState get initialState => VideoListJabarInitial();

  @override
  Stream<VideoListJabarState> mapEventToState(
    VideoListJabarEvent event,
  ) async* {
    if (event is VideoListJabarLoad) {
      yield VideoListJabarLoading();

      try {
        List<VideoModel> records = await videoRepository.fetchRecords();
        yield VideoListJabarLoaded(records: records);
      } catch (e) {
        bool hasRecords = await videoRepository.hasVideosJabarLocal();
        if (hasRecords) {
          List<VideoModel> records = await videoRepository.getVideosJabarLocal();
          yield VideoListJabarLoaded(records: records);
        } else {
          print(e.toString());
          yield VideoListJabarFailure(
              error: CustomException.onConnectionException(e.toString()));
        }
      }
    }
  }
}
