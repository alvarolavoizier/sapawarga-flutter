import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/repositories/PhoneBookRepository.dart';
import 'Bloc.dart';

class AdministrationDetailBloc
    extends Bloc<AdministrationDetailEvent, AdministrationDetailState> {
  final PhoneBookRepository phoneBookRepository;

  AdministrationDetailBloc({@required this.phoneBookRepository});

  @override
  AdministrationDetailState get initialState => AdministrationDetailInitial();

  @override
  Stream<AdministrationDetailState> mapEventToState(
    AdministrationDetailEvent event,
  ) async* {
    if (event is AdministrationDetailLoad) {
      yield AdministrationDetailLoading();
      var phoneBookModel;
      try {
        phoneBookModel = await phoneBookRepository.getNearByLocationInstansi(
            nameInstansi: event.instansi);
        yield AdministrationDetailLoaded(phoneBookModel: phoneBookModel);
      } catch (e) {
        if ((e.toString()
            .toLowerCase()
            .contains(ErrorException.socketException.toLowerCase()))) {
          yield AdministrationDetailFailure(
              error: CustomException.onConnectionException(e.toString()));
        } else if (phoneBookModel == null) {
          yield AdministrationDetailLoaded(phoneBookModel: null);
        }
      }
    }
  }
}
