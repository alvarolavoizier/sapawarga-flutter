import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import './Bloc.dart';

class ImportantInfoDetailBloc
    extends Bloc<ImportantInfoDetailEvent, ImportantInfoDetailState> {
  final ImportantInfoRepository importantInfoRepository;

  ImportantInfoDetailBloc({@required this.importantInfoRepository}):assert(importantInfoRepository != null);

  @override
  ImportantInfoDetailState get initialState => ImportantInfoDetailInitial();

  @override
  Stream<ImportantInfoDetailState> mapEventToState(
    ImportantInfoDetailEvent event,
  ) async* {
    if (event is ImportantInfoDetailLoad) {
      yield ImportantInfoDetailLoading();
      try {
        ImportantInfoModel record =
        await importantInfoRepository.getDetail(event.id);
        yield ImportantInfoDetailLoaded(record: record);
      } catch (e) {
        yield ImportantInfoDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is ImportantInfoDetailLike) {

        await importantInfoRepository.sendLike(id: event.importantInfoId);
    }
  }
}
