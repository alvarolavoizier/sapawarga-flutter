import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';

@immutable
abstract class ImportantInfoDetailState extends Equatable {
  ImportantInfoDetailState([List props = const <dynamic>[]]);
}

class ImportantInfoDetailInitial extends ImportantInfoDetailState {
  @override
  List<Object> get props => [];
}

class ImportantInfoDetailLoading extends ImportantInfoDetailState {
  @override
  String toString() {
    return 'State ImportantInfoDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoDetailLoaded extends ImportantInfoDetailState {
  final ImportantInfoModel record;

  ImportantInfoDetailLoaded({@required this.record}) : assert(record != null);

  @override
  String toString() {
    return 'State ImportantInfoDetailLoaded';
  }

  @override
  List<Object> get props => [record];
}

class ImportantInfoDetailFailure extends ImportantInfoDetailState {
  final String error;

  ImportantInfoDetailFailure({this.error});

  @override
  String toString() {
    return 'State ImportantInfoDetailFailure{error: $error}';
  }

  List<Object> get props => [error];
}
