import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ImportantInfoDetailEvent extends Equatable {
  ImportantInfoDetailEvent([List props = const <dynamic>[]]);
}

class ImportantInfoDetailLoad extends ImportantInfoDetailEvent {
  final int id;

  ImportantInfoDetailLoad({this.id});

  @override
  String toString() {
    return 'Event ImportantInfoDetailLoad';
  }

  @override
  List<Object> get props => [id];
}


class ImportantInfoDetailLike extends ImportantInfoDetailEvent {
  final int importantInfoId;

  ImportantInfoDetailLike({@required this.importantInfoId}) : super([importantInfoId]);

  @override
  String toString() {
    return 'Event ImportantInfoDetailLike{importantInfoId: $importantInfoId}';
  }

  @override
  List<Object> get props => [importantInfoId];
}
