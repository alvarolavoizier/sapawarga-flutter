import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ImportantInfoCommentAddEvent extends Equatable {
  const ImportantInfoCommentAddEvent([List props = const <dynamic>[]]);
}

class ImportantInfoCommentAdd extends ImportantInfoCommentAddEvent {
  final int id;
  final String text;

  ImportantInfoCommentAdd({@required this.id, @required this.text}):super([id, text]);


  @override
  String toString() {
    return 'Event ImportantInfoCommentAdd{id: $id, text: $text}';
  }

  @override
  List<Object> get props => [id, text];
}