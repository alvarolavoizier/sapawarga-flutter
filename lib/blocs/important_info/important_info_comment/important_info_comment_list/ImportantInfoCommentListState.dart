import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';

abstract class ImportantInfoCommentListState extends Equatable {
  const ImportantInfoCommentListState([List props = const <dynamic>[]]);
}

class ImportantInfoCommentListInitial extends ImportantInfoCommentListState {
  @override
  List<Object> get props => [];
}

class ImportantInfoCommentListLoading extends ImportantInfoCommentListState {
  @override
  String toString() {
    return 'State ImportantInfoCommentListLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoCommentListLoaded extends ImportantInfoCommentListState {

  final ImportantInfoCommentModel records;

  ImportantInfoCommentListLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State ImportantInfoCommentListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class ImportantInfoCommentListFailure extends ImportantInfoCommentListState {
  final String error;

  ImportantInfoCommentListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State ImportantInfoCommentListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}