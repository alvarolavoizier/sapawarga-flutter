import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import '../Bloc.dart';

class ImportantInfoCommentListBloc extends Bloc<ImportantInfoCommentListEvent, ImportantInfoCommentListState> {

  final ImportantInfoRepository repository;

  ImportantInfoCommentListBloc(this.repository);

  @override
  ImportantInfoCommentListState get initialState => ImportantInfoCommentListInitial();

  @override
  Stream<ImportantInfoCommentListState> mapEventToState(
    ImportantInfoCommentListEvent event,
  ) async* {
    if (event is ImportantInfoCommentListLoad) {
      if (event.page == 1) yield ImportantInfoCommentListLoading();

      try {
        ImportantInfoCommentModel records = await repository.fetchCommentRecords(id: event.id, page: event.page);
        yield ImportantInfoCommentListLoaded(records);
      } catch (e) {
        yield ImportantInfoCommentListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
