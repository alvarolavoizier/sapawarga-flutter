import 'package:equatable/equatable.dart';

abstract class ImportantInfoSearchEvent extends Equatable {
  const ImportantInfoSearchEvent([List props = const <dynamic>[]]);
}

class ImportantInfoSearch extends ImportantInfoSearchEvent {
  final String keyword;
  final int page;

  ImportantInfoSearch(this.keyword, this.page);

  @override
  String toString() {
    return 'Event ImportantInfoSearch';
  }

  @override
  List<Object> get props => [];
}
