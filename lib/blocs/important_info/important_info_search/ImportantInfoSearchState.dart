import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';

abstract class ImportantInfoSearchState extends Equatable {
  const ImportantInfoSearchState();
}

class ImportantInfoSearchInitial extends ImportantInfoSearchState {
  @override
  List<Object> get props => [];
}

class ImportantInfoSearchLoading extends ImportantInfoSearchState {
  @override
  String toString() {
    return 'State ImportantInfoSearchLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoSearchLoaded extends ImportantInfoSearchState {
  final List<ImportantInfoModel> records;
  final int maxLength;

  ImportantInfoSearchLoaded(this.records, this.maxLength);

  @override
  String toString() {
    return 'State ImportantInfoSearchLoaded';
  }

  @override
  List<Object> get props => [records];
}

class ImportantInfoSearchFailure extends ImportantInfoSearchState {
  final String error;

  ImportantInfoSearchFailure({this.error});

  @override
  String toString() {
    return 'State ImportantInfoSearchFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}