import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/enums/CategoryTypes.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/repositories/MasterCategoryRepository.dart';
import './Bloc.dart';

class ImportantInfoCategoriesBloc
    extends Bloc<ImportantInfoCategoriesEvent, ImportantInfoCategoriesState> {

  MasterCategoryRepository masterCategoryRepository;

  ImportantInfoCategoriesBloc({@required this.masterCategoryRepository});

  @override
  ImportantInfoCategoriesState get initialState =>
      InitialImportantInfoCategoriesState();

  @override
  Stream<ImportantInfoCategoriesState> mapEventToState(
      ImportantInfoCategoriesEvent event,) async* {
    if (event is ImportantInfoCategoriesLoad) {
      yield ImportantInfoCategoriesLoading();
      try {
        List<MasterCategoryModel> records = await masterCategoryRepository.getMasterCategories(CategoryType.news_important);
        yield ImportantInfoCategoriesLoaded(records: records);
      } catch (e) {
        yield ImportantInfoCategoriesFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
