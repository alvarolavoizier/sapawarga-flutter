import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';

@immutable
abstract class ImportantInfoHomeState extends Equatable {
  ImportantInfoHomeState([List props = const <dynamic>[]]);
}

class ImportantInfoHomeInitial extends ImportantInfoHomeState {
  @override
  List<Object> get props => [];
}

class ImportantInfoHomeLoading extends ImportantInfoHomeState {
  @override
  String toString() {
    return 'State ImportantInfoHomeLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoHomeLoaded extends ImportantInfoHomeState {
  final List<ImportantInfoModel> records;

  ImportantInfoHomeLoaded(this.records);

  @override
  String toString() {
    return 'State ImportantInfoHomeLoad';
  }

  @override
  List<Object> get props => [records];
}

class ImportantInfoHomeFailure extends ImportantInfoHomeState {
  final String error;

  ImportantInfoHomeFailure({this.error});

  @override
  String toString() {
    return 'State ImportantInfoHomeFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
