import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ImportantInfoListTopEvent extends Equatable {
  ImportantInfoListTopEvent([List props = const <dynamic>[]]);
}

class ImportantInfoListTopLoad extends ImportantInfoListTopEvent {
  final int categoryId;

  ImportantInfoListTopLoad(this.categoryId);

  @override
  String toString() {
    return 'Event ImportantInfoListTopLoad';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoListTopLoadRefreshViewer extends ImportantInfoListTopEvent {
  final int categoryId;

  ImportantInfoListTopLoadRefreshViewer(this.categoryId);

  @override
  String toString() {
    return 'Event ImportantInfoListTopLoadRefreshViewer';
  }

  @override
  List<Object> get props => [];
}
