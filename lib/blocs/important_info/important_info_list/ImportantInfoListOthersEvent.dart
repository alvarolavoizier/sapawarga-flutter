import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ImportantInfoListOthersEvent extends Equatable {
  ImportantInfoListOthersEvent([List props = const <dynamic>[]]);
}

class ImportantInfoListOthersLoad extends ImportantInfoListOthersEvent {
  final int categoryId;

  ImportantInfoListOthersLoad(this.categoryId);

  @override
  String toString() {
    return 'Event ImportantInfoListOthersLoad';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoListOthersLoadRefreshViewer
    extends ImportantInfoListOthersEvent {
  final int categoryId;

  ImportantInfoListOthersLoadRefreshViewer(this.categoryId);

  @override
  String toString() {
    return 'Event ImportantInfoListOthersLoadRefreshViewer';
  }

  @override
  List<Object> get props => [];
}
