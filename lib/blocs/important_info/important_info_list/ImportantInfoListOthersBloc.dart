import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import './Bloc.dart';

class ImportantInfoListOthersBloc
    extends Bloc<ImportantInfoListOthersEvent, ImportantInfoListOthersState> {
  final ImportantInfoRepository importantInfoRepository;

  ImportantInfoListOthersBloc({this.importantInfoRepository});

  @override
  ImportantInfoListOthersState get initialState =>
      ImportantInfoListOthersInitial();

  @override
  Stream<ImportantInfoListOthersState> mapEventToState(
    ImportantInfoListOthersEvent event,
  ) async* {
    if (event is ImportantInfoListOthersLoad) {
      yield ImportantInfoListOthersLoading();
      try {
        List<ImportantInfoModel> records =
            await importantInfoRepository.getOtherRecords(event.categoryId);

        yield ImportantInfoListOthersLoaded(records: records);
      } catch (e) {
        yield ImportantInfoListOthersFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is ImportantInfoListOthersLoadRefreshViewer) {
      try {
        List<ImportantInfoModel> records =
            await importantInfoRepository.getOtherRecords(event.categoryId);
        yield ImportantInfoListOthersLoadingRefreshViewer();

        yield ImportantInfoListOthersLoaded(records: records);
      } catch (e) {
        yield ImportantInfoListOthersFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
