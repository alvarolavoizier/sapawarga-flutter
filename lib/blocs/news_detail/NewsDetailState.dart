import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/NewsModel.dart';

@immutable
abstract class NewsDetailState extends Equatable {
  NewsDetailState([List props = const <dynamic>[]]);
}

class NewsDetailInitial extends NewsDetailState {
  @override
  List<Object> get props => [];
}

class NewsDetailLoading extends NewsDetailState {
  @override
  String toString() {
    return 'State NewsDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class NewsDetailLoaded extends NewsDetailState {
  final NewsModel newsDetail;
  final List<NewsModel> latestNews;

  NewsDetailLoaded({@required this.newsDetail, this.latestNews}):assert(newsDetail != null);

  @override
  String toString() {
    return 'State NewsDetailLoaded';
  }

  @override
  List<Object> get props => [newsDetail, latestNews];
}

class NewsDetailFailure extends NewsDetailState {
  final String error;

  NewsDetailFailure({this.error});

  @override
  String toString() {
    return 'NewsDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
