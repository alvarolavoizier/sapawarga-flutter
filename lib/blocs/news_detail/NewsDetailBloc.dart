import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';

import './Bloc.dart';

class NewsDetailBloc extends Bloc<NewsDetailEvent, NewsDetailState> {

  final NewsRepository newsRepository;


  NewsDetailBloc({@required this.newsRepository}):assert(newsRepository != null);

  @override
  NewsDetailState get initialState => NewsDetailInitial();

  @override
  Stream<NewsDetailState> mapEventToState(NewsDetailEvent event,) async* {
    if (event is NewsDetailLoad) {
      yield NewsDetailLoading();

      try {
        NewsModel newsDetail = await newsRepository.getNewsDetail(
            newsId: event.newsId);
        try {
          List<NewsModel> latestNews = await newsRepository.getLatestNews(
              newsId: event.newsId);
          yield NewsDetailLoaded(
              newsDetail: newsDetail, latestNews: latestNews);
        } catch (_) {
          yield NewsDetailLoaded(newsDetail: newsDetail, latestNews: []);
        }

      } catch (e) {
        yield NewsDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
        print(e.toString());
      }
    }

    if (event is NewsDetailLike) {
      try {
        await newsRepository.sendLike(id: event.newsId);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
