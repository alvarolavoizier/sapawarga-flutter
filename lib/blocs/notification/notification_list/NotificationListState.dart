import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/NotificationModel.dart';

@immutable
abstract class NotificationListState extends Equatable {
  NotificationListState([List props = const <dynamic>[]]);
}

class NotificationListInitial extends NotificationListState {
  @override
  List<Object> get props => [];
}

class NotificationListLoading extends NotificationListState {
  @override
  String toString() {
    return 'State NotificationListLoading';
  }

  @override
  List<Object> get props => [];
}

class NotificationListLoaded extends NotificationListState {
  final List<NotificationModel> records;

  NotificationListLoaded({@required this.records}) : assert(records != null);

  @override
  String toString() {
    return 'State NotificationListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class NotificationListFailure extends NotificationListState {
  final String error;

  NotificationListFailure({this.error});

  @override
  String toString() {
    return 'State NotificationListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
