import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NotificationBadgeEvent extends Equatable {
  NotificationBadgeEvent([List props = const <dynamic>[]]);
}

class CheckNotificationBadge extends NotificationBadgeEvent {
  @override
  String toString() {
    return 'Event CheckNotificationBadge';
  }

  @override
  List<Object> get props => [];
}
