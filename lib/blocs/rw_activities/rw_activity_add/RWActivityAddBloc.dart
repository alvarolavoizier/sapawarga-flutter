import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/enums/CategoryTypes.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/models/RWActivityModel.dart';
import 'package:sapawarga/repositories/MasterCategoryRepository.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';
import '../Bloc.dart';

class RWActivityAddBloc extends Bloc<RWActivityAddEvent, RWActivityAddState> {

  final RWActivityRepository repository;
  final MasterCategoryRepository masterCategoryRepository;

  RWActivityAddBloc(this.repository, this.masterCategoryRepository);

  @override
  RWActivityAddState get initialState => RWActivityAddInitial();

  @override
  Stream<RWActivityAddState> mapEventToState(
    RWActivityAddEvent event,
  ) async* {
    if (event is RWActivityAdd) {
      yield RWActivityAddLoading();

      try {
        ItemRWActivity record = await repository.postActivity(image: event.image, description: event.description, tag: event.tags);
        yield RWActivityAdded(record);
      } catch (e) {
        yield RWActivityAddFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }

   else  if (event is RWActivityCategoryLoad) {
      yield RWActivityCategoryLoading();
      try {
        List<MasterCategoryModel> records = await masterCategoryRepository.getMasterCategories(CategoryType.user_post);
        yield RWActivityCategoryAdded(records);
      } catch (e) {
        yield RWActivityCategoryFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }


  }
}
