import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RWActivityAddEvent extends Equatable {
  const RWActivityAddEvent([List props = const <dynamic>[]]);
}

class RWActivityAdd extends RWActivityAddEvent {
  final List <File> image;
  final String tags;
  final String description;

  RWActivityAdd({@required this.image, @required this.description, this.tags}):super([description]);

  @override
  String toString() {
    return 'Event RWActivityAdd{description: $description}';
  }

  @override
  List<Object> get props => [description];
}

class RWActivityCategoryLoad extends RWActivityAddEvent {

  @override
  String toString() {
    return 'Event RWActivityCategoryLoad';
  }

  @override
  List<Object> get props => [];
}
