import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';

abstract class KabKotaCategoryListState extends Equatable {
  const KabKotaCategoryListState([List props = const <dynamic>[]]);
}

class KabKotaCategoryLIstInitial extends KabKotaCategoryListState {
  @override
  List<Object> get props => [];
}

class KabKotaCategoryListLoading extends KabKotaCategoryListState {
  @override
  String toString() {
    return 'State KabKotaCategoryListLoading';
  }

  @override
  List<Object> get props => [];
}

class KabKotaCategoryListLoaded extends KabKotaCategoryListState {
  final AreaModelCategory records;

  KabKotaCategoryListLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State KabKotaCategoryListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class KabKotaCategoryListFailure extends KabKotaCategoryListState {
  final String error;

  KabKotaCategoryListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State KabKotaCategoryListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}