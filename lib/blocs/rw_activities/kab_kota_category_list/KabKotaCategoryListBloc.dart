import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/blocs/rw_activities/kab_kota_category_list/KabKotaCategoryListEvent.dart';
import 'package:sapawarga/blocs/rw_activities/kab_kota_category_list/KabKotaCategoryListState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';

class KabKotaCategoryListBloc
    extends Bloc<KabKotaCategoryListEvent, KabKotaCategoryListState> {

  final RWActivityRepository repository;

  KabKotaCategoryListBloc(this.repository);

  @override
  KabKotaCategoryListState get initialState => KabKotaCategoryLIstInitial();

  @override
  Stream<KabKotaCategoryListState> mapEventToState(
      KabKotaCategoryListEvent event,) async* {
    if (event is KabKotaCategoryListLoad) {
       yield KabKotaCategoryListLoading();

      try {
       AreaModelCategory records = await repository.getCategoryListArea(null);
        yield KabKotaCategoryListLoaded(records);
      } catch (e) {
        yield KabKotaCategoryListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
    if (event is KabKotaCategorySearchLoad) {
       yield KabKotaCategoryListLoading();

      try {
       AreaModelCategory records = await repository.getCategoryListArea(event.name);
        yield KabKotaCategoryListLoaded(records);
      } catch (e) {
        yield KabKotaCategoryListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
