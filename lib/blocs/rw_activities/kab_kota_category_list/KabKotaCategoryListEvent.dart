import 'package:equatable/equatable.dart';

abstract class KabKotaCategoryListEvent extends Equatable {
  const KabKotaCategoryListEvent([List props = const <dynamic>[]]);
}

class KabKotaCategoryListLoad extends KabKotaCategoryListEvent {

  KabKotaCategoryListLoad():super([]);

  @override
  String toString() {
    return 'Event KabKotaCategoryListLoad';
  }

  @override
  List<Object> get props => [];

}

class KabKotaCategorySearchLoad extends KabKotaCategoryListEvent {
  final String name;

  KabKotaCategorySearchLoad(this.name):super([]);

  @override
  String toString() {
    return 'Event KabKotaCategorySearchLoad $name';
  }

  @override
  List<Object> get props => [name];

}


