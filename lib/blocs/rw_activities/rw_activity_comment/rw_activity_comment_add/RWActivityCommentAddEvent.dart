import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RWActivityCommentAddEvent extends Equatable {
  const RWActivityCommentAddEvent([List props = const <dynamic>[]]);
}

class RWActivityCommentAdd extends RWActivityCommentAddEvent {
  final int id;
  final String text;

  RWActivityCommentAdd({@required this.id, @required this.text}):super([id, text]);


  @override
  String toString() {
    return 'Event RWActivityCommentAdd{id: $id, text: $text}';
  }

  @override
  List<Object> get props => [id, text];
}