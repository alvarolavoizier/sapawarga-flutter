import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/RWActivityCommentModel.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';
import '../Bloc.dart';

class RWActivityCommentListBloc extends Bloc<RWActivityCommentListEvent, RWActivityCommentListState> {

  final RWActivityRepository repository;

  RWActivityCommentListBloc(this.repository);

  @override
  RWActivityCommentListState get initialState => RWActivityCommentListInitial();

  @override
  Stream<RWActivityCommentListState> mapEventToState(
    RWActivityCommentListEvent event,
  ) async* {
    if (event is RWActivityCommentListLoad) {
      if (event.page == 1) yield RWActivityCommentListLoading();

      try {
        RWActivityCommentModel records = await repository.fetchCommentRecords(id: event.id, page: event.page);
        yield RWActivityCommentListLoaded(records);
      } catch (e) {
        yield RWActivityCommentListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
