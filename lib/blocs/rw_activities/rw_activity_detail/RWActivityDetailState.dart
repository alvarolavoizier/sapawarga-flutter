import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/RWActivityModel.dart';

abstract class RWActivityDetailState extends Equatable {
  const RWActivityDetailState([List props = const <dynamic>[]]);
}

class RWActivityDetailInitial extends RWActivityDetailState {
  @override
  List<Object> get props => [];
}

class RWActivityDetailLoading extends RWActivityDetailState {
  @override
  String toString() {
    return 'State RWActivityDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class RWActivityDetailLoaded extends RWActivityDetailState {
  final ItemRWActivity records;

  RWActivityDetailLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State RWActivityDetailLoaded';
  }

  @override
  List<Object> get props => [records];
}

class RWActivityDetailFailure extends RWActivityDetailState {
  final String error;

  RWActivityDetailFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State RWActivityDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}