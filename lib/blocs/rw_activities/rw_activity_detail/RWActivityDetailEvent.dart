import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/RWActivityModel.dart';

abstract class RWActivityDetailEvent extends Equatable {
  const RWActivityDetailEvent([List props = const <dynamic>[]]);
}

class RWActivityDetailLoad extends RWActivityDetailEvent {
  final int id;
  final ItemRWActivity record;

  RWActivityDetailLoad(this.id, this.record):super([id, record]);

  @override
  String toString() {
    return 'Event RWActivityDetailLoad{id: $id, record: $record}';
  }

  @override
  List<Object> get props => [id, record];

}



