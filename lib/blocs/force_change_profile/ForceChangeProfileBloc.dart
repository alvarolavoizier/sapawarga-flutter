import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import './Bloc.dart';

class ForceChangeProfileBloc extends Bloc<ForceChangeProfileEvent, ForceChangeProfileState> {
  final AuthProfileRepository authProfileRepository;


  ForceChangeProfileBloc({@required this.authProfileRepository}):assert(authProfileRepository != null);

  @override
  ForceChangeProfileState get initialState => ForceChangeProfileInitial();

  @override
  Stream<ForceChangeProfileState> mapEventToState(
    ForceChangeProfileEvent event,
  ) async* {
    if (event is CheckForceChangeProfile) {
      yield ForceChangeProfileLoading();
      try {
        UserInfoModel userInfo = await authProfileRepository.getUserInfo(forceFetch: true);

        if (userInfo.passwordUpdatedAt == null) {
          yield ForceChangePasswordRequired();
        } else {
          if (userInfo.profileUpdatedAt == null) {
            yield ForceChangeProfileRequired();
          } else {
            yield ForceChangeProfileNotRequired();
          }
        }

      } catch (e) {
        yield ForceChangeProfileFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
