import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ForceChangeProfileState extends Equatable {
  ForceChangeProfileState([List props = const <dynamic>[]]);
}

class ForceChangeProfileInitial extends ForceChangeProfileState {
  @override
  List<Object> get props => [];
}

class ForceChangeProfileLoading extends ForceChangeProfileState {
  @override
  String toString() {
    return 'State ForceChangeProfileLoading';
  }

  @override
  List<Object> get props => [];
}

class ForceChangePasswordRequired extends ForceChangeProfileState {
  @override
  String toString() {
    return 'State ForceChangePasswordRequired';
  }

  @override
  List<Object> get props => [];
}

class ForceChangeProfileRequired extends ForceChangeProfileState {
  @override
  String toString() {
    return 'State ForceChangeProfileRequired';
  }

  @override
  List<Object> get props => [];
}

class ForceChangeProfileNotRequired extends ForceChangeProfileState {
  @override
  String toString() {
    return 'State ForceChangeProfileNotRequired';
  }

  @override
  List<Object> get props => [];
}

class ForceChangeProfileFailure extends ForceChangeProfileState {
  final String error;

  ForceChangeProfileFailure({this.error});

  @override
  String toString() {
    return 'State ForceChangeProfileFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
