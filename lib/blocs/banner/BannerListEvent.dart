import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BannerListEvent extends Equatable {
  BannerListEvent([List props = const <dynamic>[]]);
}

class BannerListLoad extends BannerListEvent {
  @override
  String toString() {
    return 'Event BannerListLoad';
  }

  @override
  List<Object> get props => [];
}
