import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/UsulanModel.dart';

@immutable
abstract class UsulanListState extends Equatable {
  UsulanListState([List props = const <dynamic>[]]);
}

class UsulanListInitial extends UsulanListState {
  @override
  String toString() => 'State UsulanListInitial';

  @override
  List<Object> get props => [];
}

class UsulanListLoading extends UsulanListState {
  @override
  String toString() {
    return 'State UsulanListLoading';
  }

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class UsulanListLoaded extends UsulanListState {
  final List<UsulanModel> records;
  final int maxLengthData;
  bool isLoad;

  UsulanListLoaded({@required this.records, @required this.maxLengthData, @required this.isLoad}) : super([records, maxLengthData]);

  @override
  String toString() {
    return 'State UsulanListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class UsulanListFailure extends UsulanListState {
  final String error;

  UsulanListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State UsulanListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
