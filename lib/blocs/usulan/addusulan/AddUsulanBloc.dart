import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'Bloc.dart';

class AddUsulanBloc extends Bloc<AddUsulanEvent, AddUsulantState> {
  final UsulanRepository usulanRepository;

  AddUsulanBloc({@required this.usulanRepository}):assert(usulanRepository != null);

  @override
  AddUsulantState get initialState => AddUsulanInitial();

  @override
  Stream<AddUsulantState> mapEventToState(
    AddUsulanEvent event,
  ) async* {
    if (event is AddUsulanSubmit) {
      yield AddUsulanLoading();

      try {
        await usulanRepository.sendUsulan(
          title: event.title,
          description: event.description,
          status: event.status,
          categoryId: event.categoryId,
          attachment: event.attachment,
          id: event.id
        );

        yield AddUsulanUpdated();
      } catch (e) {
        yield AddUsulanFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if(event is AddPhotoSubmit){
      yield AddUsulanLoading();
      try{
        var addPhotoModel =  await usulanRepository.addPhoto(event.image);
        yield AddUsulanPhotoDone(addPhotoModel: addPhotoModel);
      }catch(e){
        yield AddUsulanPhotoFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
