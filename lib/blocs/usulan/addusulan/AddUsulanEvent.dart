import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';

@immutable
abstract class AddUsulanEvent extends Equatable {
  AddUsulanEvent([List props = const <dynamic>[]]);
}

class AddUsulanSubmit extends AddUsulanEvent {
  final String title;
  final String description;
  final String status;
  final String categoryId;
  final int id;
  final List<AddPhotoModel> attachment;

  AddUsulanSubmit({
    @required this.title,
    @required this.description,
    @required this.status,
    @required this.categoryId,
    @required this.attachment,
    this.id,
  }) : super([title, description, status, categoryId, attachment, id]);

  @override
  String toString() {
    return 'Event AddUsulanlLoad';
  }

  @override
  List<Object> get props =>
      [title, description, status, categoryId, attachment];
}

class AddPhotoSubmit extends AddUsulanEvent {
  final image;

  AddPhotoSubmit({@required this.image}) : super([image]);

  @override
  String toString() => 'Event AddPhotoSubmit';

  @override
  List<Object> get props => [image];
}
