import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MyUsulanListEvent extends Equatable {
  MyUsulanListEvent([List props = const <dynamic>[]]);
}

class MyUsulanListLoad extends MyUsulanListEvent {
  final int page;

  MyUsulanListLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event MyUsulanListLoad';
  }

  @override
  List<Object> get props => [page];
}

class MyUsulanListRefresh extends MyUsulanListEvent {
  @override
  String toString() {
    return 'Event MyUsulanListRefresh';
  }

  @override
  List<Object> get props => [];
}
