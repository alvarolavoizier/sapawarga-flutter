import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'Bloc.dart';


class AddCategoryUsulanBloc extends Bloc<AddCategoryUsulanLoad, AddCategoryUsulantState> {
  final UsulanRepository usulanRepository;
  List<MasterCategoryModel> recordsCategory = [];

  AddCategoryUsulanBloc({@required this.usulanRepository}):assert(usulanRepository != null);

  @override
  AddCategoryUsulantState get initialState => AddCategoryUsulanInitial();

  @override
  Stream<AddCategoryUsulantState> mapEventToState(
      AddCategoryUsulanEvent event,
  ) async* {
    if (event is AddCategoryUsulanLoad) {
      if (event.page == 1) yield AddCategoryUsulanLoading();
      try {
        var recordLainnya;
        List<MasterCategoryModel> categoryUsulanList = [];
        recordsCategory.addAll(await usulanRepository.getRecordsCategoryUsulan(page: event.page, forceRefresh: true));
        if(recordsCategory.length > 1){
          recordsCategory.forEach((records){
            if(records.name == 'Lainnya'){
              recordLainnya = records;
            }else{
              categoryUsulanList.add(records);
            }
          });
          categoryUsulanList.add(recordLainnya);
        }else{
          categoryUsulanList = recordsCategory;
        }
        yield AddCategoryUsulanLoaded(records: categoryUsulanList);
      } catch (e) {
        yield AddCategoryUsulanFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
