import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';
import 'NewsListFeaturedState.dart';
import 'NewsListlFeaturedEvent.dart';

class NewsListFeaturedBloc extends Bloc<NewsListFeaturedEvent, NewsListFeaturedState> {

  final NewsRepository newsRepository;


  NewsListFeaturedBloc({@required this.newsRepository}):assert(newsRepository != null);

  @override
  NewsListFeaturedState get initialState => NewsListFeaturedInitial();

  @override
  Stream<NewsListFeaturedState> mapEventToState(NewsListFeaturedEvent event,) async* {
    if (event is NewsListFeaturedLoad) {
      yield NewsListFeaturedLoading();

      try {
        List<NewsModel> listNews = await newsRepository.getFeaturedNews(isKotaId: event.isIdKota );
        try {
          String cityName = await newsRepository.getCityName();
          yield NewsListFeatuedLoaded(listtNews: listNews, cityName:cityName);
        } catch (_) {
          yield NewsListFeatuedLoaded(listtNews: []);
        }

      } catch (e) {
        yield NewsListFeaturedFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}

