import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UpdateAppState extends Equatable {
  UpdateAppState([List props = const <dynamic>[]]);
}

class UpdateAppInitial extends UpdateAppState {
  @override
  List<Object> get props => [];
}

class UpdateAppLoading extends UpdateAppState {
  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'State UpdateAppLoading';
  }
}

class UpdateAppRequired extends UpdateAppState {
  final String version;

  UpdateAppRequired({this.version});

  @override
  String toString() {
    return 'State UpdateAppRequired{version: $version}';
  }

  @override
  List<Object> get props => [version];
}

class UpdateAppUpdated extends UpdateAppState {
  final String version;

  UpdateAppUpdated({this.version});

  @override
  String toString() {
    return 'State UpdateAppUpdated{version: $version}';
  }

  @override
  List<Object> get props => [version];
}

class UpdateAppFailure extends UpdateAppState {
  final String error;

  UpdateAppFailure({this.error});

  @override
  String toString() {
    return 'State UpdateAppFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
