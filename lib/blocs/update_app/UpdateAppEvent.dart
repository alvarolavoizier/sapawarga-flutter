import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UpdateAppEvent extends Equatable {
  UpdateAppEvent([List props = const <dynamic>[]]);
}

class CheckUpdate extends UpdateAppEvent {
  @override
  String toString() {
    return 'CheckUpdate';
  }

  @override
  List<Object> get props => [];
}
