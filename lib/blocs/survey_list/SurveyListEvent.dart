import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SurveyListEvent extends Equatable {
  SurveyListEvent([List props = const <dynamic>[]]);
}

class SurveyListLoad extends SurveyListEvent {
  final int page;

  SurveyListLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event SurveyListLoad';
  }

  @override
  List<Object> get props => [];
}
