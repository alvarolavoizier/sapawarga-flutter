import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/SurveyModel.dart';

@immutable
abstract class SurveyListState extends Equatable {
  SurveyListState([List props = const <dynamic>[]]);
}

class SurveyListInitial extends SurveyListState {
  @override
  List<Object> get props => [];
}

class SurveyListLoading extends SurveyListState {
  @override
  String toString() {
    return 'SurveyListLoading';
  }

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class SurveyListLoaded extends SurveyListState {
  final List<SurveyModel> records;
  final int maxData;
  bool isLoad;

  SurveyListLoaded({@required this.records, @required this.maxData, @required this.isLoad}) : super([records]);

  @override
  String toString() {
    return 'SurveyListLoaded{records: $records}';
  }

  @override
  List<Object> get props => [records];
}

class SurveyListFailure extends SurveyListState {
  final String error;

  SurveyListFailure({this.error});

  @override
  String toString() {
    return 'SurveyListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
