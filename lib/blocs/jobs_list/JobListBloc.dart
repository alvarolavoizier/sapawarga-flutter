import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GeneralTitleModel.dart';
import 'package:sapawarga/repositories/JobRepository.dart';
import 'Bloc.dart';

class JoblistBloc extends Bloc<JoblistEvent, JoblistState> {
  JobRepository jobRepository = JobRepository();

  JoblistBloc({@required this.jobRepository}) : assert(jobRepository != null);

  @override
  JoblistState get initialState => InitialJoblistState();

  @override
  Stream<JoblistState> mapEventToState(
    JoblistEvent event,
  ) async* {
    if (event is JobsLoad) {
      yield JobsListLoading();

      try {
        List<GeneralTitleModel> record = await jobRepository.fetchJobs();

        yield JobsLoaded(record: record);
      } catch (e) {
        yield JobsFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
