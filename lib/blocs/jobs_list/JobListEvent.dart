import 'package:equatable/equatable.dart';

abstract class JoblistEvent extends Equatable {
  JoblistEvent([List props = const []]);
}

class JobsLoad extends JoblistEvent {
  @override
  String toString() => 'Event JobsLoad';

  @override
  List<Object> get props => [];
}
